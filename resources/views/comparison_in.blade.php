@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])

@section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-12">
                     <span class="text-danger">You can compare up to 5 brands.</span>
                    </div>
                    <div class="col-md-5 col-8 align-self-center">
                   <!--      <h3 class="text-themecolor">Comparison</h3> -->
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Comparison</a></li>
                            <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>

                      <input type="hidden" id="brand_name" name="brand_name" value="{{$project_name}}">
                        </ol>

                    </div>

                     <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                         <div class="col-lg-4">
                         <button type="button" id="btn_add_project" class="btn waves-effect waves-light btn-block btn-info">More Brands</button>
                         </div>
                         <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                         <div class='input-group mb-3'>
                         <input type='text' class="form-control dateranges" style="datepicker" />
                         <div class="input-group-append">
                         <span class="input-group-text">
                         <span class="ti-calendar"></span>
                         </span>
                         </div>
                         </div>
                         </div>
                          
                          </div>
                    </div>
                </div>

                <div id="add-project" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                                                    
                                                        <!-- /.modal-content -->
                       <div class="modal-content">
                                                           
                           <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Add Brand</h4>
                                                                <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                         
                            <div class="message-box">
                            <div class="form-group has-success">
                                                    <label class="control-label">Brand</label>
                                                    <select id="brand_id" class="form-control custom-select">
                                                   
                                                   
                                          </select>
                                                    <small class="form-control-feedback"> Choose a brand to compare </small> </div>
                                                     <hr>
                                         
                                        <button type="button" id="btn_add_brand" class="btn btn-success"><i class="fa fa-check"></i> Add </button>
                            </div>
                  
                                                            </div>                              
                       </div>
                    </div>
                                                    <!-- /.modal-dialog -->
                       
               </div>

                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                    <div class="row" id="page-div-1">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Sentiment By Page</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="category-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="category-bar-chart" style="width:100%; height:400px;"></div>   
                            </div>
                        </div>
                    </div>
                
                </div>
              
              <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Social Statistic</h4>
                              <div id="div_reaction" class="table-responsive">
                                   <!--  <table  class="table table-bordered">
                                        <thead>
                                            <tr id="tr_header">
                                                <th>Reaction</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="tr_like">
                                               <td><a href="javascript:void(0)">Like</td>
                                              
                                                
                                            </tr>
                                            <tr id="tr_love">
                                                <td><a href="javascript:void(0)">Love</a></td>
                                           
                                              
                                            </tr>
                                            <tr id="tr_wow">
                                                <td><a href="javascript:void(0)">/a></td>
                                           
                                            </tr>
                                            <tr id="tr_haha">
                                                <td><a href="javascript:void(0)">HA HA</a></td>
                                                
                                            </tr>
                                            <tr id="tr_sad">
                                                <td><a href="javascript:void(0)">Sad</a></td>
                                               
                                            </tr>
                                            <tr id="tr_angry">
                                                <td><a href="javascript:void(0)">Angry</a></td>
                                               
                                            </tr>
                                             <tr id="tr_share">
                                                <td><a href="javascript:void(0)">Share</a></td>
                                              
                                            </tr>
                                        </tbody>
                                    </table> -->
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div>
                 <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="summary-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Page Summary</h4>
                              <div id="div_page_summary" class="table-responsive">
                                 
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div>
                  <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="ranking-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Page Summary</h4>
                              <div id="div_page_ranking" class="table-responsive">
                                 
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div>
                
       
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   
   <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript')}}" defer ></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript')}}" defer></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script type="text/javascript">
var startDate;
var endDate;
var colors=[ "#98c0d8","#dc3545","#01ad9d","#cb73a9","#a1c652","#7b858e","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];



/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/
$(document).ready(function() {
  //generate legend data

  Clear_reaction_table();
  Clear_page_summary_table();
  Clear_page_ranking_table();
var compare_count=0;
var Legend_Data=[];var brand_id_arr=[];var seriesList=[];var mentionLabel = [];var arr_mention_total=[];arr_mention_label=[];
var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_social_total=[];
var Senti_xAxisData=[];var Senti_data1=[];var Senti_data2=[]; var Senti_seriesList=[];var Senti_pos_total=0;
var Senti_neg_total=0;
var arr_post=[];var arr_reaction=[];var arr_comment=[]; var arr_shared=[];var arr_ov_pos=[];
var arr_ov_neg=[];



    startDate = moment().subtract(3, 'month');
    endDate = moment();
    var periodType= '';
       
            if(periodType == '')
            {
              periodType='month';
            }
                  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};



function Bind_Pages()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#brand_id").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getallpages')}}", // This is the URL to the API
      data: { brand_id:brand_id}
    })
    .done(function( data ) {//   <option value="">Choose</option>
           $('#brand_id').append($('<option>', {
            value: '',
            text: 'Choose'
        }));
     for(var i in data) {

      $('#brand_id').append($('<option>', {
            value: data[i],
            text: data[i]
        }));
     }

     brand_id_arr.push(data[0]);
     Legend_Data.push(data[0]);
     ChooseDate(startDate,endDate,data[0],0);
     
    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
Bind_Pages();
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Comparison'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();
function ChooseDate(start,end,brand_id,sr_of_brand)
{
   $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          startDate=start;
          endDate=end;
      var brand_id = brand_id;
         
        socialdetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        requestsentimentbycategory(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand)
        appendtoSummarytable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       // appendtoRankingtable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
}

        function socialdetail(fday,sday,brand_id,sr_of_brand)
    {//alert(keyword);
     /* var socialchart = document.getElementById('social-chart');
      var socialChart = echarts.init(socialchart);*/

      // $("#social-spin").show();
      $("#reaction-spin").show();
      
     
        //var brand_id = 22;
       /* alert (brand_id);
       alert(periodType);*/
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getinboundReaction')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),filter_page_name:brand_id,periodType:'month'}
    })
    .done(function(data) {//alert(data);alert("hhi");
//var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_mention_total=[];

       var socials = [];
       var social_total=0; var like_total=0; var love_total=0; var wow_total=0; var haha_total=0; var sad_total=0; var angry_total=0; 
       var share_total=0;var post_total=0;



       for(var i in data) 
       {
   
        var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].shared)+parseInt(data[i].post_count)//add share
         socials.push(Math.round(mediaReach));
         if(jQuery.inArray(data[i].periodLabel, socialLabel)=='-1')
         socialLabel.push(data[i].periodLabel);
         like_total+=parseInt(data[i].Like);love_total+=parseInt(data[i].Love);wow_total+=parseInt(data[i].Wow);
         haha_total+=parseInt(data[i].Haha);sad_total+=parseInt(data[i].Sad);angry_total+=parseInt(data[i].Angry);
         share_total+=parseInt(data[i].shared);
         console.log(data[i].post_count);
         post_total+=parseInt(data[i].post_count);

       }

      
       appendtoReactiontable(Legend_Data[sr_of_brand],like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,sr_of_brand,post_total);
       $("#reaction-spin").hide();

//        social_total = kFormatter(social_total);

// arr_social_total.push(Legend_Data[sr_of_brand]+' : '+social_total);

// Social_seriesList.push({
//   name:arr_social_total[sr_of_brand],
//   type:'line',
//   data:socials,
//   barMaxWidth:30,
//   markPoint : {
//     large:true,
//     label: {
//       normal: {
//         formatter: function (param) {
//           return kFormatter(param.value);
//         },
//         textStyle: {
//           color:"#f5f2f2"
//         },
//         position: 'inside'
//       }
//     },
//     data : [
//     {type : 'max', name: 'maximum'},
//     {type : 'min', name: 'minimum'},

//     ]
//   }
// },

//                            );//end-push

     
// social_option = {
//   color: colors,

//       tooltip: {
//           trigger: 'axis',
//           axisPointer: {
//             type: 'cross'
//           },
//           formatter: function (params) {
//             var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
//             let rez = '<p>' + params[0].name + '</p>';
//        /* console.log(rez);*/ //quite useful for debug
//        params.forEach(item => {
//             /*console.log(item);
//             console.log(item.data);*/
//             var seriesname =item.seriesName.split(":");
//             var xx = '<p>'   + colorSpan(item.color) + ' ' + seriesname[0] + ': ' + kFormatter(item.data)  + '</p>'
//             rez += xx;
//           });

//        return rez;
//      }
//    },

//   legend: {
//     data:arr_social_total,
//     formatter: '{name}: '+arr_social_total[sr_of_brand],
//     padding :0,
//   },
//   toolbox: {
//     show : true,
//     feature : {
//       mark : {show: false},
//       dataView : {show: false, readOnly: false},
//       magicType : {show: true, type: ['line','bar']},
//       restore : {show: true},
//       saveAsImage : {show: true}
//     }
//   },
//   calculable : true,
//   xAxis : [
//   {
//    type: 'category', 
//    axisLabel: {
//     formatter: function (value, index) {
//     // Formatted to be month/day; display year only in the first label
//     const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
//     "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

//     var arr = value.split(' - ');
//     var date = new Date(arr[0]);
//        //console.log(date);
//        if(periodType === 'day')
//        {
//         var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];

//       }
//       else 
//       {

//         var texts = [date.getFullYear(), monthNames[date.getMonth()]];
//       }



//       return texts.join('-');

//     }
//   },
//   data : socialLabel
// }
// ],
// yAxis : [
// {
//   type : 'value',
//   axisLabel: {
//     formatter: function (e) {
//       return kFormatter(e);
//     }
//   }
// }
// ],

// series : Social_seriesList,
// };
// $("#social-spin").hide();
// $("#reaction-spin").hide();

//    // socialChart.setOption(option);
   
//    socialChart.setOption(social_option, true), $(function() {
//     function resize() {//alert("hi");
//     setTimeout(function() {
//       socialChart.resize()
//     }, 100)
//   }
//   $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
// });
  

//    socialChart.resize();

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function requestsentimentbycategory(fday,sday,brand_id,sr_of_brand){//alert(fday);
      
var categorychart = document.getElementById("category-bar-chart");
var categoryChart = echarts.init(categorychart);
 


$("#category-spin").show();
        
       // var brand_id = GetURLParameter('pid');
       /* var brand_id = 22;*/
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getsentimentbypage')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),filter_page_name:brand_id }
    })
    .done(function( data ) {//alert(data);
          
       console.log(data);
       console.log("getsentimentbypage");

var xAxisData=[];
var data1 = 0;
var data2 = 0;
var data2_pos_sign = [];
var positive_total=0;
var negative_total=0;
var Senti_seriesList=[];



for(var i in data) 
        {
     xAxisData.push(data[i].page_name);
    data1=Math.round(data[i].positive);
    if(data[i].negative >0 )
    {
    data2=Math.round(-data[i].negative);
   
    }
    else
    {
     data2=0;
    }
  
 
}

var itemStyle = {
    normal: {
    },
    emphasis: {
        barBorderWidth: 1,
        shadowBlur: 10,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        shadowColor: 'rgba(0,0,0,0.5)'
    }
};



Senti_xAxisData.push(xAxisData);
Senti_data1.push(data1);
Senti_data2.push(data2);
Senti_seriesList.push({
            name: Legend_Data[sr_of_brand],
            type: 'bar',
            barMaxWidth:30,
            stack: 'one',
            color:colors[0],
            itemStyle: itemStyle,
            data: Senti_data1
        },
        {
            name: Legend_Data[sr_of_brand],
            type: 'bar',
            barMaxWidth:30,
            stack: 'one',
            color:colors[1],
            itemStyle: itemStyle,
            data: Senti_data2
        },);

console.log(Senti_seriesList);

option = {
      color:colors,
    backgroundColor: 'rgba(0, 0, 0, 0)',
     dataZoom:[  {
            type: 'slider',
            show: true,
            xAxisIndex: [0],
            start: 70,
            end: 100
        },
         {
            type: 'inside',
            xAxisIndex: [0],
            start: 1,
            end: 35
        }
    ],
        calculable : true,
    // legend: {
    //     data:['positive', 'negative'],
    //        x: 'center',
    //          y: 'top',
    //           padding :0,
        
    // },

    toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['stack','tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
    tooltip: {},
    calculable : true,
    dataZoom : {
        show : true,
        realtime : true,
        start : 20,
        end : 80
    },
    xAxis: {
        data: Senti_xAxisData,
        name: 'page name',
        silent: false,
        axisLine: {onZero: true},
        splitLine: {show: false},
        splitArea: {show: false},
   
    },
    yAxis: {
        name: 'comment count',
        inverse: false,
        splitArea: {show: false}
    },
   grid: {
            top: '12%',
            left: '3%',
            right: '10%',
            containLabel: true
        },
  
    series:Senti_seriesList,
};

$("#category-spin").hide();
/*categoryChart.on('brushSelected', renderBrushed);

function renderBrushed(params) {
    var brushed = [];
    var brushComponent = params.batch[0];

    for (var sIdx = 0; sIdx < brushComponent.selected.length; sIdx++) {
        var rawIndices = brushComponent.selected[sIdx].dataIndex;
        brushed.push('[Series ' + sIdx + '] ' + rawIndices.join(', '));
    }
}
*/
    categoryChart.setOption({
        title: {
            backgroundColor: '#333',
          /*  text: 'SELECTED DATA INDICES: \n' + brushed.join('\n'),*/
            bottom: 0,
            right: 0,
            width: 100,
            textStyle: {
                fontSize: 12,
                color: '#fff'
            }
        }
    });

    categoryChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            categoryChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   categoryChart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data = education
   console.log(params.seriesName); //bar type name ="positive"
   console.log(params.value);//count 8
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid+"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&category="+ params.name +"&type="+params.seriesName+"&from_graph=category" , '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});


   

  
    })
     .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

  }


    

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        seriesList=[];mentionLabel = [];arr_mention_total=[];arr_mention_label=[];
        Social_seriesList=[]; socialLabel = [];arr_social_total=[];
        Senti_data1=[];Senti_data2=[];Senti_xAxisData=[];
  
        Clear_reaction_table();
        Clear_page_summary_table();
        Clear_page_ranking_table();
        compare_count=0;

        refreshGraph(startDate,endDate);
      });


function refreshGraph(startDate,endDate)
{//alert(brand_id_arr);
  var arrayLength=brand_id_arr.length;
  if(arrayLength >1)
  {
  for (var i = 0; i < arrayLength; i++) {
    compare_count=i;
   // alert(Legend_Data[compare_count]);
    ChooseDate(startDate,endDate,brand_id_arr[i],compare_count);
  }
  }
  else
  {
    ChooseDate(startDate,endDate,brand_id_arr[0],0);
  }

}


   

$("#btn_add_project").click(function(){
var arrayLength=brand_id_arr.length;
  if(arrayLength >0)
  {
    for (var i = 0; i < arrayLength; i++) {
           var brand_id= brand_id_arr[i];
    //Do something
    // $('#'+brand_id).prop('disabled', true);
     $('#brand_id').children('option[value="'+brand_id+'"]').css('display','none');
}   
   /* $("select#brand_id").val(''); */
   $("select#brand_id").prop('selectedIndex', 0);
   $('#add-project').modal('show'); 
  }
  
});

$("#btn_add_brand").click(function(){
   $('#add-project').modal('hide'); 
  var selected_text=$( "#brand_id option:selected" ).text();
  var selected_value=$( "#brand_id option:selected" ).val();
 // alert(selected_value);
  if(selected_value !== '')
  {
    compare_count=compare_count+1;
    Legend_Data.push(selected_value);
    brand_id_arr.push(selected_value);
    ChooseDate(startDate,endDate,selected_value,compare_count);
  }
 
  
  //add to legend array

});



function appendtoReactiontable(brand_name,like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,sr_no,total_mention)
{
    total_mention=0;
  $("#tr_header").append("<th style='color:"+colors[sr_no]+"'><div>"+brand_name+"</div><div>Total:"+total_mention+"</div></th>");
  $("#tr_like").append("<td>"+kFormatter(like_total)+"</td>");
  $("#tr_love").append("<td>"+kFormatter(love_total)+"</td>");
  $("#tr_wow").append("<td>"+kFormatter(wow_total)+"</td>");
  $("#tr_haha").append("<td>"+kFormatter(haha_total)+"</td>");
  $("#tr_sad").append("<td>"+kFormatter(sad_total)+"</td>");
  $("#tr_angry").append("<td>"+kFormatter(angry_total)+"</td>");
  $("#tr_share").append("<td>"+kFormatter(share_total)+"</td>");
}
 function appendtoSummarytable(fday,sday,brand_id,sr_of_brand)
    {
      $("#summary-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),filter_page_name:brand_id}
    })
    .done(function(data) {//alert(data);alert("hhi");


  $("#tr_summary_header").append("<th style='color:"+colors[sr_of_brand]+"'><div>"+brand_id+"</div></th>");
  $("#tr_post").append("<td>"+kFormatter(data[0][0]['total_post'])+"</td>");
  $("#tr_reaction").append("<td>"+kFormatter(data[0][0]['total_reaction'])+"</td>");
  $("#tr_summary_share").append("<td>"+kFormatter(data[0][0]['shared'])+"</td>");
  $("#tr_comment").append("<td>"+kFormatter(data[1][0]['total_comment'])+"</td>");
  $("#tr_cmt_pos").append("<td>"+kFormatter(data[1][0]['cmt_positive'])+"</td>");
  $("#tr_cmt_neg").append("<td>"+kFormatter(data[1][0]['cmt_negative'])+"</td>");
  $("#tr_overall_pos").append("<td>"+kFormatter(data[2][0]['ov_positive'])+"</td>");
  $("#tr_overall_neg").append("<td>"+kFormatter(data[2][0]['ov_negative'])+"</td>");

       

         var table = document.getElementById('summary_table');
         var rowCount = table.rows.length;
         
    
         var sr_count=sr_of_brand+1;
         var post_arr=[];var react_arr=[];var comment_arr=[];var cmt_pos_arr=[];var cmt_neg_arr=[];
         var shared_arr=[];var ov_post_arr=[];var ov_comment_arr=[];
         for(var i=0;i<sr_count;i++)
         {
            var title = (table.rows[0].cells[i].textContent.trim());
            var value = (table.rows[1].cells[i].textContent.trim());
            postArray= {};
            postArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
            postArray['value'] =convertKtoThousand(table.rows[1].cells[i+1].textContent.trim());
          
            post_arr.push(postArray);

            reactArray= {};
            reactArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
            reactArray['value'] =convertKtoThousand(table.rows[2].cells[i+1].textContent.trim());

            react_arr.push(reactArray);

            commentArray= {};
            commentArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
            commentArray['value'] =convertKtoThousand(table.rows[3].cells[i+1].textContent.trim());

            comment_arr.push(commentArray);

            commentPOSArray= {};
            commentPOSArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
            commentPOSArray['value'] =convertKtoThousand(table.rows[4].cells[i+1].textContent.trim());

            cmt_pos_arr.push(commentPOSArray);

            commentNegArray= {};
            commentNegArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
            commentNegArray['value'] =convertKtoThousand(table.rows[5].cells[i+1].textContent.trim());

            cmt_neg_arr.push(commentNegArray);

            sharedArray= {};
            sharedArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
            sharedArray['value'] =convertKtoThousand(table.rows[6].cells[i+1].textContent.trim());

            shared_arr.push(sharedArray);

            OV_postArray= {};
            OV_postArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
            OV_postArray['value'] =convertKtoThousand(table.rows[7].cells[i+1].textContent.trim());

            ov_post_arr.push(OV_postArray);

            OV_commentArray = {};
            OV_commentArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
            OV_commentArray['value'] =convertKtoThousand(table.rows[8].cells[i+1].textContent.trim());

            ov_comment_arr.push(OV_commentArray);
           
           
         }
   
        post_arr.sort( function( a, b ) {
    return b.value - a.value;
}); 
          react_arr.sort( function( a, b ) {
    return b.value - a.value;
}); 
            comment_arr.sort( function( a, b ) {
    return b.value - a.value;
}); 
    cmt_pos_arr.sort( function( a, b ) {
    return b.value - a.value;
}); 
    cmt_neg_arr.sort( function( a, b ) {
    return b.value - a.value;
}); 
              shared_arr.sort( function( a, b ) {
    return b.value - a.value;
}); 
                ov_post_arr.sort( function( a, b ) {
    return b.value - a.value;
}); 
                  ov_comment_arr.sort( function( a, b ) {
    return b.value - a.value;
}); 
  
        appendtoRankingtable(post_arr,react_arr,comment_arr,cmt_pos_arr,cmt_neg_arr,shared_arr,ov_post_arr,ov_comment_arr,sr_count);
       $("#summary-spin").hide();


 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function appendtoRankingtable(post_arr,react_arr,comment_arr,cmt_pos_arr,cmt_neg_arr,shared_arr,ov_post_arr,ov_comment_arr,sr_count)
    {
        Clear_page_ranking_table();
      $("#ranking-spin").show();
   var test='';

   for(var i=0;i<sr_count;i++)
   {
    var no=i+1;
  
  $("#tr_ranking_header").append("<th><div>"+LevelCheck(no)+"</div></th>");
  $("#tr_rank_post").append("<td>"+post_arr[i]['title']+"</td>");
  $("#tr_rank_reaction").append("<td>"+react_arr[i]['title']+"</td>");
  $("#tr_rank_share").append("<td>"+shared_arr[i]['title']+"</td>");
  $("#tr_rank_comment").append("<td>"+comment_arr[i]['title']+"</td>");
  $("#tr_rank_cmt_pos").append("<td>"+cmt_pos_arr[i]['title']+"</td>");
  $("#tr_rank_cmt_neg").append("<td>"+cmt_neg_arr[i]['title']+"</td>");
  $("#tr_rank_pos").append("<td>"+ov_post_arr[i]['title']+"</td>");
  $("#tr_rank_neg").append("<td>"+ov_comment_arr[i]['title']+"</td>");
   }
  

       

      
     
  
       $("#ranking-spin").hide();



}



function Clear_reaction_table()
{
 $("#div_reaction").empty();

 $("#div_reaction").append(" <table  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_header'>"+
                           "<th>Reaction</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                           "<tr id='tr_like'>"+
                           "<td><a href='javascript:void(0)'>👍 Like</a></td>"+
                            "</tr>"+
                             "<tr id='tr_love'>"+
                             "<td><a href='javascript:void(0)'>😍 Love</a></td>"+
                             "</tr>"+
                             "<tr id='tr_wow'>"+
                             "<td><a href='javascript:void(0)'>😯 WOW</a></td>"+
                             " </tr>"+
                             "<tr id='tr_haha'>"+
                             "<td><a href='javascript:void(0)'>😆 HA HA</a></td>"+
                             "</tr>"+
                             "<tr id='tr_sad'>"+
                             "<td><a href='javascript:void(0)'>😥 Sad</a></td>"+
                             "</tr>"+
                             "<tr id='tr_angry'>"+
                             "<td><a href='javascript:void(0)'>😡 Angry</a></td>"+
                              "</tr>"+
                              "<tr id='tr_share'>"+
                              "<td><a href='javascript:void(0)'>⤴ Share</a></td>"+
                              "</tr>"+
                              "</tbody>"+
                              "</table>");
}
function Clear_page_summary_table()
{
 $("#div_page_summary").empty();

 $("#div_page_summary").append(" <table id='summary_table' name='summary_table'  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_summary_header'>"+
                           "<th>Benchmarks</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                           "<tr id='tr_post'>"+
                           "<td><a href='javascript:void(0)'>Posts</a></td>"+
                            "</tr>"+
                             "<tr id='tr_reaction'>"+
                             "<td><a href='javascript:void(0)'>Reactions</a></td>"+
                             "</tr>"+
                             "<tr id='tr_comment'>"+
                             "<td><a href='javascript:void(0)'>Comments</a></td>"+
                             " </tr>"+
                              "<tr id='tr_cmt_pos'>"+
                             "<td><a href='javascript:void(0)'>Positive Comments</a></td>"+
                             " </tr>"+
                              "<tr id='tr_cmt_neg'>"+
                             "<td><a href='javascript:void(0)'>Negative Comments</a></td>"+
                             " </tr>"+
                             "<tr id='tr_summary_share'>"+
                             "<td><a href='javascript:void(0)'>Shares</a></td>"+
                             "</tr>"+
                             "<tr id='tr_overall_pos'>"+
                             "<td><a href='javascript:void(0)'>Overall Positive</a></td>"+
                             "</tr>"+
                             "<tr id='tr_overall_neg'>"+
                             "<td><a href='javascript:void(0)'>Overall Negative</a></td>"+
                              "</tr>"+
                              
                              "</tbody>"+
                              "</table>");
}

function Clear_page_ranking_table()
{
 $("#div_page_ranking").empty();

 $("#div_page_ranking").append(" <table id='ranking_table' name='ranking_table'  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_ranking_header'>"+
                           "<th>Benchmarks</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                           "<tr id='tr_rank_post'>"+
                           "<td><a href='javascript:void(0)'>Plenty Posts</a></td>"+
                            "</tr>"+
                             "<tr id='tr_rank_reaction'>"+
                             "<td><a href='javascript:void(0)'>Highest Reaction</a></td>"+
                             "</tr>"+
                             "<tr id='tr_rank_comment'>"+
                             "<td><a href='javascript:void(0)'>Most Comments</a></td>"+
                             " </tr>"+
                             "<tr id='tr_rank_cmt_pos'>"+
                             "<td><a href='javascript:void(0)'>Most Positive Cmt:</a></td>"+
                             " </tr>"+
                             "<tr id='tr_rank_cmt_neg'>"+
                             "<td><a href='javascript:void(0)'>Most Negative Cmt:</a></td>"+
                             " </tr>"+
                             "<tr id='tr_rank_share'>"+
                             "<td><a href='javascript:void(0)'>Most Shared</a></td>"+
                             "</tr>"+
                             "<tr id='tr_rank_pos'>"+
                             "<td><a href='javascript:void(0)'>Most Positive</a></td>"+
                             "</tr>"+
                             "<tr id='tr_rank_neg'>"+
                             "<td><a href='javascript:void(0)'>Most Negative</a></td>"+
                              "</tr>"+
                              
                              "</tbody>"+
                              "</table>");
}



//local customize function



     function kFormatter(num) {
    return num > 999 ? (num/1000).toFixed(1) + 'k' : Math.round(num)
}

 

 function convertKtoThousand(s)
{

    var str=s;
    str=str.toUpperCase();
    if (str.indexOf("K") !== -1) {
    str = str.replace("K",'');
    return parseFloat(str) * 1000;
  } else if (str.indexOf("M") !== -1) {
    str = str.replace("M",'');
     return parseFloat(str) * 1000000;
  } else {
    return parseFloat(str);
  }
  
}

 function LevelCheck(s)
{

    if(parseInt(s) === 1) return "1st";else if(parseInt(s) === 2) return "2nd";else if(parseInt(s) === 3) return "1rd";
    else if(parseInt(s) > 3) return s+"th";
  
}
    function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x1F60D', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }
 

       
  });
    </script>
<style>
.modal-dialog {
    position: absolute;
    top: 200px;
    right: 100px;
    bottom: 0;
    left: 100px;
    z-index: 10040;
    /* overflow: auto; */
}
</style>
@endpush

