<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\stdClass;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\GlobalController;
use App\ProjectKeyword;
use App\MongoProfile;
use DB;
use MongoDB;
use App\Comment;
use App\Project;
use App\tags;
use App\User;
use App\cron_log;
use App\MongodbData;
use App\MongoPages;
// get_mentionDataYearly
class Sync_Mention_Yearly extends Command
{
     use GlobalController;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Sync_Mention_Yearly:SyncMentionData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Mention Data for one year for the fist time ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $projects = DB::connection('mysql_cron_wunzin')->table('cron_log')->select('*')->where('finish_status',0)->where('cron_type','sync_mention')->get();
        
      foreach($projects as $project)
      {
        $id = $project->brand_id;
      
        $result =$this->Insert_Mention_Data($id);
        if($result)
          {
             DB::table('cron_log')
            ->where('brand_id', $id)
            ->update(['finish_status' => 1]);
          }
       
      }
      if(count($projects)>0)
      {
       $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Sync Mention for one year Successfully.'. $date_time);
      }
     
    }
    public function Insert_Mention_Data($project_id)
    {
      //for edit
     $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];

     
     
     $keyword_data = $this->getprojectkeyword($project_id);
     if(isset($keyword_data [0]['main_keyword']))
         {


     $filter['$or'] = $this->getkeywordfilter($keyword_data);
// $this->info($filter);
     $pagename = $this->getpagename($project_id);//this filter will use later to filter (to avoid including data from monitor pages)
     $pagename= explode(',', $pagename[0]["monitor_pages"]);
     $pagename= $this->Format_Page_Name($pagename);
    
    // $page['$or']= $this->getpagefilter($other_pages);
 
    /* dd($page);*/
     $today = date('d-m-Y h:i:s');//22-09-2018//  date('d-m-Y')

    $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
    $date_Begin=date('Y-m-d', strtotime('-1 year', strtotime($today_plus)));
    // dd($date_Begin);
    //start with today (00:00:00) hour
    $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
   
    $date_End=date('Y-m-d h:i:s',strtotime($today));
    $date_End=date('Y-m-d', strtotime('+1 day', strtotime($date_End)));
    //start with tomorrow (00:00:00) hour
    $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);

 $mention_post_id_array = $this->get_table_id_mention($project_id,"posts","id");
 
 $mention_post_result=MongodbData::raw(function ($collection) use($filter,$mention_post_id_array,$dateBegin,$dateEnd) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             [ 'id'=>[ '$nin'=> $mention_post_id_array ] ],
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
              $filter
                      ]
        ]  
                   
       ],
       ['$sort' =>['created_time'=>-1]],
       // ['$limit' =>2000]
     
        
      ]);
})->toArray();

// dd($mention_post_result);

// return $mention_post_result;


$data=[];
 $page_array=[];
 $data_message=[];

 foreach ($mention_post_result as  $key => $row) {
                $id ='';$datetime=NULL;
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';$original_message='';$unicode = '';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                $isDeleted=0;
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                    if(isset($row['original_message'])) $original_message = $row['original_message'];

                    if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['created_time']))
                    {
                    $utcdatetime = $row["created_time"];
                    $datetime = $utcdatetime->toDateTime();
                    $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                    $datetime = $datetime->format('Y-m-d H:i:s');
                 }
             
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['mark'])) 
                {
                  if($row['mark'] == 'deleted')
                {
                   $isDeleted =1;
                }
                 
                }

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'original_message' => $original_message,
                    'unicode' => $unicode,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>'',
                    'checked_sentiment' =>'',
                    'decided_keyword' =>'',
                    'emotion' =>'',
                    'checked_emotion' =>'',
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'isDeleted'=>$isDeleted,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

  }
  $path = storage_path('app/data_output/mention_posts'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data);

 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,original_message,unicode,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,created_time,change_predict,checked_predict,isBookMark,isDeleted,created_at,updated_at)";


 $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
 $pdo->exec($query_load);


 $post_id = $this->getMentionPostId($project_id);/*dd( $filter);*/
// dd($post_id);
 
$mention_cmt_id_array = $this->get_table_id_mention($project_id,"comments","id");
  /*dd($inbound_cmt_id_array);*/
 $mention_comment_result=Comment::raw(function ($collection) use($filter,$mention_cmt_id_array,$post_id,$dateBegin,$dateEnd) {//print_r($filter);
 
    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             // ['post_id'=>['$in'=>$post_id]],
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             ['id'=>[ '$nin'=> $mention_cmt_id_array ] ],
             $filter
           
            
                      ]
        ]  
                   
       ],
       ['$sort' =>['created_time'=>-1]],
       
        
    ]);
})->toArray();

 $profile_id=[];
  $data_comment=[];
 foreach ($mention_comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';$profile='';$page_name='';
                $comment_count =0;$original_message='';$unicode='';
                $sentiment='';$emotion='';$parent='';
                $tags='';
                if(isset($row['profile']))
                {
                $profile = $row['profile'];
                $profile_id[]=new MongoDB\BSON\ObjectId((string)$profile);
                } 

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['original_message'])) $original_message = $row['original_message'];
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['parent'])) $parent =  $row['parent'];
                if(isset($row['page_name'])) $page_name = $row['page_name'];
           

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'wb_message' =>'',
                    'original_message' =>$original_message,
                    'unicode' =>$unicode,
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    'sentiment' =>'',
                    'checked_sentiment' =>'',
                    'decided_keyword' =>'',
                    'emotion' =>'',
                    'checked_emotion' =>'',
                    'interest' =>'',
                    'profile'=>$profile,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'parent'=>$parent,
                    'isBookMark' =>0,
                    'manual_date'=>'NULL',
                    'page_name'=>$page_name,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }

$path = storage_path('app/data_output/mention_comments'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_comment);
 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,interest,profile,change_predict,checked_predict,parent,isBookMark,manual_date,page_name,created_at,updated_at)";

 $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
 $pdo->exec($query_load);

 }
 return true;



 

}
//local function


  // public function get_table_id_mention($project_id,$table,$require_var,$related_pages=[])
  //   {
     
  //     $filter_pages=$this->getPageWhere($related_pages);
      
  //     $query = "SELECT ".$require_var." from temp_".$project_id."_".$table ."  Where 1=1 " . $filter_pages;
  //     //dd($query);
  //     $id_result = DB::select($query);
  //     $id_array = array_column($id_result, $require_var);
  //     return $id_array;
  //   }
    

// public function getPageWhere($related_pages)
// {
//     $pages='';
//      $filter_pages = '';
//       foreach ($related_pages as $key => $value) {
        
//         if((int)$key === 0)
//         $pages ="'".$value."'";
//       else
//         $pages .=",'".$value."'";

//         $filter_pages="  AND page_name in (".$pages.")";
//       }
//     return  $filter_pages;
// }
//    function doCSV($path, $array)
// {
//    $fp = fopen($path, 'w');
// $i = 0;
// foreach ($array as $fields) {
//     if($i == 0){
//         fputcsv($fp, array_keys($fields));
//     }
//     fputcsv($fp, array_values($fields));
//     $i++;
// }

// fclose($fp);
// }

//     function convertKtoThousand_SyncMongo($s)
// {
//     if (strpos(strtoupper($s), "K") != false) {
//     $s = rtrim($s, "kK");
//     return floatval($s) * 1000;
//   } else if (strpos(strtoupper($s), "M") != false) {
//     $s = rtrim($s, "mM");
//     return floatval($s) * 1000000;
//   } else {
//     return floatval($s);
//   }
// }
  
}
