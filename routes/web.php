<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', ['as' => 'need-to-check','uses' => 'HomeController@checked']);
Route::get('searchById', ['as' => 'searchById','uses' => 'HomeController@searchById']);
/*Auth::routes();*/
// Route::get('/', function () {
//     echo "hih";
//     return;
//   });
Route::get('login', ['as' => 'login','uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login',['as' => '','uses' => 'Auth\LoginController@login']);
// Route::get('passwordCorrect',['as' => '','passwordCorrect' => 'MongoCRUDController@passwordCorrect']);

Route::get('get_inbound_data', ['as' => 'get_inbound_data','uses' => 'ServerjobController@get_inbound_data']);
Route::get('update_inboundComment', ['as' => 'update_inboundComment','uses' => 'ServerjobController@update_inboundComment']);
Route::get('update_inboundPost', ['as' => 'update_inboundPost','uses' => 'ServerjobController@update_inboundPost']);
Route::get('get_mention_data', ['as' => 'get_mention_data','uses' => 'ServerjobController@get_mention_data']);
Route::get('update_mentionComment', ['as' => 'update_mentionComment','uses' => 'ServerjobController@update_mentionComment']);
Route::get('update_mentionPost', ['as' => 'update_mentionPost','uses' => 'ServerjobController@update_mentionPost']);
Route::get('get_mentionDataYearly', ['as' => 'get_mentionDataYearly','uses' => 'ServerjobController@get_mentionDataYearly']);
Route::get('get_updatePostReaction', ['as' => 'get_updatePostReaction','uses' => 'ServerjobController@get_updatePostReaction']);
Route::get('get_commentSentiForMention', ['as' => 'get_commentSentiForMention','uses' => 'ServerjobController@get_commentSentiForMention']);


Route::get('get_postSentiForMention', ['as' => 'get_postSentiForMention','uses' => 'ServerjobController@get_postSentiForMention']);
Route::get('get_commentSentiForInbound', ['as' => 'get_commentSentiForInbound','uses' => 'ServerjobController@get_commentSentiForInbound']);
Route::get('get_postSentiForInbound', ['as' => 'get_postSentiForInbound','uses' => 'ServerjobController@get_postSentiForInbound']);
Route::get('get_commentWbForMention', ['as' => 'get_commentWbForMention','uses' => 'ServerjobController@get_commentWbForMention']);
Route::get('get_postWbForMention', ['as' => 'get_postWbForMention','uses' => 'ServerjobController@get_postWbForMention']);
Route::get('get_commentWbForInbound', ['as' => 'get_commentWbForInbound','uses' => 'ServerjobController@get_commentWbForInbound']);
Route::get('get_postWbForInbound', ['as' => 'get_postWbForInbound','uses' => 'ServerjobController@get_postWbForInbound']);
Route::get('add_profiles', ['as' => 'add_profiles','uses' => 'ServerjobController@add_profiles']);
Route::get('update_profiles', ['as' => 'update_profiles','uses' => 'ServerjobController@update_profiles']);
Route::get('tag_mentionComment', ['as' => 'tag_mentionComment','uses' => 'ServerjobController@tag_mentionComment']);
Route::get('tag_inboundComment', ['as' => 'tag_inboundComment','uses' => 'ServerjobController@tag_inboundComment']);
Route::get('get_inboundComment', ['as' => 'get_inboundComment','uses' => 'ServerjobController@get_inboundComment']);

Route::get('get_web_mention_data', ['as' => 'get_web_mention_data','uses' => 'ServerjobController@get_web_mention_data']);
Route::get('get_WbForWebMention', ['as' => 'get_WbForWebMention','uses' => 'ServerjobController@get_WbForWebMention']);
Route::get('get_SentiForWebMention', ['as' => 'get_inboundComment','uses' => 'ServerjobController@get_SentiForWebMention']);
Route::get('update_articleTitle', ['as' => 'update_articleTitle','uses' => 'ServerjobController@update_articleTitle']);
Route::get('update_articleTitle', ['as' => 'update_article','uses' => 'ServerjobController@update_article']);






Route::get('need-to-check',['as' => 'need-to-check','uses' => 'HomeController@checked']);
Route::get('confirmed',['as' => 'confirmed','uses' => 'HomeController@confirmed']);
Route::get('require',['as' => 'require','uses' => 'HomeController@require']);
Route::get('taken',['as' => 'taken','uses' => 'HomeController@taken']);

Route::get('getRelatedComments', ['as' => 'getRelatedComments','uses' => 'HomeController@getRelatedComments']);
Route::get('getRelatedPosts', ['as' => 'getRelatedPosts','uses' => 'HomeController@getRelatedPosts']);
Route::get('getMentionComments', ['as' => 'getMentionComments','uses' => 'HomeController@getMentionComments']);
Route::get('getMentionPosts',['as'=>'getMentionPosts','uses'=>'HomeController@getMentionPosts']);

// Route::get('checked', ['as' => 'nee','uses' => 'HomeController@checked']);
// Route::get('confirmed', ['as' => 'getRelatedComments','uses' => 'HomeController@comfirmed']);
Route::get('export',['as' => 'export','uses' => 'HomeController@export']);
Route::get('history',['as' => 'history','uses' => 'HomeController@history']);
Route::get('get_history',['as' => 'get_history','uses' => 'HomeController@get_history']);
Route::get('getExcelData', [
  'as'   => 'getExcelData',
  'uses' => 'HomeController@getExcelData'
]);

Route::get('datatrack',['as' => 'datatrack','uses' => 'HomeController@track']);
Route::get('getMentionTrackingData',['as' => 'getMentionTrackingData','uses' => 'HomeController@getMentionTrackingData']);
Route::get('getTrackingData',['as' => 'getTrackingData','uses' => 'HomeController@getTrackingData']);
Route::get('getMonitorPages',['as' => 'getMonitorPages','uses' => 'HomeController@getMonitorPages']);
Route::get('getRelatedMention',['as'=>'getRelatedMention','uses'=>'HomeController@getRelatedMention']);
Route::get('getRelatedMentionComment',['as'=>'getRelatedMentionComment','uses'=>'HomeController@getRelatedMentionComment']);

Route::get('fetch', [
  'as'   => 'fetch',
  'uses' => 'HomeController@fetchData'
]);

Route::get('changefont', [
  'as'   => 'changefont',
  'uses' => 'HomeController@changefont'
]);
Route::get('changeMentionfont', [
  'as'   => 'changeMentionfont',
  'uses' => 'HomeController@changeMentionfont'
]);
Route::get('changeMentionPostfont', [
  'as'   => 'changeMentionPostfont',
  'uses' => 'HomeController@changeMentionPostfont'
]);
Route::get('changeMentionCommentfont', [
  'as'   => 'changeMentionCommentfont',
  'uses' => 'HomeController@changeMentionCommentfont'
]);
Route::get('comment', ['as' => 'updateComment','uses' => 'MongoCRUDController@commentUpdate']);

Route::get('tag',['as' => 'MakeTag','uses' => 'MongoCRUDController@doTag']);

// Password Reset Routes...
Route::post('password/email', [
  'as' => 'password.email',
  'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
  'as' => 'password.request',
  'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
  'as' => '',
  'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
  'as' => 'password.reset',
  'uses' => 'Auth\ResetPasswordController@showResetForm'
]);
Route::get('register', [
  'as' => 'register',
  'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::get('projectRegister', [
  'as' => 'projectRegister',
  'uses' => 'Auth\RegisterController@show_projectRegister'
]);
Route::post('projectRegister', [
  'as' => '',
  'uses' => 'Auth\RegisterController@ProjectRegister'
]);
Route::post('register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register'
]);


Route::group(['middleware' => 'auth'], function () {
  Route::post('/notification/tag/notification','tagsController@notification');
  

Route::get('gethiddendiv', [
  'as'   => 'gethiddendiv',
  'uses' => 'HomeController@gethiddendiv'
]);

Route::resource('users','UserController');
Route::get('deleteuser/{id}', [ 'as' => 'deleteuser', 'uses' => 'UserController@destroy']);
Route::get('delete_assignUser/{uid}/{pid}/{id}', [ 'as' => 'delete_assignUser', 'uses' => 'UserController@delete_assignUser']);

Route::get('getuserlist', [ 'as' => 'getuserlist', 'uses' => 'UserController@getUserData']);
Route::get('getassignlist', [ 'as' => 'getassignlist', 'uses' => 'UserController@getassignlist']);
Route::get('edit_assignUser/{uid}/{pid}/{id}', [ 'as' => 'edit_assignUser', 'uses' => 'UserController@edit_assignUser']);
Route::get('update_assignUser', [ 'as' => 'update_assignUser', 'uses' => 'UserController@update_assignUser']);

Route::get('register', [
  'as' => 'register',
  'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register'
]);
Route::post('logout',['as' => 'logout','uses' => 'Auth\LoginController@logout']);

Route::get('setUpdatedPredict', [
  'as'=>'setUpdatedPredict',
  'uses' => 'HomeController@Updatepredict'
]);
Route::post('setMentionPredict', [
  'as'=>'setMentionPredict',
  'uses' => 'HomeController@UpdateMention'
]);
// Route::get('setMentionPostPredict', [
//   'as'=>'setMentionPostPredict',
//   'uses' => 'HomeController@UpdatePostMention'
// ]);
Route::get('SetHideMention', [
  'as'=>'SetHideMention',
  'uses' => 'HomeController@UpdateHideMention'
]);
Route::get('SetHideMentionPost', [
  'as'=>'SetHideMentionPost',
  'uses' => 'HomeController@UpdateHideMentionPost'
]);
Route::post('setUpdatedPostPredict', [
  'as'=>'setUpdatedPostPredict',
  'uses' => 'HomeController@UpdatedPostPredict'
]);
//tags
Route::resource('tags', 'tagsController');
Route::get('page_filter', [ 'as' => 'page_filter', 'uses' => 'HomeController@page_filter']);
Route::get('page_remove/{pg_name}', [ 'as' => 'page_remove', 'uses' => 'HomeController@page_remove']);
Route::get('page_restore/{pg_name}', [ 'as' => 'page_restore', 'uses' => 'HomeController@page_restore']);
Route::get('get_Mongopages', [ 'as' => 'get_Mongopages', 'uses' => 'HomeController@get_Mongopages']);
Route::get('get_Removepages', [ 'as' => 'get_Removepages', 'uses' => 'HomeController@get_Removepages']);
Route::get('gettaglist', [ 'as' => 'gettaglist', 'uses' => 'tagsController@gettaglist']);
Route::get('deletetag/{id}', [ 'as' => 'deletetag', 'uses' => 'tagsController@destroy']);
Route::get('convert_tag', [ 'as' => 'convert_tag', 'uses' => 'tagsController@convert']);


Route::get('getPageName',['as'=>'getPageName','uses'=>'HomeController@getPageName']);
Route::get('getMentionPage',['as'=>'getMentionPage','uses'=>'HomeController@getMentionPage']);
Route::get('getProjectName',['as'=>'getProjectName','uses'=>'HomeController@getProjectName']);


});
//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});
 
Route::get('page_delete',['as'=>'pagedelete','uses'=>'ProjectController@page_delete']);