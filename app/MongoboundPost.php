<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MongoboundPost extends Eloquent
{
    //
    protected $connection = 'mongodb';
	protected $collection = 'inbound_posts';
    protected $dates = ['created_time'];

  public function getDates() {
        return array();
  }
}
