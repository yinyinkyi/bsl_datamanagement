@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

<div class="row page-titles">
  <div class="col-md-5 col-8 align-self-center">
    <h3 class="text-themecolor m-b-0 m-t-0">Customer Care</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="javascript:void(0)">Customer Care</a></li>
      <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>
    </ol>
  </div>
  <div class="col-md-7 col-4 align-self-center">
    <div class="d-flex m-t-10 justify-content-end">
      <div class="d-flex m-r-20 m-l-10 hidden-md-down">
        <div class='input-group mb-3'>
          <input type='text' class="form-control dateranges" style="datepicker" />
          <div class="input-group-append">
            <span class="input-group-text">
              <span class="ti-calendar"></span>
            </span>
          </div>
        </div>
      </div>
   <!--    <div class="">
        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
      </div> -->
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

<!-- Row -->


        <!--  <div class="row" id="page-div-1">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Sentiment By Page</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="category-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="category-bar-chart" style="width:100%; height:400px;"></div>   
                            </div>
                        </div>
                    </div>
                
                </div> -->
  <div class="row" id="page-div-3">
  <!-- Column -->
  <div class="col-lg-12">
    <div class="card">
 
        <ul class="nav nav-tabs profile-tab" role="tablist">
         <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Popular" role="tab">Customer Voice</a> </li>
        <!-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Latest" role="tab">Text</a> </li> -->
        </ul>
         <div class="tab-content">
         <div class="tab-pane active" id="Popular" role="tabpanel">
          <div class="card-body">
          <div style="display:none"  align="center" style="vertical-align: top;" id="modal-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
  
  <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body"  style="height:60px">
                        
                                <div class="row">
                                    <div class="col-sm-3 nopadding">
                                        <div class="form-group">
        <select class="form-control custom-select" id="global_senti">
          <option value="">Sentiment</option>
          <option value="pos">Positive</option>
          <option value="neg">Negative</option>
        </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 nopadding">
                                        <div class="form-group">
                               <select class="form-control custom-select" id="global_emo">
          <option value="">Emotion</option>
          <option value="anger">anger</option>
          <option value="interest">interest</option>
          <option value="disgust">disgust</option>
          <option value="fear">fear</option>
          <option value="joy">joy</option>
          <option value="like">like</option>
          <option value="love">love</option>
          <option value="neutral">neutral</option>
          <option value="sadness">sadness</option>
          <option value="surprise">surprise</option>
          <option value="trust">trust</option>
          
        </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 nopadding">
                                        <div class="form-group">

                                          <div class="switchery-demo m-b-30">
                                           <span class="text-success"> ALL</span> <input type="checkbox" id="global_interest"  class="js-switch" data-color="#009efb" /> <span class="text-info">Enquires</span>
                                        </div>
                            <!--      <div class="demo-checkbox">
                                    
                                    <input type="checkbox" id="global_interest" class="filled-in" />
                                    <label for="global_interest">Interest</label>
                                   
                                </div> -->
                                        </div>
                                    </div>
                                    <div class="col-sm-3 nopadding">
                                        <div class="form-group">
                                            <button type="button" id="global-search" class="btn btn-primary">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <table id="all_mention" class="table" style="width:100%;margin-top:-2em">
          <thead style='display:none;'>
            <tr>
              <th >Rendering engine</th>
              <th>Setting1</th>
                         </tr>
          </thead>

        </table>
         </div>
            </div>
            <div id="show-task" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Comment Type</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="message-box">
                                <div class="comment-widgets message-scroll  comment_data">
                                    <!-- Message -->
                                  
                                   
                                </div>
                            </div>
                                                            </div>
                                                     
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
          
       </div>
        


  
    </div>
  </div>
 <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="postModal">Large modal</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body post_data">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

</div>

                <div class="row" id="page-div-2">
  <!-- Column -->
  <div class="col-lg-12">
    <div class="card">
 
        <ul class="nav nav-tabs profile-tab" role="tablist">
         <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#top_post" role="tab">Top Post</a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Latest" role="tab">Latest Post</a> </li>
        </ul>
         <div class="tab-content">
         <div class="tab-pane active" id="top_post" role="tabpanel">
          <div class="card-body">
            <div class="d-flex no-block align-items-center" >
           <div class="ml-auto" style="float:right;">
            <ul class="list-inline">
              <li>
               <button type="button" class="btn btn-success" name="bookmark" id="btn_bookmark">Bookmark</button>
               
              </li>

            </ul>
          </div>
        </div>
         <div style="display:none"  align="center" style="vertical-align: top;" id="modal-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
     
        <table id="tbl_top_post" class="table" style="width:100%;margin-top:-1.5em">
          <thead style='display:none;'>
            <tr>
              <th>Rendering engine</th>
              <th>Setting</th>
            </tr>
          </thead>

        </table>
         </div>
            </div>
            <div id="show-task" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Comment Type</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="message-box">
                                <div class="comment-widgets message-scroll  comment_data">
                                    <!-- Message -->
                                  
                                   
                                </div>
                            </div>
                                                            </div>
                                                     
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
           <div class="tab-pane" id="Latest" role="tabpanel">
         <div class="card-body">
          <div class="d-flex no-block align-items-center">
           <div class="ml-auto" style="float:right;">
            <ul class="list-inline">
              <li>
               <button type="button" class="btn btn-success" name="bookmark_comment" id="btn_bookmark_comment">Bookmark</button>
               
              </li>

            </ul>
          </div>
        </div>
        <table id="tbl_latest" class="table" style="width:100%;margin-top:-1.5em">
          <thead style='display:none;'>
            <tr>
              <th>Rendering engine</th>
              <th>Setting</th>
            </tr>
          </thead>

        </table>
        </div>
           </div>
       </div>
     
    </div>
  </div>

</div>
<!-- Row -->






@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
   <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}"></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
  <script type="text/javascript">

  var startDate;
  var endDate;
  var mention_total;
  var bookmark_array=[];
  var bookmark_remove_array=[];
  var bookmark_comment_array=[];
  var bookmark_comment_remove_array=[];
var colors =["#98c0d8","#7b858e","#01ad9d","#cb73a9","#a1c652","#dc3545","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/
$(document).ready(function() {

  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
  $("#range_influencer").ionRangeSlider({
    grid: true,
    min: 0,
    max: 10,
    from: 0,
    prefix: "Score ",
    max_postfix: "+"
  });

  startDate = moment().subtract(3, 'month');
  endDate = moment();

  var periodType= '';
periodType=$('input[name=options]:checked').val();
            //alert(periodType);
            if(periodType == '')
            {
              periodType='month';
            }
  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };
  
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Page Management'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();
  function ChooseDate(start,end,is_search)
{//alert(start);alert(end),alert(keyword)
 $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
 startDate=start;
 endDate=end;


 var brand_id = GetURLParameter('pid');
 var start = new Date(startDate);
 var end = new Date(endDate);
 var timeDiff = Math.abs(end.getTime() - start.getTime());
 var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 


          if(parseInt(diffDays) < 7) //in terms of day
          { 
            //alert("7");
            $("#day").show();
            $("#week").hide();
            $("#month").hide();
          }

          
         else if (parseInt(diffDays) == 7)//in terms of week
         {

           //alert("==7");
           $("#day").show();
           $("#week").show();
           $("#month").hide();


         }
          else if (parseInt(diffDays) > 7)//in terms of month
          {
            //alert(">7");

            $("#day").show();
            $("#week").show();
            $("#month").show();

          }
          //requestsentimentbycategory(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),is_search);
          Create_DataTable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),is_search);
          

        }
// function requestsentimentbycategory(fday,sday,is_search){//alert(fday);
      
// var categorychart = document.getElementById("category-bar-chart");
// var categoryChart = echarts.init(categorychart);
//       if(is_search === 1)
//           {
//             var txtkeyword =$("#txtkeyword").val();
//             var e = document.getElementById("sentiment_option");
//             var opt_sentiment = e.options[e.selectedIndex].value;
            
//           }


// $("#category-spin").show();
        
//        var brand_id = GetURLParameter('pid');
//        /* var brand_id = 22;*/
//        // alert (brand_id);
//     $.ajax({
//       type: "GET",
//       dataType:'json',
//       contentType: "application/json",
//       url: "{{route('getsentimentbypage')}}", // This is the URL to the API
//       data: { fday: fday,sday:sday,brand_id:brand_id,keyword:txtkeyword,sentiment:opt_sentiment }
//     })
//     .done(function( data ) {//alert(data);
          
//        console.log(data);//mention
//     var xAxisData = [];
// var data1 = [];
// var data2 = [];
// var data2_pos_sign = [];
// var positive_total=0;
// var negative_total=0;

// /*for (var i = 0; i < 10; i++) {
//     xAxisData.push('Category' + i);
//     data1.push((Math.random() * 2).toFixed(2));
//     data2.push(-Math.random().toFixed(2));
 
// }*/

// for(var i in data) 
//         {
//     xAxisData.push(data[i].page_name);
//     data1.push(Math.round(data[i].positive));
//     if(data[i].negative >0 )
//     {
//     data2.push(Math.round(-data[i].negative));
//     data2_pos_sign.push(Math.round(data[i].negative));
//     }
//     else
//     {
//      data2.push(0);
//     }
  
 
// }
// $.each(data1,function(){positive_total+=parseInt(this) || 0;});
// $.each(data2_pos_sign,function(){negative_total+=parseInt(this) || 0;});

// var itemStyle = {
//     normal: {
//     },
//     emphasis: {
//         barBorderWidth: 1,
//         shadowBlur: 10,
//         shadowOffsetX: 0,
//         shadowOffsetY: 0,
//         shadowColor: 'rgba(0,0,0,0.5)'
//     }
// };


// option = {
//       color:colors,
//     backgroundColor: 'rgba(0, 0, 0, 0)',
//      dataZoom:[  {
//             type: 'slider',
//             show: true,
//             xAxisIndex: [0],
//             start: 70,
//             end: 100
//         },
//          {
//             type: 'inside',
//             xAxisIndex: [0],
//             start: 1,
//             end: 35
//         }
//     ],
//         calculable : true,
//     legend: {
//         data: ['positive', 'negative'],
//            x: 'center',
//              y: 'top',
//               padding :0,
//                    formatter: function (name) {
//             if(name === 'positive')return name + ': ' + kFormatter(positive_total) ;
//             if(name === 'negative')return name + ': ' + kFormatter(negative_total);
            
         
//             },
//     }/*,
//     brush: {
//         toolbox: ['rect', 'polygon', 'lineX', 'lineY', 'keep', 'clear'],
//         xAxisIndex: 0
//     }*/,

//     toolbox: {
//             show : true,
//             feature : {
//                 mark : {show: true},
//                 dataView : {show: false, readOnly: false},
//                 magicType : {show: true, type: ['stack','tiled']},
//                 restore : {show: true},
//                 saveAsImage : {show: true}
//             }
//         },
//     tooltip: {},
//     calculable : true,
//     dataZoom : {
//         show : true,
//         realtime : true,
//         start : 20,
//         end : 80
//     },
//     xAxis: {
//         data: xAxisData,
//         name: 'page name',
//         silent: false,
//         axisLine: {onZero: true},
//         splitLine: {show: false},
//         splitArea: {show: false},
//        /*  axisLabel: {
//             textStyle: {
//                 color: '#fff'
//             }
//         },*/
//     },
//     yAxis: {
//         name: 'comment count',
//         inverse: false,
//         splitArea: {show: false}
//     },
//    grid: {
//             top: '12%',
//             left: '3%',
//             right: '10%',
//             containLabel: true
//         },
//   /*  visualMap: {
//         type: 'continuous',
//         dimension: 1,
//         text: ['High', 'Low'],
//         inverse: true,
//         itemHeight: 200,
//         calculable: true,
//         min: -2,
//         max: 6,
//         top: 60,
//         left: 10,
//         inRange: {
//             colorLightness: [0.4, 0.8]
//         },
//         outOfRange: {
//             color: '#bbb'
//         },
//         controller: {
//             inRange: {
//                 color: '#2f4554'
//             }
//         }
//     },*/
//     series: [
//         {
//             name: 'positive',
//             type: 'bar',
//             barMaxWidth:30,
//             stack: 'one',
//             color:colors[0],
//             itemStyle: itemStyle,
//             data: data1
//         },
//         {
//             name: 'negative',
//             type: 'bar',
//             barMaxWidth:30,
//             stack: 'one',
//             color:colors[1],
//             itemStyle: itemStyle,
//             data: data2
//         }
//     ]
// };

// $("#category-spin").hide();
// /*categoryChart.on('brushSelected', renderBrushed);

// function renderBrushed(params) {
//     var brushed = [];
//     var brushComponent = params.batch[0];

//     for (var sIdx = 0; sIdx < brushComponent.selected.length; sIdx++) {
//         var rawIndices = brushComponent.selected[sIdx].dataIndex;
//         brushed.push('[Series ' + sIdx + '] ' + rawIndices.join(', '));
//     }
// }
// */
//     categoryChart.setOption({
//         title: {
//             backgroundColor: '#333',
//           /*  text: 'SELECTED DATA INDICES: \n' + brushed.join('\n'),*/
//             bottom: 0,
//             right: 0,
//             width: 100,
//             textStyle: {
//                 fontSize: 12,
//                 color: '#fff'
//             }
//         }
//     });

//     categoryChart.setOption(option, true), $(function() {
//     function resize() {
//         setTimeout(function() {
//             categoryChart.resize()
//         }, 100)
//     }
//     $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
// });
//    categoryChart.on('click', function (params) {
//    console.log(params);
//    console.log(params.name); // xaxis data = education
//    console.log(params.seriesName); //bar type name ="positive"
//    console.log(params.value);//count 8
//    var pid = GetURLParameter('pid');
//    var source = GetURLParameter('source');
//    window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid+"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&category="+ params.name +"&type="+params.seriesName+"&from_graph=category" , '_blank');
//    //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
// });


   

  
//     })
//      .fail(function(xhr, textStatus, error) {
//        console.log(xhr.statusText);
//       console.log(textStatus);
//       console.log(error);
//       // If there is no communication between the server, show an error
//      // alert( "error occured" );
//     });

//   }
   function top_posts(fday,sday,keyword,sentiment)
{//alert("popular");


   $(".popup").unbind('click');
 var oTable = $('#tbl_top_post').DataTable({
        "pageLength": 5,
        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": false,
        "destroy": true,
        "ordering": false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getTopAndLatestPost') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
          "data": {
            "fday": fday,
            "sday": sday,
            "keyword": keyword,
            "sentiment": sentiment,
            "format_type": 'top_post',
             "limit":10,
            "brand_id": GetURLParameter('pid'),
          }

        },
        "initComplete": function( settings, json ) {
          console.log(json);
        },

        columns: [
        {data: 'post_div', name: 'post_div',orderable: false,searchable: true},
        {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        
      }).on('click', '.btn-bookmark', function () {
       //change css to bookmark star

       var current = $(this).attr('class');
       var id = $(this).attr('id');
       var name = $(this).attr('name');
  //if no bookmark prior 1- Change it to Bookmark css 2- Push to bookmark_array 3- remove from bookmark remove array if it is exist
       if(current == 'mdi mdi-star-outline text-yellow btn-bookmark')
       {
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star text-yellow btn-bookmark');
        if (jQuery.inArray(id, bookmark_array)=='-1') {
         bookmark_array.push(id) ;
        //need to remove it is exist in remove array
           if (jQuery.inArray(id, bookmark_remove_array)!='-1') {
        
              bookmark_remove_array.splice(jQuery.inArray(id,bookmark_remove_array), 1);
            

          } 
    
        } 

      }
//if  bookmark prior 1- Change it to No Bookmark css 2- Push to bookmark_remove_array 3- remove from bookmark array if it is exist
      else
      {
      /*  console.log("remove");
        console.log(name);*/
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star-outline text-yellow btn-bookmark');
           if (jQuery.inArray(id, bookmark_remove_array)=='-1') {
            bookmark_remove_array.push(id) ;
        if (jQuery.inArray(id, bookmark_array)!='-1') {
        
              bookmark_array.splice(jQuery.inArray(id,bookmark_array), 1);
            

          } 
    

          }
    
      

        
     }

      
    
  }).on('click', '.popup', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
  //  alert(name);
    var post_id = $(this).attr('id');
  //  alert(post_id);
    $("#modal-spin").show();
    $(".comment_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getInboundcomments") }}',
         type: 'GET',
         data: {id:post_id,cmt_type:name,brand_id:GetURLParameter('pid')},
         success: function(response) { console.log(response);
          var data=JSON.parse(response);
          console.log(data);

            for(var i in data) {
          
                   /*    $(".comment_data").append("<a> "+
                                  " <div class='user-img'> <span class='round'>A</span> <span class='profile-status away pull-right'></span> </div> "+
                                     "  <div class='mail-contnet'> " +
                                  " <h5>"+data[i].created_time+"</h5> <span class='mail-desc'>"+data[i].message+"</span> <span class='time'>"+data[i].created_time+"</span> </div> " +
                                 " </a>");

                */
               var html ='<div class="d-flex flex-row comment-row"> '+
                  ' <div class="user-img"> <span class="round">A</span> <span class="profile-status away pull-right"></span> </div> '+
                    '   <div class="comment-text w-100" style="padding:15px 0px 15px 10px;"> '+
               '<span class="text-muted pull-right">'+data[i].created_time+'</span>';
               html+='<p>Sentiment: <select class="form-control custom-select sentiment-color comment-select" id="sentiment_comment_'+data[i].id+'"  >';
                 if(data[i].sentiment == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="pos"';
                if(data[i].sentiment == "pos")
                html+= 'selected="selected"';
                html+= '>pos</option><option value="neg"';
                if(data[i].sentiment == "neg")
                html+= 'selected="selected"';
                html+= '>neg</option><option value="neutral"';
                if(data[i].sentiment == "neutral")
                html+= 'selected="selected"';      
                html+= '>neutral</option><option value="NA"';
                if(data[i].sentiment == "NA")
                html+= 'selected="selected"';  
                html+= '>NA</option></select>';

                html+= ' Emotion: <select class="form-control custom-select emotion-color comment-select" id="emotion_comment_'+data[i].id+'"  >';
                if(data[i].emotion == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="anger"';
                if(data[i].emotion == "anger")
                html+= 'selected="selected"';
                html+= '>anger</option><option value="interest"';
                if(data[i].emotion == "interest")
                html+= 'selected="selected"';     
                html+= '>interest</option><option value="disgust"';
                if(data[i].emotion == "disgust")
                html+= 'selected="selected"';  
                html+= '>disgust</option><option value="fear"';
                if(data[i].emotion == "fear")
                html+= 'selected="selected"'; 
                html+= '>fear</option><option value="joy"';
                if(data[i].emotion == "joy")
                html+= 'selected="selected"'; 
                html+= '>joy</option><option value="like"';
                if(data[i].emotion == "like")
                html+= 'selected="selected"'; 
                html+= '>like</option><option value="love"';
                if(data[i].emotion == "love")
                html+= 'selected="selected"'; 
                html+= '>love</option><option value="neutral"';
                if(data[i].emotion == "neutral")
                html+= 'selected="selected"';
                html+= '>neutral</option><option value="sadness"';
                if(data[i].emotion == "sadness")
                html+= 'selected="selected"';
                html+= '>sadness</option><option value="surprise"';
                if(data[i].emotion == "surprise")
                html+= 'selected="selected"';
                html+= '>surprise</option><option value="trust"';
                if(data[i].emotion == "trust")
                html+= 'selected="selected"';
                html+= '>trust</option><option value="NA"';
                if(data[i].emotion == "NA")
                html+= 'selected="selected"';
                html+= '>NA</option></select>';
                if(data[i].edit_permission === 1)
                html+= ' <a class="edit_predict_comment" id="'+data[i].id+'" href="javascript:void(0)" style="width:10%;"><i class="ti-pencil-alt"></i></a> ';
                html+=' </span></p> ' + 
                  ' <div class="m-b-5">'+data[i].message+'</div>'+
                 '<div class="comment-footer">'+
                  
                 '</div>'+
                 '</div>'+
                  '</div>';
       
                $(".comment_data").append(html);
        }


         $("#modal-spin").hide();
         $("#myModalLabel").text(name);
         $('#show-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.edit_post_top_post_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
  var sentiment = $('#sentiment_top_post_'+post_id+' option:selected').val()
  var emotion = $('#emotion_top_post_'+post_id+' option:selected').val()
/*  alert(post_id + sentiment + emotion );*/
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) { /*alert(response)*/
          if(response>0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  });

}

 function latest_posts(fday,sday,keyword,sentiment)
{//alert("popular");
   $(".popup").unbind('click');
 var oTable = $('#tbl_latest').DataTable({
        "pageLength": 5,
        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": false,
        "destroy": true,
        "ordering": false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getTopAndLatestPost') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
          "data": {
            "fday": fday,
            "sday": sday,
            "keyword": keyword,
            "sentiment": sentiment,
            "format_type": 'latest_post',
             "limit":10,
            "brand_id": GetURLParameter('pid'),
          }

        },
        "initComplete": function( settings, json ) {
          console.log(json);
        },

        columns: [
        {data: 'post_div', name: 'post_div',orderable: false,searchable: true},
        {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        
      }).on('click', '.btn-bookmark', function () {
       //change css to bookmark star

       var current = $(this).attr('class');
       var id = $(this).attr('id');
       var name = $(this).attr('name');
  //if no bookmark prior 1- Change it to Bookmark css 2- Push to bookmark_array 3- remove from bookmark remove array if it is exist
       if(current == 'mdi mdi-star-outline text-yellow btn-bookmark')
       {
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star text-yellow btn-bookmark');
        if (jQuery.inArray(id, bookmark_array)=='-1') {
         bookmark_array.push(id) ;
        //need to remove it is exist in remove array
           if (jQuery.inArray(id, bookmark_remove_array)!='-1') {
        
              bookmark_remove_array.splice(jQuery.inArray(id,bookmark_remove_array), 1);
            

          } 
    
        } 

      }
//if  bookmark prior 1- Change it to No Bookmark css 2- Push to bookmark_remove_array 3- remove from bookmark array if it is exist
      else
      {
      /*  console.log("remove");
        console.log(name);*/
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star-outline text-yellow btn-bookmark');
           if (jQuery.inArray(id, bookmark_remove_array)=='-1') {
            bookmark_remove_array.push(id) ;
        if (jQuery.inArray(id, bookmark_array)!='-1') {
        
              bookmark_array.splice(jQuery.inArray(id,bookmark_array), 1);
            

          } 
    

          }
    
      

        
     }

      
    
  }).on('click', '.popup', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".comment_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getInboundcomments") }}',
         type: 'GET',
         data: {id:post_id,cmt_type:name,brand_id:GetURLParameter('pid')},
         success: function(response) { console.log(response);
          var data=JSON.parse(response);
          console.log(data);

            for(var i in data) {
          var html ='<div class="d-flex flex-row comment-row"> '+
                  ' <div class="user-img"> <span class="round">A</span> <span class="profile-status away pull-right"></span> </div> '+
                    '   <div class="comment-text w-100" style="padding:15px 0px 15px 10px;"> '+
               '<span class="text-muted pull-right">'+data[i].created_time+'</span>';
               html+='<p>Sentiment: <select class="form-control custom-select sentiment-color comment-select" id="sentiment_comment_'+data[i].id+'"  >';
                 if(data[i].sentiment == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="pos"';
                if(data[i].sentiment == "pos")
                html+= 'selected="selected"';
                html+= '>pos</option><option value="neg"';
                if(data[i].sentiment == "neg")
                html+= 'selected="selected"';
                html+= '>neg</option><option value="neutral"';
                if(data[i].sentiment == "neutral")
                html+= 'selected="selected"';  
                html+= '>neutral</option><option value="NA"';
                if(data[i].sentiment == "NA")
                html+= 'selected="selected"';  
                html+= '>NA</option></select>';

                html+= ' Emotion: <select class="form-control custom-select emotion-color comment-select" id="emotion_comment_'+data[i].id+'"  >';
                if(data[i].emotion == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="anger"';
                if(data[i].emotion == "anger")
                html+= 'selected="selected"';
                html+= '>anger</option><option value="interest"';
                if(data[i].emotion == "interest")
                html+= 'selected="selected"';     
                html+= '>interest</option><option value="disgust"';
                if(data[i].emotion == "disgust")
                html+= 'selected="selected"';  
                html+= '>disgust</option><option value="fear"';
                if(data[i].emotion == "fear")
                html+= 'selected="selected"'; 
                html+= '>fear</option><option value="joy"';
                if(data[i].emotion == "joy")
                html+= 'selected="selected"'; 
                html+= '>joy</option><option value="like"';
                if(data[i].emotion == "like")
                html+= 'selected="selected"'; 
                html+= '>like</option><option value="love"';
                if(data[i].emotion == "love")
                html+= 'selected="selected"'; 
                html+= '>love</option><option value="neutral"';
                if(data[i].emotion == "neutral")
                html+= 'selected="selected"';
                html+= '>neutral</option><option value="sadness"';
                if(data[i].emotion == "sadness")
                html+= 'selected="selected"';
                html+= '>sadness</option><option value="surprise"';
                if(data[i].emotion == "surprise")
                html+= 'selected="selected"';
                html+= '>surprise</option><option value="trust"';
                if(data[i].emotion == "trust")
                html+= 'selected="selected"';
                html+= '>trust</option><option value="NA"';
                if(data[i].emotion == "NA")
                html+= 'selected="selected"';
                html+= '>NA</option></select>';
                if(data[i].edit_permission === 1)
                html+= ' <a class="edit_predict_comment" id="'+data[i].id+'" href="javascript:void(0)" style="max-width:10%;"><i class="ti-pencil-alt"></i></a> ';
                html+=' </span></p> ' + 
                  ' <div class="m-b-5">'+data[i].message+'</div>'+
                 '<div class="comment-footer">'+
                  
                 '</div>'+
                 '</div>'+
                  '</div>';
       
                $(".comment_data").append(html);
        }


         $("#modal-spin").hide();
         $("#myModalLabel").text(name);
         $('#show-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.edit_post_latest_post_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
  var sentiment = $('#sentiment_latest_post_'+post_id+' option:selected').val();
  var emotion = $('#emotion_latest_post_'+post_id+' option:selected').val();
 /* alert(post_id);*/
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) { /*alert(response)*/
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  });
}
$(document).on('click', '.edit_predict_comment', function(e) {
     var comment_id = $(this).attr('id');
   
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_comment_'+comment_id+' option:selected').val();
  var emotion = $('#emotion_comment_'+comment_id+' option:selected').val();
  //alert(comment_id+sentiment + emotion);

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:comment_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {//alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
     
});
 function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}

$('.dateranges').daterangepicker({
  locale: {
    format: 'MMMM D, YYYY'
  },
  ranges: {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  startDate: startDate,
  endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,0);
      });


     //   $('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {alert("hihi");
     //     $('#daterange-btn span').html(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
     //     startDate=picker.startDate;
     //     endDate=picker.endDate;
     //     ChooseDate(startDate,endDate,'');

     /*$('#all_mention').DataTable().rows().remove().draw();*/
          /* $('#all_mention').DataTable().destroy();
          $('#all_mention').empty();*/


//});


$('input[name=options]').mouseup(function(){
    //alert("Before change "+$('input[name=options]:checked').val());
}).change(function(){
     periodType=$('input[name=options]:checked').val();
     ChooseDate(startDate,endDate);
    //alert("After change "+$('input[name=options]:checked').val());
})

$( "#btn_search" ).click(function() {//alert("hihi");

 // $("#search_form").hide();
  
  ChooseDate(startDate,endDate,1);
  //Create_DataTable(txtkeyword);

});


ChooseDate(startDate,endDate,0);

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   //alert(target);

   window.dispatchEvent(new Event('resize'));

 });

    

    function Create_DataTable(fday,sday,is_search)
    {
      if(is_search === 1)
          {
            var txtkeyword =$("#txtkeyword").val();
            var e = document.getElementById("sentiment_option");
            var opt_sentiment = e.options[e.selectedIndex].value;
          }
     top_posts(fday,sday,txtkeyword,opt_sentiment);
     latest_posts(fday,sday,txtkeyword,opt_sentiment); 
     All_Comment_DataTable(fday,sday,txtkeyword,opt_sentiment);
    

    }

    function All_Comment_DataTable(fday,sday,keyword,sentiment)
    { //alert($('#global_interest').prop("checked"));
      $(".popup_post").unbind('click');
 var voiceTable = $('#all_mention').DataTable({
        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": false,
        "destroy": true,
        "ordering": false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getRelatedcomment') }}',
           // data: function (d) {
           //      d.tsearch_senti = $('#global_senti option:selected').val(),
           //      d.tsearch_emo = $('#global_emo option:selected').val(),
           //  }
          "data": {
            "fday": fday,
            "sday": sday,
            "keyword": keyword,
            "sentiment": sentiment,
            "brand_id": GetURLParameter('pid'),
            "tsearch_senti":$('#global_senti option:selected').val(),
            "tsearch_emo": $('#global_emo option:selected').val(),
            "tsearch_interest":$('#global_interest').prop("checked"),
          }

        },
        "initComplete": function( settings, json ) {
          console.log(json);
       
        },
        drawCallback: function() {
     $('.select2').select2();
  },

        columns: [
        {data: 'post_div', name: 'post_div',orderable: false,searchable: true},
        {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        
      }).on('click', '.popup_post', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".post_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getRelatedPosts") }}',
         type: 'GET',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { console.log(response);
          var data=JSON.parse(response);
          console.log(data);

            for(var i in data) {//alert(data[i].message);

         /*       $(".post_data").append("<div class='d-flex flex-row comment-row'> "+
                  " <div class='user-img'> <span class='round'>A</span> <span class='profile-status away pull-right'></span> </div> "+
                  " <div class='comment-text w-100'> "+
               "<span class='text-muted pull-right'>"+data[i].created_time+"</span>"+ 
                  " <div class='m-b-5'>"+data[i].message+"</div>"+
                  "<div class='comment-footer'>"+
                  
                  "</div>"+
                  "</div>"+
                  "</div>");*/
 var html='<div class="profiletimeline"><div class="sl-item">'+
               '  <div class="sl-left">'+
               '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
                ' <div class="sl-right">'+
                '<div><a href="'+data[i].link+'" target="_blank">'+data[i].page_name+'</a> <span class="sl-date">'+data[i].created_time+'</span>'+
                 '</div>';
           // html + =  '<p>Sentiment: <select class="form-control custom-select sentiment-color customize-select" id="sentiment_post_'+data[i].id+'"  >';
           //      if(data[i].sentiment == "")
           //      html+= '<option value="" selected="selected"></option>';
           //      html+='<option value="pos"';
           //      if(data[i].sentiment == "pos")
           //      html+= 'selected="selected"';
           //      html+= '>pos</option><option value="neg"';
           //      if(data[i].sentiment == "neg")
           //      html+= 'selected="selected"';     
           //      html+= '>neg</option><option value="neutral"';
           //      if(data[i].sentiment == "neutral")
           //      html+= 'selected="selected"'; 
           //      html+= '>neutral</option><option value="NA"';
           //      if(data[i].sentiment == "NA")
           //      html+= 'selected="selected"';  
           //      html+= '>NA</option></select>';

           //      html+= ' | Emotion: <select class="form-control custom-select emotion-color customize-select" id="emotion_post_'+data[i].id+'" ><option value="" selected="selected"><option value="anger"';
           //      if(data[i].emotion == "anger")
           //      html+= 'selected="selected"';
           //      html+= '>anger</option><option value="interest"';
           //      if(data[i].emotion == "interest")
           //      html+= 'selected="selected"';     
           //      html+= '>interest</option><option value="disgust"';
           //      if(data[i].emotion == "disgust")
           //      html+= 'selected="selected"';  
           //      html+= '>disgust</option><option value="fear"';
           //      if(data[i].emotion == "fear")
           //      html+= 'selected="selected"'; 
           //      html+= '>fear</option><option value="joy"';
           //      if(data[i].emotion == "joy")
           //      html+= 'selected="selected"'; 
           //      html+= '>joy</option><option value="like"';
           //      if(data[i].emotion == "like")
           //      html+= 'selected="selected"'; 
           //      html+= '>like</option><option value="love"';
           //      if(data[i].emotion == "love")
           //      html+= 'selected="selected"'; 
           //      html+= '>love</option><option value="neutral"';
           //      if(data[i].emotion == "neutral")
           //      html+= 'selected="selected"';
           //      html+= '>neutral</option><option value="sadness"';
           //      if(data[i].emotion == "sadness")
           //      html+= 'selected="selected"';
           //      html+= '>sadness</option><option value="surprise"';
           //      if(data[i].emotion == "surprise")
           //      html+= 'selected="selected"';
           //      html+= '>surprise</option><option value="trust"';
           //      if(data[i].emotion == "trust")
           //      html+= 'selected="selected"';
           //      html+= '>trust</option><option value="NA"';
           //      if(data[i].emotion == "NA")
           //      html+= 'selected="selected"';
           //      html+= '>NA</option></select>';
           //      if(data[i].edit_permission === 1)
           //      html+= ' <a class="edit_predict_post" id="'+data[i].id+'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a> ';
                html+=' </span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                       readmore(data[i].message) +
                         '...<a href="'+data[i].link+'" target="_blank">' + 
                       ' Read More</a>' + 
                    '</p> </div></div>';
                  
 $(".post_data").append(html);
     
        }


         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.edit_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    var tags = $('#tags_'+post_id).val();
/*  alert(tags);
  return;*/
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags},
         success: function(response) {// alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  });
    }
 $('#global-search').on('click', function(e) {

        All_Comment_DataTable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),'','');
        e.preventDefault();
    });

$(document).on('click', '.edit_predict_post', function(e) {
     var post_id = $(this).attr('id');
   
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_post_'+post_id+' option:selected').val();
  var emotion = $('#emotion_post_'+post_id+' option:selected').val();
  //alert(sentiment + emotion);

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {//alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
     
});



$( "#btn_bookmark" ).click(function() {
  //alert(bookmark_array);
  //alert(bookmark_remove_array);
  if(Object.keys(bookmark_array).length > 0 || Object.keys(bookmark_remove_array).length > 0)
  {
        //save bookmark data;
        console.log(bookmark_remove_array);
        event.preventDefault();

        $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("in_postbookmark") }}',
         type: 'POST',
         data: {bookmark_array:bookmark_array,bookmark_remove_array:bookmark_remove_array,id:GetURLParameter('pid')},
         success: function(response) {//alert(response);
         swal({   
            title: "Bookmark!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
         
        }
      });


      }
    });


$( "#btn_bookmark_comment" ).click(function() {
/*  alert(bookmark_comment_array);
  alert(bookmark_comment_remove_array);*/
  if(Object.keys(bookmark_comment_array).length > 0 || Object.keys(bookmark_comment_remove_array).length > 0)
  {
        //save bookmark data;
    
        event.preventDefault();

        $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("inbound_comment_bookmark") }}',
         type: 'POST',
         data: {bookmark_comment_array:bookmark_comment_array,bookmark_comment_remove_array:bookmark_comment_remove_array,id:GetURLParameter('pid')},
         success: function(response) {//alert(response);
         swal({   
            title: "Bookmark!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
         
        }
      });


      }
    });




//local customize function



function kFormatter(num) {
  return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
function readmore(message){
      // alert("hi hi ");
      var string = String(message);
      var length = string.length; 
         // alert(length);
         if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.

            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
          }
          return string;


        }

        function judge_sentiment_icon(sentiment)
        {
         var emojis = ['0x1F60E', '0x1F626', '0x1F610'];//
         if (sentiment ==="pos") return  String.fromCodePoint(emojis[0]) + " positive"; 
         else if(sentiment ==="neg") return  String.fromCodePoint(emojis[1])+ " negative" ;
         else return String.fromCodePoint(emojis[2]) + " neutral" ;
       }

       function judge_emotion_icon(emotion)
       {
        var emojis = ['0x1F620', '0x1F604', '0x1F61D', '0x1F628', '0x1F604', '0x1F44D',
            '0x2764', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

            if (emotion ==="anger") return emojis[0]; 
            else if(emotion ==="anticipation") return emojis[1] ;
            if (emotion ==="disgust") return emojis[2] ; 
            else if(emotion ==="fear") return emojis[3] ;
            if (emotion ==="joy") return emojis[4]; 
            else if(emotion ==="like") return emojis[5] ;
            if (emotion ==="love") return emojis[6] ; 
            else if(emotion ==="neutral") return emojis[7] ;
            if (emotion ==="sadness")  return emojis[8]; 
            else if(emotion ==="surprise") return emojis[9]  ;
            else if(emotion === "trust") return emojis[10];

          }

        });
</script>
<script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
<link href="{{asset('css/own.css')}}" rel="stylesheet">

@endpush