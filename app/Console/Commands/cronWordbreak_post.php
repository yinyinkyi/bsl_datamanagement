<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class cronWordbreak_post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wordbreak_post:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update word break message in post table every 2 second';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $projects = DB::table('projects')->select('*')->get();
      foreach($projects as $project)
      {
        $post_table = 'temp_'.$project->id.'_posts';
        $query = "select temp_id,message from ".$post_table." where message <> '' and message is not null and ( wb_message is null or wb_message ='') LIMIT 500 "; 
        $posts = DB::select($query);
        $data = [];
        $data_message = [];
        foreach($posts as $post)
        {
        
          $request['id'] = $post->temp_id;
          $message = $post->message;
          $z_count = substr_count(strtolower($message), 'zawgyi');
          $u_count = substr_count(strtolower($message), 'unicode');
           if ($z_count == 1 && $u_count == 1)
           {
            
                   $request['message'] = $this ->fontCheck($message);
                  
           }
          
           else{
            $request['message']  = $message;
          }

          $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$request['message']);
          $data[] =$request;
          
        }

        if(count($data_message)> 0)
        {
          $client = new Client(['base_uri' => 'http://35.185.97.177:5002/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_wb = 'wb_posTag';
          
              $formData = array(
    'raw' =>  $data_message,
   
); 
    
           $formData = json_encode($formData);
           try{
           $wb_response = $client->post($uri_wb, [
                                'form_params' => [
                                'raw' =>  $formData,
                                ],
                             ]);
       
            $wb_result = ($wb_response->getBody()->getContents());
           //
            $json_wb_array = json_decode($wb_result, true);
          }
                    catch (\Exception $e) {
  
                $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                $error_message = explode('<html>',$error_message);
                // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                //dd($error_message[0]);
                $error_message=$error_message[0];

                //$error_message ='test';
                $sms_data=json_encode([
                  'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                  'group_id' =>2,
                ]);
                $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                  'headers' => ['Content-Type' => 'application/json'],
                  'body' => $sms_data
                ]);
                 //dd($sms_response->getBody()->getContents());
                  // return false;
                }

            $wb_result = $json_wb_array['raw'];
             // dd($wb_result);
            $count = (Int)count($data);
            for($i=0;$i<$count;$i++)
            {
                $id = $data[$i]['id'];
                
           $wb =  DB::table($post_table)->where('temp_id', $id)->update(['wb_message' => $wb_result[$i],'message' => $data[$i]['message'],'updated_at' => now()->toDateTimeString()]);
            }

   
        }
        
    }
  }

    function fontCheck($message)
    {

   
      $firstCharacter = substr(strtolower($message), 0, 8);
      if (strpos($firstCharacter, 'zawgyi') !== false) {
            
      $split = explode("unicode",strtolower($message));
      $z_msg = $split[0];
      
      $last_space_position = strrpos($z_msg, ' ');
      $message = substr($z_msg, 0, $last_space_position);
      $message = str_replace("zawgyi", "", strtolower($message));
      $first2Characters = substr(strtolower($message), 0, 2);
      $message = trim($message,$firstCharacter);

           
        }
        else
        {
       
          $split = explode("zawgyi",strtolower($message));
    
          $z_msg = $split[1];
     
          $firstCharacter = substr(strtolower($z_msg), 0, 1);
          
          $message = trim($z_msg,$firstCharacter);
        
            }
         
     
    
    
     return $message;
   }
}
