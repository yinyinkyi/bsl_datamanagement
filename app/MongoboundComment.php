<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MongoboundComment extends Eloquent
{
    //
    protected $connection = 'mongodb';
	protected $collection = 'inbound_comments';
    protected $dates = ['created_time'];

	  public function getDates() {
	        return array();
	  }
}
