@extends('layouts')
<!-- @section('menu')

<ul id="sidebarnav">
<li><a href="" ><i class="fa fa-circle-o"></i> Need to Check</a></li>
    <li><a href="" ><i class="fa fa-circle-o"></i>Confirmed</a></li>



  </ul>
     @endsection   -->
     @section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-4 col-4 align-self-center">
                        <div class="form-group">
                            <select class="form-control custom-select" id="db_selection">
                              @foreach($databases as $db)
                              <option value="{{ $db->db_name }}"> {{ $db->db_name }} </option>
                              @endforeach
                             
                            </select>
                         </div>
                    
                    </div>
            <div class="col-md-4 col-4 align-self-center">
                         <div class="form-group">
                          <select class="form-control custom-select" id="project_filter">
                          @if (isset($project_ids))
                          @foreach($project_ids as $index =>$project_ids)
                          @if($index == 0 )
                          <option value="{{$project_ids->id}}"  selected>{{$project_ids->name}}</option>
                          @else
                           <option value="{{$project_ids->id}}" >{{$project_ids->name}}</option>
                          @endif
                          @endforeach
                          @endif
                         </select>
                         </div>
                       </div>
                         <div class="col-md-4 col-4 align-self-center">
                         <div class="form-group">
                          <select class="form-control custom-select" id="admin_page_filter">
                          @if (isset($pages))
                          @foreach($pages as $index =>$pages)
                          @if($default_page == $pages )
                          <option value="{{$pages}}" id="{{$pages}}" selected>{{$pages}}</option>
                          @else
                           <option value="{{$pages}}" id="{{$pages}}" >{{$pages}}</option>
                          @endif
                          @endforeach
                          @endif
                         </select>
                         </div>
                       </div>
                </div>

              
                <!-- Row -->
<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row"> 
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <select class="form-control custom-select" id="global_senti">
                                              <option value="">ALL Sentiment</option>
                                              <option value="pos">Positive</option>
                                              <option value="neg">Negative</option>
                                              <option value="neutral">Neutral</option>
                                              <option value="NA">NA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                      <div class="form-group">
                                            <div class="switchery-demo m-b-30">
                                                <span class="text-success"> ALL</span> <input type="checkbox" id="global_interest"  class="js-switch" data-color="#009efb" /> <span class="text-info">Enquires</span>
                                            </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-4">
                                         <div class="d-flex m-t-8">
                                                <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                                    <div class='input-group mb-3'>
                                                      <input type='text' id="calender" class="form-control dateranges" style="color: #67757c !important; width:210px; padding: 0.2em 0.2em 0px;" />
                                                      <div class="input-group-append">
                                                        <span class="input-group-text">
                                                          <span class="ti-calendar"></span>
                                                        </span>
                                                      </div>
                                                    </div>
                                                </div>
                                         </div>
                                    </div>
                                    <div class="col-lg-2">
                                       <div class="form-group">
                                          <button type="button" id="global-search" style="background:#1e88e4;border:none;" class="btn btn-primary">Search</button>
                                          <button type="button"  id="more" style="border:none;float: right;" class="btn btn-danger">More..</button>
                                      </div>
                                    </div>
                               </div>
                               <div class="row" id="search_div" style="display: none">
                                  <div class="col-sm-12">
                                      <div class="card">
                                          <div class="card-body">
                                              <div class="col-sm-3 ">
                                                  <div class="form-group">
                                                      <input type="text" placeholder="Search by id" id="search" name="search" /> 
                                                  </div>
                                              </div>
                                              <div class="col-sm-3 ">
                                                  <div class="form-group">
                                                    <input type="text" id="searchby_name" placeholder="Search by reviewer"> 
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                               </div>
                              <div id="comment-text">Confirmed</div>
                              <div class="table-responsive m-t-10">
                                <table id="comment_table" class="table" style="width:100%">
                                    <thead style="display: none">
                                    <tr>
                                      <th >Rendering engine</th>
                                      <th>Setting1</th>
                                    </tr>
                                    </thead>
                                </table>
                             </div>
                             <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="postModal">Large modal</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body post_data"></div>
                                        <div class="modal-footer">
                                            <button type="button" style="background:#1e88e4;border:none;" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>          <!-- /.modal-content -->
                                </div>         <!-- /.modal-dialog -->
                            </div> 
                        </div>
                     </div>
                </div>
            </div> 

              
                
              
                
    
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
   <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}"></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
  <script type="text/javascript">
  	var startDate;
    var endDate;

    $(document).ready(function() {
     // alert(text);
    //   $("#sidebarnav li a").click(function() {
    // $(this).parent().addClass('selected').siblings().removeClass('selected');

    // });
      // $('#global_senti').change(function(){
      //   alert("selected box changed");
      // });
      // $('#global_interest').change(function(){
      //      alert("checkbox changed");
      // });
      //   $('#calender').change(function(){
      //      alert("calender changed");
      // });


      $('li > a').click(function() {
          $('li').removeClass();
          $(this).parent().addClass('active');
      });;

        $('.dateranges').daterangepicker({
  locale: {
    format: 'MMM D, YYYY'
  },
  ranges: {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  startDate: startDate,
  endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;

        startDate = start;
        endDate = end;
        var id_search = document.getElementById("search").value;
        var name_search = document.getElementById("searchby_name").value;
        ChooseDate(startDate,endDate,0,id_search,name_search);
      });
        startDate = moment().subtract(3, 'month');
    endDate = moment();


     $("#sidebarnav li a").click(function(){
    
      // var text = $('#sidebarnav .nav').attr('id');
    //alert(text);
       
      // var text = $("#sidebarnav li.active a").text();   /*needtocheck or confrim*/
      // alert(sectionId);
      var text = $(this).text();
       // alert(text);
      
       document.getElementById('comment-text').innerHTML = text;

      var e = document.getElementById("db_selection");
      var db_name = e.options[e.selectedIndex].text;
      // alert(startDate);
      // alert(endDate);
      // Create_DataTable(startDate,endDate,0,db_name,text);
      var id_search = document.getElementById("search").value;
      var name_search = document.getElementById("searchby_name").value;
      ChooseDate(startDate,endDate,0,id_search,name_search);
      e.preventDefault();
     

});

    

    var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };

   var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
          $("#admin_page_filter").change(function(){
      var id_search = document.getElementById("search").value;
      var name_search = document.getElementById("searchby_name").value;
      ChooseDate(startDate,endDate,0,id_search,name_search);

  })
  $( "#db_selection" ).change(function()   {

  
        var db_name = $(this).val();
        var text = $("#sidebarnav li.active").text();   /*needtocheck or confrim*/
     
        $('#project_filter').empty();
         $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getProjectName')}}", // This is the URL to the API
      data: { db_name:db_name}
   
    })
    .done(function( data ) {//$("#popular").html('');
      // console.log(data);
     for(var i in data) {

     $('#project_filter').append( '<option value="'+data[i]+'">'+data[i]+'</option>' );
}
  var id_search = document.getElementById("search").value;
  var name_search = document.getElementById("searchby_name").value;
  ChooseDate(startDate,endDate,0,id_search,name_search);
     

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });


    });
  $( "#project_filter" ).change(function()   {

   var selected_prj = document.getElementById("project_filter");
               var prj_name = selected_prj.options[selected_prj.selectedIndex].text;
               document.getElementById("prj").innerHTML = prj_name;
var project_id = $(this).val();
// alert(project_id);
var text = $("#sidebarnav li.active").text();   /*needtocheck or confrim*/

$('#admin_page_filter').empty();
 $.ajax({
type: "GET",
dataType:'json',
contentType: "application/json",
url: "{{route('getPageName')}}", // This is the URL to the API
data: { project_id:project_id}

})
.done(function( data ) {//$("#popular").html('');
for(var i in data) {

$('#admin_page_filter').append( '<option value="'+data[i]+'">'+data[i]+'</option>' );
}
var id_search = document.getElementById("search").value;
var name_search = document.getElementById("searchby_name").value;
ChooseDate(startDate,endDate,0,id_search,name_search);


})
.fail(function(xhr, textStatus, error) {
console.log(xhr.statusText);
console.log(textStatus);
console.log(error);
// If there is no communication between the server, show an error
// alert( "error occured" );
});


})




$('input[name=options]').mouseup(function(){
    //alert("Before change "+$('input[name=options]:checked').val());
}).change(function(){
     periodType=$('input[name=options]:checked').val();
     var id_search = document.getElementById("search").value;
     var name_search = document.getElementById("searchby_name").value;
     ChooseDate(startDate,endDate,0,id_search,name_search);
    //alert("After change "+$('input[name=options]:checked').val());
})

$( "#btn_search" ).click(function() {//alert("hihi");

 // $("#search_form").hide();
  var id_search = document.getElementById("search").value;
  var name_search = document.getElementById("searchby_name").value;
  ChooseDate(startDate,endDate,1,id_search,name_search);
  //Create_DataTable(txtkeyword);

});

var id_search = document.getElementById("search").value;
var name_search = document.getElementById("searchby_name").value;
ChooseDate(startDate,endDate,0,id_search,name_search);

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   //alert(target);

   window.dispatchEvent(new Event('resize'));

 });




    function ChooseDate(start,end,is_search,id_search,name_search)
   {
     $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
     startDate=start;
     endDate=end;


       var brand_id = GetURLParameter('pid');
       // var brand_id = 17;
       var start = new Date(startDate);
       var end = new Date(endDate);
       var timeDiff = Math.abs(end.getTime() - start.getTime());
       var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 


          if(parseInt(diffDays) < 7) //in terms of day
          { 
            //alert("7");
            $("#day").show();
            $("#week").hide();
            $("#month").hide();
          }

          
         else if (parseInt(diffDays) == 7)//in terms of week
         {

           //alert("==7");
           $("#day").show();
           $("#week").show();
           $("#month").hide();


         }
          else if (parseInt(diffDays) > 7)//in terms of month
          {
            //alert(">7");

            $("#day").show();
            $("#week").show();
            $("#month").show();

          }
        //  var text = $("#sidebarnav li.active").text();   /*needtocheck or confrim*/
           var selected_db = document.getElementById("db_selection");
            var db_name = selected_db.options[selected_db.selectedIndex].text;

            
          //requestsentimentbycategory(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),is_search);
          Create_DataTable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),is_search,id_search,name_search);
          

        }
         $('#search').on('keyup',function(){
        var id_search=$(this).val();
        var name_search = document.getElementById("searchby_name").value;
        ChooseDate(startDate,endDate,0,id_search,name_search);
        
        })

        $('#searchby_name').on('keyup',function(){
        var name_search=$(this).val();
        var id_search = document.getElementById("search").value;
        ChooseDate(startDate,endDate,0,id_search,name_search);
        
        })

  function readmore(message){
      // alert("hi hi ");
      var string = String(message);
      var length = string.length; 
         // alert(length);
         if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.

            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
          }
          return string;


        }

     function Create_DataTable(fday,sday,is_search,id_search,name_search)
    {
      // alert(fday);
      // alert(sday);
      // alert(db_name);
      // alert(text);
     
            // var txtkeyword =$("#txtkeyword").val();
            var selected_db = document.getElementById("db_selection");
            var db_name = selected_db.options[selected_db.selectedIndex].text;
             var selected_page = document.getElementById("admin_page_filter");
             var page_name = selected_page.options[selected_page.selectedIndex].text;
              
            // var text = $('#sidebarnav li.active').text();  /*need to check or confirm*/

            // var text = $text;
              // alert(text);
            // alert(text);
            // alert(text);
            // var text = $('ul#sidebarnav').find('li.active').data('id');
           // var e = document.getElementById("sentiment_option");
            //ar opt_sentiment = e.options[e.selectedIndex].value;
        
     // top_posts(fday,sday,txtkeyword,opt_sentiment);
     // latest_posts(fday,sday,txtkeyword,opt_sentiment); 
      // alert(db_name);
      // alert(text);
     data_table(fday,sday,db_name,page_name,id_search,name_search);
    

    }

  function data_table(fday,sday,db_name,page_name,id_search,name_search)
    { 

      // alert(db_name);
       // alert(fday);
       // alert(sday);
      // alert(GetURLParameter('from'));
      // alert($('#global_interest').prop("checked"));
// alert($('#global_senti option:selected').val());
      $(".popup_post").unbind('click');
      $(".edit_predict1").unbind('click');
      $(".change_font").unbind('click');
      var voiceTable = $('#comment_table').DataTable({
        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": true,
        "destroy": true,
        "ordering": false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        

        "ajax": {
          "url": '{{ route('getRelatedComments') }}',

          "data": {
            "db_name": db_name,
            "page_name": page_name,
            "fday": fday,
            "sday": sday,
            "text" : 'Confirmed',
            "id_search":  id_search,
            "name_search":name_search,
            "brand_id":$('#project_filter option:selected').val(),
            "tsearch_senti":$('#global_senti option:selected').val(),
            "tsearch_interest":$('#global_interest').prop("checked"),
           
          }

        },
        "initComplete": function( settings, json ) {
         // console.log(data);
       
        },
        drawCallback: function() {
     $('.select2').select2();
  },

        columns: [
        {data: 'post_div', name: 'post_div',orderable: false,searchable: true},
        {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        
      }).on('click', '.popup_post', function (e) {
    if (e.handled !== true) {
      var name = $(this).attr('name');
      var e = document.getElementById("project_filter");
      var brand_id = e.options[e.selectedIndex].value;
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();
      
      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:brand_id},
           success: function(response) { //console.log(response);
            $(".post_data").empty();
            var res_array=JSON.parse(response);
            var data=res_array[0];
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

            

              for(var i in data) {//alert(data[i].message);
                // var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                //  var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                //  var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                //  var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                //  var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                //  pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                //  neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                //  neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                //  pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                //  neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                //  neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
        
                 var page_name=data[i].page_name;
                // if(isNaN(page_name)==false)
                // {
                  
                //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
                //  page_name = b[0];
                
                // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
        
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large';                         
                //data[i].full_picture
                // var html = '';
                var html ='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                         ' <div class="sl-left">'+
                         ' <img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                         ' <div class="sl-right">'+
                         ' <div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                         ' <div class="sl-right"> '+
                         ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> ';
                          //            ' <div style="width:55%;display: inline-block">';
                          //   if(neg_pcent!=='')
                          //   html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                          // if(pos_pcent!=='')
                          //   html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                          // if(neutral_pcent!=='')
                          //   html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                    html+=' </div></div></span></p> ' +                
                          '   <p class="m-t-10" align="justify">' + 
                              data[i].message +
                          '</p>';
                   if(data[i]['post_type'] =='photo')
                  {
                     html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                    '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                    '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                    '</span><span class="postPanel_nameTextContainer"></span></a>';
                  }
                  else if(data[i]['post_type'] =='video')
                  {
                     html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                    '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                    '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                    '</span><span class="postPanel_nameTextContainer"></span></a>';
                  }
                  html +='</div></div>';
                  $(".post_data").append(html);
       
          }


           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
      }
              });
            e.handled = true;
    }
          
    }).on('click', '.edit_predict1', function (e) {
    // alert('hi'); 
    e.preventDefault();
  // if (e.handled !== true) {

    var post_id = $(this).attr('id');
  
/*    var sentiment = $('input[nacme=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
   // var emotion = $('#emotion_'+post_id+' option:selected').val();

    var tags = $('#tags_'+post_id+' option:selected').map(function () {
        return $(this).text();
    }).get().join(',');

    var tags_id = $('#tags_'+post_id).val();
    
    var decided_keyword = $('#decided_kw_'+post_id).val();
    var brand_id = $('#brand_'+post_id).val();

   
     var e = document.getElementById("db_selection");
     var db_name = e.options[e.selectedIndex].text;


 // alert($('#brand_'+post_id).val());
   // alert(sentiment);
 //  alert(tags);
 // return;
 // alert("hhh");
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:brand_id,sentiment:sentiment,tags:tags,checked_tags_id:tags_id,db_name:db_name,decided_keyword:decided_keyword},
         success: function(response) { 
          // alert(response);
          if(response>0)
          {
              // alert("success");
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
                 $("#"+post_id).closest('tr').remove();

          }

              else if(response=0)
          {
              // alert("success");
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
                 $("#"+post_id).closest('tr').remove();

          }


        }


            });
         return false;
          // e.handled = true;
          
       // }
     
    
        
  });
    }
 $('#global-search').on('click', function(e) {
     // alert(startDate);
     var e = document.getElementById("db_selection");
     var db_name = e.options[e.selectedIndex].text;
     var text = $('#sidebarnav li.active').text();  /*need to check or confirm*/
      // var text = $('.check_or_confirm a').text();
     // alert(startDate);
     // alert(endDate);
     // data_table(startDate,endDate,1,db_name,text);
     // data_table(db_name,text);
     var id_search = document.getElementById("search").value;
     var name_search = document.getElementById("searchby_name").value;
     ChooseDate(startDate,endDate,1,id_search,name_search)
     // Create_DataTable(startDate,endDate,1,db_name,text);
     e.preventDefault();
    });

$(document).on('click', '.edit_predict_post', function(e) {
     var post_id = $(this).attr('id');
   
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_post_'+post_id+' option:selected').val();
  var emotion = $('#emotion_post_'+post_id+' option:selected').val();
  //alert(sentiment + emotion);

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {//alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });

          }
        }
            });
     
});


// change font function

     $(document).on('click', '.change_font', function(e) {
        var comment_id = $(this).attr('id');
        var brand_id = $('#brand_'+comment_id).val();
        var message = $('#message_'+comment_id).text();

        $('#spin_'+comment_id).show();

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("changefont") }}',
         type: 'GET',
         data: {comment_id:comment_id,brand_id:brand_id},
         success: function(response) {
          // console.log(response);
           $('#spin_'+comment_id).hide();
          
            if(response[0] !== '')
            {
             $('#message_'+comment_id).html(response[0]);
            }
        //  else{
        //   // alert("else");
        //   $('#message_'+comment_id).html(response[1]);
        // }
        //   if(response>=0)
        //   {
        //         swal({   
        //     title: "Updated!",   
        //     text: "Done!",   
        //     timer: 1000,   
        //     showConfirmButton: false 
        // });

        //   }
        }
            });
     
});

// end change font function


      });
      $("#more").click(function () {
            $("#search_div").toggle();
        });
  </script>
   <link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush

