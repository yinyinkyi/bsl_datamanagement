<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use MongoDB;
use App\Profile;
use App\MongoProfile;
use MongoDB\BSON\ObjectID;
use App\MongoboundComment;

class update_profile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:profile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Profile field from bound_comment table in mongodb and update in mysql comment table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        foreach($projects as $project)
      {
        $id = $project->id;
        // $id = '17';
        $comment_table = "temp_".$id."_inbound_comments";
        // $id_array = "Select id from ".$comment_table." where is_update = 0 and message <> '' order by created_time desc limit 500";
        $id_array = "Select id from ".$comment_table." where (profile is NULL or profile = '') and is_update = 0 order by timestamp(created_time) desc limit 100";
      
        $id_array = DB::select($id_array);

        foreach ($id_array as $value) 
        {
          $array[] = $value->id;
        }
         if(!empty($array))
        {
       
         $inbound_comment_result=MongoboundComment::raw(function ($collection) use($array) { 
         // print_r($array);
 
          return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                  
                   ['id'=> ['$exists'=> true]],
                   // ['profile'=> ['$exists'=> true]],
                   ['id'=>[ '$in'=> $array ] ],
                 ]
              ]  
                         
             ],
              
          ]);
  
      })->toArray(); 
       // dd($inbound_comment_result);
            foreach($inbound_comment_result as $result)
            { 
              $id = $result['id'];
              if(isset($result['profile'])){ $profile = $result['profile']; }
              else{ $profile = ''; }
              $res = DB::table($comment_table)->where('id',$id)->update(['profile' => $profile,'is_update' => 1,'updated_at' => now()->toDateTimeString()]);
      
              if( $profile <> '') {
                       
                      $profile_result=MongoProfile::raw(function ($collection) use($profile) {
                        return $collection->aggregate([
                            [
                            '$match' =>
                                 ['_id'=> new ObjectId($profile) ],
                            
                                       
                           ],
                            
                        ]);
                
                     })->toArray();

              // dd($profile_result);  
                      foreach($profile_result as $data)
                      {

                        $_id ='';$profile_id='';$name='';$type='';

                        if(isset($data['_id']))$_id = $data['_id'] ;
                        if(isset($data['id']))$profile_id = $data['id'];
                        if(isset($data['name'])) $name = $data['name']; 
                        if(isset($data['type']))$type = $data['type'];
                        // dd($profile_id);
                        // $profile_data = DB::table('temp_profiles')->insert(['temp_id' => $_id , 'id' => $id ,'name' => $name , 'type' => $type]);
                        // $profile_data = Profile::firstOrCreate(['temp_id' => $_id , 'id' => $profile_id ,'name' => $name , 'type' => $type]);
                          $query = "select * from temp_profiles WHERE id='".$profile_id."'";
                          $data = DB::connection('mysql_cron_wunzin')->select($query);
                          if(!isset($data))
                          {
                            $row = "insert into temp_profiles('temp_id','id','name','type') values($_id,$profile_id,$name,$type)";
                            $new_data = DB::connection('mysql_cron_wunzin')->select($row);
                          }

                      }
                  }

              
        
        
      }
    }
    }
    $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Profile Updated Successfully.'. $date_time);
    }
}
