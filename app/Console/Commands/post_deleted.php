<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Notifications\NewTagNotification;
use App\Http\Controllers\stdClass;

use Illuminate\Support\Facades\Input;


use DB;
use MongoDB;
use App\Project;
use App\MongoboundPost;


class post_deleted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:isdelete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' If mark = deleted in bound_posts , change isDeleted status 1 in inbound_posts table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        $data =  [];
        foreach($projects as $project)
        {
        $id = $project->id;
        $post_table = 'temp_'.$id.'_inbound_posts';
        $monitor_pages = $project->monitor_pages;
        $monitor_pages = explode(',', $monitor_pages);
        $page['$or']= $this->getpagefilter($monitor_pages);

         $inbound_post_result=MongoboundPost::raw(function ($collection) use($page)  {
         
          return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   ['mark'=> ['$exists'=> true]],
                   ['id'=> ['$exists'=> true]],
                   $page
                  
                  
                            ]
              ] 
                         
             ], [ '$limit' => 100]
              
          ]);
      })->toArray();
        
         foreach ($inbound_post_result as $result) {
           $post_id = $result['id'];
           $del = 0;
             if(isset($result['mark']))
             {

              if($result['mark'] == 'deleted')
              {
                $del = 1;
               
                $query = DB::table($post_table)->where('id',$post_id)->update(['isDeleted'=> $del]);
              }
             }
             
          
         }
       }
       $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('isDelete status updated Successfully.'. $date_time);

    }

    public function getpagefilter($page_data)
    {
      $conditional=[];
      
      $conditional_require_or=[];
      foreach($page_data as $i =>$element)
                                {

                                $require_keyword_filter[] = [ 'page_name' =>  $element];
                                $conditional_require_or['$or']=$require_keyword_filter;
                                }
     $conditional[] =$conditional_require_or;
     
     return  $conditional;
    }

}
