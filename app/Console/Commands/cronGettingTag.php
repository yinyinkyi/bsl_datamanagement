<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\tags;
use App\User;
use App\Notifications\NewTagNotification;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;


class cronGettingTag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listener:tag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update tags in comment table every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $users = User::all();
     \Notification::send($users,new NewTagNotification(tags::all()));
      $tag =  DB::table('tags')->where('id',3)->update(['updated_at' => now()->toDateTimeString()]);
  }
    }

