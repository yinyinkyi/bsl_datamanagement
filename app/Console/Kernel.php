<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\ServerjobController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->call(function () {
        //     $controller = new ServerjobController();
        //     $controller->get_inbound_data();
        // })->everyMinute()->runInBackground();

        // $schedule->call(function () {
        //     $controller = new ServerjobController();
        //     $controller->tag_inboundComment();
        // })->everyMinute()->runInBackground();
       
        $schedule->call('App\Http\Controllers\ServerjobController@get_inbound_data')->everyTenMinutes()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@update_mentionComment')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@update_mentionPost')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_updatePostReaction')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_commentWbForMention')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_commentSentiForMention')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_postWbForMention')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_postSentiForMention')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_commentWbForInbound')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_commentSentiForInbound')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_postWbForInbound')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_postSentiForInbound')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_WbForWebMention')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_SentiForWebMention')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@add_profiles')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@update_profiles')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@tag_inboundComment')->everyMinute()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@update_articleTitle')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@update_article')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_mention_data')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_mentionDataYearly')->hourly()->runInBackground();
        $schedule->call('App\Http\Controllers\ServerjobController@get_web_mention_data')->hourly()->runInBackground();

        // $schedule->call('App\Http\Controllers\ServerjobController@tag_mentionComment')->everyMinute()->runInBackground();
        
       
        


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
