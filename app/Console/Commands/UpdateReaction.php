<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use MongoDB\BSON\ObjectID;
use App\MongoboundPost;
// get_updatePostReaction
class UpdateReaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:reaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get and updated Reaction from Mongo to Mysql';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
        // function convertKtoThousand_SyncMongo($s)
        // {
        //     if (strpos(strtoupper($s), "K") != false) {
        //     $s = rtrim($s, "kK");
        //     return floatval($s) * 1000;
        //   } else if (strpos(strtoupper($s), "M") != false) {
        //     $s = rtrim($s, "mM");
        //     return floatval($s) * 1000000;
        //   } else {
        //     return floatval($s);
        //   }
        // }

        // public function getpagefilter($page_data)
        // {
        //   $conditional=[];
          
        //   $conditional_require_or=[];
        //   foreach($page_data as $i =>$element)
        //                             {

        //                             $require_keyword_filter[] = [ 'page_name' =>  $element];
        //                             $conditional_require_or['$or']=$require_keyword_filter;
        //                             }
        //  $conditional[] =$conditional_require_or;
         
        //  return  $conditional;
        // }

        // public function getInboundPostId_Reaction($brand_id,$related_pages)
        //     {
        //         $filter_pages='';
        //         //$filter_pages=$this->getPageWhere($related_pages);
        //         $id_query="select id from temp_".$brand_id."_inbound_posts Where reaction_update=0 Order By timestamp(created_time) DESC Limit 100 ";
        //         $id_result = DB::connection('mysql_cron_wunzin')->select($id_query);
        //         $id_array = array_column($id_result, 'id');
        //         return  $id_array;
        //     }
        // public function getPageWhere($related_pages)
        //     {
        //         $pages='';
        //          $filter_pages = '';
        //           foreach ($related_pages as $key => $value) {
                    
        //             if((int)$key === 0)
        //             $pages ="'".$value."'";
        //           else
        //             $pages .=",'".$value."'";

        //             $filter_pages="  AND page_name in (".$pages.")";
        //           }
        //         return  $filter_pages;
        //     }

    public function handle()
    {
      $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
      $data =  [];
      
      foreach($projects as $project)
      {
        $id = $project->id;
        $monitor_pages = $project->monitor_pages;
        $monitor_pages = explode(',', $monitor_pages);

        $monitor_pages_count = count($monitor_pages);
        if($monitor_pages_count>0)
        {
            $this->Update_Reaction_Data($id,$monitor_pages);
        } 
        
        
      }
       $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Reaction Update  Successfully.'. $date_time);
    }

    public function Update_Reaction_Data($project_id,$monitor_pages)
    {

        $post_id = $this->getInboundPostId_Reaction($project_id,$monitor_pages);

        $inbound_post_result=MongoboundPost::raw(function ($collection) use($post_id) {//print_r($filter);

            return $collection->aggregate([
                [
                '$match' =>[
                     '$and'=> [ 
                     ['id'=> ['$exists'=> true]],
                     ['id'=>[ '$in'=> $post_id ] ],
                 
                              ]
                ]  
                           
               ],
                
            ]);
        })->toArray();

         foreach ($inbound_post_result as  $key => $row) {
                $id='';$share =0;$Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);
                if($Like <> 0 || $Love <> 0 || $Wow <> 0  || $Haha <> 0  || $Sad <> 0  || $Angry <> 0  )
                {
                    $res = DB::table("temp_".$project_id."_inbound_posts")->where('id',$id)->update(
                        ['shared' => $share,'Liked' => $Like,'Love' => $Love,'Wow' => $Wow,'Haha' => $Haha,'Sad' => $Sad,'Angry' => $Angry,
                        'reaction_update' => 1,'updated_at' => now()->toDateTimeString()]);
                }
           
         }
    }
}
