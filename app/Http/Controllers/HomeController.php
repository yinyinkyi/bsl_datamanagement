<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Auth;
use Rabbit;
use App\Project;
use App\ProjectKeyword;
use App\demo;
use App\MongodbData;
use App\Http\Controllers\GlobalController;
use DB;
use MongoDB;
use App\MongoboundPost;
use App\ManageDB;
use App\MongoboundComment;
use App\InboundPages;
use App\MongoPages;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\Excel;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Config;
// use Illuminate\Support\Facades\DB;
use DateTime;

class HomeController extends Controller
{

     use GlobalController;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $layout = 'layouts';
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    // }
    public function getPageName()
    {
         $pages=[];
         $project_id = Input::get('project_id');
        
         $query="Select monitor_pages from projects where id =".$project_id;
         $project_ids = DB::select($query);
         foreach ($project_ids as $project_id) 
         {
            $monitor_pages = $project_id->monitor_pages;
            $monitor_pages=explode(',', $monitor_pages);
            foreach ($monitor_pages as $monitor_page) 
            {
              $pages[]=$monitor_page;
            }
          }
             echo json_encode($pages);
    }
    public function page_filter()
    {
       return view('BSL/page_filter');
    }
    public function get_Mongopages()
    {
       // dd('hyyy');
        $data=MongoPages::raw(function ($collection)  {//print_r($filter);

        return $collection->aggregate([
            [
            '$match' =>[
                 '$and'=> [ 
                  ['id'=>['$exists' =>true]],
                 ['watch'=> 'on'],
                
              ]
            ]  
                       
           ],
           ['$sort' =>['last_crawl_date'=>-1]],
            
          ]);
    })->toArray();
        $res = [];
        foreach($data as $key => $row)
        {
          $res[$key]['page_name']= $row['page_name'];

          $utcdatetime = $row['last_crawl_date'];
          $datetime = $utcdatetime->toDateTime();
          $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
          $datetime = $datetime->format('Y-m-d H:i:s');
          $res[$key]['last_crawl_date']=$datetime;
          $res[$key]['id']=$row['id'];

          // $page [] = $row['page_name'];
 

        }
        // foreach ($res as $key => $value) {
        //  $mongoarr[] = $value['page_name'];
        // }
        // $pages = Project::select('exclude_pages')->where('id',auth()->user()->brand_id)->get();
        // foreach ($pages as $key => $value) {
        //   $page_str = $value->exclude_pages;
        //   $page_arr = explode(',', $page_str);
         
        // }
       
        // $filter_arr[]= array_diff($mongoarr,$page_arr);
        
        return Datatables::of($res)
         ->addColumn('remove', function ($res) {
               
            return '<a style="width:100%" data-remote="'. route('page_remove', $res['page_name']) .'" id="'.$res['page_name'].'" class="btn btn-xs btn-danger btn_remove"><font color="#fff" > Remove</font></a>  ';

            })
         ->rawColumns(['remove'])

        ->make(true);

             
           
    }

    public function page_remove($name)
    {

          $userid = auth()->user()->id;
          $brandid = ManageDB::select('brand_id')->where('user_id',$userid)->limit(1)->get();
          foreach ($brandid as  $id) $brandid = $id->brand_id;

          $projects = Project::select('exclude_pages')->where('id',$brandid)->get();

          foreach ($projects as $value) {
             $ex_page = $value->exclude_pages;
             if($ex_page == null)
             {
                $result = Project::where('id',$brandid)->update(['exclude_pages' => $name]);
             }
             else
             {
               $ex_page_arr = explode(',', $ex_page);
               if(!in_array($name, $ex_page_arr, true)){
               array_push($ex_page_arr, $name);

               $ex_page_string = implode(',', $ex_page_arr);
               $result = Project::where('id',$brandid)->update(['exclude_pages' => $ex_page_string]);
               
             }
             }
          }
       return $result;
    }
    public function get_Removepages()
    {

          $userid = auth()->user()->id;
          $brandid = ManageDB::select('brand_id')->where('user_id',$userid)->limit(1)->get();
          foreach ($brandid as  $id) $brandid = $id->brand_id;
             // dd($brandid);
          $projects = Project::select('exclude_pages')->where('id',$brandid)->get();
          // dd($projects);
          $page_arr = [];
          foreach ($projects as $key=> $value) {
             $ex_page = $value->exclude_pages;
             if($ex_page !== null)
             {
                $page_arr = explode(',', $ex_page);

             }

             
          }
        $count = count(array_filter($page_arr, 'strlen'));
        $res = [];
       if($count <> 0){
        // dd('hhh');
          foreach ($page_arr as $key => $value) {
                      $res[$key]['page_name']= $value;
                     
                     }
                  
       }
       // dd($res);
       return Datatables::of($res)
               ->addColumn('restore', function ($res) {
               
            return '<a style="width:100%" data-remote="'. route('page_restore', $res['page_name']) .'"  id="'.$res['page_name'].'"  class="btn btn-xs btn-danger btn_restore" > <font color="#fff" > Restore </font></a>';

            })
         ->rawColumns(['restore'])

        ->make(true);
    }
    public function page_restore($name)
    {

          $userid = auth()->user()->id;
          $brandid = ManageDB::select('brand_id')->where('user_id',$userid)->limit(1)->get();
          foreach ($brandid as  $id) $brandid = $id->brand_id;

          $projects = Project::select('exclude_pages')->where('id',$brandid)->get();
          foreach ($projects as $value) {
             $ex_page = $value->exclude_pages;
             $ex_page_arr = explode(',', $ex_page);
             
             if (($key = array_search($name, $ex_page_arr)) !== false) {
             unset($ex_page_arr[$key]);
             $ex_page_string = implode(',', $ex_page_arr);
             $result = Project::where('id',$brandid)->update(['exclude_pages' => $ex_page_string]);
             }

           

          }
         return $result;
      
    }

    public function getProjectName()
    {
         $pages=[];
         $db_name = Input::get('db_name');
        
         $query="Select names from ".$db_name.".projects";
         $project_names = DB::select($query);
         foreach ($project_names as $project_name) 
         {
            $names[] = $project_names->names;
         }
            echo json_encode($names);
    }

    public function checked()
    {
        if($user=Auth::user())
        {
            $res = ManageDB::where('user_id',Auth()->user()->id)->get()->toArray();
            if(!empty($res))
            {
              $user_id = Auth::user()->id;
              $databases =  DB::table('manage_dbs')->select('*')->where('user_id',$user_id)->get();
              $db_name = $databases[0]->db_name;
              $project_ids = DB::table('manage_dbs')->select('projects.monitor_pages','projects.name','projects.id','manage_dbs.user_id')->where('manage_dbs.user_id',Auth::user()->id)->join('projects','projects.id','=','manage_dbs.brand_id')->orderby('manage_dbs.brand_id','DESC')->get();
              if(count($project_ids)>0)
              $project_id = $project_ids[0]->id;
              $project_monitor = DB::table('projects')->select('projects.monitor_pages','projects.default_page')->where('projects.id',$project_id)->join('manage_dbs','projects.id','=','manage_dbs.brand_id')->limit(1)->get();
              $pages = [];
              foreach ($project_monitor as $project_monitor) 
              {
                 $monitor_pages = $project_monitor->monitor_pages;
                 $monitor_pages =explode(',', $monitor_pages);
                 foreach ($monitor_pages as $monitor_page) 
                 {
                   $pages[]=$monitor_page;
                 }
                   $default_page = $project_monitor->default_page;
              
              }
                  return view('BSL/home',compact('databases','pages','project_ids','default_page'));
                   
            }
            else
            {
           Auth::logout();
           $title="Login";
           return view('auth.login',compact('title'));
            }
        }
        else
        {
           $title="Login";
           return view('auth.login',compact('title'));
        }
    }
    public function require()
    {
        if($user=Auth::user())
        {
          $res = ManageDB::where('user_id',Auth()->user()->id)->get()->toArray();
          if(!empty($res))
          {
            $user_id = Auth::user()->id;
            // $project_id=0;
            $databases =  DB::table('manage_dbs')->select('*')->where('user_id',$user_id)->get();
            $db_name = $databases[0]->db_name;
           $project_ids = DB::table('manage_dbs')->select('projects.monitor_pages','projects.name','projects.id','manage_dbs.user_id')->where('manage_dbs.user_id',Auth::user()->id)->join('projects','projects.id','=','manage_dbs.brand_id')->orderby('manage_dbs.brand_id','DESC')->get();
            // $project_ids = DB::select($query);
            if(count($project_ids)>0)
            $project_id = $project_ids[0]->id;
            $keyword_data = $this->getprojectkeyword($project_id);
            $keyword_filter = $this->getkeywordfilter_MYSQL($keyword_data,'cmts');
            $pages=[];
            if($keyword_filter <> '')
            {
              $query="Select Distinct(page_name) from ".$db_name.".temp_comments cmts Where ".$keyword_filter;
              $project_monitor = DB::select($query);
              
              foreach ($project_monitor as $page) 
              {
                $pages [] =$page->page_name;
              }
            }
              return view('BSL/mention_home',compact('databases','pages','project_ids'));
          }
          else
          {
             Auth::logout();
             $title="Login";
             return view('auth.login',compact('title'));
          }
      }
        else
        {
           $title="Login";
           return view('auth.login',compact('title'));
        }
    }
    public function taken()
    {
        if($user=Auth::user())
        {
          $res = ManageDB::where('user_id',Auth()->user()->id)->get()->toArray();
          if(!empty($res))
          {
            $user_id = Auth::user()->id;
            // $project_id=0;
            $databases =  DB::table('manage_dbs')->select('*')->where('user_id',$user_id)->get();
            $db_name = $databases[0]->db_name;
           $project_ids = DB::table('manage_dbs')->select('projects.monitor_pages','projects.name','projects.id','manage_dbs.user_id')->where('manage_dbs.user_id',Auth::user()->id)->join('projects','projects.id','=','manage_dbs.brand_id')->orderby('manage_dbs.brand_id','DESC')->get();
            // $project_ids = DB::select($query);
            if(count($project_ids)>0)
            $project_id = $project_ids[0]->id;
            $keyword_data = $this->getprojectkeyword($project_id);
            $keyword_filter = $this->getkeywordfilter_MYSQL($keyword_data,'cmts');
            $pages=[];
            if($keyword_filter <> '')
            {
              $query="Select Distinct(page_name) from ".$db_name.".temp_comments cmts Where ".$keyword_filter;
              $project_monitor = DB::select($query);
              
              foreach ($project_monitor as $page) 
              {
                $pages [] =$page->page_name;
              }
            }
              return view('BSL/mention_confirmed',compact('databases','pages','project_ids'));
          }
          else
          {
             Auth::logout();
             $title="Login";
             return view('auth.login',compact('title'));
          }
      }
        else
        {
           $title="Login";
           return view('auth.login',compact('title'));
        }
    }
    public function getMentionPage()
    {
         $pages=[];
         $project_id = Input::get('project_id');
         $keyword_data = $this->getprojectkeyword($project_id);
         $keyword_filter = $this->getkeywordfilter_MYSQL($keyword_data,'cmts');
         $add_inbound_con ="";
         $inboundpages=$this->getInboundPages($project_id);
         if ($inboundpages !== '')
         {
            $add_inbound_con = " page_name  not in (".$inboundpages.") or page_name is NULL";
         }
         if($keyword_filter <> '')
         {
            $query="Select Distinct(page_name) from temp_comments cmts  Where ".$add_inbound_con." AND (".$keyword_filter.")";
            $project_monitor = DB::select($query);
            foreach ($project_monitor as $page) 
            {
              if($page->page_name <> '')
              $pages [] =$page->page_name;
            }
         }
            return json_encode($pages);
    }
    public function fetchData()
    {        
           $previous_count = Input::get('previous_count');
           $page_name = Input::get('page_name');
           $projects = Project::all();
           foreach ($projects as $project) 
           {
              $id = $project->id;
              $comment_table = 'temp_inbound_comments';
              $count_from_db = "SELECT Count(*) count FROM temp_inbound_comments as comment JOIN temp_inbound_posts as post on post.id=comment.post_id where post.page_name ='".$page_name."'";
               // $count_from_db =  DB::table($comment_table)->select('*')->count();
              $count_from_db = DB::select($count_from_db);
              $count_of_noti=0;
              foreach ($count_from_db as $key => $value) 
              {
                $count_of_noti=$value->count;
              }
                // $count_from_db = count($count_from_db);
               // return $count_from_db[0];
                $res = 0;
                if($count_of_noti > $previous_count)
                {
                  $res = $count_of_noti - $previous_count ;
                  if($res == $count_of_noti)
                  $res = 0 ;
                  $previous_count = $count_of_noti;
                }       
           
            }
                 // return $res;
                 return array($res,$previous_count);
    }
    public function confirmed()
    {
        if($user=Auth::user())
        {
           $res = ManageDB::where('user_id',Auth()->user()->id)->get()->toArray();
           if(!empty($res))
           {
              $user_id = Auth::user()->id;
              $databases =  DB::table('manage_dbs')->select('*')->where('user_id',$user_id)->get();
              $db_name = $databases[0]->db_name;
              $project_ids = DB::table('manage_dbs')->select('projects.monitor_pages','projects.name','projects.id','manage_dbs.user_id')->where('manage_dbs.user_id',Auth::user()->id)->join('projects','projects.id','=','manage_dbs.brand_id')->orderby('manage_dbs.brand_id','DESC')->get();
              if(count($project_ids)>0)
              $project_id = $project_ids[0]->id;
              $project_monitor = DB::table('projects')->select('projects.monitor_pages','projects.default_page')->where('projects.id',$project_id)->join('manage_dbs','projects.id','=','manage_dbs.brand_id')->limit(1)->get();
              $pages = [];
              foreach ($project_monitor as $project_monitor) 
              {
               $monitor_pages = $project_monitor->monitor_pages;
               $monitor_pages =explode(',', $monitor_pages);
                 foreach ($monitor_pages as $monitor_page) 
                 {
                   $pages[]=$monitor_page;
                 }
                   $default_page = $project_monitor->default_page;
              }
                    return view('BSL/confirmed',compact('databases','pages','project_ids','default_page'));
                   
          }
          else
          {
             Auth::logout();
             $title="Login";
             return view('auth.login',compact('title'));
          }
        }
        else
        {
           $title="Login";
           return view('auth.login',compact('title'));
        }

    }
    public function getRelatedComments()
    {  
          $id_search_con='';
          if(null !== Input::get('id_search'))
          {
            $id_search=Input::get('id_search');
            $id_search=str_replace(' ','',$id_search);
            $id_search_con = " and cmts.id Like'%".$id_search."%'";
          }
          $name_search_con='';
          if(null !== Input::get('name_search'))
          {
            $name_search=Input::get('name_search');
            $name_search=str_replace(' ','',$name_search);
            $name_search_con = " and cmts.manual_by Like'".$name_search."%'";
          }
          $groupType=Input::get('periodType');
          $filter_sentiment ='';
          $page_name='';
          $text = Input::get('text');
          $global_senti=Input::get('tsearch_senti'); 

          if(null !== Input::get('sentiment'))
          $filter_sentiment=Input::get('sentiment');


          $add_global_filter='';
          if(null !== Input::get('tsearch_senti'))
          {
            	if($text == 'Confirmed')
              {
                  // $global_senti=Input::get('tsearch_senti'); 
                 $add_global_filter .=" and cmts.checked_sentiment = '".$global_senti."'";
              }
              else
              {
            	   // $global_senti=Input::get('tsearch_senti'); 
                $add_global_filter .=" and cmts.sentiment = '".$global_senti."'";
              }
          }

          if(null !== Input::get('tsearch_interest') && Input::get('tsearch_interest') === "true")
          {
              $add_global_filter .=" and cmts.interest = 1";
          }
          if(null !== Input::get('fday'))
          {
              $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
              $dateBegin=date('Y-m-d');
          }
          if(null !==Input::get('sday'))
          {
              $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          else
          {
              $dateEnd=date('Y-m-d');
          }
            $add_checked_predict= '';
            if(null !==Input::get('text'))
            {
                $text = Input::get('text');
                if($text == 'Confirmed')
                {
                  $add_checked_predict = ' and (cmts.checked_predict = 1 or cmts.change_predict = 1)';
                  // $add_date_order = 'cmts.updated_at';
                }
                else
                {
                  $add_checked_predict = ' and (cmts.checked_predict = 0 and cmts.change_predict = 0)';
                  // $add_date_order = 'cmts.created_time';
                }
            }

            if(null !== Input::get('page_name'))
            $page_name=Input::get('page_name');
            $db_name =Input::get("db_name");
            $project_id = Input::get('brand_id');
            $add_con='';
            if($filter_sentiment !== '')
            $add_con .= "and cmts.sentiment = '".$filter_sentiment."'";

              // $my_pages= $this->getOwnPage($project_id);
              // $my_pages=explode(',', $my_pages);
              // $filter_pages=$this->getPageWhereGen($my_pages);

            $tags =$this->gettags($project_id);   
         
            $query="SELECT pros.id profileID,pros.type as profileType,pros.name as profileName,IF(cmts.sentiment = '','~',cmts.sentiment) old_senti,IF(cmts.tags = '','~',cmts.tags) old_tags,cmts.post_id,cmts.id,cmts.message,manual_by,".$project_id." project_id,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
              " cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags = cmts.checked_tags || cmts.checked_tags = '','','human predict!') hp_tags,IF(cmts.checked_sentiment <> '',cmts.checked_sentiment,cmts.sentiment) sentiment,IF(cmts.checked_tags <> '',cmts.checked_tags,cmts.tags) tags,cmts.decided_keyword,cmts.checked_predict,cmtType,cmtLink".
              " FROM ".$db_name.".temp_inbound_comments cmts LEFT JOIN  ".$db_name.".temp_inbound_posts  posts on posts.id=cmts.post_id "." LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id "."WHERE posts.id IS NOT NULL".
              " and  parent='' and cmts.tag_flag = 1 and posts.page_name='".$page_name."' and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con . $add_global_filter. $add_checked_predict . $id_search_con . $name_search_con ." ORDER by cmts.created_time DESC LIMIT 500";
             $data = DB::select($query);

            //  DB::table('tracetable')
            //  ->insert('message' => $query);

          //   DB::table('tracetable')->insert(
          //     ['message' => $query]
          // );

              // dd($query);
              // dd($data);
             return Datatables::of($data)
             ->addColumn('action', function ($data)  {
            // return '<button type="button" class="btn waves-effect waves-light btn-primary popup_post" id="'.$data->post_id.'" name="'.$data->page_name.'" style="background:#1e88e4;border:none;">See Post</button>';
                  })
             ->addColumn('post_div', function ($data) use($tags){
                $page_name=$data->page_name ;
                if(is_numeric($page_name))
                {
                   $input = preg_quote($page_name, '~'); // don't forget to quote input string!
                   $keys = preg_grep('~' . $input . '~', $monitor_pages);
                   $vals = array();
                   foreach ( $keys as $key )
                   {
                      $page_name= $key;
                   }
                }
                //generate comment link
                $profilePhoto='';$ProfileName='';
                $profileLink='javascript:void(0)';
                $fb_p_id=explode('_', $data->post_id);
                $fb_p_id=$fb_p_id[1];
                $fb_c_id=explode('_', $data->id);
                $fb_c_id=$fb_c_id[1];
                if($data->profileName <> '') $ProfileName=$data->profileName; 
                else if($data->profileID <> '')  $ProfileName=$data->profileID;
                else  $ProfileName=$page_name;

                if($data->profileType === 'number')
                {
                  $profilePhoto='https://graph.facebook.com/'.$data->profileID.'/picture?type=large';
                  $ProfileName=$page_name;
                }
                
                if(isset($data->profileID))
                $profileLink='https://www.facebook.com/'.$data->profileID;

                $page_id='';
                $arr_page_id=explode("-",$page_name);
                if(count($arr_page_id)>0)
                {
                  $last_index=$arr_page_id[count($arr_page_id)-1];
                  if(is_numeric($last_index))
                  $page_id = $last_index;
                  else
                  $page_id = $page_name;
                }
                else
                {
                  $page_id=$page_name;
                }

                $img_spin ='/assets/images/ajax-loader.gif';
                $classforbtn='';$stylefora='';$bgcolor='';
                if($data->sentiment=='neg')
                {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
                $html = '<div class="profiletimeline"><div class="sl-item">';
                if($profilePhoto<>'')
                $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                else
               /*$html .= '<div class="sl-left user-img"> <span class="round"><i class="mdi mdi-comment-account"></i></span> </div>';*/
                $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                $html .='<div class="form-material sl-right"><div> 
                <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a>';
                if($data->hp <> '' && $data->hp_tags <>'')
                $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Sentiment : '.$data->old_senti.'  => '. $data->sentiment."\n".'Tags :'.$data->old_tags .' => '.$data->tags.'" data-toggle="tooltip"></i></a>';
                else if($data->hp <> '')
                $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Sentiment: '.$data->old_senti.'  => '. $data->sentiment.'" data-toggle="tooltip"></i></a>';
                else if($data->hp_tags <> '')
                $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Tags: '.$data->old_tags.'  => '. $data->tags.'" data-toggle="tooltip"></i></a>';
                // $html .= '<pre> | ' .$data->id .'</pre>';
                $html .=' | <input type="text" class="form-control col-sm-2 " value="'.$data->id.'" style="max-width:30%;" disabled/> '; 
                if($data->manual_by <> '')
                $html .=' | reviewd by '.$data->manual_by;
                $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';



            // if($data->checked_predict != 1){
                $html.= '<p>Sentiment : <select class="form-control sentiment-color in-comment-select" id="sentiment_'.$data->id.'"  >';
                if($data->sentiment == "")
                $html.= '<option value="" selected="selected"></option>';
                $html.='<option value="pos"';
                if($data->sentiment == "pos")
                $html.= 'selected="selected"';
                $html.= '>pos</option><option value="neg"';
                if($data->sentiment == "neg")
                $html.= 'selected="selected"';
                $html.= '>neg</option><option value="neutral"';
                if($data->sentiment == "neutral")
                $html.= 'selected="selected"';      
                $html.= '>neutral</option><option value="NA"';
                if($data->sentiment == "NA")
                $html.= 'selected="selected"';  
                $html.= '>NA</option></select>';
              // }
        
              // $html.= ' | Emotion : <select class="form-control custom-select emotion-color in-comment-select" id="emotion_'.$data->id.'" >';
              // if($data->emotion == "")
              // $html.= '<option value="" selected="selected"></option>';
              // $html.='<option value="anger"';
              // if($data->emotion == "anger")
              // $html.= 'selected="selected"';
              // $html.= '>anger</option><option value="interest"';
              // if($data->emotion == "interest")
              // $html.= 'selected="selected"';     
              // $html.= '>interest</option><option value="disgust"';
              // if($data->emotion == "disgust")
              // $html.= 'selected="selected"';  
              // $html.= '>disgust</option><option value="fear"';
              // if($data->emotion == "fear")
              // $html.= 'selected="selected"'; 
              // $html.= '>fear</option><option value="joy"';
              // if($data->emotion == "joy")
              // $html.= 'selected="selected"'; 
              // $html.= '>joy</option><option value="like"';
              // if($data->emotion == "like")
              // $html.= 'selected="selected"'; 
              // $html.= '>like</option><option value="love"';
              // if($data->emotion == "love")
              // $html.= 'selected="selected"'; 
              // $html.= '>love</option><option value="neutral"';
              // if($data->emotion == "neutral")
              // $html.= 'selected="selected"';
              // $html.= '>neutral</option><option value="sadness"';
              // if($data->emotion == "sadness")
              // $html.= 'selected="selected"';
              // $html.= '>sadness</option><option value="surprise"';
              // if($data->emotion == "surprise")
              // $html.= 'selected="selected"';
              // $html.= '>surprise</option><option value="trust"';
              // if($data->emotion == "trust")
              // $html.= 'selected="selected"';
              // $html.= '>trust</option><option value="NA"';
              // if($data->emotion == "NA")
              // $html.= 'selected="selected"';
              // $html.= '>NA</option></select>';
                 $html.=' | Tag :<select id="tags_'.$data->id.'" class="form-control  select2" multiple="multiple" data-placeholder="Select tag"
                      style="width:550px;height:36px">';
              // foreach ($tags as $key => $value) {
              //   # code...
              //   if (strpos($data->tags, $value->name) !== false)
              //  $html.= '<option value="'.$value->name.'" selected="selected">'.$value->name.'</option>';
              //   else
              //  $html.= '<option value="'.$value->name.'">'.$value->name.'</option>';
              // }

                 $tag_array=explode(',', $data->tags);
                 foreach ($tags as $key => $value) {
                 # code...
                   //if (strpos($data->tags, $value->name) !== false)
                  if ( in_array( $value->name,$tag_array))
                  $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                   else
                  $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                  $html.='</select> <input type="text" class="form-control col-sm-2 " value="'.$data->decided_keyword.'" placeholder="keyword" style="
                  max-width: 50%;" name="keyword" id="decided_kw_'.$data->id.'"/> ';
                  $html.='<input type="hidden" value="'.$data->project_id.'" id="brand_'.$data->id.'"/>';
                  // $html.= ' <a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a></p>';
                  $html.= '<a class="edit_predict1" id="'.$data->id.'" href="javascript:void(0)">
                  <span class="mdi mdi-content-save" style="font-size:30px;"></span></a></p>';
                  $html.='<div style="display:none"  align="center" style="vertical-align: top;" id="spin_'.$data->id.'"> <img src="assets/images/ajax-loader.gif" id="loader"></div>';
                  // $html.='<p class="m-t-10" id="message_'.$data->id.'">'.$data->message.'</p><a class="change_font" id="'.$data->id.'" href="javascript:void(0)">
                  // <span class="mdi mdi-format-font" style="font-size:30px;"></span></a></div></div></div></div>';
                  // $html.='<p class="m-t-10" id="message_'.$data->id.'">'.$data->message.'</p><a class="change_font" id="'.$data->id.'" href="javascript:void(0)">
                  // <i class="icon icon-refresh" style="font-size:30px;"></i>change font</a></div></div></div></div>';
                  $html.='<p class="m-t-10" id="message_'.$data->id.'">'.$data->message.'</p>';
                  if($data->cmtType =='sticker' || $data->cmtType =='photo' || $data->cmtType =='share' || $data->cmtType =='share_large_image' || $data->cmtType =='animated_image_share'  )
                  {
                    $html .= '<p><a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'.
                             '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                             '<img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">'.
                        '</span><span class="postPanel_nameTextContainer"></span></a></p>';
                  }
                  else if($data->cmtType =='video_inline' || $data->cmtType =='animated_image_video' || $data->cmtType =='video_share_youtube' || $data->cmtType =='video_share_highlighted')
                  {
                     $html .= '<p><a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'.$data->id.'">'.
                      '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                      '<iframe src="https://www.facebook.com/plugins/video.php?href='.$data->cmtLink.'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'.
                      '</span><span class="postPanel_nameTextContainer"></span></a></p>';
                  }
                  $html.='<p><a class="change_font" id="'.$data->id.'" href="javascript:void(0)" style="text-decoration: underline;color: green;font-style:italic;padding-right:20px;">Change font   </a><a href="#" id="'.$data->post_id.'" class="btn waves-effect waves-light btn-sm btn-green popup_post" style="margin-right:20px;">See Post</a><a href="https://www.facebook.com/'.$data->id.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See on FB</a></p></div></div></div></div>';
                return $html;
            })   
             //mdi-format-font  <input type= "hidden" value="'.$data->message.'">

            ->rawColumns(['post_div','action'])
 
            ->make(true);

              // }
              //  else
              //     {
              //          $title="Login";
              //         return view('auth.login',compact('title'));
              //     }
    }
    public function getMentionComments()
    {
          $id_search_con='';
          if(null !== Input::get('id_search'))
          {
            $id_search=Input::get('id_search');
            $id_search=str_replace(' ','',$id_search);
            $id_search_con = " and cmts.id Like'%".$id_search."%'";
          }
          $name_search_con='';
          if(null !== Input::get('name_search'))
          {
            $name_search=Input::get('name_search');
            $name_search=str_replace(' ','',$name_search);
            $name_search_con = " and cmts.manual_by Like'".$name_search."%'";
          }
          $brand_id=Input::get('brand_id');
          // $groupType=Input::get('periodType');
          $filter_keyword ='';
          $filter_sentiment ='';
          
          $period_type = '';
          $additional_filter=" AND 1=1 ";
          
           //  if(null !==Input::get('period'))
           //  {
           // $period=Input::get('period');
           //  $char_count = substr_count($period,"-");
           //  $format_type = '';
           //  if($char_count == 1)
           //  {
           //      $format_type ='%Y-%m';
           //  }
           //  else if($char_count == 2)
           //  {
           //       $format_type ='%Y-%m-%d';
           //  }   
           //  else if($char_count > 2)
           //  {
           //      /*this is week*/
           //      $pieces = explode(" - ", $period);
           //      $format_type ='%Y-%m-%d';
           //      $period_type ='week';
           //  }

                  
            // if($period_type == 'week')
            // {
            //  $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

            // }
            // else
            // {
            //     $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
            // }

            //        }
                

          if(null !== Input::get('sentiment'))
          $filter_sentiment=Input::get('sentiment');
          $add_global_filter='';
        	if(null !== Input::get('fday'))
          {
            	$dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
              $dateBegin=date('Y-m-d');
          }
        

          if(null !==Input::get('sday'))
          {
               $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
        
          else
          {
                $dateEnd=date('Y-m-d');
          }



            $add_con=' 1=1';

           if(null !== Input::get('tsearch_senti'))
            {
                // $global_senti=Input::get('tsearch_senti'); 
                // if($global_senti == 'neutral')
                // $add_global_filter .=" and (cmts.checked_sentiment = '".$global_senti."' or cmts.checked_sentiment = 'NA')";
                // else
                // $add_global_filter .=" and cmts.checked_sentiment = '".$global_senti."'";
                $add_con .= ' AND ' . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
            }
           // $keyword_data = $this->getprojectkeyword($brand_id);
           //  if(isset($keyword_data [0]['main_keyword']))
           //  $add_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")"; 
           //  else
           //  $add_con .= ' AND 1=2';
           if(null !== Input::get('keyword_group'))
          {
              $keyword_group = Input::get('keyword_group') ;
          }
          else
          {
              $keyword_group = '' ;
          }
             $keyword_data = $this->getprojectkeyword($brand_id);
             if(isset($keyword_data [0]['main_keyword']))
             $additional_filter  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")"; 
             else
             $additional_filter .= ' AND 1=2';

              //     if(null !== Input::get('page_name'))
              // {
              //  $additional_filter .= ' AND cmts.page_name="' .Input::get('page_name') . '" ';
              // }

        
             if(null !== Input::get('tsearch_keyword'))
             { 
               $add_con .= ' AND ' . $this->getKeywordWhereGen(Input::get('tsearch_keyword'),'cmts');
             }
       
             if(null !== Input::get('tsearch_tag'))
             {
                $global_tag=Input::get('tsearch_tag'); 
                $add_con .=" and FIND_IN_SET('".$global_tag."', checked_tags_id) >0";
             }

             if(null !== Input::get('tsearch_emo'))
             {
                $global_emo=Input::get('tsearch_emo');
                $add_con .=" and cmts.emotion = '".$global_emo."'";

             }
             if(null !== Input::get('tsearch_interest') && Input::get('tsearch_interest') === "true")
             {
              // echo "hihi";
                $add_con .=" and cmts.interest = 1";
             }
             if(null !==Input::get('text'))
             {
                  $text = Input::get('text');
                  if($text == 'Confirmed')
                  {
                      $add_checked_predict = ' and (cmts.checked_predict = 1 or cmts.change_predict = 1)';
                  // $add_date_order = 'cmts.updated_at';
                  }
                  else
                  {
                      $add_checked_predict = ' and (cmts.checked_predict = 0 and cmts.change_predict = 0)';
                  // $add_date_order = 'cmts.created_time';
                  }
              }

                  // if(null !== Input::get('post_id'))
                  //  $add_con = "and cmts.post_id='".Input::get('post_id')."'"; 

                  // $tags =$this->gettags($brand_id);

                  // $date_filter='';
                  // if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
                  // {
                  //   $date_filter = ' 1=1 ';
                  // }
                  // else
                  // {
                  $date_filter=" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
                 // }
                  $add_inbound_con ="";$add_expage_con="";
                  $inboundpages=$this->getInboundPages($brand_id);
                  if ($inboundpages !== '')
                  {
                    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
                  }

                  $excludepages=$this->getExcludePages($brand_id);
                  // dd($excludepages);
                  if ($excludepages !== "''")
                  {
                      $add_expage_con = " and (cmts.page_name  not in (".$excludepages.") or cmts.page_name is NULL)";
                  }

                     $query="SELECT pros.id profileID,pros.type as profileType,pros.name as profileName,".$brand_id." project_id,cmts.manual_by,IF(cmts.sentiment = '','~',cmts.sentiment) old_senti,cmts.decided_keyword,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
                    " IF(cmts.checked_sentiment <> '',cmts.checked_sentiment,cmts.sentiment) sentiment,cmts.emotion,cmts.page_name ,". 
                    " IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp ".
                    " FROM temp_comments cmts LEFT JOIN  temp_posts  posts on posts.id=cmts.post_id ".
                    " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
                    " WHERE cmts.isHide=0 and ".$date_filter."  AND ". $add_con .  $add_inbound_con . $add_expage_con . $additional_filter . $add_checked_predict . $id_search_con . $name_search_con .

                    " ORDER by timestamp(cmts.created_time) DESC LIMIT 500";
                        // dd($query);

                        // print_r($query);
                        // return;
                      $data = DB::select($query);
                     // dd($data);

                          // $permission_data = $this->getPermission(); 
                          // if($is_competitor == "false")
                          // {
                       return Datatables::of($data)
          
                       ->addColumn('post_div', function ($data) {
                          $page_name = '';
                          $page_name=$data->page_name ;
                          if($page_name == '') $page_name = 'Unknown'; 
                          // if(is_numeric($page_name))
                          // {
                          //    $input = preg_quote($page_name, '~'); // don't forget to quote input string!
                          //    $keys = preg_grep('~' . $input . '~', $monitor_pages);
                          //    $vals = array();
                          //       foreach ( $keys as $key )
                          //       {
                          //          $page_name= $key;
                          //       }
                               
                          // }
                                  //generate comment link
                          $profilePhoto='';$ProfileName='';
                          $profileLink='javascript:void(0)';
                          $fb_p_id=explode('_', $data->post_id);
                          $fb_p_id=$fb_p_id[1];
                          $fb_c_id=explode('_', $data->id);
                          $fb_c_id=$fb_c_id[1];
                          if($data->profileName <> '') $ProfileName=$data->profileName; 
                          else if($data->profileID <> '')  $ProfileName=$data->profileID;
                          else  $ProfileName=$page_name;
                          if($data->profileType === 'number')
                          {
                              $profilePhoto='https://graph.facebook.com/'.$data->profileID.'/picture?type=large';
                              $ProfileName=$page_name;
                          }
                          if(isset($data->profileID))
                          $profileLink='https://www.facebook.com/'.$data->profileID;

                          $page_id='';
                          $arr_page_id=explode("-",$page_name);
                          if(count($arr_page_id)>0)
                          {
                              $last_index=$arr_page_id[count($arr_page_id)-1];
                              if(is_numeric($last_index))
                              $page_id = $last_index;
                              else
                              $page_id = $page_name;
                          }
                          else
                          {
                              $page_id=$page_name;
                          }

                          $img_spin ='/assets/images/ajax-loader.gif';
                          $classforbtn='';$stylefora='';$bgcolor='';
                          if($data->sentiment=='neg')
                          {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                          else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                          else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
                          $html = '<div class="profiletimeline"><div class="sl-item">';
                          if($profilePhoto<>'')
                          $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                          else
                  
                          $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 

                          $html .='<div class="form-material sl-right"><div> 
                          <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a>';

                         if($data->hp <> '')
                          $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Sentiment : '.$data->old_senti.'  => '. $data->sentiment.'" data-toggle="tooltip"></i></a>';
                          // $html .= '<pre> | ' .$data->id .'</pre>';
                           $html .=' | <input type="text" class="form-control col-sm-2 " value="'.$data->id.'" style="max-width:30%;" disabled/> ';
                           if($data->manual_by <> '')
                           $html .=' | reviewd by '.$data->manual_by;
                           $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';

                            $html.= '<p>Sentiment : <select class="form-control sentiment-color in-comment-select" id="sentiment_'.$data->id.'"  >';
                            if($data->sentiment == "")
                            $html.= '<option value="" selected="selected"></option>';
                            $html.='<option value="pos"';
                            if($data->sentiment == "pos")
                            $html.= 'selected="selected"';
                            $html.= '>pos</option><option value="neg"';
                            if($data->sentiment == "neg")
                            $html.= 'selected="selected"';
                            $html.= '>neg</option><option value="neutral"';
                            if($data->sentiment == "neutral")
                            $html.= 'selected="selected"';      
                            $html.= '>neutral</option><option value="NA"';
                            if($data->sentiment == "NA")
                            $html.= 'selected="selected"';  
                            $html.= '>NA</option></select>';

                            $html.='<input type="text" class="form-control col-sm-2 " value="'.$data->decided_keyword.'" placeholder="keyword" style="
                            max-width: 50%;" name="keyword" id="decided_kw_'.$data->id.'"/> ';
                            $html.='<input type="hidden" value="'.$data->project_id.'" id="brand_'.$data->id.'"/>';
                         
                            $html.= '<a class="edit_predict_comment" id="'.$data->id.'" href="javascript:void(0)">
                            <span class="mdi mdi-content-save" style="font-size:30px;"></span></a></p>';
                            $html.='<div style="display:none"  align="center" style="vertical-align: top;" id="spin_'.$data->id.'"> <img src="assets/images/ajax-loader.gif" id="loader"></div>';
                           
                            $html.='<p class="m-t-10" id="message_'.$data->id.'">'.$data->message.'</p><a class="change_commentfont" id="'.$data->id.'" href="javascript:void(0)" style="text-decoration: underline;color: green;font-style:italic;padding-right:20px;">Change font   </a>';
                            // $html.='<button type="button" class="btn waves-effect waves-light btn-primary popup_post" id="'.$data->post_id.'" name="'.$data->page_name.'" style="background:#1e88e4;border:none;">See Post</button>';
                            $html.=' <div style="width:45%;display: inline-block" ><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-gray comment_hide"  id="hide_'.$data->id.'" >Hide Comment</a></div></div></div></div></div>';

                            return $html;

                       })   

      
                   ->rawColumns(['post_div'])
       
                   ->make(true);
           }

    public function getMentionPosts()
    {
          $id_search_con='';
          if(null !== Input::get('id_search'))
          {
            $id_search=Input::get('id_search');
            $id_search=str_replace(' ','',$id_search);
            $id_search_con = " and cmts.id Like'%".$id_search."%'";
          }

          $brand_id=Input::get('brand_id');
          if(null !==Input::get('text'))
          {
              $text = Input::get('text');
              if($text == 'Confirmed')
              {
                  $add_checked_predict = ' and (posts.checked_predict = 1 or posts.change_predict = 1)';
                  // $add_date_order = 'cmts.updated_at';
              }
              else
              {
                  $add_checked_predict = ' and (posts.checked_predict = 0 and posts.change_predict = 0)';
                  // $add_date_order = 'cmts.created_time';
              }
          }


              if(null !== Input::get('fday'))
              {
                   $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
                    $dateBegin=date('Y-m-d');
              }
              if(null !==Input::get('sday'))
              { 
                   $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
              }
    
              else
              {
                  $dateEnd=date('Y-m-d');
              }


                  $add_inbound_con ="";$add_expage_con = "";
                  $inboundpages=$this->getInboundPages($brand_id);
                  // dd($excludepages);
                  if ($inboundpages !== '')
                  {
                      $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
                  }
                  $excludepages=$this->getExcludePages($brand_id);
                  if ($excludepages !== '')
                  {
                      $add_expage_con = " and (posts.page_name  not in (".$excludepages.") or posts.page_name is NULL)";
                  }
                  $period_type = '';
                  $additional_filter=" ";
      
                    //         if(null !==Input::get('period'))
                    //         {
                    //        $period=Input::get('period');
                    //         $char_count = substr_count($period,"-");
                    //         $format_type = '';
                    //         if($char_count == 1)
                    //         {
                    //             $format_type ='%Y-%m';
                    //         }
                    //         else if($char_count == 2)
                    //         {
                    //              $format_type ='%Y-%m-%d';
                    //         }   
                    //         else if($char_count > 2)
                    //         {
                    //             this is week
                    //             $pieces = explode(" - ", $period);
                    //             $format_type ='%Y-%m-%d';
                    //             $period_type ='week';
                    //         }

      
                    // if($period_type == 'week')
                    // {
                    //  $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

                    // }
                    // else
                    // {
                    //     $additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
                    // }

                    //        }

                      if(null !== Input::get('tsearch_senti'))
                      {
                          $additional_filter .= ' AND ' . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
                      }
                          // $keyword_data = $this->getprojectkeyword($brand_id);
                      
                        // if(isset($keyword_data [0]['main_keyword']))

                      if(null !== Input::get('keyword_group'))
                      {
                          $keyword_group = Input::get('keyword_group') ;
                      }
                      else
                      {
                          $keyword_group = '' ;
                      }
                         $keyword_data = $this->getprojectkeyword($brand_id);
                         if(isset($keyword_data [0]['main_keyword']))
                         $additional_filter  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 
                         else
                         $additional_filter .= ' ';
                          // dd($additional_filter);
                         if(null !== Input::get('tsearch_keyword'))
                         {
                            $additional_filter .= ' AND ' .$this->getKeywordWhereGen(Input::get('tsearch_keyword'),'posts');
                         }
                         if(null !== Input::get('page_name'))
                         {
                            $additional_filter .= ' AND posts.page_name="' .Input::get('page_name') . '" ';
                         }

                          $query='';
                          $query_result=[];

                          $query = "SELECT posts.id,posts.message,posts.page_name,posts.decided_keyword,".$brand_id." project_id,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
                          " created_time,posts.link,posts.full_picture,IF(posts.checked_sentiment <> '',posts.checked_sentiment,posts.sentiment) sentiment,IF(posts.sentiment = '','~',posts.sentiment) old_senti,posts.emotion,posts.isDeleted,IF(posts.sentiment = posts.checked_sentiment,'','human predict!') hp ".
                          " FROM temp_posts posts LEFT JOIN temp_comments cmts on posts.id=cmts.post_id ".
                          " WHERE posts.isHide=0 and  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con . $add_expage_con . $additional_filter . $add_checked_predict . $id_search_con .
                          " ORDER by timestamp(posts.created_time) DESC ";
                           // dd($query);
                          $query_result = DB::select($query);
                          // dd($query_result);  

                          $data = [];
                          $data_mention=[];


                          foreach ($query_result as  $key => $row) 
                          {
                            //change page name from id to orginal user register name.
                              $message=$this->readmore($row->message);
                              $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8'); 
                              $page_name = $row->page_name;
                              $data[$key]['id'] = $row->id;
                              $data[$key]['message']=$message;
                              $data[$key]['page_name']=$page_name;
                              $data[$key]['created_time']=$row->created_time;
                              $data[$key]['hp']=$row->hp;
                              $data[$key]['old_senti']=$row->old_senti;
                              $data[$key]['project_id']=$row->project_id;
                              $data[$key]['sentiment']=$row->sentiment;
                              $data[$key]['decided_keyword']=$row->decided_keyword;
                          }

                              // dd($data);
                              // $data_mention = $data;
                         return Datatables::of($data)
      
                   ->addColumn('post', function ($data){
                      $page_name=$data['page_name'] ;
                      // if(is_numeric($page_name))
                      // {
                      //    $input = preg_quote($page_name, '~'); // don't forget to quote input string!
                      //    $keys = preg_grep('~' . $input . '~', $monitor_pages);
                      //    $vals = array();
                      //       foreach ( $keys as $key )
                      //       {
                      //          $page_name= $key;
                      //       }
                           
                      // }
                            //generate comment link
                        $fb_p_id=explode('_', $data['id']);
                        $fb_p_id=$fb_p_id[1];
                        $page_id='';
                        $arr_page_id=explode("-",$data['page_name']);
                        if(count($arr_page_id)>0)
                        {
                          $last_index=$arr_page_id[count($arr_page_id)-1];
                          if(is_numeric($last_index))
                            $page_id = $last_index;
                          else
                            $page_id = $data['page_name'];
                        }
                        else
                        {
                          $page_id=$data['page_name'];
                        }

                        $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
                        $page_Link= 'https://www.facebook.com/' . $data['page_name'] ;

                        // $page_id='';
                        // $arr_page_id=explode("-",$page_name);
                        // if(count($arr_page_id)>0)
                        // {
                        //   $last_index=$arr_page_id[count($arr_page_id)-1];
                        //   if(is_numeric($last_index))
                        //   $page_id = $last_index;
                        //   else
                        //   $page_id = $page_name;
                        // }
                        // else
                        // {
                        //   $page_id=$page_name;
                        // }

                        $img_spin ='/assets/images/ajax-loader.gif';
                        $classforbtn='';$stylefora='';$bgcolor='';
                        if($data['sentiment']=='neg')
                        {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                        else if($data['sentiment']=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                        else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}

                        $html = '<div class="profiletimeline"><div class="sl-item">';
                        
                        $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 

                        $html .='<div class="form-material sl-right"><div> 
                        <a href="'.$page_Link.'" class="link" target="_blank">'.$data['page_name'].'</a>';
                        if($data['hp'] <> '')
                        $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Sentiment: '.$data['old_senti'].'  => '. $data['sentiment'].'" data-toggle="tooltip"></i></a>';
                       // $html .= '<pre> | ' .$data->id .'</pre>';
                        $html .=' | <input type="text" class="form-control col-sm-2 " value="'.$data['id'].'" style="max-width:30%;" disabled/> ';
                        
                        $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data['created_time'].'</span></div>';

                        $html.='<div style="display:none"  align="center" style="vertical-align: top;" id="spin_'.$data['id'].'"> <img src="assets/images/ajax-loader.gif" id="loader"></div>';
                        $html.='<p class="m-t-10" id="message_'.$data['id'].'">'.$data['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data['id'].'">View More</a></p><a class="change_font" id="'.$data['id'].'" href="javascript:void(0)" style="text-decoration: underline;color: green;font-style:italic;padding-right:20px;">Change font   </a>
                       <div style="width:45%;display: inline-block" ><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-gray post_hide"  id="hide_'.$data['id'].'" >Hide Post</a></div></div></div></div></div></div>';

                        return $html;
                     })   
  
                    ->rawColumns(['post'])
                 
                    ->make(true);
           }

          function readmore($string)
          {
             $string = strip_tags($string);
             if (strlen($string) > 500) 
             {
                $stringCut = substr($string, 0, 500);
                $endPoint = strrpos($stringCut, ' ');
                //if the string doesn't contain any space then it will cut without word basis.
                $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
             }
            return $string;
          }

          public function getRelatedMention()
          {
              $post_id=Input::get('id');
              $brand_id=Input::get('brand_id');
              // dd($brand_id);
               // $permission_data = $this->getPermission();
               // $edit_permission=$permission_data['edit'];

              $query="SELECT  posts.id,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
              " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.full_picture) full_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion".
              " ,ANY_VALUE(posts.isDeleted) isDeleted from temp_posts posts ".
              "  where posts.id='".$post_id."'  GROUP BY id";
              // dd($query);
              $data = DB::select($query);
              //if no data above query we need to just show post data without comment data , in order to get post data remove cmts.parent''
              // dd($data);
              echo json_encode(array($data));
          }
          public function getRelatedMentionComment()
          {
              $post_id=Input::get('post_id');
              $brand_id=Input::get('brand_id');
              // dd($brand_id);
               // $permission_data = $this->getPermission();
               // $edit_permission=$permission_data['edit'];

              $query="SELECT  posts.id,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
              " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.full_picture) full_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion".
              " ,ANY_VALUE(posts.isDeleted) isDeleted from temp_posts posts ".
              "  where posts.id='".$post_id."'  GROUP BY id";
              // dd($query);
               $data = DB::select($query);
                //if no data above query we need to just show post data without comment data , in order to get post data remove cmts.parent''
                // dd($data);
 
               echo json_encode(array($data));
          }

    public function getMentionRelatedPosts()
    {
          $post_id=Input::get('post_id');
          $db_name=Input::get('db_name');
          $brand_id=Input::get('brand_id');
          $page_name=Input::get('page_name');
          $page_filter = '';
          if($page_name == '' or $page_name == NULL)
          {
            $page_filter = " and posts.page_name='".$page_name."'";
          }

          $query="SELECT posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
          " created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,posts.sentiment ".
          " sentiment,posts.emotion emotion".
          " FROM ".$db_name.".temp_posts posts where posts.id='".$post_id."'".$page_filter."  ORDER BY DATE(posts.created_time) DESC";
          // echo $query;
          // return;

         $data = DB::select($query);
          // dd($data);  
         // echo count($data);
         echo json_encode($data);
    }
    public function UpdateMention()
    {
          $comment_id = Input::post('id');
          $brand_id = Input::post('brand_id');
          $sentiment = Input::post('sentiment');
          $username = Auth::user()->username;
          $decided_keyword = Input::post('decided_keyword');
          if($decided_keyword == Null)
          {
            $decided_keyword = "";
          }
          $manual_date = new \DateTime("now", new \DateTimeZone('Asia/Rangoon') );
          $manual_date = $manual_date->format('Y-m-d H:i:s');
          $db_name = Input::post('db_name');

          $result =DB::table('temp_comments')
           
            ->where('id', $comment_id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('checked_sentiment' => $sentiment,'checked_predict'=>1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));  // update the record in the DB. 

          DB::table($db_name.'.predict_logs')
              ->where('id', $comment_id) 
              ->where('type','mention_comment')
              ->limit(1)
              ->get()->toArray();
              // ztd 18/02/2019 14:50
          if(!empty($row))
          {
            // dd("data exists");
              DB::table($db_name.'.predict_logs')->where('id', $comment_id)->where('type','mention_comment')->delete();
          }
              DB::table($db_name.'.predict_logs')
                ->insert(array('id' => $comment_id,'type' => 'mention_comment','checked_sentiment' => $sentiment,'checked_predict'=>1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
            return $result;
    }
    // public function UpdatePostMention()
    // {
    //       $post_id = Input::get('id');
    //       $brand_id = Input::get('brand_id');
    //       $sentiment = Input::get('sentiment');
    //       $username = Auth::user()->username;
    //       $decided_keyword = Input::get('decided_keyword');
    //       if($decided_keyword == Null)
    //       {
    //         $decided_keyword = "";
    //       }
    //       $manual_date = new \DateTime("now", new \DateTimeZone('Asia/Rangoon') );
    //       $manual_date = $manual_date->format('Y-m-d H:i:s');
    //       $db_name = Input::post('db_name');
    //       $result =DB::table($db_name.'.temp_'.$brand_id.'_posts')
    //         ->where('id', $post_id)  
    //         ->limit(1)  
    //         ->update(array('checked_sentiment' => $sentiment,'checked_predict'=>1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));

    //       DB::table('predict_'.$brand_id.'_logs')
    //           ->where('id', $post_id) 
    //           ->where('type','mention_post')
    //           ->limit(1)
    //           ->get()->toArray();
    //       if(!empty($row))
    //       {
    //         // dd("data exists");
    //           DB::table($db_name.'.predict_'.$brand_id.'_logs')->where('id', $post_id)->where('type','mention_post')->delete();
    //       }
    //           DB::table($db_name.'.predict_'.$brand_id.'_logs')
    //             ->insert(array('id' => $post_id,'type' => 'mention_post','checked_sentiment' => $sentiment,'checked_predict'=>1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
         
    //       return $result;
    // }

    public function UpdateHideMention()
    {
          $manual_date = new \DateTime("now", new \DateTimeZone('Asia/Rangoon') );
          $manual_date = $manual_date->format('Y-m-d H:i:s');
          $username = Auth::user()->username;
          $id = Input::get('id');
          $brand_id = Input::get('brand_id');
          $result =DB::table('temp_comments')
            ->where('id', $id)
            ->limit(1)  
            ->update(array('isHide' => 1));
          $row = DB::table('predict_logs')
                ->where('id', $id) 
                ->where('type','mention_comment')
                ->limit(1)
                ->get()->toArray();
              // ztd 18/02/2019 14:50
          if(!empty($row))
          {
               // dd("data exists");
              // DB::table('predict_'.$brand_id.'_logs')->where('id', $id)->where('type','mention_comment')->delete();
              DB::table('predict_logs')->where('id',$id)->where('type','mention_comment')
                ->update(array('isHide' => 1));
          }
          else
          {
            // dd("data not exist");
              DB::table('predict_logs')
                ->insert(array('id' => $id,'type' => 'mention_comment','isHide' => 1,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
          }

          return $result;
    }

    public function UpdateHideMentionPost()
    {
          $id = Input::get('id');
          $brand_id = Input::get('brand_id');
          $result =DB::table('temp_posts')
            ->where('id', $id) 
            ->limit(1) 
            ->update(array('isHide' => 1));
          $row = DB::table('predict_logs')
              ->where('id', $id) 
              ->where('type','mention_post')
              ->limit(1)
              ->get()->toArray();
            if(!empty($row))
           {
               // dd("data exists");
              // DB::table('predict_'.$brand_id.'_logs')->where('id', $id)->where('type','mention_comment')->delete();
              DB::table('predict_logs')->where('id',$id)->where('type','mention_post')
                ->update(array('isHide' => 1));
           }
           else
           {
              DB::table('predict_logs')
                ->insert(array('id' => $id,'type' => 'mention_post','isHide' => 1,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
           }
          return $result;
    }

    public function export()
    {
        if($user=Auth::user())
        {
            $user_id = Auth::user()->id;
            $databases =  DB::table('manage_dbs')->select('*')->where('user_id',$user_id)->get();
            return view('BSL/export',compact('databases'));
        }
        else
        {
           $title="Login";
           return view('auth.login',compact('title'));
        }  
    }

    public function getExcelData()
    { 
        $db_name = Input::get('db_name');
        if(null !== Input::get('fday'))
        {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
        else
        {
          $dateBegin=date('Y-m-d');
        }
        if(null !==Input::get('sday'))
        {
          $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
        else
        {
          $dateEnd=date('Y-m-d');
        }
        $query="Select id from ".$db_name.".projects";
        $project_ids = DB::select($query);
        foreach ($project_ids as $project_id)
        {
            $project_id = $project_id->id;
            $query="SELECT cmts.message,cmts.temp_id,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
          " cmts.sentiment,cmts.emotion,cmts.checked_sentiment,cmts.decided_keyword,cmts.checked_predict".
          " FROM ".$db_name.".temp_inbound_comments cmts where (cmts.message <> '' or cmts.message <> null) and parent='' and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ORDER by DATE(cmts.created_time) DESC";
            $data = DB::select($query);
        }
        $cmt_data_array=[];
        $cmt_data_array[] = array ('temp_id','message','created_time');
        foreach($data as $result)
        {
          $cmt_data_array[] =  array(
                                     'temp_id'=>$result->temp_id,
                                     'message'=>$result->message,
                                     'created_time'=>$result->created_time
                                   );
        }
          // return $cmt_data_array;
        $path =  public_path('reports/commentData_' . $dateBegin . '_to_' . $dateEnd);
        $this->delete_file($path); 
          // $spreadsheet = new Spreadsheet();

          // $spreadsheet->getActiveSheet()->fromArray($cmt_data_array,NULL,'A1')->setCellValue('A4029','')->setCellValue('A4033','');
          // $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
          // // // $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
          // // $writer = new Xlsx($spreadsheet);
          // $writer->save($path.'.xlsx');

          // return $writer->save('reports/commentData_' . $dateBegin . '_to_' . $dateEnd);

        return Excel::create('commentData_' . $dateBegin . '_to_' . $dateEnd, function($excel) use ($cmt_data_array) {

            $excel->sheet('Comment Data', function($sheet) use ($cmt_data_array)
            {
              $sheet->fromArray($cmt_data_array,null,'A1',false, false);
              $sheet->setCellValue('B3065', 'formula Error');
              $sheet->setCellValue('B7209', 'formula Error');
              $sheet->setCellValue('B2349', 'formula Error');
              $sheet->setCellValue('B4118', 'formula Error');
            
                });
             })->store('xlsx', 'public/reports/', true);
    }

    public function delete_file($file)
   {
        if ( file_exists($file) ) 
        { 
            unlink($file);
        }   
          return true;
   }

    public function changefont()
    {
          $comment_id = Input::get('comment_id');
          $brand_id= Input::get('brand_id');
          $original_msg = '';$uni_msg = '';
          $comment_table = "temp_inbound_comments";
          $result = DB::table($comment_table)->where('id',$comment_id)->get();
          foreach ($result as $value) 
          {
            $original_msg = $value->original_message;
            $uni_msg = $value->message;
          }
            $original_msg = Rabbit::zg2uni($original_msg);
            $uni_msg = Rabbit::zg2uni($uni_msg);
          if($original_msg <> '')
          {
             DB::table($comment_table)->where('id',$comment_id)->update(['message' => $original_msg]);
          }
      
          return   array($original_msg, $uni_msg);
    
    }

    public function changeMentionfont()
    {
          $comment_id = Input::get('comment_id');
          $brand_id= Input::get('brand_id');
          $original_msg = '';$uni_msg = '';
          $comment_table = "temp_comments";
          $result = DB::table($comment_table)->where('id',$comment_id)->get();
          foreach ($result as $value) 
          {
            $original_msg = $value->original_message;
            $uni_msg = $value->message;
          }
            $original_msg = Rabbit::zg2uni($original_msg);
            $uni_msg = Rabbit::zg2uni($uni_msg);
      
          if($original_msg <> '')
          {
            DB::table($comment_table)->where('id',$comment_id)->update(['message' => $original_msg]);
          }
            //  else
            //  {
            //   // dd($comment_id);
            //   $result = DB::table($comment_table)->where('id',$comment_id)->limit(1)->update(['message' => $uni_msg]);
            // dd($result);
            //  }
            return   array($original_msg, $uni_msg);
    } 
    public function changeMentionPostfont()
    {
          $post_id = Input::get('post_id');
          $brand_id= Input::get('brand_id');
          $original_msg = '';$uni_msg = '';
          $post_table = "temp_posts";
          $result = DB::table($post_table)->where('id',$post_id)->get();
          foreach ($result as $value)
          {
            $original_msg = $value->original_message;
            $uni_msg = $value->message;
          }
            $original_msg = Rabbit::zg2uni($original_msg);
            $uni_msg = Rabbit::zg2uni($uni_msg);
          if($original_msg <> '')
          {
            DB::table($post_table)->where('id',$post_id)->update(['message' => $original_msg]);
          }
          //  else
          //  {
          //   // dd($comment_id);
          //   $result = DB::table($comment_table)->where('id',$comment_id)->limit(1)->update(['message' => $uni_msg]);
          // dd($result);
          //  }
          return   array($original_msg, $uni_msg);
    }
	  public function changeMentionCommentfont()
    {
          $comment_id = Input::get('comment_id');
          $brand_id= Input::get('brand_id');
          $original_msg = '';$uni_msg = '';
          $comment_table = "temp_comments";
          $result = DB::table($comment_table)->where('id',$comment_id)->get();
          foreach ($result as $value) 
          {
            $original_msg = $value->original_message;
            $uni_msg = $value->message;
          }
            $original_msg = Rabbit::zg2uni($original_msg);
            $uni_msg = Rabbit::zg2uni($uni_msg);
          if($original_msg <> '')
          {
            DB::table($comment_table)->where('id',$comment_id)->update(['message' => $original_msg]);
          }
          //  else
          //  {
          //   // dd($comment_id);
          //   $result = DB::table($comment_table)->where('id',$comment_id)->limit(1)->update(['message' => $uni_msg]);
          // dd($result);
          //  }
          return   array($original_msg, $uni_msg);
    }
    public function getRelatedPosts()
    {       
           //  $post_id=Input::get('post_id');
           //  $db_name=Input::get('db_name');
           //  $brand_id=Input::get('brand_id');
           //  $page_name=Input::get('page_name');
           //  $query="SELECT posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
           //  " created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,posts.sentiment ".
           //  " sentiment,posts.emotion emotion".
           //  " FROM ".$db_name.".temp_".$brand_id."_inbound_posts posts where posts.id='".$post_id."' and posts.page_name='".$page_name."'  ORDER BY DATE(posts.created_time) DESC";
           
           // $data = DB::select($query);
           // echo json_encode($data);
          $post_id=Input::get('id');
          $brand_id=Input::get('brand_id');

          //dd($post_id);
          $query="SELECT  posts.id,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
        " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.type) post_type,ANY_VALUE(posts.full_picture) full_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion,".
        " ANY_VALUE(posts.isDeleted) isDeleted from temp_inbound_posts posts  LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id".
        "  where posts.id='".$post_id."'  and cmts.parent='' GROUP BY id";

          $data = DB::select($query);
        //if no data above query we need to just show post data without comment data , in order to get post data remove cmts.parent''
          if (count($data) <=0 )
          {
                $query="SELECT  posts.id,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
              " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.type) post_type,ANY_VALUE(posts.full_picture) full_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion, ".
              " ANY_VALUE(posts.isDeleted) isDeleted from temp_inbound_posts posts  LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id".
              "  where posts.id='".$post_id."'  GROUP BY id";
                $data = DB::select($query);
          }
          $project = Project::find($brand_id);
          $monitor_pages = $project->monitor_pages;
          $monitor_pages = explode(',', $monitor_pages);
          $mongo_page = $this->Format_Page_Name($monitor_pages);
          //dd($mongo_page);

        //get page id from mongo
        $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

          return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   ['page_name'=>['$in'=>$mongo_page]],
                  
                            ]
              ]  
                         
             ],
             

             ['$sort' =>['_id'=>1]]
              
          ]);
        })->toArray();
        // dd($InboundPages_result);
        $mongo_page_name=[];
        $mongo_page_id=[];
        $mongo_page_full_name=[];
        foreach ($InboundPages_result as  $key => $row) {
          $id='';$page_name_new='';$full_page_name='';
          if(isset($row['id'])) $id = $row['id'];
          if(isset($row['page_name'])) $page_name = $row['page_name'];
          if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

          $mongo_page_name[] =[
             'page_name' =>$page_name,
            ];
            $mongo_page_id[] =[
              'id' => $id,
            ];
          $mongo_page_full_name[] =[
              'full_page_name' =>$full_page_name,
             ];
          

        }
        $mongo_page_name = array_column($mongo_page_name,'page_name');
        $mongo_page_full_name = array_column($mongo_page_full_name,'full_page_name');
        $mongo_page_id = array_column($mongo_page_id,'id');

       /* echo count($data);*/
        echo json_encode(array($data,$mongo_page_name,$mongo_page_full_name,$mongo_page_id));
    }

    public function Updatepredict()
    { 
          $post_id = Input::get('id');
          $brand_id = Input::get('brand_id');
          $sentiment = Input::get('sentiment');
          $username = Auth::user()->username;
          $decided_keyword = Input::get('decided_keyword');
          if($decided_keyword == Null)
          {
            $decided_keyword = "";
          }
          $manual_date = new \DateTime("now", new \DateTimeZone('Asia/Rangoon') );
          $manual_date = $manual_date->format('Y-m-d H:i:s');
          $db_name = Input::get('db_name');
          $tags ='';
          if(null !== Input::get('tags'))
          {
            $tags=Input::get('tags');
            $tags_id=implode(",",Input::get('checked_tags_id'));
          }

          $row =DB::table($db_name.'.predict_logs')
              ->where('id', $post_id) 
              ->where('type','inbound_comment')
              ->limit(1)
              ->get()->toArray();
           if(!empty($row))
          {
            // dd("data exists");
              DB::table($db_name.'.predict_logs')->where('id', $post_id)->where('type','inbound_comment')->delete();
          }

          if($tags !=='')
          {
              $result =DB::table($db_name.'.temp_inbound_comments')
              ->where('id', $post_id)  
              ->limit(1) 
              ->update(array('checked_sentiment' => $sentiment,'checked_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));

                // temp_id,id,type,checked_sentiment,decided_keyword text  ,checked_emotion,checked_tags  ,checked_tags_id ,tag_flag  , isHide ,checked_predict ,change_predict ,action_status ,action_taken ,isBookMark  ,manual_date ,manual_by ,created_at  ,updated_at
                DB::table($db_name.'.predict_logs')
                ->insert(array('id' => $post_id,'type' => 'inbound_comment','checked_sentiment' => $sentiment,'checked_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
          }

          else
          {
				    $result =DB::table($db_name.'.temp_inbound_comments')
    				->where('id', $post_id)  // find your user by their email
    				->limit(1)  // optional - to ensure only one record is updated.
    				->update(array('checked_sentiment' => $sentiment,'checked_predict'=>1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));  // update the record in the DB. 

            // ztd 18/02/2019 14:32 PM
            DB::table($db_name.'.predict_logs')
                ->insert(array('id' => $post_id,'type' => 'inbound_comment','checked_sentiment' => $sentiment,'checked_predict'=>1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
			    }
            return $result;
    }
    public function setMentionPostPredict()
    {  
          // $post_id = Input::post('id');

          // $brand_id = Input::post('brand_id');
       
          // $sentiment = Input::post('sentiment');
          // // dd($sentiment);
          // // $emotion = Input::post('emotion');\
          // $username = Auth::user()->username;
          // $decided_keyword = Input::post('decided_keyword');
          // if($decided_keyword == Null)
          // {
          //   $decided_keyword = "";
          // }
          // $manual_date = new \DateTime("now", new \DateTimeZone('Asia/Rangoon') );
          // $manual_date = $manual_date->format('Y-m-d H:i:s');
          // $db_name = Input::post('db_name');
          // $tags ='';
          // if(null !== Input::post('tags'))
          // {
          //   $tags=Input::post('tags');
          //   $tags_id=implode(",",Input::post('checked_tags_id'));
          // }
            

          //   if($tags !=='')
          //   {

          //    $result =DB::table($db_name.'.temp_'.$brand_id.'_inbound_comments')
           
          //   ->where('id', $post_id)  // find your user by their email
          //   ->limit(1)  // optional - to ensure only one record is updated.
          //   ->update(array('checked_sentiment' => $sentiment,'checked_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));  // update the record in the DB. 
          //   }
          //   else
          //      {

          //    $result =DB::table($db_name.'.temp_'.$brand_id.'_inbound_comments')
           
          //   ->where('id', $post_id)  // find your user by their email
          //   ->limit(1)  // optional - to ensure only one record is updated.
          //   ->update(array('checked_sentiment' => $sentiment,'checked_predict'=>1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));  // update the record in the DB. 
          //      }

         
          //   return $result;

    }
    public function track()
    {
      if($user=Auth::user())
      {
         $user_id = Auth::user()->id;
         $query="Select * from projects order by id desc";
         $projects= DB::select($query);
         return view('BSL/datatracking',compact('projects'));
      }
      else
      {
         $title="Login";
         return view('auth.login',compact('title'));
      }
    }
    // public function getMonitorPages()
    // {
    //   $id = Input::get('project_id');
    //   // $id = 33;
    //   $query = "Select * from projects where id=".$id;
    //   $result = DB::select($query);
    //   // dd($result);
    //   $pages = $result[0]->monitor_pages;
     
    //   $page_arr = explode(',', $pages);
    //   return $page_arr;
    // }
    public function getMentionTrackingData()
    {
       $project_id = Input::get('project_id');
       $fday = Input::get('fday');
       $fday = new DateTime(str_replace(' ', '-', $fday));
       $fday = $fday->setTime(0, 0);
       $fday = $fday->format('Y-m-d');

       $sday = Input::get('sday');
       $sday = new DateTime(str_replace(' ', '-', $sday));
       $sday = $sday->format('Y-m-d');
       $sday = date('Y-m-d', strtotime($sday . ' +1 day'));

       if(null !== $fday )
        {
         $dateBegin1=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
         $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$fday))* 1000);
        }
       else
        {
          $dateBegin = new MongoDB\BSON\UTCDateTime(str_replace('-','/',date('Y-m-d'))* 1000);
        }
        
        if(null !== $sday)
        {
           $dateEnd1=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
           $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$sday))* 1000);
        }

        else
        {
           $dateBegin = new MongoDB\BSON\UTCDateTime(str_replace(' ','/',date('Y-m-d'))* 1000);
        }

         $all_mention = $this->get_Mention_Data($project_id,$dateBegin,$dateEnd,$dateBegin1,$dateEnd1);
         // dd($all_mention);
         return Datatables::of($all_mention) 
               ->addColumn('mongopostcount', function($all_mention) {
                   return $all_mention['mongopostcount'];
                 })
               ->addColumn('mypostcount',function($all_mention) {
                return $all_mention['mypostcount'];
               })
               ->addColumn('page_name',function($all_mention) {
                return $all_mention['page_name'];
               })
               ->make(true);
    }

    public function getkeywordfilter_MYSQL($keyword_data,$tbl)
    {
        $conditional_and = '';
        $conditional_or = '';
        
        foreach ($keyword_data as $key => $row) 
        {
              $main_con ='';$include_con = '';$exclude_con = '';$conditional_and='';
              $main_con = $tbl.".message Like '%".$row['main_keyword']."%'";
              $require_keyword = explode(",", $row['require_keyword']);
              foreach($require_keyword as $req =>$element)
              {
                  if( $element  <> '')
                  {
                     if((int)$req === 0)
                     $include_con = $tbl.".message Like '%".$element."%'";
                     else
                     $include_con .="  or ".$tbl.".message Like '%".$element."%'";
                  }
              }

            $exclude_keyword = explode(",", $row['exclude_keyword']);
            foreach($exclude_keyword as $exc =>$element)
            {
                if( $element <> '')
                {
                    if((int)$req === 0)
                    $exclude_con = $tbl.".message NOT LIKE '%".$element."%'";
                    else
                    $exclude_con .="  or ".$tbl.".message NOT LIKE '%".$element."%'";
                }
            }

           if(!empty($main_con))
           {
            $conditional_and .=  $main_con . "  " ;
           }
           if(!empty($include_con))
           {
            $conditional_and  .= " and ( " . $include_con . " )  " ;
           }
           if(!empty($exclude_con))
           {
            $conditional_and .= " and ( " . $exclude_con . " )  " ;
           }

           if($conditional_and <> '')
           {
            if($conditional_or == '')
            $conditional_or = "( " . $conditional_and . " ) ";
            else
            $conditional_or .= " or ( " . $conditional_and . " ) ";
            
           }
        }
            return  $conditional_or;
    }
    public function get_Mention_Data($project_id,$dateBegin,$dateEnd,$dateBegin1,$dateEnd1)
    {
       $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];
       $keyword_data = $this->getprojectkeyword($project_id,'posts');
       $keyword_filter_post = $this->getkeywordfilter_MYSQL($keyword_data,'posts');
       $keyword_filter_comment = $this->getkeywordfilter_MYSQL($keyword_data,'cmts');
       $keyword_con = '';
        // if($keyword_filter <> ''){
       $filter['$or'] = $this->getkeywordfilter($keyword_data);
        // dd($keyword_filter);
       // if($keyword_filter <> '') 
       $keyword_con_post =" and (".$keyword_filter_post.")";
       $keyword_con_comment =" and (".$keyword_filter_comment.")";
       // $mention_post_id_array = $this->get_table_id($project_id,"posts","id");
       $query = "select count(*) count,page_name from temp_posts posts where (Date(created_time) BETWEEN '".$dateBegin1."' and '".$dateEnd1."')".$keyword_con_post." group by posts.page_name order by posts.page_name ";
       $my_mention_postresult=DB::select($query);
            // dd($my_mention_postresult);
       $my_mention_postdata=[];
       if(!empty($my_mention_postresult))
       {
           foreach ($my_mention_postresult as $key => $value) 
           {
              $my_mention_postdata[$key]['page_name']= $value->page_name;
              $my_mention_postdata[$key]['count']= $value->count;
            }
       }
             // dd($my_mention_postdata);
            // comment count for mention///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = "select count(*) count,page_name from temp_comments cmts where (Date(created_time) BETWEEN '".$dateBegin1."' and '".$dateEnd1."')".$keyword_con_comment."  AND cmts.page_name <> ''  group by cmts.page_name order by cmts.page_name ";
            // dd($res);
            $my_mention_commentresult=DB::select($res);
            // dd($my_mention_commentresult);
            $my_mention_commentdata=[];
            if(!empty($my_mention_commentresult))
            {
              foreach ($my_mention_commentresult as $key => $value) 
              {
                 $my_mention_commentdata[$key]['page_name']= $value->page_name;
                 $my_mention_commentdata[$key]['count']= $value->count;
              }
            }
            // dd($my_mention_postdata,$my_mention_commentdata);
            $post_page_array = [];$comment_page_array = [];
            foreach($my_mention_postdata as $key=>$value)
            {
              $post_page_array[$key]['page_name'] = $value['page_name'];
              // $post_page_array[$key]['post_count'] = $value['count'];

            }
            foreach ($my_mention_commentdata as $key => $value) {
               $comment_page_array[$key]['page_name'] = $value['page_name'];
               // $comment_page_array[]['comment_count'] = $value['count'];


            }
            // dd($post_page_array,$comment_page_array);
            // $post_page_array = array_reverse($post_page_array);
            // $comment_page_array = array_reverse($comment_page_array);
            // $output = $post_page_array + $comment_page_array;
            $output = array_merge($post_page_array,$comment_page_array);
            // dd($output);  
            $out = [];
            foreach($output as $key => $val) {
              $out[] = $val['page_name'];
              // $out[$key]['post_count'] = $val['post_count'];
              // $out[$key]['comment_count'] = $val['comment_count'];
            }
            // dd($out);
            // $page_array = array_unique($out); 
            // // dd($page_array);
            // $page_array = array_values($page_array);   /* reindexing */
            
            // foreach($my_mention_postdata as $mpost) $m_page[] = $mpost['page_name'];
            // // dd($m_page,$page_array);
            // $post_zero_array = array_diff($page_array,$m_page);
            // dd($post_zero_array)
            // $mysql_count_collection = [];

          //   for($i = 0 ; $i < count($page_array) ; $i ++){
          //     foreach($my_mention_postdata as $key => $value){
          //       if (in_array($value['page_name'],$page_array))
          //       {
          //         // dd(in_array($my_mention_postdata[$i]['page_name'],$page_array));
          //         $mysql_count_collection[$key]['page_name'] = $value['page_name'];
          //         $mysql_count_collection[$key]['post_count'] = $value['count'];
          //       }
          //       else
          //       {
          //         $mysql_count_collection[$key]['page_name'] = $page_array;
          //         $mysql_count_collection[$key]['post_count'] = 0;
          //       }
          //     }
          // }
          // dd($mysql_count_collection);
           
            // foreach($my_mention_postdata as $key => $value)
            // {
              // for($i = 0 ; $i < count($page_array) ; $i ++)
              //   {
              //     foreach($my_mention_postdata as $key => $value){
                    
              //       // if (false !== stripos($value['page_name'], $page_array[$i])) {
              //       //     $mysql_count_collection[$key]['page_name']=$value['page_name'];
              //       //     $mysql_count_collection[$key]['post_count'] =$value['count'];
              //       // }
              //       // else
              //       // {
              //       //     $mysql_count_collection[$key]['page_name']=$value['page_name'];
              //       //     $mysql_count_collection[$key]['post_count'] =0;
              //       // }
    
              //     // if($page_array[$i] === $value['page_name'])
              //     // {
              //     //   $mysql_count_collection[$key]['page_name'] =$value['page_name'];
              //     //   $mysql_count_collection[$key]['post_count'] =$value['count'];
              //     // }
              //     // else
              //     // {
              //     //   $mysql_count_collection[$key]['page_name'] =$value['page_name'];
              //     //   $mysql_count_collection[$key]['post_count'] = 0;
              //     // }
              //   }
              //   }
                    // foreach($my_mention_commentdata as $data)
                    // {
                    //   if (in_array($data['page_name'], $value))
                    //   $mysql_count_collection[$key]['comment_count'] =$data['count'];
                    // }
            // }
            // dd($mysql_count_collection);

            foreach ($mysql_count_collection as $key => $value) 
            {
              if (! array_key_exists("post_count",$value))
              $mysql_count_collection[$key]['post_count'] = 0;
              elseif(! array_key_exists("comment_count",$value))
              $mysql_count_collection[$key]['comment_count'] = 0;
            }
              // dd($mysql_count_collection);

              $mention_post_result=MongodbData::raw(function ($collection) use($filter,$dateBegin,$dateEnd) {//print_r($filter);

              return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   ['id'=> ['$exists'=> true]],
                   ['created_time' => ['$gte' => $dateBegin ,'$lte' => $dateEnd]],
                   
                    $filter
                            ]
              ]  
                         
             ],
       
             ['$group' => ['_id'=> '$page_name', 'count' => ['$sum'=> 1]]],
             ['$addFields'=> [ 'page_name'=> '$_id']],
             ['$project'=> [ '_id'=> 0 ]],
             ['$sort' => [ 'page_name'=> 1] ],
            ]);
            })->toArray();
 
             // comment count for mention
            // $mention_comment_result=Comment::raw(function ($collection) use($filter,$dateBegin,$dateEnd) {//print_r($filter);

            //     return $collection->aggregate([
            //         [
            //         '$match' =>[
            //              '$and'=> [ 
            //              ['id'=> ['$exists'=> true]],
            //              ['created_time' => ['$gte' => $dateBegin ,'$lte' => $dateEnd]],
                         
            //               $filter
            //                       ]
            //         ]  
                               
            //        ],
                   
            //        ['$group' => ['_id'=> '$page_name', 'count' => ['$sum'=> 1]]],
            //        ['$addFields'=> [ 'page_name'=> '$_id']],
            //        ['$project'=> [ '_id'=> 0 ]],
            //        ['$sort' => [ 'page_name'=> 1] ],
            //       ]);
                 
              
            // })->toArray();
             // end

            // dd($mention_post_result,$my_mention_data);
            $all_mention =[];
            // $all_mention[2]['mycount'] =0;
           foreach($mention_post_result as $key => $value)
           {
              $all_mention[$key]['page_name'] =$value['page_name'];
              $all_mention[$key]['mongopostcount'] =$value['count'];
              foreach($my_mention_postdata as $data)
              {
                // if($value['page_name'] == $data['page_name'])
                // {
                    if (in_array($data['page_name'], $value))

                    $all_mention[$key]['mypostcount'] =$data['count'];
                 // }
               }
            }
            // dd($all_mention);
           foreach ($all_mention as $key => $value) 
           {
            if (! array_key_exists("mypostcount",$value))
                $all_mention[$key]['mypostcount'] = 0;
           }
            // dd($all_mention);
          return $all_mention;
            // }
             // dd($all_mention);
          return array();
    }
    public function get_table_id($project_id,$table,$require_var,$related_pages=[])
    {
      $filter_pages=$this->getPageWhere($related_pages);
      $query = "SELECT ".$require_var." from temp_".$table ."  Where 1=1 " . $filter_pages;
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }
    public function getPageWhere($related_pages)
    {
      $pages='';
      $filter_pages = '';
      foreach ($related_pages as $key => $value) 
      {
        if((int)$key === 0)
        $pages ="'".$value."'";
        else
        $pages .=",'".$value."'";
        $filter_pages="  AND page_name in (".$pages.")";
      }
        return  $filter_pages;
    }
    public function getprojectkeyword($brand_id)
    {
         $keyword_data = ProjectKeyword::select('main_keyword','require_keyword','exclude_keyword')->where('project_id','=',$brand_id)->get()->toArray();
         return $keyword_data;
    }
    public function getkeywordfilter($keyword_data)
    {
        $conditional=[];
        foreach ($keyword_data as  $key => $row) 
        {
           //  $request['main_keyword']       =    '.*' . $row['main_keyword'];
             //$request['require_keyword']       =    $row['require_keyword'];
               $main_keyword_filter =[ 'message' => new MongoDB\BSON\Regex(".*" . $row['main_keyword'],'i' )];
               $require_keyword = explode(",", $row['require_keyword']);
               $conditional_require_or=[];
               $require_keyword_filter=[];
               foreach($require_keyword as $i =>$element)
               {
                  $require_keyword_filter[] = [ 'message' => new MongoDB\BSON\Regex(".*". $element,'i'  )];
                  $conditional_require_or['$or']=$require_keyword_filter;
                }       
                  $exclude_keyword = explode(",", $row['exclude_keyword']);
                  $conditional_exclude_or=[];
                  $exclude_keyword_filter=[];
                  foreach($exclude_keyword as $i =>$element)
                  {
                      if(!empty($element))
                      {
                            $exclude_keyword_filter[] = [ 'message' => ['$not'=>new MongoDB\BSON\Regex(".*". $element,'i'  )]];
                            $conditional_exclude_or['$or']=$exclude_keyword_filter;
                      }
                  }
                            $conditional_and['$and'] =[$main_keyword_filter];
                            if(!empty($conditional_require_or))
                            {
                                $conditional_and['$and']=[$conditional_and,$conditional_require_or];
                            }                
                            if(!empty($conditional_exclude_or))
                            {
                                $conditional_and['$and']=[$conditional_and,$conditional_exclude_or];
                            }
                              //   $conditional_and['$and'] =[$main_keyword_filter,$conditional_require_or,$conditional_exclude_or];
                                $conditional[] =$conditional_and;
        }

        return  $conditional;

    }
    public function getTrackingData()
   {
          // for monitor page inbound post table
          $pages='';
          $project_id = Input::get('project_id');
          // dd($pages); 
          $query = "Select * from projects where id=".$project_id;
          $result = DB::select($query);
          $pages = $result[0]->monitor_pages;
          
          $page_arr =[];
          $page_arr = explode(',', $pages);
          
          $fday = Input::get('fday');

          // set time to 00:00:00
          $fday = new DateTime(str_replace(' ', '-', $fday));
          $fday = $fday->setTime(0, 0);
          $fday = $fday->format('Y-m-d');
          // $fday = $fday->format('Y-m-d');
          // $fday = date('Y-m-d', strtotime($fday . ' -1 day'));
          

          $sday = Input::get('sday');

          // Add one day to End date
          $sday = new DateTime(str_replace(' ', '-', $sday));
          $sday = $sday->format('Y-m-d');
          $sday = date('Y-m-d', strtotime($sday . ' +1 day'));
          // dd($fday,$sday);
          if(null !== $fday )
          {
           $dateBegin1=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
           $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$fday))* 1000);
           // dd($dateBegin);
          }
          else
          {
            $dateBegin = new MongoDB\BSON\UTCDateTime(str_replace('-','/',date('Y-m-d'))* 1000);
          }
          
          if(null !== $sday)
          {
             $dateEnd1=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
             $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$sday))* 1000);
            
          }
  
          else
          {
             $dateBegin = new MongoDB\BSON\UTCDateTime(str_replace(' ','/',date('Y-m-d'))* 1000);
          }
          $all_monitor = $this->get_Monitor_Data($project_id,$dateBegin,$dateEnd,$dateBegin1,$dateEnd1,$page_arr);
          // dd($all_monitor);
          return Datatables::of($all_monitor) 
                 ->addColumn('mongopostcount', function($all_monitor) {
                     return $all_monitor['mongo_post_count'];
                   })
                 ->addColumn('mypostcount',function($all_monitor) {
                  return $all_monitor['mysql_post_count'];
                 })
                 ->addColumn('mongocommentcount', function($all_monitor) {
                     return $all_monitor['mongo_comment_count'];
                   })
                 ->addColumn('mycommentcount',function($all_monitor) {
                  return $all_monitor['mysql_comment_count'];
                 })
                 ->addColumn('page_name',function($all_monitor) {
                  return $all_monitor['page_name'];
                 })
                 ->make(true);
         
    }
    public function get_Monitor_Data($project_id,$dateBegin,$dateEnd,$dateBegin1,$dateEnd1,$page_arr)
    {
        $inbound_pages = $this->getInboundPages($project_id);
        $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];
        $query = "select count(*) count,page_name from temp_inbound_posts where (Date(created_time) BETWEEN '".$dateBegin1."' and '".$dateEnd1."') and (page_name in (".$inbound_pages.")) group by page_name order by page_name ";
          $my_monitor_postresult=DB::select($query);
          $my_monitor_postdata=[];
          foreach ($my_monitor_postresult as $key => $value) 
          {
              $my_monitor_postdata[$key]['count']= $value->count;
              $my_monitor_postdata[$key]['page_name']= $value->page_name;
          }

          // inbound comments count_from_mysql
          $res = "select count(*) count,page_name from temp_inbound_comments where (Date(created_time) BETWEEN '".$dateBegin1."' and '".$dateEnd1."') and (page_name in (".$inbound_pages.")) group by page_name order by page_name ";
          $my_monitor_commentresult=DB::select($res);
          $my_monitor_commentdata =[];
          foreach ($my_monitor_commentresult as $key => $value) 
          {
              $my_monitor_commentdata[$key]['count']= $value->count;
              $my_monitor_commentdata[$key]['page_name']= $value->page_name;
          }

              $mysql_count_collection=[];
		          foreach($my_monitor_commentdata as $key => $value)
			        {
      				  $mysql_count_collection[$key]['page_name'] =$value['page_name'];
      				  $mysql_count_collection[$key]['comment_count'] =$value['count'];
				        foreach($my_monitor_postdata as $data)
				        {
    				      if (in_array($data['page_name'], $value))
    				      $mysql_count_collection[$key]['post_count'] =$data['count'];
			          }
              }

        			   foreach ($mysql_count_collection as $key => $value) 
        			   {
          				  if (! array_key_exists("post_count",$value))
          				  $mysql_count_collection[$key]['post_count'] = 0;
        			   }

           // dd($mysql_count_collection);

			          $monitor_post_result=MongoboundPost::raw(function ($collection) use($dateBegin,$dateEnd,$page_arr) {//print_r($filter);

			         return $collection->aggregate([
    			        [
    			        '$match' =>[
    			             '$and'=> [ 
    			             ['id'=> ['$exists'=> true]],
    			             ['created_time' => ['$gte' => $dateBegin ,'$lte' => $dateEnd]],
    			             ['page_name' => ['$in' => $page_arr ]]
    			                      ]
    			        ]  
    			                   
    			       ],
    			       
    			       ['$group' => ['_id'=> '$page_name', 'count' => ['$sum'=> 1]]],
    			       ['$addFields'=> [ 'page_name'=> '$_id']],
    			       ['$project'=> [ '_id'=> 0 ]],
    			       // ['$sort' => [ 'page_name'=> 1] ],
    			      ]);
			           })->toArray();
                // bound comments count retrieve

                 $monitor_comment_result=MongoboundComment::raw(function ($collection) use($dateBegin,$dateEnd,$page_arr) {//print_r($filter);

                  return $collection->aggregate([
                      [
                      '$match' =>[
                           '$and'=> [ 
                           ['id'=> ['$exists'=> true]],
                           ['created_time' => ['$gte' => $dateBegin ,'$lte' => $dateEnd]],
                           ['page_name' => ['$in' => $page_arr ]]
                                    ]
                      ]  
                                 
                     ],
                     
                     ['$group' => ['_id'=> '$page_name', 'count' => ['$sum'=> 1]]],
                     ['$addFields'=> [ 'page_name'=> '$_id']],
                     ['$project'=> [ '_id'=> 0 ]],
                     // ['$sort' => [ 'page_name'=> 1] ],
                    ]);
                   
               
              })->toArray();

           $mongo_count_collection=[];
           foreach($monitor_comment_result as $key => $value)
		       {
        		  $mongo_count_collection[$key]['page_name'] =$value['page_name'];
        		  $mongo_count_collection[$key]['comment_count'] =$value['count'];
        		  foreach($monitor_post_result as $data)
        		  {
        		     if (in_array($data['page_name'], $value))
        			   $mongo_count_collection[$key]['post_count'] =$data['count'];
        		  }
           }

      		  foreach ($mongo_count_collection as $key => $value) 
      		  {
      			  if (! array_key_exists("post_count",$value))
      			 $mongo_count_collection[$key]['post_count'] = 0;
      		  }
          // dd($mongo_count_collection,$mysql_count_collection);s
            $all_monitor=[];
			      foreach ($mongo_count_collection as $key => $value) 
			      {
        				$all_monitor[$key]['page_name'] = $value['page_name'];
        				$all_monitor[$key]['mongo_comment_count'] = $value['comment_count'];
        				$all_monitor[$key]['mongo_post_count'] = $value['post_count'];
					      foreach($mysql_count_collection as $data)
					      {
					  	     if($value['page_name'] == $data['page_name'])
      						    {
      						        $all_monitor[$key]['mysql_comment_count'] =$data['comment_count'];
      			              $all_monitor[$key]['mysql_post_count'] =$data['post_count'];
        						      // if (in_array($data['page_name'], $value))
        						      // $all_monitor[$key]['mysql_comment_count'] =$data['comment_count'];
        			            //$all_monitor[$key]['mysql_post_count'] =$data['post_count'];
      						    }
						    }
		      	}
              // dd($all_monitor);

              // foreach ($monitor_post_result as $value) {
              //   # code...
              //   $all_monitor []= $value;
              // }
              // // dd($all_monitor);
              //  $all_monitor =[];
              //  // $all_mention[2]['mycount'] =0;
              //  foreach($monitor_post_result as $key => $value)
              //  {
              //   $all_monitor[$key]['page_name'] =$value['page_name'];
              //   $all_monitor[$key]['mongopostcount'] =$value['mongo_post_count'];
              //   foreach($my_monitor_postdata as $data)
              //   {
              //     // if($value['page_name'] == $data['page_name'])
              //     // {
              //       if (in_array($data['page_name'], $value))

              //       $all_monitor[$key]['mypostcount'] =$data['count'];

              //     // }

              //   }
              //  }
              //  // dd($all_mention);
              //  foreach ($all_monitor as $key => $value) {
              //    # code...
              //   if (! array_key_exists("mypostcount",$value))

              //       $all_monitor[$key]['mypostcount'] = 0;

              //  }
              //  // dd($all_monitor);
                  return $all_monitor;
    }
    public function get_history()
    {
      // dd($fday);
      $fday = Input::get('fday');
      $sday = Input::get('sday');
      // return $fday;
      if(null !== $fday)
      {
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',$fday)));
      }
      else
      {
        $dateBegin=date('Y-m-d');
      }
      if(null !==$sday)
      {
        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',$sday)));
      }
      else
      {
        $dateEnd=date('Y-m-d');
      }
      $projects = DB::table('projects')->select('*')->where('id',34)->get();
      foreach($projects as $project)
      {
        $project_id = $project->id;
        // $data = DB::table('predict_'.$project_id.'_logs')->select('*')->get();
        $query = "Select manual_by,manual_date,checked_sentiment,decided_keyword,checked_tags,isHide from predict_logs where (timestamp(manual_date) BETWEEN '".$dateBegin."' AND '".$dateEnd."') order by timestamp(manual_date) desc";
        // dd($logs);
        // dd($query);
        $data = DB::select($query);
      }
        return Datatables::of($data)
        ->make(true);
    }

    public function history()
    {
      return view('BSL/history');
    }

  }

   