@extends('layouts')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form role="form" class="" action="#" method="post" id="myform">
                     {{csrf_field()}}
                         <div class="row">
                          	<div class="col-md-12">
                       			 <div class="card">
                           			 <div class="card-body"  style="height:60px">
                               			 <div class="row"  style="margin-top:-10px">
                                          <div class="col-sm-4 col-4">
                                             <div class="form-group">
                                                <select class="form-control custom-select" id="project_filter">
                                                  @if (isset($projects))
                                                  @foreach($projects as $index =>$projects)
                                                  @if($index == 0 )
                                                  <option value="{{$projects->id}}"  selected>{{$projects->name}}</option>
                                                  @else
                                                   <option value="{{$projects->id}}" >{{$projects->name}}</option>
                                                  @endif
                                                  @endforeach
                                                  @endif
                                               </select>
                                             </div>
                                         </div>
                 										    <div class="col-sm-4 col-4">
                   											 	<div class="d-flex input-group mb-3">
                     												<div class="d-flex input-group mb-3">
                          											<input type='text' id="calender" class="form-control dateranges" style="color: #67757c !important;" />
                         													  <div class="input-group-append">
                	           													<span class="input-group-text">
                	            												 	<span class="ti-calendar"></span>
                	            												</span>
                          													</div>
                     												</div>
                    											</div>
                 											  </div>
                                         <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="radio" class="check" id="square-radio-1" name="square-radio" data-radio="iradio_square-red" value="Monitor"  checked>
                                                    <label for="square-radio-1">Monitor</label>
                                                    <input type="radio" class="check" id="square-radio-2" name="square-radio" data-radio="iradio_square-red" value="Mention">
                                                    <label for="square-radio-2" >Mention</label>
                                                </div>
                                            </div>
                                          </div>
                                		</div>
                           			 </div>
                       			 </div>
                  			</div>
               			 </div>
                      <div class="table-responsive m-t-40">
                        <table id="tracking_table" class="table">
                          <thead>
                            <!-- <tr><th>Page name</th><th>Count</th></tr> -->
                            <!-- <tr>

                              <th >Rendering engine</th>
                              <th>Setting1</th>
                            </tr> -->
                        </thead>
                		   </table>
                     </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>

     @endsection
     @push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}"></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>

    <script src="{{asset('assets/plugins/icheck/icheck.min.js')}}" defer></script>
    <!-- <script src="{{asset('assets/plugins/icheck/icheck.init.js')}}" defer></script> -->
  <script type="text/javascript">
    var startDate;
    var endDate;
    $(document).ready(function() {


    $('.dateranges').daterangepicker({
      locale: {
        format: 'MMM D, YYYY'
      },
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: startDate,
      endDate: endDate,
      // alert(startDate);
        },function(start, end) {//alert("hihi")

        var startDate;
        var endDate;
// alert(start);
        startDate = start;
        endDate = end;
         var selected_prj = document.getElementById("project_filter");
         var project_id = $(selected_prj).val();
         // alert (project_id);
        ChooseDate(startDate,endDate,project_id);
      });
        startDate = moment();
        endDate = moment();
// alert(startDate);
         var selected_prj = document.getElementById("project_filter");
         var project_id = $(selected_prj).val();
        ChooseDate(startDate,endDate,project_id);

          $( "#project_filter" ).change(function()   {

               var selected_prj = document.getElementById("project_filter");
               // var prj_name = selected_prj.options[selected_prj.selectedIndex].text;
               var project_id = $(this).val();
               ChooseDate(startDate,endDate,project_id);

  


    });


    function ChooseDate(start,end,project_id)
   {
     // alert(start);
     $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY')); startDate=start;
     endDate=end;
// alert(startDate);
       var start = new Date(startDate);
       // alert(start);
       var end = new Date(endDate);
       var timeDiff = Math.abs(end.getTime() - start.getTime());
       // alert(timeDiff);
       var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
       // alert(diffDays);


          if(parseInt(diffDays) < 7) //in terms of day
          { 
            //alert("7");
            $("#day").show();
            $("#week").hide();
            $("#month").hide();
          }

          
         else if (parseInt(diffDays) == 7)//in terms of week
         {

           //alert("==7");
           $("#day").show();
           $("#week").show();
           $("#month").hide();


         }
          else if (parseInt(diffDays) > 7)//in terms of month
          {
            //alert(">7");

            $("#day").show();
            $("#week").show();
            $("#month").show();

          }
          if (document.getElementById('square-radio-1').checked) {
        var radio = document.getElementById('square-radio-1').value;
      }
      else {var radio = document.getElementById('square-radio-2').value;}
          Create_DataTable(startDate.format('YYYY MM DD '),endDate.format('YYYY MM DD '),project_id,radio);
          

        }


        function Create_DataTable(fday,sday,project_id,radio)
        {
            // alert(radio);
                    
                  // alert(radio);
             //        if(radio == 'Monitor'){
             //          // alert(radio);
             //           var monitor_track = $('#tracking_table').DataTable({
             //       "lengthChange": true,"info": false, "searching": true,
             //        processing: true,
             //        serverSide: false,
             //        "bDestroy": true,
             //         "headers": {
             //          'X-CSRF-TOKEN': '{{csrf_token()}}' 
             //        },
             
             //        "ajax": {
             //          "url": '{{ route('getTrackingData') }}',

             
             //         "data": {
             //            "fday": fday,
             //            "sday": sday,
             //            "project_id":project_id,
             //          }
             // },
             //       "initComplete": function( settings, json ) {
             //         console.log(json);
             //          // var data=JSON.parse(response);
                   
             //        },

             //         columns: [
             //            {data: 'page_name', name: 'page_name',title:'Monitor Page',orderable: false,searchable: true},
             //            {data: 'mongopostcount', name: 'mongopostcount', title:'<p style="text-align:center">Mongo  <br>Post Count</p>',orderable: false, searchable: true},
             //             {data: 'mypostcount', name: 'mypostcount', title:'<p style="text-align:center">MYSQL  <br>Post Count</p>',orderable: false, searchable: true},
             //              {data: 'mongocommentcount', name: 'mongocommentcount', title:'<p style="text-align:center">Mongo  <br>Comment Count</p>',orderable: false, searchable: true},
             //               {data: 'mycommentcount', name: 'mycommentcount', title:'<p style="text-align:center">MYSQL  <br>Comment Count</p>',orderable: false, searchable: true}
             //        ],
             //         columnDefs: [
             //            {
             //              "targets": [1,2,3,4], // your case first column
             //              "className": "text-center",
                          
             //            }
             //        ]
                    
             //    })
             //         }

              if(radio !== 'Monitor')
              {
                  // alert(radio);
                  $('#tracking_table').DataTable({
                     "lengthChange": true,"info": false, "searching": true,
                      processing: true,
                      serverSide: false,
                      "bDestroy": true,
                       "headers": {
                        'X-CSRF-TOKEN': '{{csrf_token()}}' 
                      },
                      
                      "ajax": {
                        "url": '{{ route('getMentionTrackingData') }}',

               
                       "data": {
                          "fday": fday,
                          "sday": sday,
                          "project_id":project_id,

                        }
                      }, 
                     "initComplete": function( settings, json ) {
                       // console.log(json);
                        // var data=JSON.parse(response);
                     
                      },

                      columns: [
                          {data: 'page_name', name: 'page_name',title:'Mention Page',orderable: false,searchable: true},
                          {data: 'mongopostcount', name: 'mongopostcount', title:'<p style="text-align:center">Mongo  <br>Post Count</p>',orderable: false, searchable: true},
                           {data: 'mypostcount', name: 'mypostcount', title:'<p style="text-align:center">MYSQL  <br>Post Count</p>',orderable: false, searchable: true}
                      ],
                      columnDefs: [
                          {
                            "targets": [1,2], // your case first column
                            "className": "text-center",
                            
                          }
                      ]
                  })
              }
        } /*endoffunction*/


         $('input[type="radio"]').change(function() {
          // alert('radio changed');
                 var rd_flag = $(this).val();
                 // alert(rd_flag);
                 ChooseDate(startDate,endDate,project_id);
               
              });
              //   if (document.getElementById('square-radio-1').checked) {
              //   var radio = document.getElementById('square-radio-1').value;
              // }
              // else var radio = document.getElementById('square-radio-2').value;
              // alert(radio);
            });
   </script>
   <link href="{{asset('css/own.css')}}" rel="stylesheet">
   <link href="{{asset('assets/plugins/icheck/skins/all.css')}}" rel="stylesheet">
  @endpush
