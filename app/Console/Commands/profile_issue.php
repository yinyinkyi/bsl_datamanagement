<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use MongoDB;
use App\Profile;
use App\MongoProfile;
use MongoDB\BSON\ObjectID;
use App\MongoboundComment;
// add_profiles
class profile_issue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profile:issue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert profiles in temp_profiles table which are present in comment table but not in temp_profiles table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        foreach($projects as $project)
        {
            $id = $project->id;
            // $id = '17';
            $comment_table = "temp_".$id."_inbound_comments";
            $id_array = "SELECT * FROM temp_17_inbound_comments WHERE profile NOT IN ( SELECT temp_id FROM temp_profiles) AND profile <> '' ORDER BY timestamp(created_time) DESC";
            $id_array = DB::select($id_array);
 
            foreach($id_array as $result)
            { 
              $profile_id = $result->profile;
              if( $profile_id <> '') {
                  $profile_result=MongoProfile::raw(function ($collection) use($profile_id) {
                     return $collection->aggregate([
                            [
                            '$match' =>
                                 ['_id'=> new ObjectId($profile_id) ],
                            
                                       
                           ],
                            
                        ]);
                
                     })->toArray();

                      foreach($profile_result as $data)
                      {

                          $_id ='';$name='';$type='';$profile_id='';
               
                          if(isset($data['name'])) $name = $data['name']; 
                          if(isset($data['_id'])) $_id = $data['_id']; 
                          if(isset($data['type'])) $type = $data['type']; 
                          if(isset($data['id'])) 
                          {
                            $profile_id = $data['id']; 
                          }
                          else if (isset($data['href'])) 
                          {
                            $profile_id = $data['href']; 
                          }

                         
                          // $profile_data = Profile::firstOrCreate(['temp_id' => $_id , 'id' => $profile_id ,'name' => $name , 'type' => $type]);
                          $query = "select * from temp_profiles WHERE id='".$profile_id."'";
                          $data = DB::connection('mysql_cron_wunzin')->select($query);
                          if(!isset($data))
                          {
                            $row = "insert into temp_profiles('temp_id','id','name','type') values($_id,$profile_id,$name,$type)";
                            $new_data = DB::connection('mysql_cron_wunzin')->select($row);
                          }
            
                      }
                  }
               }
             }
                       $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                       $date_time = $date->format('dmYHis');
                       $this->info('Profile inserted  Successfully.'. $date_time);
      
    }
}
