<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    //
      protected $table = 'tags';
  protected $dates = ['created_at','updated_at'];
    protected $fillable = [
        'id','name','keywords','brand_id','exclude','fromApi','created_at','updated_at','manual_tag'
    ];
}