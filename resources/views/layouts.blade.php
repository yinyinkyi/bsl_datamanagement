<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon3.png')}}">
    <title>{{ config('app.name', 'Laravel') }} - @if (isset($title)) {{$title}} @endif</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
     <link href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
     <!-- range slider -->
     <link href="{{asset('assets/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/ion-rangeslider/css/ion.rangeSlider.skinModern.css')}}" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="{{asset('assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="{{asset('assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{asset('assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/css-chart/css-chart.css')}}" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="{{asset('assets/plugins/morrisjs/morris.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('css/colors/green.css')}}" id="theme" rel="stylesheet">
    <link href="{{asset('assets/plugins/sweetalert2/sweetalert2.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=myanmar3' />
    <link href="{{asset('assets/plugins/select2/dist/css/select2.min.css')}}" id="theme" rel="stylesheet">
     <link href="{{asset('assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />

      <link href="{{asset('assets/plugins/dropify/dist/css/dropify.min.css')}}" rel="stylesheet">
         
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
  .table td, .table th {
      padding: .35rem;
      height: 60px;
       vertical-align: middle;
      border-top: 1px solid #dee2e6;
  }
.btn-gray, .btn-gray.disabled {
    background: #7e7979;
    border: 1px solid #6a6868;
    color: #ffffff;
    -webkit-transition: 0.2s ease-in;
    -o-transition: 0.2s ease-in;
    transition: 0.2s ease-in;
.user-profile .profile-text{
    padding-top: 31px;
    position: relative; }

     .user-profile .profile-img::before {
      -webkit-animation: 2.5s blow 0s linear infinite;
      animation: 0s  0s  ;
      position: absolute;
      content: '';
      width: 50px;
      height: 50px;
      top: 35px;
      margin: 0 auto;
      border-radius: 50%;
      z-index: 0;background:##1e88e4; }
      .user-profile .profile-text > a:after {
        position: absolute;
        right: 20px;
        top: 45px; }


       /*     .user-profile .profile-img img {
      width: 100%;
      border-radius: 100%; }*/
  /*.user-profile .profile-text {
    padding: 5px 0px;
    position: relative; }*/
    .user-profile .profile-text > a {
      color: #ffffff !important;
      width: 100%;
      padding: 6px 30px;
      /*background: #4267b2;*/
      background:#1e88e4;
      display: block; }
   /*   .user-profile .profile-text > a:after {
        position: absolute;
        right: 20px;
        top: 20px; }*/
 /* .user-profile .dropdown-menu {
    left: 0px;
    right: 0px;
    width: 180px;
    margin: 0 auto; }*/

button.btn.btn-info.dropdown-toggle.dropdown-toggle-split::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid;
    border-right: .3em solid transparent;
    border-bottom: 0;
    border-left: .3em solid transparent;
}
       /* .dropdown-menu {
    position: absolute;
    top: 100%;
    left: 0;
     z-index: 1000; 
    display: none;
    float: left;
    min-width: 10rem;
    padding: .5rem 0;
    margin: .125rem 0 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #383f48;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
}*/


/*    .label-light-success {
    background-color: #e8fdeb;
    color: #fcb22e;
}*/

.sidebar-nav > ul > li.active > a, .sidebar-nav > ul > li.active:hover > a {
    color: #ffffff !important;
    background: #1e88e4 !important;


    .topbar .navbar-light .navbar-nav .nav-item > a.nav-link {
    color: #FFFFFF !important;

.text-muted {
    color: #fff !important;
}


input[type="text"].col-sm-2::-webkit-input-placeholder {
  opacity: 0.5; 
}
/*.sidebar-nav {
    background: #fff;
}*/
/*style.css 5404*/
.scroll-sidebar {
    padding-bottom: 60px;
    background: #fff;
}




</style>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
  <div id="app">
    <div id="main-wrapper">
      
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" style="background:#1e88e4;">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" style="background-color:#1c81da">
                    <a class="navbar-brand" href="">
                                            <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="{{asset('assets/images/favicon.png')}}" alt="homepage" class="dark-logo" /> -->
                            <!-- Light Logo icon -->
                            <img src="{{asset('assets/images/logo-icon4.png')}}" style ="width:50px;height:50px" alt="homepage" class="light-logo" />
                        </b>
                        <!-- Logo text -->
                        <span style="color:#FFFFFF;font-size: 18px;" >
                         <!-- dark Logo text -->
                        <!--  <img src="{{asset('assets/images/logo-text.png')}}" alt="homepage" class="dark-logo" /> -->
                         <!-- Light Logo text -->    
                         <!-- <img src="" class="light-logo" alt="" />Bagan Social Listener</span> </a> -->
                         <img src="{{asset('assets/images/BAGAN_SOCIAL_LISTENER_180_19.png')}}" class="light-logo" style="width:180px" alt="" /></span> </a>
                </div>

                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">


                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                     <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>

                   
                        <li class="nav-item">
                            <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="" onclick="reload()"><img src="{{asset('assets/images/notification.png')}}" alt="user" class="profile-pic" />
                              <input type="hidden" id="previous_count" value="0">
                                                     <div class="notify">
                                                        <!-- <span  id="parent"></span> -->
                                                        <!-- <span class="point" style=""></span> -->
                                                         <!-- <span class="numberPoint" id= "count" style="color:#fff;font-size:12px;font-weight: bold;"></span> -->
                                                          <span class="numberPoint" id= "count"></span>
                                                
                          </div>
                            </a>

                          
                        </li>
                    </ul>
  
                   
                       
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                         @yield('comparison')
  
                   


            <!--     <tag v-bind:tags="tags"></tag> -->
                        <li class="nav-item dropdown">
           <!--     <a href="{{ route('logout') }}" class="nav-link dropdown-toggle text-muted waves-effect waves-dark link" data-toggle="tooltip" title="Logout" style="color:#30373d !important" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a> -->
                <div class="button-group">
                                  <button type="button" class="btn btn-info" style="background:#FFFFFF;" >
                                    <a href="{{ route('logout') }}" style="color:#67757c" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" ><i class="mdi mdi-power" ></i>Logout</a></button>
                                                    <!--  <span class="" style="font-size:;color:#fff;">3    </span> --> 
                                                 

                </div>
                <!-- mdi-bell-outline -->

                               <!-- <div class="button-group">
<button type="button" class="btn btn-info"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="mdi mdi-power" ></i>Logout</a></button>
                </div> -->
                       
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{asset('assets/images/users/1.jpg')}}" alt="user"></div>
                                            <div class="u-text">
                                                <h4>{{ Auth::user()->name }}</h4>
                                                <p class="text-muted">{{ Auth::user()->email }}</p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                                   </li>
                                </ul>
                            </div>
                    </li>
                
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <!-- <div class="user-profile" style="background: url({{asset('assets/images/background/user_info_bg_silver.png')}}) no-repeat;"> -->
                    <div class="user-profile" style="background: url({{asset('assets/images/background/user_info_bg_silver80.png')}}) no-repeat;">

                    <!-- User profile image -->
                    <div class="profile-img"> <img src="" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text" > 
                      <a href="#" class="" role="button" id="prj"></a>
               <!--          <div class="dropdown-menu animated flipInY">
                            <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                            <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                            <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                            <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                            <div class="dropdown-divider"></div> <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a>
                        </div> -->
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->

                 
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
            <!--               <li  class="nav-devider"></li>
                        <li class="@if (Request::is('need-to-check/*') || Request::is('confirmed/*')) {{'active'}}  @endif"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i>Monitor Pages</a>
                            <ul aria-expanded="false"  class="collapse">
                               
                                  <li class="nav-item {{ str_is('need-to-check*', Request::route()->getName()) ? 'active' : '' }} nav-small-cap" ><a class=" {{ str_is('need-to-check*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('need-to-check')}}"><i class="mdi mdi-alert" style="color:red;"></i> Action Require </a></li>
                                  
                                  <li class="nav-item {{ str_is('confirmed*', Request::route()->getName()) ? 'active' : '' }} nav-small-cap"><a class=" {{ str_is('confirmed*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('confirmed')}}"><i class="mdi mdi-account-check" style="color:green;"></i> Action Taken</a></li>
                            </ul>
                        </li>

                        <li  class="nav-devider"></li>
                  
                        <li class="@if (Request::is('require/*') || Request::is('taken/*')) {{'active'}}  @endif"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i>Mention Pages</a>
                            <ul aria-expanded="false"  class="collapse">
                               
                                  <li class="nav-item {{ str_is('require*', Request::route()->getName()) ? 'active' : '' }} nav-small-cap" ><a class=" {{ str_is('require*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('require')}}"><i class="mdi mdi-alert" style="color:red;"></i> Action Require </a></li>
                                  
                                  <li class="nav-item {{ str_is('taken*', Request::route()->getName()) ? 'active' : '' }} nav-small-cap" ><a class=" {{ str_is('taken*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('taken')}}"><i class="mdi mdi-account-check" style="color:green;"></i> Action Taken</a></li>
                            </ul>
                        </li> -->
                    <!-- temporary menu -->

                    <li class="nav-small-cap">Data</li>
                   <li class="nav-item {{ str_is('need-to-check*', Request::route()->getName()) ? 'active' : '' }}"><a class=" {{ str_is('need-to-check*', Request::route()->getName()) ? 'active' : '' }}" href="{{ route('need-to-check') }}" ><i class="mdi mdi-view-parallel"></i>Pages</a></li>
                   <li class="nav-item {{ str_is('require*', Request::route()->getName()) ? 'active' : '' }}"><a href="{{ route('require') }}" class="{{ str_is('require*', Request::route()->getName()) ? 'active' : '' }}"><i class="mdi mdi-view-module"></i>Mentions</a></li>
                   <li class="{{ str_is('track*', Request::route()->getName()) ? 'active' : '' }}"><a href="{{route('datatrack')}}"><i class="mdi mdi-calendar-clock"></i> Track Data </a></li>

                   <li class="nav-devider"></li>
                   <li class="nav-small-cap">Checked Data</li>
                   <li class="nav-item {{ str_is('confirmed', Request::route()->getName()) ? 'active' : '' }}"><a class=" {{ str_is('confirmed*', Request::route()->getName()) ? 'active' : '' }}" href="{{ route('confirmed') }}" ><i class="mdi mdi-view-parallel"></i>Completed Pages</a></li>
                   <li class="nav-item {{ str_is('taken', Request::route()->getName()) ? 'active' : '' }}"><a href="{{ route('taken') }}" class="{{ str_is('taken*', Request::route()->getName()) ? 'active' : '' }}"><i class="mdi mdi-view-module"></i>Complete Mentions</a></li>
                   @if(auth()->user()->role == 'Admin')
                   <li><a href="{{ route('export') }}" ><i class="mdi mdi-export"></i>Data Export</a></li>
                   <li><a href="{{ route('history') }}" ><i class="mdi mdi-history"></i>Posts By Operator <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<font color="red">on progress</font>)</a></li>
                   @endif
                   <li class="nav-devider"></li>
                   <li class="nav-small-cap">Setting</li>
                   <li class="{{ str_is('tags*', Request::route()->getName()) ? 'active' : '' }}"><a href="{{route('tags.index')}}"><i class="mdi mdi-tag"></i> Tags Entry </a></li>
                  <li class="{{ str_is('page_filter*', Request::route()->getName()) ? 'active' : '' }}"><a href="{{route('page_filter')}}"><i class="mdi mdi-tag"></i> Page Filter </a></li>
                    @if(auth()->user()->role == 'Admin')
                   <li class="{{ str_is('users*', Request::route()->getName()) ? 'active' : '' }}"><a href="{{route('register')}}"><i class="mdi mdi-account-plus"></i> User Registration </a></li>
                   <li class="{{ str_is('users*', Request::route()->getName()) ? 'active' : '' }}"><a href="{{route('projectRegister')}}"><i class="mdi mdi-account-plus"></i> Project Assignment </a></li>
                   @endif
               </ul>
            </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
         <!--    <div class="sidebar-footer">
               
                <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                
                <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mprevious_countdi-gmail"></i></a>
                
                <a href="{{ route('logout') }}" class="link" data-toggle="tooltip" title="Logout" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a>
            </div> -->
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                @yield('content')
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 Bagan Social Listener by Bagan Innovation Technology</footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
</div>
    <!-- ============================================================== -->
   <!-- defer -->

    
     <!-- <script src="{{asset('js/app.js')}}" defer></script> -->
     <!-- jQuery 2.2.3 -->
     <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('js/sidebarmenu.js')}}" ></script>
     
        <script type="text/javascript">
            $(document).ready(function() {
               var selected_prj = document.getElementById("project_filter");
               var prj_name = selected_prj.options[selected_prj.selectedIndex].text;
               document.getElementById("prj").innerHTML = prj_name;
              // alert(prj_name);
             // if ($('#count') > 0){
             //    $('#parent').addClass('heartbit');
             //            }else{
             //              $('#parent').addClass('');
             //           }
            
                  function load_unseen_notification(){
                    var previous_count = document.getElementById('previous_count').value;
                    // var count = document.getElementById('count').textContent;
                    // alert(previous_count);
                    var selected_page = document.getElementById("admin_page_filter");
                    var page_name = selected_page.options[selected_page.selectedIndex].text;
                       $.ajax({

                        url:"{{ route('fetch') }}",
                        method:"GET",
                        data:{previous_count:previous_count,page_name:page_name},
                        dataType:"json",
                        success:function(data){
                              console.log(data);
                              if(data[0] != '0'){
                                console.log("noti shown");
                                document.getElementById('count').innerHTML = data[0];
                                $('#count').css({"color":"#fff","font-size":"12px","font-weight":"bold","padding-left":"8px","padding-right":"8px","padding-top":"4px","padding-bottom":"4px","left":"9px","top":"26px","text-align":"center","background":"#fa3e3e","border-radius":"50px"});
                                 }  
                              else{
                                console.log("No new record");
                                document.getElementById('count').innerHTML = '';
                                }
                            
                                // document.getElementById('count').innerHTML = data[0];
                                // $('#count').css({"color":"#fff","font-size":"12px","font-weight":"bold"});
                                document.getElementById('previous_count').value = data[1];
                             }
                       });
                    }
                    
                    setInterval(function(){load_unseen_notification();;}, 30000);   /*call function every 30 seconds (1sec = 1000 milliseconds) */

                  function reload(){
                    location.reload();
                    // document.getElementById("count").innerHTML = "0";
                    }
            });
       </script>

     @stack('scripts')
        <style type="text/css">
  /*          .numberPoint {
	            
	            padding-left: 8px;
                padding-right: 8px;
                padding-top: 4px;
                padding-bottom: 4px;
	            left: 9px;
	            top: 26px;
	            text-align: center;
	            
	            background-color: #fa3e3e;
	            border-radius: 50%;
	            
            }*/
		    .notify
		        {
	        	position: absolute;
			    top: -5px;
			    right: 5px;
		        }
           .text-muted {
             color: #fff !important; }

      </style>
</body>

</html>

<!-- <script>

window.Laravel =<?php echo json_encode([
    'csrfToken' => csrf_token(),

]);?>

</script>
@if(!auth()->guest())
<script> 
window.Laravel.userId = <?php echo auth()->user()->id; ?>
</script>
@endif -->
