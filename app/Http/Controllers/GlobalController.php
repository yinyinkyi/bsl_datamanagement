<?php

namespace App\Http\Controllers;

use App\MongodbData;
use App\Mongodbpage;
use App\Comment;
use App\ProjectKeyword;
use App\Project;
use App\user_permission;
use App\demo;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
trait GlobalController
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function getPermission()
    {
          $login_user = auth()->user()->id;
          $permission_data = user_permission::select('*')->where('user_id', $login_user);
          $permission_data=$permission_data->orderBy('id','DESC')->first();
          return  $permission_data;
    }
    public function getProject()
    {
          $login_user = auth()->user()->id;
          $project_data = Project::select('*')->where('user_id', $login_user);
          $project_data=$project_data->orderBy('id','DESC')->get();
          return  $project_data;
    }
     public function getIsDemouser()
    {
          $login_user = auth()->user()->id;
          $demo_user= user_permission::select('*')->where('user_id', $login_user)->where('demo', 1);
          $demo_user=$demo_user->orderBy('id','DESC')->get();
          if(count($demo_user)>0)
          {
             return  true;
          }
          else
          {
              return  false;
          }
    }
    public function getInboundPostId($brand_id)
    {
        $id_query="select id from temp_inbound_posts";
        $id_result = DB::select($id_query);
        $id_array = array_column($id_result, 'id');
        return  $id_array;
    }

    public function getInboundPostId_Reaction($related_pages)
    {
        $filter_pages='';
        //$filter_pages=$this->getPageWhere($related_pages);
        $id_query="select id from temp_inbound_posts Where reaction_update=0 Order By timestamp(created_time) DESC Limit 100 ";
        $id_result = DB::connection('mysql_cron_wunzin')->select($id_query);
        $id_array = array_column($id_result, 'id');
        return  $id_array;
    }

    public function getDemoHideDiv($view_name)
    {
        $hidden_div = demo::select('*')->where('is_hide',1)->where('view_name',$view_name);
        $hidden_div=$hidden_div->orderBy('id','DESC')->get();
        $hidden_div_name='';
        foreach ($hidden_div as $key => $value) {
          if($key === 0 )
            $hidden_div_name .=$value['div_name'];
          else
            $hidden_div_name .="," . $value['div_name'];
        }
        return  $hidden_div_name;
    }
    public function getInboundPages($brand_id)
    {
      //$page_query="select DISTINCT page_name from temp_".$brand_id."_inbound_posts";
     // $page_result = DB::select($page_query);
      $page_result=[];
      $project_data_id = $this->getProjectByid($brand_id);
      // dd($project_data_id);
       if(count($project_data_id)>0)
        {
            $monitorpage=$project_data_id[0]['monitor_pages'];
            $page_result=explode(',', $monitorpage);
        }
       $pages='';
       foreach ($page_result as  $key => $row) 
       {
          if ($key == 0) 
            {           
              $pages  =  "'" . $row . "'";
            }
          else
          {
              $pages  = $pages .",'". $row. "'";
          }
       }
       // dd($pages);
       return $pages;
    }
    public function getExcludePages($brand_id)
    {
      //$page_query="select DISTINCT page_name from temp_".$brand_id."_inbound_posts";
     // $page_result = DB::select($page_query);
      $page_result=[];
      $project_data_id = $this->getProjectByid($brand_id);
       if(count($project_data_id)>0)
        {
            $excludepage=$project_data_id[0]['exclude_pages'];
            $page_result=explode(',', $excludepage);
        }
       $pages='';
       foreach ($page_result as  $key => $row) 
       {
          if ($key == 0) 
            {           
              $pages  =  "'" . $row . "'";
            }
          else
          {
              $pages  = $pages .",'". $row. "'";
          }
       }
       // dd($pages);
       return $pages;
    }
    public function doCSV($path, $array)
    {
        $fp = fopen($path, 'w');
        $i = 0;
        foreach ($array as $fields) 
        {
            if($i == 0)
            {
                fputcsv($fp, array_keys($fields));
            }
            fputcsv($fp, array_values($fields));
            $i++;
        }

        fclose($fp);
    }
    public function getFilterInboundPostId($related_pages)
    {
        $filter_pages='';
        $filter_pages=$this->getPageWhere($related_pages);
        $id_query="select id from temp_inbound_posts Where 1=1 " .$filter_pages;
        $id_result = DB::connection('mysql_cron_wunzin')->select($id_query);
        $id_array = array_column($id_result, 'id');
        return  $id_array;
    }

    public function gettags($project_id)
    {
      $query="select name,keywords,id from tags where brand_id=".$project_id;
      $result = DB::select($query);
      // dd($query);
      return $result;
    }
    public function getProjectByid($id)
    {
      $project_data = Project::select('*')->where('id', $id)->get();
      return  $project_data;
    }
    public function getProjectByExcludeid($id)
    {
      $login_user = auth()->user()->id;
      $project_data = Project::select('*')->where('user_id', $login_user);
      $project_data= $project_data->where('id','<>',$id);
      $project_data=$project_data->orderBy('id','DESC')->get();
      return  $project_data;
    }

    public function getProjectCount($project_data)
    {
     $count = $project_data->count();
     return $count;
    }

    
    public function getprojectkeyword($brand_id)
    {
     $keyword_data = ProjectKeyword::select('main_keyword','require_keyword','exclude_keyword')->where('project_id','=',$brand_id)->get()->toArray();
     return $keyword_data;
    }

    public function getpagename($brand_id)
    {
      $page_list = Project::select('monitor_pages')->where('id','=',$brand_id)->get()->toArray();
      return $page_list;
    }
    public function fontCheck($message)
    {
      $firstCharacter = substr(strtolower($message), 0, 8);
      if (strpos($firstCharacter, 'zawgyi') !== false) 
      {   
        $split = explode("unicode",strtolower($message));
        $z_msg = $split[0];
        
        $last_space_position = strrpos($z_msg, ' ');
        $message = substr($z_msg, 0, $last_space_position);
        $message = str_replace("zawgyi", "", strtolower($message));
        $first2Characters = substr(strtolower($message), 0, 2);
        $message = trim($message,$firstCharacter);
      }
      else
      {
        $split = explode("zawgyi",strtolower($message));
        $z_msg = $split[1];
        $firstCharacter = substr(strtolower($z_msg), 0, 1);
        $message = trim($z_msg,$firstCharacter);
        
      }
     return $message;
    }
    public function Format_Page_Name($array)
    {
       $new_array=[];
       foreach ($array as  $key => $row)
       {
          $arr_page_name=explode("-",$row);
          if(count($arr_page_name)>0)
          {
              $last_index=$arr_page_name[count($arr_page_name)-1];
              if(is_numeric($last_index))
              $new_array[] = $last_index;
              else
              $new_array[] = $row;
          }
          else
          {
              $new_array[]=$row;
          }
       }
      return  $new_array;
    }
   public function get_lastUpdatedDate($tbl)
   {
      $query = "SELECT lastUpdated from tbl_logs where table_name='".$tbl."'";
      $result = DB::connection('mysql_cron_wunzin')->select($query);
      foreach($result as $log) $last_updated_date =  $log->lastUpdated;
      $last_updated_date=date('Y-m-d', strtotime($last_updated_date));
      $last_updated_date = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$last_updated_date))* 1000);
      return $last_updated_date;
   }
   public function getMentionPostId()
   {
      $id_query="select id from temp_posts";
      $id_result = DB::select($id_query);
      $id_array = array_column($id_result, 'id');
      return  $id_array;
    }
    public function getPageWhere($related_pages)
    {
        $pages='';
        $filter_pages = '';
        foreach ($related_pages as $key => $value) 
        {
            if((int)$key === 0)
                $pages ="'".$value."'";
            else
                $pages .=",'".$value."'";
            $filter_pages="  AND page_name in (".$pages.")";
        }
        return  $filter_pages;
    }
    public function get_table_id($dateBegin,$dateEnd,$table,$require_var,$related_pages=[])
    {   
        $dateBegin = date('Y-m-d',strtotime($dateBegin));
        $dateEnd = date('Y-m-d',strtotime($dateEnd));
        $filter_pages=$this->getPageWhere($related_pages);
        $query = "SELECT ".$require_var." from temp_".$table ."  Where 1=1 ".
                    " AND (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $filter_pages;
        $id_result = DB::connection('mysql_cron_wunzin')->select($query);
        $id_array = array_column($id_result, $require_var);
        return $id_array;
    }
    public function get_table_id_mention($table,$require_var,$related_pages=[])
    {
      $filter_pages=$this->getPageWhere($related_pages);
      $query = "SELECT ".$require_var." from temp_".$table ."  Where 1=1 " . $filter_pages;
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }
    public function get_common_table_id($table,$require_var)
    {
      $id_result=[];
      $query = "SELECT Distinct ".$require_var." from ".$table ."  Where 1=1 ";
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      foreach ($id_array as $key => $value) 
      {
          if(null !== $value)
              $id_result[]=new MongoDB\BSON\ObjectId((string)$value);
      }
      return $id_result;
    }
    public function get_table_page($project_id,$table,$require_var)
    {
      $query = "SELECT ".$require_var." from temp_".$project_id."_".$table ;
      $id_result = DB::connection('mysql_cron_wunzin')->select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }
    public function getPageWhereGen($related_pages)
    {
      $pages='';
      $filter_pages = '';
      $related_pages = $this->Format_Page_Name($related_pages);
      foreach ($related_pages as $key => $value) 
      {
        if((int)$key === 0)
          $pages ="'".$value."'";
        else
          $pages .=",'".$value."'";
        $filter_pages="  posts.page_name in (".$pages.")";
      }
      return  $filter_pages;
    }
    public function getOwnPage($brand_id)
   {
     $keyword_data = Project::select('own_pages')->where('id','=',$brand_id)->get()->toArray();
     return $keyword_data[0]['own_pages'];
   }
   public function getComparisonPage($brand_id)
  {  
     $keyword_data = Project::select('monitor_pages')->where('id','=',$brand_id)->get()->toArray();
     return $keyword_data[0]['monitor_pages'];
  }
  
 
  public function getpagefilter($page_data)
  {
    $conditional=[];
    $conditional_require_or=[];
    foreach($page_data as $i =>$element)
                              {

                              $require_keyword_filter[] = [ 'page_name' =>  $element];
                              $conditional_require_or['$or']=$require_keyword_filter;
                              }
   $conditional[] =$conditional_require_or;
   return  $conditional;
  }
   public function getcampaignkeyword($brand_id)
    {
      
      $query="select campaign_keyword from campaignGroup cg inner join ".
             " campaignGroupList cgl on cg.id=cgl.campaignGroup_id " .
             " where brand_id=" .$brand_id . " order by campaign_keyword" ;

       $result = DB::select($query);
    
       return $result;
    }
    public function getCampaignfilter($brand_id)//if not mention sync $brand_id is not need.
  {
     
     $campaign_keyword=[];
     $campaign_keyword_or=[];
     $conditional_keyword=[];
     if($brand_id <>'')
     {
      $campaign_keyword = $this->getcampaignkeyword($brand_id);
     }
     foreach ($campaign_keyword as $key => $value) {
      // Keyword 'OR' Condition
                 $keyword_filter=[];
                 if(!empty($value))
                {
                  $keyword_filter= [ 'message' => new MongoDB\BSON\Regex(".*". $value->campaign_keyword,'i'  )];
                  $conditional_keyword[]=$keyword_filter;
                 
                }
                         
     }

    
              //Combine Project Keyword and Campaign Keyword
              if(count($conditional_keyword)>0)
              {
                $campaign_keyword_or ['$or']=$conditional_keyword;
               

              }
             
                return  $campaign_keyword_or;
              
              

  }
  public function getkeywordfilter($keyword_data,$brand_id)//if not mention sync $brand_id is not need.
  {
     $conditional=[];
     $conditional_Campaign=[];
      if($brand_id <> '')
      {
         $conditional_Campaign=$this->getCampaignfilter($brand_id);
      }
   

     foreach ($keyword_data as  $key => $row) {
                      
     $main_keyword_filter =[ 'message' => new MongoDB\BSON\Regex(".*" . $row['main_keyword'],'i' )];
      
                        //Require Keyword 'OR' Condition
                         $require_keyword = explode(",", $row['require_keyword']);
                          $conditional_require_or=[];
                         $require_keyword_filter=[];
                            foreach($require_keyword as $i =>$element)
                            {
                               if(!empty($element))
                                {
                                 $require_keyword_filter[] = [ 'message' => new MongoDB\BSON\Regex(".*". $element,'i'  )];
                            $conditional_require_or['$or']=$require_keyword_filter;
                              }
                           
                            }
                         
                          //Require Keyword 'AND' Condition
                          $exclude_keyword = explode(",", $row['exclude_keyword']);
                          $conditional_exclude_or=[];
                          $exclude_keyword_filter=[];
                        
                            foreach($exclude_keyword as $i =>$element)
                            {
                                if(!empty($element))
                                {
                             
                            $exclude_keyword_filter[] = [ 'message' => ['$not'=>new MongoDB\BSON\Regex(".*". $element,'i'  )]];
                            $conditional_exclude_or['$and']=$exclude_keyword_filter;
                                }
                            }
                      
                            //Create 'AND' condition between main,require and exclude keyword
                             $conditional_and['$and'] =[$main_keyword_filter];

                            if(!empty($conditional_require_or))
                            {
                                $conditional_and['$and']=[$conditional_and,$conditional_require_or];
                            }
                           
                                               
                            if(!empty($conditional_exclude_or))
                            {
                               
                               $conditional_and['$and']=[$conditional_and,$conditional_exclude_or];
                            }
                           
                          //   $conditional_and['$and'] =[$main_keyword_filter,$conditional_require_or,$conditional_exclude_or];

                        
                        $conditional[] =$conditional_and;
                       
              }

            
                if(count($conditional_Campaign)>0)
                {
                  $conditional[] =$conditional_Campaign;
                }
                return  $conditional;
              
              

  }

   public function createTable($table_name, $fields = [])
   {
        // check if table is not already exists
        if (!Schema::hasTable($table_name)) {
            Schema::create($table_name, function (Blueprint $table) use ($fields, $table_name) {
                $table->increments('temp_id');
                if (count($fields) > 0) {
                    foreach ($fields as $field) {
                      if(isset($field['index']))
                      {
                        if(isset($field['length']))
                        $table->{$field['type']}($field['name'],$field['length'])->nullable()->index();
                       else
                        $table->{$field['type']}($field['name'])->nullable()->index();
                      }
                      else
                      {
                         $table->{$field['type']}($field['name'])->nullable();
                      }
                    }
                }
                $table->timestamps();
            });
            return response()->json(['message' => 'Given table has been successfully created!'], 200);
        }
 
            return response()->json(['message' => 'Given table is already existis.'], 400);
   }
    public function new_table_add($brand_id)
    {
       // set dynamic table name according to your requirements
 
        $table_inbound_posts = 'temp_inbound_posts';
        $table_inbound_comments = 'temp_inbound_comments';
     
        $fields_inbound_posts = [
            ['name' => 'id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'full_picture', 'type' => 'text'],
            ['name' => 'link', 'type' => 'text'],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'type', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'shared', 'type' => 'text'],
            ['name' => 'Liked', 'type' => 'text'],
            ['name' => 'Love', 'type' => 'text'],
            ['name' => 'Haha', 'type' => 'text'],
            ['name' => 'Wow', 'type' => 'text'],
            ['name' => 'Sad', 'type' => 'text'],
            ['name' => 'Angry', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'change_predict','type'=>'boolean'],
            ['name' => 'isBookMark', 'type' => 'boolean']

        ];


       $fields_inbound_comments = [
            ['name' => 'id', 'type' => 'text'],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'post_id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'comment_count', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'interest', 'type' => 'boolean'],
            ['name' => 'tags', 'type' => 'text'],
            ['name' => 'change_predict', 'type' => 'boolean'],
            ['name' => 'parent', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'isBookMark', 'type' => 'boolean']
        ];
         //  return; //need to remove if you want to add this new table to brand
 
       $this->createTable($table_inbound_posts, $fields_inbound_posts);
       return $this->createTable($table_inbound_comments, $fields_inbound_comments);

    }

      public function operate($brand_id)
    {
        // set dynamic table name according to your requirements
 
        $table_posts = 'temp_posts';
        $table_comments = 'temp_comments';
        $table_pages = 'temp_pages';
        $table_followers = 'temp_followers';
        $table_inbound_posts = 'temp_inbound_posts';
        $table_inbound_comments = 'temp_inbound_comments';
      
 
        // set your dynamic fields (you can fetch this data from database this is just an example)
        $fields_posts = [
            ['name' => 'id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'full_picture', 'type' => 'text'],
            ['name' => 'link', 'type' => 'text'],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'type', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'shared', 'type' => 'text'],
            ['name' => 'Liked', 'type' => 'text'],
            ['name' => 'Love', 'type' => 'text'],
            ['name' => 'Haha', 'type' => 'text'],
            ['name' => 'Wow', 'type' => 'text'],
            ['name' => 'Sad', 'type' => 'text'],
            ['name' => 'Angry', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'change_predict','type'=>'boolean'],
            ['name' => 'isBookMark', 'type' => 'boolean']

        ];

        $fields_comments = [
            ['name' => 'id', 'type' => 'text'],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'post_id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'comment_count', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'interest', 'type' => 'boolean'],
            ['name' => 'change_predict', 'type' => 'boolean'],
            ['name' => 'parent', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'isBookMark', 'type' => 'boolean']
        ];
         $fields_pages = [
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'about', 'type' => 'text'],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'category', 'type' => 'text'],
            ['name' => 'sub_category', 'type' => 'text'],
            ['name'=>'profile','type' => 'text','length'=>'5']
        ];
         $fields_followers = [
            ['name' => 'id', 'type' => 'text'],
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'fan_count', 'type' => 'text'],
            ['name' => 'date', 'type' => 'text']
        ];
    $fields_inbound_posts = [
            ['name' => 'id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'full_picture', 'type' => 'text'],
            ['name' => 'link', 'type' => 'text'],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'type', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'shared', 'type' => 'text'],
            ['name' => 'Liked', 'type' => 'text'],
            ['name' => 'Love', 'type' => 'text'],
            ['name' => 'Haha', 'type' => 'text'],
            ['name' => 'Wow', 'type' => 'text'],
            ['name' => 'Sad', 'type' => 'text'],
            ['name' => 'Angry', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'change_predict','type'=>'boolean'],
            ['name' => 'isBookMark', 'type' => 'boolean']

        ];

        $fields_inbound_comments = [
            ['name' => 'id', 'type' => 'text'],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'post_id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'comment_count', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'interest', 'type' => 'boolean'],
            ['name' => 'tags', 'type' => 'text'],
            ['name' => 'change_predict', 'type' => 'boolean'],
            ['name' => 'parent', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'isBookMark', 'type' => 'boolean']
        ];

 
       $this->createTable($table_posts, $fields_posts);
       $this->createTable($table_pages, $fields_pages);
       $this->createTable($table_followers, $fields_followers);
        $this->createTable($table_comments, $fields_comments);
       $this->createTable($table_inbound_posts, $fields_inbound_posts);
       return $this->createTable($table_inbound_comments, $fields_inbound_comments);
       
    }
    // public function convertKtoThousand($value)
    // {
    //     $result=0;
    //     if(!empty($value[0]))
    //     {
    //       if(strpos($value[0],'K') !== false)
    //       {
    //          $new = str_replace("K", "", $value[0]);
    //          $new = 1000*$new;
    //       }
    //       else
    //       {
    //          $new=$value[0];   
    //       }
    //          $result = $result + (float)$new;
    //     } 
    //          return $result;
    // }
    public function convertKtoThousand_SyncMongo($s)
    {
        if (strpos(strtoupper($s), "K") != false) 
        {
            $s = rtrim($s, "kK");
            return floatval($s) * 1000;
        } 
        else if (strpos(strtoupper($s), "M") != false) 
        {
            $s = rtrim($s, "mM");
            return floatval($s) * 1000000;
        }
        else 
        {
            return floatval($s);
        }
    }
    function get_idArray($tbl)
    {
      $query = "SELECT id from temp_".$tbl." order by timestamp(created_time) desc";
      $id_result = DB::connection('mysql_cron_wunzin')->select($query);
      foreach($id_result as $res) $id_array[] = $res->id;
      return $id_array;
    }

   
    /**
     * To delete the tabel from the database 
     * 
     * @param $table_name
     *
     * @return bool
     */
    public function removeTable($table_name)
    {
        Schema::dropIfExists($table_name); 
        
        return true;
    }

}
