@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Project Assignment to Users</h4>
                    </div>
                </div>


                @if(!isset($users))
                <div class="">
                </div>
                @if(session()->has('message'))
                <div class="">
                {{ session()->get('message') }}
                </div>
                @endif
                <!-- Row -->
                 <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                               <form method="POST" role="form" class="form-material m-t-10" action="{{ route('projectRegister') }}" aria-label="{{ __('ProjectRegister') }}">
                                  {{csrf_field()}}
                                      <div class="form-group">
                                            <label for="username" class="control-label">{{ __('Username') }}</label>
                                            <select class="form-control custom-select" name="user_select" id="user_filter">
                                                @if (isset($usernames))
                                                @foreach($usernames as $index =>$usernames)
                                                @if($index == 0 )
                                                <option value="{{$usernames->id}}"  selected>{{$usernames->username}}</option>
                                                @else
                                                 <option value="{{$usernames->id}}" >{{$usernames->username}}</option>
                                                @endif
                                                @endforeach
                                                @endif
                                            </select>
                                      </div>
                                      <div class="form-group">
                                          <label for="project" class="control-label">{{ __('Project') }}</label>
                                          <select class="form-control custom-select" name="project_select" id="project_filter">
                                              @if (isset($projects))
                                              @foreach($projects as $index =>$projects)
                                              @if($index == 0 )
                                              <option value="{{$projects->id}}" id="{{$projects->id}}" selected>{{$projects->name}}</option>
                                              @else
                                               <option value="{{$projects->id}}" id="{{$projects->id}}" >{{$projects->name}}</option>
                                              @endif
                                              @endforeach
                                              @endif
                                         </select>
                                      </div>
                                      <div class="form-group">
                                          <button type="submit" class="btn btn-primary">  {{ __('Register') }}</button>
                                      </div>
                                </form>
                            </div>
                        </div>
                    </div>
              </div>
              @else
             <div class="">
             </div>
              @if(session()->has('message'))
             <div class="">
             {{ session()->get('message') }}
             </div>
             @endif
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                              
                               <form method="GET" role="form" class="form-material m-t-10" action="{{ route('update_assignUser') }}" aria-label="{{ __('ProjectRegister') }}">
                               {{csrf_field()}}
                                  <div class="form-group">
                                       <label for="name" class="control-label">{{ __('User Name') }}</label>
                                        <select class="form-control custom-select" name="user_select" id="user_filter">
                                            @if (isset($users))
                                            @foreach($users as $user)
                                            @if($user->id == $uid)
                                             <option value="{{ $user->id }}" selected> {{ $user->username }} </option>
                                             
                                            @else
                                             <option value="{{ $user->id }}"> {{ $user->username }} </option>
                                            
                                            @endif
                                            
                                            @endforeach
                                            @endif
                                        </select>
                                       @if ($errors->has('user_select'))
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('user_select') }}</strong>
                                          </span>
                                       @endif
                                  </div>
                                  <div class="form-group">
                                      <label for="projectname" class="control-label">{{ __('Project Name') }}</label>
                                       <select class="form-control custom-select" name="project_select" id="user_filter" >
                                            @if (isset($projects))
                                            @foreach($projects as $project)
                                            @if($project->id == $pid)
                                             <option value="{{ $project->id }}" selected>{{$project->name}}</option>
                                             
                                            @else
                                            <option value="{{ $project->id }}">{{$project->name}}</option>
                                           
                                            @endif

                                            @endforeach
                                            @endif
                                       </select>
                                        @if ($errors->has('project_select'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('project_select') }}</strong>
                                            </span>
                                        @endif
                                  </div>
                                  <input type="hidden" name="id" value="{{ $id }}"
                                  <div class="form-group">
                                      <button type="submit" class="btn btn-primary">  {{ __('Save') }}</button>
                                  </div>
                                  </form>
                            </div>
                        </div>
                    </div>
              </div>
              @endif
              <!-- Row -->
              <div class="row">
                  <div class="col-12">
                      <div class="card">
                          <div class="card-body">
                            <h4 class="card-title">Project Assigned Users List</h4>
                              <table id="user_table" class="table">
                                <thead>
                                  <tr>
                                    <th>User Name</th>
                                    <th>Role</th>
                                    <th>Brand Name</th>
                                    <th></th>
                                    </tr>
                                </thead>
                              </table>
                          </div>
                      </div>
                  </div>
              </div> 
              @endsection
              @push('scripts')

              <!-- jQuery 2.2.3 -->
              <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
              <!-- Bootstrap tether Core JavaScript -->
              <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
              <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
              <!-- slimscrollbar scrollbar JavaScript -->
              <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
              <!--Wave Effects -->
              <script src="{{asset('js/waves.js')}}" defer></script>
              <!--Menu sidebar -->
              <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
              <!--stickey kit -->
              <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
              <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
              <!--Custom JavaScript -->
              <script src="{{asset('js/custom.min.js')}}" defer></script>
              <!-- ============================================================== -->
              <!-- This page plugins -->
              <!-- ============================================================== -->
              <!-- Chart JS -->
              <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

              <!-- Flot Charts JavaScript -->
              <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
              <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
              <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
              <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

              <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
              <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

              <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
              <!-- Date range Plugin JavaScript -->
              <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
              <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
              <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

              <!-- DataTables -->
              <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
              <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
              <!-- This is data table -->
              <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

              <!-- ============================================================== -->
              <!-- Style switcher -->
              <!-- ============================================================== -->
              <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
              <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
              <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
              <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
              <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
              <script type="text/javascript">
                    $(document).ready(function() {
                            $('#user_table').DataTable({
                                 "lengthChange": true,"info": false, "searching": true,
                                  processing: true,
                                  serverSide: false,
                                     "headers": {
                                    'X-CSRF-TOKEN': '{{csrf_token()}}' 
                                  },
                                  "ajax": {
                                    "url": '{{ route('getassignlist') }}',
                                    "data": {
                                    }

                                  },
                                   columns: [
                                      {data: 'username', name: 'username'},
                                      {data: 'role', name: 'role'},
                                      {data: 'name', name: 'name'},
                                      {data: 'action', name: 'action', orderable: false, searchable: false}
                                  ]
                                  
                              }).on('click', '.del_assignUser[data-remote]', function (e) {
                                    var url = $(this).data('remote');
                                        swal({
                                        title: 'Are you sure?',
                                        text: "You won't be able to revert this!",
                                        type: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Yes, delete it!'
                                      }).then((result) => {
                                        if (result.value) {
                                          // alert('go to del fun');
                                             $.ajax({
                                            url: url,
                                            type: 'GET',
                                            dataType: 'json',
                                            data: {method: '_DELETE', submit: true}
                                        }).always(function (data) {
                                            swal(
                                              'Deleted!',
                                              'Your file has been deleted.',
                                              'success'
                                                  )
                                              $('#user_table').DataTable().rows().invalidate().draw();
                                              window.location.reload()
                                           });
                                         }
                                       })
                                    })
                               });

              </script>
              @endpush

