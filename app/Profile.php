<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $table = 'temp_profiles';
	protected $dates = ['created_at'];
    protected $fillable = [
        'temp_id','name','type','created_at','updated_at','id'
    ];
}
