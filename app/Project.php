<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Project extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = 'projects';
	protected $dates = ['created_at'];
    protected $fillable = [
        'id','name','new_mentions','created_at','user_id','monitor_pages'
    ];

    public function keywords()
    {
        return $this->hasMany('App\ProjectKeyword');
    }


}
