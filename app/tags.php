<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tags extends Model
{
    //
      protected $table = 'tags';
  protected $dates = ['created_at','updated_at'];
    protected $fillable = [
        'id','name','original_name','keywords','exclude','fromApi','created_at','updated_at','brand_id'
    ];
}
