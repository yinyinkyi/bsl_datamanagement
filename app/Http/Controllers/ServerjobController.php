<?php

namespace App\Http\Controllers;

use App\RedshiftData;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Notifications\NewTagNotification;
use App\Http\Controllers\GlobalController;
use App\Http\Controllers\stdClass;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use App\MongoProfile;
use DB;
use MongoDB;
use App\Comment;
use App\Project;
use App\tags;
use App\User;
use App\Website;
use App\MongodbData;
use MongoDB\BSON\ObjectID;
use App\MongoPages;
use App\MongoFollowers;
use App\MongoboundPost;
use App\MongoboundComment;

class ServerjobController extends Controller
{
    use GlobalController;
    public function __construct()
    {   
    }

    public function __invoke()
    {
        // $this->get_inbound_data();
        

    }


    public function get_inbound_data()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->where('id', [34, 35,38])->get();
        $data =  [];
  
        foreach($projects as $project)
        {
            $total = [0,0];
            $id = $project->id;
            $monitor_pages = $project->monitor_pages;
            $monitor_pages = explode(',', $monitor_pages);
            $monitor_pages = $this->Format_Page_Name($monitor_pages);
            $admin_pages = $project->own_pages;
            // dd($admin_pages);
            $admin_pages = explode(',', $admin_pages);
            // dd($admin_pages);
            $admin_pages = $this->Format_Page_Name($admin_pages);
            // dd($admin_pages);
            $other_pages=array_values(array_diff($monitor_pages,$admin_pages));
            // dd($other_pages);
            $admin_pages_count = count($admin_pages);
            // dd($admin_pages_count);
            $other_pages_count = count($other_pages);
            // dd($other_pages_count);
            if($admin_pages_count > 1) 
                {
                                $total = $this->insert_Inbound_Data($id,$admin_pages,'inbound_page');}
            if($other_pages_count > 1) 
                {
                                $total = $this->insert_Inbound_Data($id,$other_pages,'outbound_page'); }
        }
        
        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
        $date_time = $date->format('d-m-Y H:i:s');
        echo 'get_inbound_data  Successfully.(Post Total='. $total[0] .' , Comment Total = '. $total[1]. ') '. $date_time;
        echo "";
        DB::connection('mysql_cron_wunzin')->table('tbl_logs')->where('table_name', '=', 'inserted_inbound_data')->delete();
        DB::connection('mysql_cron_wunzin')->table('tbl_logs')->insert(['table_name' => 'inserted_inbound_data', 'lastUpdated' => $date]);
        //$this->info('Sync  Successfully.'. $date_time);
    }
    public function get_inboundComment()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->whereIn('id', [34, 35,38])->get();
        $data =  [];
  
        foreach($projects as $project)
        {
            $total = 0;
            $id = $project->id;
            $monitor_pages = $project->monitor_pages;
            $monitor_pages = explode(',', $monitor_pages);
            $monitor_pages = $this->Format_Page_Name($monitor_pages);
            $admin_pages = $project->own_pages;
            // dd($admin_pages);
            $admin_pages = explode(',', $admin_pages);
            // dd($admin_pages);
            $admin_pages = $this->Format_Page_Name($admin_pages);
            // dd($admin_pages);
            $other_pages=array_values(array_diff($monitor_pages,$admin_pages));
            // dd($other_pages);
            $admin_pages_count = count($admin_pages);
            // dd($admin_pages_count);
            $other_pages_count = count($other_pages);
            // dd($other_pages_count);
            if($admin_pages_count > 1) 
                {
                    // dd("false");
                    $total = $this->insert_Inbound_Comment($id,$admin_pages,'inbound_page');}
            if($other_pages_count > 1) 
                {
                    $total = $this->insert_Inbound_Comment($id,$other_pages,'outbound_page'); 
                }
        }
        
        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
        $date_time = $date->format('d-m-Y H:i:s');
        echo 'insert inbound comment to temp_000_inbound_comments  Successfully.(Total='. $total .') '. $date_time;
        echo "";
        DB::connection('mysql_cron_wunzin')->table('tbl_logs')->where('table_name', '=', 'temp_000_inbound_comments')->delete();
        DB::connection('mysql_cron_wunzin')->table('tbl_logs')->insert(['table_name' => 'temp_000_inbound_comments', 'lastUpdated' => $date]);
    }
    public function insert_Inbound_Comment($project_id,$my_pages,$crawl_type)
    {
        $today = date('d-m-Y h:i:s');//22-09-2018//  date('d-m-Y')
        $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
        $date_Begin=date('Y-m-d', strtotime('1 December 2018', strtotime($today_plus)));
        $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
        $date_End=date('Y-m-d h:i:s',strtotime($today_plus));
        $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);
        // $dateBegin = '';$dateEnd='';
        // $dateEnd='1514788200';
        $last_updated_date = $this->get_lastUpdatedDate('inbound_comments');
        $cmt_id_array=[];
        $page['$or']= $this->getpagefilter($my_pages);
         // dd($my_pages); 
        $inbound_post_id_array = $this->get_table_id($date_Begin,$date_End,"inbound_posts","id",$my_pages);
        
        // dd($total);
        $data=[];
        $page_array=[];
        $data_message=[];

        $post_id = $this->getFilterInboundPostId($my_pages);/*dd( $filter);*/
        $inbound_cmt_id_array = $this->get_table_id($date_Begin,$date_End,"inbound_comments","id");
        $inbound_comment_result=MongoboundComment::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_cmt_id_array,$post_id,$last_updated_date) {//print_r($filter);
            // dd($dateBegin,$dateEnd);
        return $collection->aggregate([
            [
            '$match' =>[
                '$and'=> [ 
                ['updatedAt'=> ['$gte' => $last_updated_date]],
                ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
                ['id'=> ['$exists'=> true]],
                ['post_id'=>['$in'=>$post_id]],
                ['id'=>[ '$nin'=> $inbound_cmt_id_array ] ],
            ]
            ]         
            ],
        ]);
        })->toArray();
        // dd($inbound_comment_result);
        $profile_id=[];
        $data_comment=[];
        $total = count($inbound_comment_result);
        foreach ($inbound_comment_result as  $key => $row) 
        { 
            // dd($row['reaction']['Like']);
            $id ='';
            $message ='';$original_message ='';$unicode ='';$profile ='';$page_name='';
            $post_id ='';
            $comment_count =0;
            $cmtLiked =0;$cmtLove=0;$cmtWow=0;$cmtHaha=0;$cmtSad=0;$cmtAngry=0;$cmtType='';$cmtLink='';
            $sentiment='';$emotion='';$parent='';
            $tags='';
            if(isset($row['profile']))
            {
                $profile = $row['profile'];
                $profile_id[]=new MongoDB\BSON\ObjectId($profile);
            } 

            $utcdatetime = $row["created_time"];
            $datetime = $utcdatetime->toDateTime();
            $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
            $datetime = $datetime->format('Y-m-d H:i:s');
            if(isset($row['id'])) $id =$row['id'];
            if(isset($row['message'])) $message = $row['message'];
            if(isset($row['original_message'])) $original_message = $row['original_message'];
            if(isset($row['unicode'])) $unicode = $row['unicode'];
            if(isset($row['post_id'])) $post_id = $row['post_id'];
            if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
            if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
            if(isset($row['emotion'])) $emotion = $row['emotion'];
            if(isset($row['parent'])) $parent =  $row['parent'];
            if(isset($row['page_name'])) $page_name = $row['page_name'];

            if(isset($row['reaction']['Like'])) $cmtLiked =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
            if(isset($row['reaction']['Love'])) $cmtLove =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
            if(isset($row['reaction']['Wow'])) $cmtWow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
            if(isset($row['reaction']['Haha'])) $cmtHaha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
            if(isset($row['reaction']['Sad'])) $cmtSad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
            if(isset($row['reaction']['Angry'])) $cmtAngry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);

            if(isset($row['type'])) $cmtType = $row['type'];
            if(isset($row['link'])) $cmtLink = $row['link'];
        
            $data_comment[] =[
            'id' => $id,
            'created_time' =>$datetime,
            'message' => $message,
            'wb_message' =>'',
            'original_message'=>$original_message,
            'unicode'=>$unicode,
            'post_id' =>$post_id,
            'comment_count' =>$comment_count,
            'sentiment' =>$sentiment,
            'checked_sentiment' =>'',
            'decided_keyword' =>'',
            'emotion' =>$emotion,
            'checked_emotion' =>$emotion,
            'interest' =>'',
            'profile'=>$profile,
            'tags'=>'',
            'tags_id'=>'',
            'checked_tags'=>'',
            'checked_tags_id'=>'',
            'change_predict'=>0,
            'checked_predict'=>0,
            'parent'=>$parent,
            'action_status'=>'',
            'action_taken'=>'',
            'isBookMark' =>0,
            'manual_date' => 'NULL',
            'page_name'=>$page_name,
            'cmtLiked' =>$cmtLiked ,
            'cmtLove' =>$cmtLove,
            'cmtWow' =>$cmtWow,
            'cmtHaha' =>$cmtHaha,
            'cmtSad' =>$cmtSad,
            'cmtAngry' =>$cmtAngry,
            'cmtType' =>$cmtType,
            'cmtLink' =>$cmtLink,
            'created_at' => now()->toDateTimeString(),
            'updated_at' => now()->toDateTimeString()
            ];                 
        }

        $path = storage_path('app/data_output/weekly_inbound_comment'.$project_id.'.csv');

        if (file_exists($path)) {
            unlink($path) ;
        } 

        $this->doCSV($path,$data_comment);

        $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_000_inbound_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,
        interest,profile,tags,tags_id,checked_tags,checked_tags_id,change_predict,checked_predict,parent,action_status,action_taken,isBookMark,manual_date,page_name,cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,created_at,updated_at)";
        // $query_load = 'Insert into temp_000_inbound_comments($id) ';
        // $query = DB::table('temp_000_inbound_comments')->insert($data_comment);
        $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
        $pdo->exec($query_load);

        if (file_exists($path)) {
            unlink($path) ;
        } 
        return $total;
    }

    public function insert_Inbound_Data($project_id,$my_pages,$crawl_type)
    {
        $today = date('d-m-Y h:i:s');//22-09-2018//  date('d-m-Y')
        $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
        $date_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));
        $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
        $date_End=date('Y-m-d',strtotime($today_plus));
        //dd($date_End);
        $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);
        $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];
        $page['$or']= $this->getpagefilter($my_pages);
         // dd($date_Begin); 2018-11-17"
        $inbound_post_id_array = $this->get_table_id($date_Begin,$date_End,"inbound_posts","id",$my_pages);
        $inbound_post_result=MongoboundPost::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_post_id_array,$crawl_type) {//print_r($filter);
            return $collection->aggregate([
            [
                    '$match' =>[
                    '$and'=> [ ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
                    ['id'=> ['$exists'=> true]],
                    ['id'=>[ '$nin'=> $inbound_post_id_array ] ],
                    $page
                ]
                ]
                ],
            ]);
        })->toArray();
        $total[] = count($inbound_post_result);
        //dd($inbound_post_result);
        $data=[];
        $page_array=[];
        $data_message=[];
        foreach ($inbound_post_result as  $key => $row) 
        {
            $id ='';$datetime=NULL;
            $full_picture ='';
            $link ='';
            $name ='';
            $message ='';$type ='';$original_message ='';$unicode ='';$visitor=0;
            $page_name ='';
            $share =0;
            $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
            $reactUpdatedAt=NULL;$isDeleted=0;
            
            if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

            if(isset($row['original_message'])) 
                $original_message = $row['original_message'];

            if(isset($row['unicode'])) 
                $unicode = $row['unicode'];

            if(isset($row['created_time']))
            {
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
            }
            
            if(isset($row['id'])) $id =$row['id'];
            if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
            if(isset($row['link'])) $link = $row['link'];
            if(isset($row['name'])) $name = $row['name'];
            if(isset($row['type'])) $type = $row['type'];
            if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
            if(isset($row['share'])) $share = $row['share'];
            if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
            if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
            if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
            if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
            if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
            if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);
            if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
            if(isset($row['emotion'])) $emotion = $row['emotion'];

            // if(isset($row['UpdatedAt']))
            // {
            //     $utcdatetime = $row["UpdatedAt"];
            //     $reactUpdatedAt = $utcdatetime->toDateTime();
            //     $reactUpdatedAt=$reactUpdatedAt->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
            //     $reactUpdatedAt = $reactUpdatedAt->format('Y-m-d H:i:s');
            // }
            
            if(isset($row['mark'])) 
            {
                if($row['mark'] == 'deleted')
                {
                    $isDeleted =1;
                }
            }
            if(isset($row['visitor'])) 
            {
               if($row['visitor'] == '1')
               {
                    $visitor =1;
               }
             }

            $data[] =[
            'id' => $id,
            'full_picture' =>$full_picture,
            'link' =>$link,
            'name' =>$name,
            'message' => $message,
            'original_message'=>$original_message,
            'unicode'=>$unicode,
            'type' =>$type,
            'wb_message' =>'',
            'page_name' =>$page_name,
            'shared' =>$share,
            'Liked' =>$Like,
            'Love' =>$Love,
            'Wow' =>$Wow,
            'Haha' =>$Haha,
            'Sad' =>$Sad,
            'Angry' =>$Angry,
            'sentiment' =>$sentiment,
            'checked_sentiment' =>'',
            'decided_keyword' =>'',
            'emotion' =>$emotion,
            'checked_emotion' =>$emotion,
            'created_time' =>$datetime,
            'change_predict'=>0,
            'checked_predict'=>0,
            'isBookMark' =>0,
            'visitor'=>$visitor,
           /* 'reactUpdatedAt' =>$reactUpdatedAt,*/
            'isDeleted' =>$isDeleted,
            'created_at' => now()->toDateTimeString(),
            'updated_at' => now()->toDateTimeString()
            ];                 
        }
        $path = storage_path('app/data_output/weekly_inbound_posts'.$crawl_type.'_'.$project_id.'.csv');
        if (file_exists($path)) {
            unlink($path) ;
        } 
        $this->doCSV($path,$data);
        $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_inbound_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,original_message,
        unicode,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,created_time,change_predict,checked_predict,isBookMark,visitor,isDeleted,created_at,updated_at)";
        $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
        $pdo->exec($query_load);
        if (file_exists($path)) {
            unlink($path) ;
        }
        $post_id = $this->getFilterInboundPostId($my_pages);/*dd( $filter);*/
        $inbound_cmt_id_array = $this->get_table_id($date_Begin,$date_End,"inbound_comments","id");
        $inbound_comment_result=MongoboundComment::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_cmt_id_array,$post_id) {//print_r($filter);
        return $collection->aggregate([
            [
            '$match' =>[
                '$and'=> [ 
                ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
                ['id'=> ['$exists'=> true]],
                ['post_id'=>['$in'=>$post_id]],
                ['id'=>[ '$nin'=> $inbound_cmt_id_array ] ],
            ]
            ]         
            ],
        ]);
        })->toArray();

        $profile_id=[];
        $data_comment=[];
         //dd($inbound_comment_result);
        $total[] = count($inbound_comment_result);
        foreach ($inbound_comment_result as  $key => $row) 
        {
            $id ='';
            $message ='';$original_message ='';$unicode ='';$profile ='';$page_name='';
            $post_id ='';
            $comment_count =0;
            $cmtLiked =0;$cmtLove=0;$cmtWow=0;$cmtHaha=0;$cmtSad=0;$cmtAngry=0;$cmtType='';$cmtLink='';
            $sentiment='';$emotion='';$parent='';
            $tags='';
            if(isset($row['profile']))
            {
                $profile = $row['profile'];
                $profile_id[]=new MongoDB\BSON\ObjectId($profile);
            } 

            $utcdatetime = $row["created_time"];
            $datetime = $utcdatetime->toDateTime();
            $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
            $datetime = $datetime->format('Y-m-d H:i:s');
            if(isset($row['id'])) $id =$row['id'];
            if(isset($row['message'])) $message = $row['message'];
            if(isset($row['original_message'])) $original_message = $row['original_message'];
            if(isset($row['unicode'])) $unicode = $row['unicode'];
            if(isset($row['post_id'])) $post_id = $row['post_id'];
            if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
            if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
            if(isset($row['emotion'])) $emotion = $row['emotion'];
            if(isset($row['parent'])) $parent =  $row['parent'];
            if(isset($row['page_name'])) $page_name = $row['page_name'];

            if(isset($row['reaction']['Like'])) $cmtLiked =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
            if(isset($row['reaction']['Love'])) $cmtLove =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
            if(isset($row['reaction']['Wow'])) $cmtWow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
            if(isset($row['reaction']['Haha'])) $cmtHaha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
            if(isset($row['reaction']['Sad'])) $cmtSad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
            if(isset($row['reaction']['Angry'])) $cmtAngry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);

            if(isset($row['type'])) $cmtType = $row['type'];
            if(isset($row['link'])) $cmtLink = $row['link'];
        
            $data_comment[] =[
            'id' => $id,
            'created_time' =>$datetime,
            'message' => $message,
            'wb_message' =>'',
            'original_message'=>$original_message,
            'unicode'=>$unicode,
            'post_id' =>$post_id,
            'comment_count' =>$comment_count,
            'sentiment' =>$sentiment,
            'checked_sentiment' =>'',
            'decided_keyword' =>'',
            'emotion' =>$emotion,
            'checked_emotion' =>$emotion,
            'interest' =>'',
            'profile'=>$profile,
            'tags'=>'',
            'tags_id'=>'',
            'checked_tags'=>'',
            'checked_tags_id'=>'',
            'change_predict'=>0,
            'checked_predict'=>0,
            'parent'=>$parent,
            'action_status'=>'',
            'action_taken'=>'',
            'isBookMark' =>0,
            'manual_date' => 'NULL',
            'page_name'=>$page_name,
            'cmtLiked' =>$cmtLiked ,
            'cmtLove' =>$cmtLove,
            'cmtWow' =>$cmtWow,
            'cmtHaha' =>$cmtHaha,
            'cmtSad' =>$cmtSad,
            'cmtAngry' =>$cmtAngry,
            'cmtType' =>$cmtType,
            'cmtLink' =>$cmtLink,
            'created_at' => now()->toDateTimeString(),
            'updated_at' => now()->toDateTimeString()
            ];                 
        }

        $path = storage_path('app/data_output/weekly_inbound_comment'.$project_id.'.csv');

        if (file_exists($path)) {
            unlink($path) ;
        } 

        $this->doCSV($path,$data_comment);

        $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_inbound_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,
        interest,profile,tags,tags_id,checked_tags,checked_tags_id,change_predict,checked_predict,parent,action_status,action_taken,isBookMark,manual_date,page_name,cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,created_at,updated_at)";
        $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
        $pdo->exec($query_load);

        if (file_exists($path)) {
            unlink($path) ;
        } 

        $profile_id_array = $this->get_common_table_id("temp_profiles","temp_id");

        $profiles_result=MongoProfile::raw(function ($collection) use($profile_id,$profile_id_array) {//print_r($filter);
        return $collection->aggregate([[
            '$match' =>[
            '$and'=> [ 
            ['_id' => ['$in' => $profile_id ] ] ,
            ['_id'=>[ '$nin'=> $profile_id_array ] ],    
            ]
            ]  
            ]
        ]);
        })->toArray();

        $data_profiles=[];
        foreach ($profiles_result as  $key => $row) {
            $_id ='';
            $id ='';
            $name ='';
            $type ='';
          
            if(isset($row['_id'])) $_id =$row['_id'];
            if(isset($row['id'])) $id =$row['id'];
            if(isset($row['name'])) $name =$row['name'];
            if(isset($row['type'])) $type = $row['type'];
            $data_profiles[] =[
                'temp_id' => $_id,
                'id' => $id,
                'name' =>$name,
                'type' => $type,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()
            ];                 
        }

        $path = storage_path('app/data_output/profiles'.$project_id.'.csv');

        if (file_exists($path)) {
            unlink($path) ;
        } 
        $this->doCSV($path,$data_profiles);

        $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_profiles FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (temp_id,id,name,type,created_at,updated_at)";

        $pdo = DB::connection()->getPdo();
        $pdo->exec($query_load);

        $page_id_array = $this->get_table_page($project_id,"pages","page_name");
        $pages_result=MongoPages::raw(function ($collection) use($my_pages,$page_id_array) {//print_r($filter);

        return $collection->aggregate([[
                '$match' =>[
                '$and'=> [ 
                ['page_name' => ['$in' => $my_pages ] ] ,
                [ 'page_name'=>[ '$nin'=> $page_id_array ] ],
                ]
                ]  
                ]
        ]);
        })->toArray();
        $data_pages=[];
        foreach ($pages_result as  $key => $row) {
            $page_name ='';
            $about ='';
            $name ='';
            $category ='';
            $sub_category ='';
            $profile='';
        
            if(isset($row['page_name'])) $page_name =$row['page_name'];
            if(isset($row['name'])) $name =$row['name'];
            if(isset($row['about'])) $about = $row['about'];
            if(isset($row['category'])) $category = $row['category'];
            if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
            if(isset($row['profile'])) $profile = $row['profile'];

            $data_pages[] =[
                'page_name' => $page_name,
                'name' =>$name,
                'about' => $about,
                'category' =>$category,
                'sub_category' =>$sub_category,
                'profile'=>$profile,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()
                ];                 
        }
        
        $path = storage_path('app/data_output/weekly_pages'.$project_id.'.csv');

        if (file_exists($path)) {
            unlink($path) ;
        } 
        $this->doCSV($path,$data_pages);

        $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_pages FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (page_name,name,about,category,sub_category,profile,created_at,updated_at)";

        $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
        $pdo->exec($query_load);

        if (file_exists($path)) {
            unlink($path) ;
        } 
         // dd($total);
        return $total;
    }

    public function update_inboundComment()
    {
      // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->whereIn('id', [34, 35])->select('*')->get();
      // foreach ($projects as $project)
      //   {
            $total  = 0;
            $id_array = [];
            // $project_id = $project->id;
            $id_array = $this->get_idArray('inbound_comments');
            $last_updated_date = $this->get_lastUpdatedDate('inbound_comments');
            $comment_result=MongoboundComment::raw(function ($collection) use($id_array,$last_updated_date) { //print_r($last_updated_date);
            return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   ['updatedAt' => ['$gte' => $last_updated_date]],
                   ['updatedAt'=> ['$exists'=> true]],
                   ['id'=> ['$exists'=> true]],
                   // ['page_name'=> ['$exists'=> true]],
                   ['id'=>[ '$in'=> $id_array ] ]
                            ] 
                          ],
              ]]);
             })->toArray();
             // dd($comment_result);
             $total = count($comment_result);
             // dd($total);
             $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
             $updated_time = $date->format('Y-m-d H:i:s');
            
            foreach($comment_result as $row)
            {
                $page_name='';$id='';$post_id='';$message='';$original_message='';$profile='';
                $cmtLiked =0;$cmtLove=0;$cmtWow=0;$cmtHaha=0;$cmtSad=0;$cmtAngry=0;$cmtType='';$cmtLink='';

                if(isset($row['profile'])){ $profile = $row['profile']; }
                if(isset($row['page_name'])) $page_name=$row['page_name'];
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['post_id'])) $post_id=$row['post_id'];
                if(isset($row['message'])) $message=$row['message'];
                if(isset($row['original_message'])) $original_message=$row['original_message'];
                if(isset($row['created_time']))
                {
                    $utcdatetime = $row["created_time"];
                    $datetime = $utcdatetime->toDateTime();
                    $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                    $datetime = $datetime->format('Y-m-d H:i:s');
                }
                if(isset($row['reaction']['Like'])) $cmtLiked =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $cmtLove =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $cmtWow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $cmtHaha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $cmtSad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $cmtAngry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);

                if(isset($row['type'])) $cmtType = $row['type'];
                if(isset($row['link'])) $cmtLink = $row['link'];

                DB::table("temp_inbound_comments")->where('id',$id)->update([
                      'post_id' =>$post_id,
                      'message' =>$message,
                      'original_message' =>$original_message,
                      'page_name' =>$page_name,
                      'created_time' =>$datetime,
                      'profile' => $profile,
                      // 'sync' => 1,
                      'cmtLiked' =>$cmtLiked ,
                      'cmtLove' =>$cmtLove,
                      'cmtWow' =>$cmtWow,
                      'cmtHaha' =>$cmtHaha,
                      'cmtSad' =>$cmtSad,
                      'cmtAngry' =>$cmtAngry,
                      'cmtType' =>$cmtType,
                      'cmtLink' =>$cmtLink,
                      'updated_at' => now()->toDateTimeString()
                     ]);
            }
                    
        // }
                DB::table('tbl_logs')->where('table_name','inbound_comments')->update(['lastUpdated' => $updated_time]);

                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'update inbound comment  Successfully.(Total='. $total .') '. $date_time ;
                echo "";
    } /*end of update inbound comment */

    public function update_inboundPost()
    {
       //  $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->whereIn('id', [34, 35])->get();
       //  foreach ($projects as $project)
       // {
            $total  = 0;
            $id_array = [];
            // $project_id  = $project->id;
            $id_array  = $this->get_idArray('inbound_posts');
            $last_updated_date = $this->get_lastUpdatedDate('inbound_posts');
            $inbound_post_result=MongoboundPost::raw(function ($collection) use($id_array,$last_updated_date) { //print_r($id_array);
            return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   ['updatedAt' => ['$gte' => $last_updated_date]],
                   ['updatedAt'=> ['$exists'=> true]],
                   ['id'=> ['$exists'=> true]],
                   ['id'=>[ '$in'=> $id_array ] ]
                            ] 
                          ],
              ]]);
             })->toArray();
             $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
             $updated_time = $date->format('Y-m-d H:i:s');

             $total = count($inbound_post_result);
            // dd($inbound_post_result);
            foreach($inbound_post_result as $row)
            {
                $id ='';$datetime=NULL;
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';$original_message ='';$unicode ='';$visitor=0;
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                $reactUpdatedAt=NULL;$isDeleted=0;

                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['mark'])) 
                  {
                    if($row['mark'] == 'deleted')
                  {
                     $isDeleted =1;
                  }
                   
                  }
                   if(isset($row['visitor'])) 
                  {
                       if($row['visitor'] == '1')
                       {
                        $visitor =1;
                      }

                   }
                if(isset($row['original_message'])) $original_message = $row['original_message'];
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['created_time']))
                {
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                }
                if(isset($row['id'])) $id =$row['id'];
               
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) $page_name = $row['page_name'];
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];

                //  if(isset($row['UpdatedAt']))
                // {
                // $utcdatetime = $row["UpdatedAt"];
                // $reactUpdatedAt = $utcdatetime->toDateTime();
                // $reactUpdatedAt=$reactUpdatedAt->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                // $reactUpdatedAt = $reactUpdatedAt->format('Y-m-d H:i:s');
                // }
                  
                    DB::table("temp_inbound_posts")->where('id',$id)->update([
                      'full_picture' =>$full_picture,
                      'link' =>$link,
                      'name' =>$name,
                      'message' => $message,
                      'original_message'=>$original_message,
                      'unicode'=>$unicode,
                      'type' =>$type,
                      'page_name' =>$page_name,
                      'shared' =>$share,
                      'Liked' =>$Like,
                      'Love' =>$Love,
                      'Wow' =>$Wow,
                      'Haha' =>$Haha,
                      'Sad' =>$Sad,
                      'Angry' =>$Angry,
                      'isDeleted'=>$isDeleted,
                      'visitor'=>$visitor,
                      'created_time' =>$datetime,
                      'updated_at' => now()->toDateTimeString()
                     ]);
            }
                    
        // }
          DB::table('tbl_logs')->where('table_name','inbound_posts')->update(['lastUpdated' => $updated_time]);
          $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
          $date_time = $date->format('d-m-Y H:i:s');
          echo 'update inbound post  Successfully. (Total='. $total .') '. $date_time;
          echo "";
    } /*end of update inbound post*/

    public function get_mention_data()
    {
        // $query = "SELECT * from projects where id=34  and id not in (select brand_id from cron_log where finish_status=0 and cron_type='sync_mention') ";
         // $query = "SELECT * from projects where id=34  and id=35";
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->whereIn('id', [34, 35,38])->get();
        // $projects = DB::select($query);
        // dd($projects);
        $data =  [];
        $total = [0,0,0];
       foreach($projects as $project)
      {
        $id = $project->id;
        $total = $this->Insert_Mention_Data($id);
      }
      // dd($total);
      $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
      $date_time = $date->format('d-m-Y H:i:s');
      echo 'get mention data  Successfully.(Post Total='. $total[0] .' , Comment Total = '. $total[2] .' , Related Comment = '. $total[1].' ) '. $date_time;
      DB::connection('mysql_cron_wunzin')->table('tbl_logs')->where('table_name', '=', 'inserted_mention_data')->delete();
      DB::connection('mysql_cron_wunzin')->table('tbl_logs')->insert(['table_name' => 'inserted_mention_data', 'lastUpdated' => $date]);
    }

    public function Insert_Mention_Data($project_id)
    {
        $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];

        $today = date('d-m-Y h:i:s');//22-09-2018//  date('d-m-Y')

        $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
        $date_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));
          
        $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
        $date_End=date('Y-m-d h:i:s',strtotime($today_plus));
        // $date_End=date('Y-m-d', strtotime('+1 day', strtotime($date_End))); /*added at 2/feb/2019*/
        $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);
     
        $keyword_data = $this->getprojectkeyword($project_id);

        if(isset($keyword_data [0]['main_keyword']))
        {
             $filter_test=[];
             $filter_condition_or=[];
             $filter['$or'] = $this->getkeywordfilter($keyword_data,$project_id);
             $pagename = $this->getpagename($project_id);//this filter will use later to filter (to avoid including data from monitor pages)
             $pagename= explode(',', $pagename[0]["monitor_pages"]);
             $pagename= $this->Format_Page_Name($pagename);

            // $filter_condition[] = $filter_condition_or;
            // dd($filter_condition_or);
            // $page['$or']= $this->getpagefilter($other_pages);
         
            /* dd($page);*/
         $mention_post_id_array = $this->get_table_id_mention("posts","id");
         // dd($mention_post_id_array);
         $mention_post_result=MongodbData::raw(function ($collection) use($filter,$mention_post_id_array,$dateBegin,$dateEnd) {//print_r($filter);

            return $collection->aggregate([
                [
                '$match' =>[
                     '$and'=> [ 
                     ['id'=> ['$exists'=> true]],
                     // [ 'id'=>[ '$nin'=> $mention_post_id_array ] ],
                     ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
                      $filter
                              ]
                ]  
                           
               ],
               ['$sort' =>['created_time'=>-1]],
               // ['$limit' =>2000]
             
                
              ]);
        })->toArray();
             $total[]=  count($mention_post_result);
          // dd($mention_post_result);
             $data=[];
             $page_array=[];
             $data_message=[];

        foreach ($mention_post_result as  $key => $row) {
                $id ='';$datetime=NULL;$visitor=0;
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';$original_message='';$unicode = '';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                $isDeleted=0;
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                if(isset($row['original_message'])) $original_message = $row['original_message'];
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['created_time']))
                {
                    $utcdatetime = $row["created_time"];
                    $datetime = $utcdatetime->toDateTime();
                    $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                    $datetime = $datetime->format('Y-m-d H:i:s');
                }
             
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['mark'])) 
                {
                    if($row['mark'] == 'deleted')
                    {
                       $isDeleted =1;
                    }
                 
                }
                  if(isset($row['visitor'])) 
                {
                    if($row['visitor'] == '1')
                    {
                       $visitor =1;
                    }
                 
                }

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'original_message' => $original_message,
                    'unicode' => $unicode,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>'',
                    'checked_sentiment' =>'',
                    'decided_keyword' =>'',
                    'emotion' =>'',
                    'checked_emotion' =>'',
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'isDeleted'=>$isDeleted,
                    'visitor' => $visitor,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

            }
         $path = storage_path('app/data_output/mention_posts'.$project_id.'.csv');

        if (file_exists($path)) {
            unlink($path) ;
        } 
        $this->doCSV($path,$data);

        $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,original_message,unicode,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,created_time,change_predict,checked_predict,isBookMark,isDeleted,visitor,created_at,updated_at)";


        $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
        $pdo->exec($query_load);


        $post_id = $this->getMentionPostId();/*dd( $filter);*/

        // post related comments
        $related_comment_result=Comment::raw(function ($collection) use($post_id,$dateBegin,$dateEnd) {//print_r($filter);
 
            return $collection->aggregate([
                [
                '$match' =>[
                     '$and'=> [ 
                     ['id'=> ['$exists'=> true]],
                     ['post_id'=>['$in'=>$post_id]],
                     ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]]
                              ]
                   ]  
                           
               ],
               ['$sort' =>['created_time'=>-1]],
            ]);
           })->toArray();
        // dd($related_comment_result);
        $total [] = count($related_comment_result);
        $related_comment=[];
        foreach ($related_comment_result as  $key => $row) {
                        $id ='';
                        $message ='';
                        $post_id ='';$profile='';$page_name='';
                        $comment_count =0;$original_message='';$unicode='';
                        $cmtLiked =0;$cmtLove=0;$cmtWow=0;$cmtHaha=0;$cmtSad=0;$cmtAngry=0;$cmtType='';$cmtLink='';
                        $sentiment='';$emotion='';$parent='';
                        $tags='';
                        if(isset($row['profile']))
                        {
                        $profile = $row['profile'];
                        $profile_id[]=new MongoDB\BSON\ObjectId((string)$profile);
                        } 

                        $utcdatetime = $row["created_time"];
                        $datetime = $utcdatetime->toDateTime();
                        $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                        $datetime = $datetime->format('Y-m-d H:i:s');
                        if(isset($row['id'])) $id =$row['id'];
                        if(isset($row['message'])) $message = $row['message'];
                        if(isset($row['original_message'])) $original_message = $row['original_message'];
                        if(isset($row['unicode'])) $unicode = $row['unicode'];
                        if(isset($row['post_id'])) $post_id = $row['post_id'];
                        if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                        if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                        if(isset($row['emotion'])) $emotion = $row['emotion'];
                        if(isset($row['parent'])) $parent =  $row['parent'];
                        if(isset($row['page_name'])) $page_name = $row['page_name'];

                        if(isset($row['reaction']['Like'])) $cmtLiked =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                        if(isset($row['reaction']['Love'])) $cmtLove =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                        if(isset($row['reaction']['Wow'])) $cmtWow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                        if(isset($row['reaction']['Haha'])) $cmtHaha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                        if(isset($row['reaction']['Sad'])) $cmtSad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                        if(isset($row['reaction']['Angry'])) $cmtAngry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);

                        if(isset($row['type'])) $cmtType = $row['type'];
                        if(isset($row['link'])) $cmtLink = $row['link'];
                   

                          $related_comment[] =[
                            'id' => $id,
                            'created_time' =>$datetime,
                            'message' => $message,
                            'wb_message' =>'',
                            'original_message' =>$original_message,
                            'unicode' =>$unicode,
                            'post_id' =>$post_id,
                            'comment_count' =>$comment_count,
                            'sentiment' =>'',
                            'checked_sentiment' =>'',
                            'decided_keyword' =>'',
                            'emotion' =>'',
                            'checked_emotion' =>'',
                            'interest' =>'',
                            'profile'=>$profile,
                            'change_predict'=>0,
                            'checked_predict'=>0,
                            'parent'=>$parent,
                            'isBookMark' =>0,
                            'manual_date'=>'NULL',
                            'page_name'=>$page_name,
                            'cmtLiked' =>$cmtLiked ,
                            'cmtLove' =>$cmtLove,
                            'cmtWow' =>$cmtWow,
                            'cmtHaha' =>$cmtHaha,
                            'cmtSad' =>$cmtSad,
                            'cmtAngry' =>$cmtAngry,
                            'cmtType' =>$cmtType,
                            'cmtLink' =>$cmtLink,
                            'created_at' => now()->toDateTimeString(),
                            'updated_at' => now()->toDateTimeString()
                            ];                 

                        }

                $path = storage_path('app/data_output/mention_comments'.$project_id.'.csv');
                if (file_exists($path)) {
                    unlink($path) ;
                } 
             $this->doCSV($path,$related_comment);
             $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,interest,profile,change_predict,checked_predict,parent,isBookMark,manual_date,page_name,cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,created_at,updated_at)";

             $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
             $pdo->exec($query_load);


       // end post related comments


        $mention_cmt_id_array = $this->get_table_id_mention("comments","id");

        $mention_comment_result=Comment::raw(function ($collection) use($filter,$mention_cmt_id_array,$post_id,$dateBegin,$dateEnd) {//print_r($filter);
 
            return $collection->aggregate([
                [
                '$match' =>[
                     '$and'=> [ 
                     ['id'=> ['$exists'=> true]],
                     // ['post_id'=>['$in'=>$post_id]],
                     ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
                     ['id'=>[ '$nin'=> $mention_cmt_id_array ] ],
                     $filter
                   
                    
                              ]
                ]  
                           
               ],
               ['$sort' =>['created_time'=>-1]],
            ]);
           })->toArray();
          $total [] = count($mention_comment_result);
          $profile_id=[];
          $data_comment=[];
          foreach ($mention_comment_result as  $key => $row) {
                        $id ='';
                        $message ='';
                        $post_id ='';$profile='';$page_name='';
                        $comment_count =0;$original_message='';$unicode='';
                        $cmtLiked =0;$cmtLove=0;$cmtWow=0;$cmtHaha=0;$cmtSad=0;$cmtAngry=0;$cmtType='';$cmtLink='';
                        $sentiment='';$emotion='';$parent='';
                        $tags='';
                        if(isset($row['profile']))
                        {
                        $profile = $row['profile'];
                        $profile_id[]=new MongoDB\BSON\ObjectId((string)$profile);
                        } 

                        $utcdatetime = $row["created_time"];
                        $datetime = $utcdatetime->toDateTime();
                        $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                        $datetime = $datetime->format('Y-m-d H:i:s');
                        if(isset($row['id'])) $id =$row['id'];
                        if(isset($row['message'])) $message = $row['message'];
                        if(isset($row['original_message'])) $original_message = $row['original_message'];
                        if(isset($row['unicode'])) $unicode = $row['unicode'];
                        if(isset($row['post_id'])) $post_id = $row['post_id'];
                        if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                        if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                        if(isset($row['emotion'])) $emotion = $row['emotion'];
                        if(isset($row['parent'])) $parent =  $row['parent'];
                        if(isset($row['page_name'])) $page_name = $row['page_name'];

                        if(isset($row['reaction']['Like'])) $cmtLiked =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                        if(isset($row['reaction']['Love'])) $cmtLove =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                        if(isset($row['reaction']['Wow'])) $cmtWow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                        if(isset($row['reaction']['Haha'])) $cmtHaha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                        if(isset($row['reaction']['Sad'])) $cmtSad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                        if(isset($row['reaction']['Angry'])) $cmtAngry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);

                        if(isset($row['type'])) $cmtType = $row['type'];
                        if(isset($row['link'])) $cmtLink = $row['link'];
                   

                          $data_comment[] =[
                            'id' => $id,
                            'created_time' =>$datetime,
                            'message' => $message,
                            'wb_message' =>'',
                            'original_message' =>$original_message,
                            'unicode' =>$unicode,
                            'post_id' =>$post_id,
                            'comment_count' =>$comment_count,
                            'sentiment' =>'',
                            'checked_sentiment' =>'',
                            'decided_keyword' =>'',
                            'emotion' =>'',
                            'checked_emotion' =>'',
                            'interest' =>'',
                            'profile'=>$profile,
                            'change_predict'=>0,
                            'checked_predict'=>0,
                            'parent'=>$parent,
                            'isBookMark' =>0,
                            'manual_date'=>'NULL',
                            'page_name'=>$page_name,
                            'cmtLiked' =>$cmtLiked ,
                            'cmtLove' =>$cmtLove,
                            'cmtWow' =>$cmtWow,
                            'cmtHaha' =>$cmtHaha,
                            'cmtSad' =>$cmtSad,
                            'cmtAngry' =>$cmtAngry,
                            'cmtType' =>$cmtType,
                            'cmtLink' =>$cmtLink,
                            'created_at' => now()->toDateTimeString(),
                            'updated_at' => now()->toDateTimeString()
                            ];                 

                        }

                $path = storage_path('app/data_output/mention_comments'.$project_id.'.csv');
                if (file_exists($path)) {
                    unlink($path) ;
                } 
             $this->doCSV($path,$data_comment);
             $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,interest,profile,change_predict,checked_predict,parent,isBookMark,manual_date,page_name,cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,created_at,updated_at)";

             $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
             $pdo->exec($query_load);

         }
             return $total;
    }  /*end of get_mention_data*/

    public function update_mentionComment()
    {
         // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->whereIn('id', [34, 35])->select('*')->get();
         // foreach ($projects as $project)
         // {
                $id_array = [];
                // $project_id = $project->id;
                // $project_id = 34;
                $total  = 0;
                $id_array = $this->get_idArray('comments');
                $last_updated_date = $this->get_lastUpdatedDate('comments');

                $comment_result=Comment::raw(function ($collection) use($id_array,$last_updated_date) { //print_r($id_array);
                return $collection->aggregate([
                  [
                  '$match' =>[
                       '$and'=> [ 
                       ['updatedAt' => ['$gte' => $last_updated_date]],
                       ['updatedAt'=> ['$exists'=> true]],
                       ['id'=> ['$exists'=> true]],
                       ['page_name'=> ['$exists'=> true]],
                       ['id'=>[ '$in'=> $id_array ] ]
                                ] 
                              ],
                  ]]);
                 })->toArray();
                 $total = count($comment_result);
                 // dd($comment_result);
                 $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                 $updated_time = $date->format('Y-m-d H:i:s');
                
                foreach($comment_result as $row)
                {
                    $page_name='';$id='';$post_id='';$message='';$original_message='';$profile='';
                    $cmtLiked =0;$cmtLove=0;$cmtWow=0;$cmtHaha=0;$cmtSad=0;$cmtAngry=0;$cmtType='';$cmtLink='';
                    if(isset($row['profile'])) $profile=$row['profile'];
                    if(isset($row['page_name'])) $page_name=$row['page_name'];
                    if(isset($row['id'])) $id =$row['id'];
                    if(isset($row['post_id'])) $post_id=$row['post_id'];
                    if(isset($row['message'])) $message=$row['message'];
                    if(isset($row['original_message'])) $original_message=$row['original_message'];
                    if(isset($row['created_time']))
                    {
                    $utcdatetime = $row["created_time"];
                    $datetime = $utcdatetime->toDateTime();
                    $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                    $datetime = $datetime->format('Y-m-d H:i:s');
                    }
                    if(isset($row['reaction']['Like'])) $cmtLiked =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                    if(isset($row['reaction']['Love'])) $cmtLove =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                    if(isset($row['reaction']['Wow'])) $cmtWow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                    if(isset($row['reaction']['Haha'])) $cmtHaha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                    if(isset($row['reaction']['Sad'])) $cmtSad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                    if(isset($row['reaction']['Angry'])) $cmtAngry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);

                    if(isset($row['type'])) $cmtType = $row['type'];
                    if(isset($row['link'])) $cmtLink = $row['link'];
                   
          
                      
                        DB::table("temp_comments")->where('id',$id)->update([
                          'post_id' => $post_id,
                          'message' =>$message,
                          'original_message' =>$original_message,
                          'page_name' =>$page_name,
                          'profile' =>$profile,
                          // 'sync' => 1,
                          'created_time' =>$datetime,
                          'cmtLiked' =>$cmtLiked ,
                          'cmtLove' =>$cmtLove,
                          'cmtWow' =>$cmtWow,
                          'cmtHaha' =>$cmtHaha,
                          'cmtSad' =>$cmtSad,
                          'cmtAngry' =>$cmtAngry,
                          'cmtType' =>$cmtType,
                          'cmtLink' =>$cmtLink,
                          'updated_at' => now()->toDateTimeString()
                         ]);
                }
           // }
            DB::table('tbl_logs')->where('table_name','comments')->update(['lastUpdated' => $updated_time]); 
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'update mention comment  Successfully.(Total = ' .$total. ') '. $date_time;
                echo "";
    } /*end of update mention comment*/

    public function update_mentionPost()
    {
        // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->whereIn('id', [34, 35])->select('*')->get();
        // foreach ($projects as $project)
        // {
            $id_array = [];
            // $project_id = $project->id;
            // $project_id = 34;
            $total  = 0;
            $id_array = $this->get_idArray('posts');
            $last_updated_date = $this->get_lastUpdatedDate('posts');

            $comment_result=MongodbData::raw(function ($collection) use($id_array,$last_updated_date) { //print_r($id_array);
            return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   ['updatedAt' => ['$gte' => $last_updated_date]],
                   ['updatedAt'=> ['$exists'=> true]],
                   ['id'=> ['$exists'=> true]],
                   ['page_name'=> ['$exists'=> true]],
                   ['id'=>[ '$in'=> $id_array ] ]
                            ] 
                          ],
              ]]);
             })->toArray();
            $total = count($comment_result);
            // dd($total);
             $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
             $updated_time = $date->format('Y-m-d H:i:s');
            
            foreach($comment_result as $row)
            {
                $page_name='';$id='';$message='';$original_message='';

                if(isset($row['page_name'])) $page_name=$row['page_name'];
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message=$row['message'];
                if(isset($row['original_message'])) $original_message=$row['original_message'];
                if(isset($row['created_time']))
                {
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                }
               
      
                  
                    DB::table("temp_posts")->where('id',$id)->update([
                       'message' =>$message,
                       'original_message' =>$original_message,
                       'page_name' =>$page_name,
                      // 'sync' => 1,
                       'created_time' =>$datetime,
                       'updated_at' => now()->toDateTimeString()
                     ]);
            }
            
      // }
         DB::table('tbl_logs')->where('table_name','posts')->update(['lastUpdated' => $updated_time]); 
         $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
         $date_time = $date->format('d-m-Y H:i:s');
         echo 'update mention post  Successfully. (Total = ' .$total. ') '. $date_time;
         echo "";
    } /*end of update mention post*/
    public function get_mentionDataYearly()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('cron_log')->select('*')->where('finish_status',0)->where('cron_type','sync_mention')->get();
        foreach($projects as $project)
       {
            $id = $project->brand_id;
            $result =$this->Insert_Mention_Data_Yearly($id);
            if($result)
              {
                 DB::table('cron_log')
                ->where('brand_id', $id)
                ->update(['finish_status' => 1]);
              }
        }
        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
        $date_time = $date->format('d-m-Y H:i:s');
        echo 'get mention data yearly  Successfully.'. $date_time;
        echo "";
    }

    public function Insert_Mention_Data_Yearly($project_id)
    {
        $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];
        $keyword_data = $this->getprojectkeyword($project_id);
        if(isset($keyword_data [0]['main_keyword']))
        {
             $filter['$or'] = $this->getkeywordfilter($keyword_data,$project_id);
             // $this->info($filter);
             $pagename = $this->getpagename($project_id);//this filter will use later to filter (to avoid including data from monitor pages)
             $pagename= explode(',', $pagename[0]["monitor_pages"]);
             $pagename= $this->Format_Page_Name($pagename);
             // $page['$or']= $this->getpagefilter($other_pages);
             /* dd($page);*/
             $today = date('d-m-Y h:i:s');//22-09-2018//  date('d-m-Y')
             $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
             $date_Begin=date('Y-m-d', strtotime('-1 year', strtotime($today_plus)));
             // dd($date_Begin);
             //start with today (00:00:00) hour
             $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
           
             $date_End=date('Y-m-d h:i:s',strtotime($today_plus));
             // $date_End=date('Y-m-d', strtotime('+1 day', strtotime($date_End)));
             //start with tomorrow (00:00:00) hour
             $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);

             $mention_post_id_array = $this->get_table_id_mention("posts","id");
 
             $mention_post_result=MongodbData::raw(function ($collection) use($filter,$mention_post_id_array,$dateBegin,$dateEnd) {//print_r($filter);

                return $collection->aggregate([
                    [
                    '$match' =>[
                         '$and'=> [ 
                         ['id'=> ['$exists'=> true]],
                         [ 'id'=>[ '$nin'=> $mention_post_id_array ] ],
                         ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
                          $filter
                                  ]
                    ]  
                               
                   ],
                   ['$sort' =>['created_time'=>-1]],
                   // ['$limit' =>2000]
                 
                    
                  ]);
            })->toArray();
             $total [] = count($mention_post_result);
             $data=[];
             $page_array=[];
             $data_message=[];

            foreach ($mention_post_result as  $key => $row) {
                $id ='';$datetime=NULL;
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';$original_message='';$unicode = '';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                $isDeleted=0;
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                    if(isset($row['original_message'])) $original_message = $row['original_message'];

                    if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['created_time']))
                    {
                    $utcdatetime = $row["created_time"];
                    $datetime = $utcdatetime->toDateTime();
                    $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                    $datetime = $datetime->format('Y-m-d H:i:s');
                 }
             
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['mark'])) 
                {
                      if($row['mark'] == 'deleted')
                    {
                       $isDeleted =1;
                    }
                 
                }

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'original_message' => $original_message,
                    'unicode' => $unicode,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>'',
                    'checked_sentiment' =>'',
                    'decided_keyword' =>'',
                    'emotion' =>'',
                    'checked_emotion' =>'',
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'isDeleted'=>$isDeleted,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];
               }
            $path = storage_path('app/data_output/mention_posts'.$project_id.'.csv');

            if (file_exists($path)) {
                unlink($path) ;
            } 
            $this->doCSV($path,$data);

            $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,original_message,unicode,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,created_time,change_predict,checked_predict,isBookMark,isDeleted,created_at,updated_at)";


            $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
            $pdo->exec($query_load);
            $post_id = $this->getMentionPostId();/*dd( $filter);*/
            // dd($post_id);
            $mention_cmt_id_array = $this->get_table_id_mention("comments","id");
            /*dd($inbound_cmt_id_array);*/
            $mention_comment_result=Comment::raw(function ($collection) use($filter,$mention_cmt_id_array,$post_id,$dateBegin,$dateEnd) {//print_r($filter);
 
                    return $collection->aggregate([
                        [
                        '$match' =>[
                             '$and'=> [ 
                             ['id'=> ['$exists'=> true]],
                             // ['post_id'=>['$in'=>$post_id]],
                             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
                             ['id'=>[ '$nin'=> $mention_cmt_id_array ] ],
                             $filter
                           
                            
                                      ]
                        ]  
                                   
                       ],
                       ['$sort' =>['created_time'=>-1]],
                       
                        
                    ]);
                })->toArray();
                  $total [] = count($mention_comment_result);
                  $profile_id=[];
                  $data_comment=[];
                 foreach ($mention_comment_result as  $key => $row) {
                                $id ='';
                                $message ='';
                                $post_id ='';$profile='';$page_name='';
                                $comment_count =0;$original_message='';$unicode='';
                                $sentiment='';$emotion='';$parent='';
                                $tags='';
                                if(isset($row['profile']))
                                {
                                $profile = $row['profile'];
                                $profile_id[]=new MongoDB\BSON\ObjectId((string)$profile);
                                } 

                                $utcdatetime = $row["created_time"];
                                $datetime = $utcdatetime->toDateTime();
                                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                                $datetime = $datetime->format('Y-m-d H:i:s');
                                if(isset($row['id'])) $id =$row['id'];
                                if(isset($row['message'])) $message = $row['message'];
                                if(isset($row['original_message'])) $original_message = $row['original_message'];
                                if(isset($row['unicode'])) $unicode = $row['unicode'];
                                if(isset($row['post_id'])) $post_id = $row['post_id'];
                                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                                if(isset($row['emotion'])) $emotion = $row['emotion'];
                                if(isset($row['parent'])) $parent =  $row['parent'];
                                if(isset($row['page_name'])) $page_name = $row['page_name'];
                           

                                  $data_comment[] =[
                                    'id' => $id,
                                    'created_time' =>$datetime,
                                    'message' => $message,
                                    'wb_message' =>'',
                                    'original_message' =>$original_message,
                                    'unicode' =>$unicode,
                                    'post_id' =>$post_id,
                                    'comment_count' =>$comment_count,
                                    'sentiment' =>'',
                                    'checked_sentiment' =>'',
                                    'decided_keyword' =>'',
                                    'emotion' =>'',
                                    'checked_emotion' =>'',
                                    'interest' =>'',
                                    'profile'=>$profile,
                                    'change_predict'=>0,
                                    'checked_predict'=>0,
                                    'parent'=>$parent,
                                    'isBookMark' =>0,
                                    'manual_date'=>'NULL',
                                    'page_name'=>$page_name,
                                    'created_at' => now()->toDateTimeString(),
                                    'updated_at' => now()->toDateTimeString()
                                    ];                 

                             }

                        $path = storage_path('app/data_output/mention_comments'.$project_id.'.csv');

                        if (file_exists($path)) {
                            unlink($path) ;
                        } 
                        $this->doCSV($path,$data_comment);
                    $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,interest,profile,change_predict,checked_predict,parent,isBookMark,manual_date,page_name,created_at,updated_at)";

                     $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
                     $pdo->exec($query_load);

            }
            return true;

    }

    public function get_updatePostReaction()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        $data =  [];
        foreach($projects as $project)
       {
            $id = $project->id;
            $monitor_pages = $project->monitor_pages;
            $monitor_pages = explode(',', $monitor_pages);

            $monitor_pages_count = count($monitor_pages);
            if($monitor_pages_count>0)
                {
                    $total = $this->Update_Reaction_Data($id,$monitor_pages);
                }
        }
        // dd($total);
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'update post reaction  Successfully.( Total = '. $total .') '. $date_time;
                 echo "";
    }
    public function Update_Reaction_Data($project_id,$monitor_pages)
    {
        $post_id = $this->getInboundPostId_Reaction($monitor_pages);
        $total  = 0;
        $inbound_post_result=MongoboundPost::raw(function ($collection) use($post_id) {//print_r($filter);

                    return $collection->aggregate([
                        [
                        '$match' =>[
                             '$and'=> [ 
                             ['id'=> ['$exists'=> true]],
                             ['id'=>[ '$in'=> $post_id ] ],
                         
                                      ]
                        ]  
                                   
                       ],
                        
                    ]);
                })->toArray();
          $total = count($inbound_post_result);
                foreach ($inbound_post_result as  $key => $row) {
                    $id='';$share =0;$Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;
                    if(isset($row['id'])) $id =$row['id'];
                    if(isset($row['share'])) $share = $row['share'];
                    if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
                    if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
                    if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
                    if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
                    if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
                    if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);
                    if($Like <> 0 || $Love <> 0 || $Wow <> 0  || $Haha <> 0  || $Sad <> 0  || $Angry <> 0  )
                    {
                        $res = DB::table("temp_inbound_posts")->where('id',$id)->update(
                            ['shared' => $share,'Liked' => $Like,'Love' => $Love,'Wow' => $Wow,'Haha' => $Haha,'Sad' => $Sad,'Angry' => $Angry,
                            'reaction_update' => 1,'updated_at' => now()->toDateTimeString()]);
                    }
               }
               return $total;
    }
    public function get_commentSentiForMention()
    {
            // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
            $data =  [];
        	// foreach($projects as $project)
        	// {
            $total  = 0;
            // $id = $project->id;
            $comment_table = "temp_comments";
            $comments = DB::connection('mysql_cron_wunzin')->table($comment_table)->select('id','wb_message')->where('sentiment','')->limit(500)->get();
            $total = count($comments);
            if(!$comments->isEmpty()){
              foreach($comments as $comment)
               {
                  $wb_message[] = $comment->wb_message;
                  $request['wb_message']= $comment->wb_message;
                  $request['id'] = $comment->id;

                  $data[]=$request;
                }
      
            if(count($wb_message) > 0)
           {
                  $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
                  $uri_sentiment = 'senti_emo_interest';
                 
                            $formData = array(
                    'raw' =>  $wb_message,
                    "threshold" => 0
                   
                );

                  $formData = json_encode($formData);
                  // $formData = htmlspecialchars(json_encode($formData), ENT_QUOTES, 'UTF-8');
                 try{
                        $api_response = $client->post($uri_sentiment, [
                                    'form_params' => [
                                    'raw' =>  $formData,
                                    ],
                                 ]);

                        $result = ($api_response->getBody()->getContents());
                        // dd($result);
                        $json_result_array = json_decode($result, true);
                     }
                    catch (\Exception $e) {
  
                            $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                            $error_message = explode('<html>',$error_message);
                            // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                            //dd($error_message[0]);
                            $error_message=$error_message[0];

                            //$error_message ='test';
                            $sms_data=json_encode([
                              'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                              'group_id' =>2,
                            ]);
                            $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                              'headers' => ['Content-Type' => 'application/json'],
                              'body' => $sms_data
                            ]);
                             //dd($sms_response->getBody()->getContents());
                              // return false;
                      }
                $result = $json_result_array[0];
                $count = (Int)count($data);
                for($i=0;$i<$count;$i++)
                {
              
                    $id = $data[$i]['id'];
                    $comment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'updated_at' => now()->toDateTimeString()]);
                    $sentiment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('id',$id)->where('checked_sentiment','')->update(['checked_sentiment' => $result['sentiment'][$i]]);
                }
            }
           }
           // }
               $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Sentiment predict for mention comment Successfully. (Total = '. $total .') '. $date_time;
                 echo "";
    }

    public function get_postSentiForMention()
    {
          // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
          $data =  [];
          // \Log::info($projects);
          // foreach($projects as $project)
          //  {
                // $id = $project->id;
                $total  = 0;
                $post_table = "temp_posts";
                $posts = DB::connection('mysql_cron_wunzin')->table($post_table)->select('id','wb_message')->where('sentiment','')->limit(500)->get();
                $total = count($posts);
                if(!$posts->isEmpty()){
                foreach($posts as $post)
               {
                  $wb_message[] = $post->wb_message;
                  $request['wb_message']= $post->wb_message;
                  $request['id'] = $post->id;

                  $data[]=$request;
               }
      
                if(count($wb_message) > 0)
                {
                  $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
                  $uri_sentiment = 'senti_emo_interest';
                 
                  $formData = array(
                    'raw' =>  $wb_message,
                    'threshold' => 0
                    );

                    $formData = json_encode($formData);
                    try{
                           $api_response = $client->post($uri_sentiment, [
                                                'form_params' => [
                                                'raw' =>  $formData,
                                                ],
                                             ]);

                            $result = ($api_response->getBody()->getContents());
                            $json_result_array = json_decode($result, true);
                        }

                    catch (\Exception $e) {

                            $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                            $error_message = explode('<html>',$error_message);
                            // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                            //dd($error_message[0]);
                            $error_message=$error_message[0];

                            //$error_message ='test';
                            $sms_data=json_encode([
                              'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                              'group_id' =>2,
                            ]);
                            $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                              'headers' => ['Content-Type' => 'application/json'],
                              'body' => $sms_data
                            ]);
                         //dd($sms_response->getBody()->getContents());
                          // return false;
                        }

  
                        $result = $json_result_array[0];
                        $count = (Int)count($data);
                        for($i=0;$i<$count;$i++)
                        {
                            $id = $data[$i]['id'];
                            // $post = DB::connection('mysql_cron_wunzin')->table($post_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                            // $sentiment = DB::connection('mysql_cron_wunzin')->table($post_table)->where('temp_id',$id)->where('checked_sentiment','')->update(['checked_sentiment' => $result['sentiment'][$i]]);
                            $post = DB::connection('mysql_cron_wunzin')->table($post_table)->where('id',$id)->update(['sentiment' => 'neutral','emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                            $sentiment = DB::connection('mysql_cron_wunzin')->table($post_table)->where('id',$id)->where('checked_sentiment','')->update(['checked_sentiment' => 'neutral']);
                        }

                   }
               }
           // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Sentiment predict for mention post Successfully. (Total = '. $total .') '. $date_time;
                 echo "";
    }
    public function get_commentSentiForInbound()
    {
        // $projects =DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        $data =  [];
        // foreach($projects as $project)
        // {
            $total  = 0;
            // $project_id = $project->id;
            $comment_table = "temp_inbound_comments";
            $comments = DB::connection('mysql_cron_wunzin')->table($comment_table)->select('id','wb_message')->where('sentiment','')->where('wb_message','<>','')->orderby('created_time','desc')->limit(500)->get();
            $total = count($comments);
            if(!$comments->isEmpty())
            {
                foreach($comments as $comment)
                {

                  $wb_message[] = $comment->wb_message;
                  $request['wb_message']= $comment->wb_message;
                  $request['id'] = $comment->id;
                  $data[]=$request;
                }
      
                if(count($wb_message) > 0)
                {
                      $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
                      $uri_sentiment = 'senti_emo_interest';
         
                      $formData = array(
                        'raw' =>  $wb_message,
                        'threshold'=>0
                    );
                      $formData = json_encode($formData);
                      try{
                           $api_response = $client->post($uri_sentiment, [
                                                'form_params' => [
                                                'raw' =>  $formData,
                                                
                                                ],
                                             ]);
                            $result = ($api_response->getBody()->getContents());
                            $json_result_array = json_decode($result, true);
                          }
                      catch (\Exception $e) {
                        $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());
                        $error_message = explode('<html>',$error_message);
                        // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                        //dd($error_message[0]);
                        $error_message=$error_message[0];

                        //$error_message ='test';
                        $sms_data=json_encode([
                          'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                          'group_id' =>2,
                        ]);
                        $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                          'headers' => ['Content-Type' => 'application/json'],
                          'body' => $sms_data
                        ]);
                         //dd($sms_response->getBody()->getContents());
                          // return false;
                       }

                        $result = $json_result_array[0];
                        // $this->info(dd($result));
                        //            return;
                        $count = (Int)count($data);

                        for($i=0;$i<$count;$i++)
                        {
                        // dd($result['tags'][$i]);
              
                            $id = $data[$i]['id'];
                            
                            //$comment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'tags' => $result['tags'][$i],'updated_at' => now()->toDateTimeString()]);
                            $comment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'updated_at' => now()->toDateTimeString()]);
                            //update checksentiment in another update query because sentiment API may train again next time. 
                            $sentiment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('id',$id)->where('checked_sentiment','')->update(['checked_sentiment' => $result['sentiment'][$i]]);

                              //    if($project_id != 17){
                              //   $tags = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('temp_id',$id)->where('checked_tags','')->update(['checked_tags' => $result['tags'][$i]]);
                              // }
                        }

                }
            } 
        // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Sentiment predict for inbound comment Successfully. (Total = '. $total .') '. $date_time;
                 echo "";
    }
    public function get_postSentiForInbound()
    {
        	// $projects =DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        	$data =  [];
      
		       //  foreach($projects as $project)
		       // {
            $total  = 0;
            	// $id = $project->id;
            $post_table = "temp_inbound_posts";
            $posts = DB::connection('mysql_cron_wunzin')->table($post_table)->select('id','wb_message')->where('sentiment','')->where('wb_message','<>','')->orderby('created_time','desc')->limit(500)->get();
            $total = count($posts);
             if(!$posts->isEmpty()){
             foreach($posts as $post)
             {
                  $wb_message[] = $post->wb_message;
                  $request['wb_message']= $post->wb_message;
                  $request['id'] = $post->id;
                  $data[]=$request;
             }
          
            if(count($wb_message) > 0)
            {
                  $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
                  $uri_sentiment = 'senti_emo_interest';
                 
                  $formData = array(
                        'raw' =>  $wb_message,
                        'threshold'=>0
                       
                    );

                   $formData = json_encode($formData);
                   try{
                   $api_response = $client->post($uri_sentiment, [
                                        'form_params' => [
                                        'raw' =>  $formData,
                                        ],
                                     ]);

                    $result = ($api_response->getBody()->getContents());
                    $json_result_array = json_decode($result, true);
                    }
                    catch (\Exception $e) {
                        $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());
                        $error_message = explode('<html>',$error_message);
                        // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                        //dd($error_message[0]);
                        $error_message=$error_message[0];

                        //$error_message ='test';
                        $sms_data=json_encode([
                          'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                          'group_id' =>2,
                        ]);
                        $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                          'headers' => ['Content-Type' => 'application/json'],
                          'body' => $sms_data
                        ]);
                         //dd($sms_response->getBody()->getContents());
                          // return false;
                        }
                        $result = $json_result_array[0];
                        $count = (Int)count($data);

                        for($i=0;$i<$count;$i++)
                        {
                            $id = $data[$i]['id'];
                            $post = DB::connection('mysql_cron_wunzin')->table($post_table)->where('id',$id)->update(['sentiment' => $result['sentiment'][$i],'checked_sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                            $sentiment = DB::connection('mysql_cron_wunzin')->table($post_table)->where('id',$id)->where('checked_sentiment','')->update(['checked_sentiment' => $result['sentiment'][$i]]);
                             
                        }

            }
          }
      		 // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Sentiment predict for inbound post Successfully. (Total = '. $total . ') '. $date_time;
                 echo "";
    }

    public function get_commentWbForMention()
    {
        // $query = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        // foreach($query as $project)
        // {
            $total  = 0;
            // $id = $project->id;
            $comment_table = 'temp_comments';
            $query = "select id,message from ".$comment_table." where message <> '' and message is not null and ( wb_message is null or wb_message ='') LIMIT 500 "; 
            $comments = DB::connection('mysql_cron_wunzin')->select($query);
            $total = count($comments);
            $data = [];
            $data_message = [];
            foreach($comments as $comment)
            {
                  $request['id'] = $comment->id;
                  $message = $comment->message;
                  $request['message']  = $message;
                      // $z_count = substr_count(strtolower($message), 'zawgyi');
                      // $u_count = substr_count(strtolower($message), 'unicode');
                      //  if ($z_count == 1 && $u_count == 1)
                      //  {
                        
                      //          $request['message'] = $this ->fontCheck($message);
                              
                      //  }
                      
                      //  else{
                      //   $request['message']  = $message;
                      // }

                  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$request['message']);
                  $data[] =$request;
              
            }

            if(count($data_message)> 0)
            {
                  $client = new Client(['base_uri' => 'http://35.185.97.177:5002/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
                  $uri_wb = 'wb_posTag';
                  $formData = array(
                    'raw' =>  $data_message,
                ); 
                   $formData = json_encode($formData);
                   try{
                       $wb_response = $client->post($uri_wb, [
                                            'form_params' => [
                                            'raw' =>  $formData,
                                            ],
                                         ]);
                   
                        $wb_result = ($wb_response->getBody()->getContents());
                       //
                        $json_wb_array = json_decode($wb_result, true);
                       }
                    catch (\Exception $e) {
                        $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                        $error_message = explode('<html>',$error_message);
                        // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                        //dd($error_message[0]);
                        $error_message=$error_message[0];

                        //$error_message ='test';
                        $sms_data=json_encode([
                          'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                          'group_id' =>2,
                        ]);
                        $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                          'headers' => ['Content-Type' => 'application/json'],
                          'body' => $sms_data
                        ]);
                         //dd($sms_response->getBody()->getContents());
                          // return false;
                        }

                        $wb_result = $json_wb_array['raw'];
                         // dd($wb_result);
                        $count = (Int)count($data);
                        for($i=0;$i<$count;$i++)
                        {
                            $id = $data[$i]['id'];
                            $wb =  DB::connection('mysql_cron_wunzin')->table($comment_table)->where('id', $id)->update(['wb_message' => $wb_result[$i],'message' => $data[$i]['message'],'updated_at' => now()->toDateTimeString()]);
                        }

       
            }
        
        // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Wb message for mention comment Successfully. (Total = ' .$total . ') '. $date_time;
                 echo "";
    }
    public function get_postWbForMention()
    {
        // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        // foreach($projects as $project)
        // {
        //     $total  = 0;
            $post_table = 'temp_posts';
            $query = "select id,message from ".$post_table." where message <> '' and message is not null and ( wb_message is null or wb_message ='') LIMIT 500 "; 
            $posts = DB::connection('mysql_cron_wunzin')->select($query);
            $total = count($posts);
            $data = [];
            $data_message = [];
            foreach($posts as $post)
            {
                  $request['id'] = $post->id;
                  $message = $post->message;
                  $request['message']  = $message;
                      // $z_count = substr_count(strtolower($message), 'zawgyi');
                      // $u_count = substr_count(strtolower($message), 'unicode');
                      //  if ($z_count == 1 && $u_count == 1)
                      //  {
                        
                      //          $request['message'] = $this ->fontCheck($message);
                              
                      //  }
                      
                      //  else{
                      //   $request['message']  = $message;
                      // }

                  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$request['message']);
                  $data[] =$request;
            }

            if(count($data_message)> 0)
           {
              $client = new Client(['base_uri' => 'http://35.185.97.177:5002/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
              $uri_wb = 'wb_posTag';
          
              $formData = array(
                 'raw' =>  $data_message,
                ); 
    
              $formData = json_encode($formData);
             try{
               $wb_response = $client->post($uri_wb, [
                                    'form_params' => [
                                    'raw' =>  $formData,
                                    ],
                                 ]);
           
                $wb_result = ($wb_response->getBody()->getContents());
                $json_wb_array = json_decode($wb_result, true);
                }
             catch (\Exception $e) {
  
                $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                $error_message = explode('<html>',$error_message);
                // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                //dd($error_message[0]);
                $error_message=$error_message[0];

                //$error_message ='test';
                $sms_data=json_encode([
                  'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                  'group_id' =>2,
                ]);
                $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                  'headers' => ['Content-Type' => 'application/json'],
                  'body' => $sms_data
                ]);
                 //dd($sms_response->getBody()->getContents());
                  // return false;
                }

                $wb_result = $json_wb_array['raw'];
                $count = (Int)count($data);
                for($i=0;$i<$count;$i++)
                {
                    $id = $data[$i]['id'];
                    $wb =  DB::connection('mysql_cron_wunzin')->table($post_table)->where('id', $id)->update(['wb_message' => $wb_result[$i],'message' => $data[$i]['message'],'updated_at' => now()->toDateTimeString()]);
                }
            }
        // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Wb message for mention post Successfully. (Total = ' .$total. ') '. $date_time;
                 echo "";
    }
    public function get_commentWbForInbound()
    {
        // $query = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        // foreach($query as $project)
        // {
            $total  = 0;
            $comment_table = 'temp_inbound_comments';
            $query = "select id,message from ".$comment_table." where message <> '' and message is not null and ( wb_message is null or wb_message ='') order by timestamp(created_time) DESC LIMIT 100 "; 
          
            $comments = DB::connection('mysql_cron_wunzin')->select($query);
            $total = count($comments);
            $data = [];
            $data_message = [];
            foreach($comments as $comment)
            {
                  $request['id'] = $comment->id;
                  $message = $comment->message;
                  $request['message']  = $message;
                      // $z_count = substr_count(strtolower($message), 'zawgyi');
                      // $u_count = substr_count(strtolower($message), 'unicode');
                      //  if ($z_count == 1 && $u_count == 1)
                      //  {
                        
                      //          $request['message'] = $this ->fontCheck($message);
                              
                      //  }
                      
                      //  else{
                      //   $request['message']  = $message;
                      // }
                  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$request['message']);
                  $data[] =$request;
              
            }

            if(count($data_message)> 0)
            {
              $client = new Client(['base_uri' => 'http://35.185.97.177:5002/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
              $uri_wb = 'wb_posTag';
              
              $formData = array('raw' =>  $data_message,); 
              $formData = json_encode($formData);
              try{
                   $wb_response = $client->post($uri_wb, [
                                        'form_params' => [
                                        'raw' =>  $formData,
                                        ],
                                     ]);
                    $wb_result = ($wb_response->getBody()->getContents());
                    $json_wb_array = json_decode($wb_result, true);
                  }
               catch (\Exception $e) {
      
                    $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                    $error_message = explode('<html>',$error_message);
                    // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                    //dd($error_message[0]);
                    $error_message=$error_message[0];

                    //$error_message ='test';
                    $sms_data=json_encode([
                      'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                      'group_id' =>2,
                    ]);
                    $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                      'headers' => ['Content-Type' => 'application/json'],
                      'body' => $sms_data
                    ]);
                     //dd($sms_response->getBody()->getContents());
                      // return false;
                    }

                    $wb_result = $json_wb_array['raw'];
                    $count = (Int)count($data);
                    for($i=0;$i<$count;$i++)
                    {
                        $id = $data[$i]['id'];   
                        if($wb_result[$i] != '')
                        {   
                            $wb =  DB::connection('mysql_cron_wunzin')->table($comment_table)->where('id', $id)->update(['wb_message' => $wb_result[$i],'message' => $data[$i]['message'],'updated_at' => now()->toDateTimeString()]);
                        }
                        else
                        {
                            $wb =  DB::connection('mysql_cron_wunzin')->table($comment_table)->where('id', $id)->update(['wb_message' => $data[$i]['message'],'message' => $data[$i]['message'],'updated_at' => now()->toDateTimeString()]);
                        }
                    }
             }   
       // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Wb message for inbound comment Successfully. (Total = ' .$total. ') '. $date_time;
                 echo "";
    }
    public function get_postWbForInbound()
    {
        // $query = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        // foreach($query as $project)
        // {
            $total  = 0;
            $post_table = 'temp_inbound_posts';
            $query = "select id,message from ".$post_table." where message <> '' and message is not null and ( wb_message is null or wb_message ='') order by created_time desc LIMIT 500 "; 
            $posts = DB::connection('mysql_cron_wunzin')->select($query);
            $total = count($posts);
            $data = [];
            $data_message = [];
            foreach($posts as $post)
            {
                  $request['id'] = $post->id;
                  $message = $post->message;
                  $request['message']  = $message;
                      // $z_count = substr_count(strtolower($message), 'zawgyi');
                      // $u_count = substr_count(strtolower($message), 'unicode');
                      //  if ($z_count == 1 && $u_count == 1)
                      //  {
                        
                      //          $request['message'] = $this ->fontCheck($message);
                              
                      //  }
                      
                      //  else{
                      //   $request['message']  = $message;
                      // }

                  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$request['message']);
                  $data[] =$request;
              
            }

            if(count($data_message)> 0)
            {
                  $client = new Client(['base_uri' => 'http://35.185.97.177:5002/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
                  $uri_wb = 'wb_posTag';
                  
                  $formData = array(
                    'raw' =>  $data_message,
                     ); 
            
                  $formData = json_encode($formData);
                  try{
                       $wb_response = $client->post($uri_wb, [
                                            'form_params' => [
                                            'raw' =>  $formData,
                                            ],
                                         ]);
                   
                        $wb_result = ($wb_response->getBody()->getContents());
                        $json_wb_array = json_decode($wb_result, true);
                     }
                   catch (\Exception $e) {
                        $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                        $error_message = explode('<html>',$error_message);
                        // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                        //dd($error_message[0]);
                        $error_message=$error_message[0];

                        //$error_message ='test';
                        $sms_data=json_encode([
                          'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                          'group_id' =>2,
                        ]);
                        $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                          'headers' => ['Content-Type' => 'application/json'],
                          'body' => $sms_data
                        ]);
                         //dd($sms_response->getBody()->getContents());
                          // return false;
                        }

                        $wb_result = $json_wb_array['raw'];
                        $count = (Int)count($data);
                        for($i=0;$i<$count;$i++)
                        {
                            $id = $data[$i]['id'];
                            $wb =  DB::connection('mysql_cron_wunzin')->table($post_table)->where('id', $id)->update(['wb_message' => $wb_result[$i],'message' => $data[$i]['message'],'updated_at' => now()->toDateTimeString()]);
                        }

       
            }
        
       // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Wb message for inbound post Successfully.(Total = ' .$total. ') '. $date_time;
                 echo "";
    }
    public function add_profiles()
    {
        // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        // foreach($projects as $project)
        // {
        //         $id = $project->id;
                // $id = '17';
                // $comment_table = "temp_".$id."_inbound_comments";
                $id_array = "SELECT * FROM temp_inbound_comments WHERE profile NOT IN ( SELECT temp_id FROM temp_profiles) AND profile <> '' ORDER BY timestamp(created_time) DESC";
                $id_array = DB::select($id_array);
                $total  = 0;
                foreach($id_array as $result)
                { 
                      $profile_id = $result->profile;
                      if( $profile_id <> '') {
                          $profile_result=MongoProfile::raw(function ($collection) use($profile_id) {
                             return $collection->aggregate([
                                    [
                                    '$match' =>
                                         ['_id'=> new ObjectId($profile_id) ],
                                    
                                               
                                   ],
                                    
                                ]);
                        
                             })->toArray();
                            $total = count($profile_result);
                            // dd($total);
                              foreach($profile_result as $data)
                              {
                                      $_id ='';$name='';$type='';$profile_id='';
                           
                                      if(isset($data['name'])) $name = $data['name']; 
                                      if(isset($data['_id'])) $_id = $data['_id']; 
                                      if(isset($data['type'])) $type = $data['type']; 
                                      if(isset($data['id'])) 
                                      {
                                        $profile_id = $data['id']; 
                                      }
                                      else if (isset($data['href'])) 
                                      {
                                        $profile_id = $data['href']; 
                                      }
                                      // $profile_data = Profile::firstOrCreate(['temp_id' => $_id , 'id' => $profile_id ,'name' => $name , 'type' => $type]);
                                      $query = "select * from temp_profiles WHERE id='".$profile_id."'";
                                      $data = DB::connection('mysql_cron_wunzin')->select($query);
                                      if(!isset($data))
                                      {
                                        $row = "insert into temp_profiles('temp_id','id','name','type') values($_id,$profile_id,$name,$type)";
                                        $new_data = DB::connection('mysql_cron_wunzin')->select($row);
                                      }
                    
                              }
                          }
                }
        // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Profiles inserted Successfully.(Total = ' .$total. ') '. $date_time;
                 echo "";
    }
   
    public function update_profiles()
    {
        // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        // foreach($projects as $project)
        // {
                $total  = 0;
        //         $id = $project->id;
                $comment_table = "temp_inbound_comments";
                $id_array = "Select profile from ".$comment_table." where (profile is not NULL or profile <> '') order by timestamp(created_time) desc limit 100";
                $id_array = DB::connection('mysql_cron_wunzin')->select($id_array);
                foreach($id_array as $result)
                { 
                       $profile_id = $result->profile;
                          if( $profile_id <> '') {
                                   
                                  $profile_result=MongoProfile::raw(function ($collection) use($profile_id) {
                                        return $collection->aggregate([
                                            [
                                            '$match' =>
                                                 ['_id'=> new ObjectId($profile_id) ],
                                            
                                                       
                                           ],
                                            
                                        ]);
                                
                                     })->toArray();

                                  // dd($profile_result);  
                                  $total = count($profile_result);
                                  foreach($profile_result as $data)
                                  {
                                        $_id ='';$name='';$type='';$id='';

                                        if(isset($data['name'])) $name = $data['name']; 
                                        if(isset($data['_id'])) $_id = $data['_id']; 
                                        if(isset($data['_id'])) $type = $data['type']; 
                                        if(isset($data['_id'])) $id = $data['id']; 
                                       
                                        // DB::table('temp_profiles')->where('temp_id',$_id)->first()->update(['name' => $name]);
                                        DB::connection('mysql_cron_wunzin')->table('temp_profiles')
                                            ->where('temp_id', $_id)
                                            ->update(['name' => $name,'type' =>$type,'id'=>$id,'updated_at' => now()->toDateTimeString()]);
                                  }
                              } 
                }
        // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Profiles updated Successfully.(Total = ' .$total. ') '. $date_time;
                 echo "";
    }
    // public function tag_mentionComment()
    // {
    //     $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
    //     foreach($projects as $project){
    //     $total  = 0;
    //     $project_id = $project->id;
    //     $table = 'temp_'.$project_id.'_comments';
    //     $comments = DB::connection('mysql_cron_wunzin')->table($table)->select('temp_id','message')->get();
    //     $total = count($comments);
    //     $tags =$this->gettags($project_id); 
    //     // dd($comments);
    //     foreach ($comments as $comment) {
    //          $message = $comment->message;
    //          $id = $comment->temp_id;
    //          $tag_string = ""; 
    //          foreach ($tags as $key => $value) {
    //           $kw = $value->keywords;
    //           $kw = explode(",",$kw);
    //          foreach ($kw as $key_kw => $value_kw) {
    //             if (!empty($value_kw)) 
    //             {
    //                 if (strpos($message, $value_kw) !== false)
    //                 {
    //                      $tag_string.= ',' .$value->name;
    //                 }
    //             }
    //           }
    //         }
    //        if($tag_string != ""){
    //          $tag_string = substr($tag_string, 1); 
    //         }     

    //        $tag =  DB::connection('mysql_cron_wunzin')->table($table)->where('temp_id', $id)->update(['tags' => $tag_string,'updated_at' => now()->toDateTimeString()]);
       
    //     }
    //   }
    //             $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
    //             $date_time = $date->format('d-m-Y H:i:s');
    //             echo 'Tag mention comment Successfully.(Total = ' .$total. ') '. $date_time;
    //              echo "";
    // }
    public function tag_inboundComment()
    {
        // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        // $project_id = $project->id;
        $table = 'temp_inbound_comments';
        $comments = DB::connection('mysql_cron_wunzin')->table($table)->select('temp_id','id','message','interest')->whereRaw("tag_flag = 0 and (sentiment != '' or message ='')  and (checked_tags_id = '' or checked_tags_id is null or checked_tags_id = ' ')")->orderby('created_time','DESC')->LIMIT(1000)->get();
        $total = count($comments);
        // dd(auth()->user()->brand_id);
        $tags =$this->gettags(34); 
        // dd($tags);
        foreach ($comments as $comment) 
        {
            $message = $comment->message;
            //$message = "ကြေငြာပေးပါအုံးနော်";
            $interest = $comment->interest;
            $message=preg_replace('/(\r\n|\r|\n|\s)+/', "",$message);
            $message = strtolower($message);
            $id = $comment->temp_id;
            //$id = 186636;
            $tag_string = "";  
            $tags_id = "";
                // added by ztd  (if interest is 1 added Enquiry tag)
            $enq_id = DB::table('tags')->select('id')->where('name','Enquiry')->get()->toArray();
            foreach($enq_id as $res) 
            {
                $enq_id = $res->id;
            }
                foreach ($tags as $key => $value)
            {
                if($interest == 1 && $value->id == $enq_id )
                {
                    $tag_string.= ',Enquiry';
                    $tags_id.= ',' .$enq_id;
                    continue;
                }
                $kw = $value->keywords;
                $kw = explode(",",$kw);
                foreach ($kw as $key_kw => $value_kw)
                {
                    $value_kw=preg_replace('/(\r\n|\r|\n|\s)+/', "",$value_kw);
                    $value_kw = strtolower($value_kw);
                    if($value_kw != '-' && $value_kw != '' && $value_kw != NULL && isset($value_kw))
                    {
                        if (strpos($message,$value_kw) > -1)
                        {
                            $tag_string.= ',' .$value->name;
                            $tags_id.= ',' .$value->id;
                            break;
                        }
                    }

                }
            }
            if($tag_string != "")
            {
                $tag_string = substr($tag_string, 1); 
            }    
            if($tags_id != "")
            {
                $tags_id = substr($tags_id, 1); 
            }  
            $tag =  DB::connection('mysql_cron_wunzin')->table($table)->where('temp_id', $id)->update(['tags' => $tag_string,'tags_id' => $tags_id,'checked_tags' => $tag_string,'checked_tags_id' => $tags_id,'tag_flag'=> 1,'updated_at' => now()->toDateTimeString()]);
            //dd($id);
        }
        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
        $date_time = $date->format('d-m-Y H:i:s');
        echo 'Tag inbound comment Successfully.(Total = ' .$total. ') '. $date_time;
        echo "";
    }
    public function get_web_mention_data()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->whereIn('id', [34, 35,38])->get();
        // dd($projects);
        $data =  [];
        $total = 0;
       foreach($projects as $project)
      {
        $id = $project->id;
        $total = $this->Insert_Web_Mention_Data($id);
      }
      // dd($total);
      $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
      $date_time = $date->format('d-m-Y H:i:s');
      echo 'get web mention data  Successfully.( Total='. $total .' ) '. $date_time;
      DB::connection('mysql_cron_wunzin')->table('tbl_logs')->where('table_name', '=', 'web_mention_data')->delete();
      DB::connection('mysql_cron_wunzin')->table('tbl_logs')->insert(['table_name' => 'web_mention_data', 'lastUpdated' => $date]);
    }
    public function Insert_Web_Mention_Data($project_id)
    {
        // dd("HY");
        $total =0;
        $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];

        $today = date('d-m-Y h:i:s');//22-09-2018//  date('d-m-Y')

        $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
        $date_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));
          
        $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
        $date_End=date('Y-m-d h:i:s',strtotime($today_plus));
        // $date_End=date('Y-m-d', strtotime('+1 day', strtotime($date_End))); /*added at 2/feb/2019*/
        $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);
     
        $keyword_data = $this->getprojectkeyword($project_id);

        if(isset($keyword_data [0]['main_keyword']))
        {
             $filter_test=[];
             $filter_condition_or=[];
             $filter['$or'] = $this->getkeywordfilter($keyword_data,$project_id);
             // dd($filter);
             // $pagename = $this->getpagename($project_id);//this filter will use later to filter (to avoid including data from monitor pages)
             // $pagename= explode(',', $pagename[0]["monitor_pages"]);
             // $pagename= $this->Format_Page_Name($pagename);

            // $filter_condition[] = $filter_condition_or;
            // dd($filter_condition_or);
            // $page['$or']= $this->getpagefilter($other_pages);
         
             // dd($dateBegin,$dateEnd);
             $web_mention_id_array = $this->get_table_id_mention("web_mentions","id");
             // dd($web_mention_id_array);
             $web_mention_result=Website::raw(function ($collection) use($filter,$web_mention_id_array,$dateBegin,$dateEnd) {//print_r($filter);

                return $collection->aggregate([
                    [
                    '$match' =>[
                         '$and'=> [ 
                         ['id'=> ['$exists'=> true]],
                         [ 'id'=>[ '$nin'=> $web_mention_id_array ] ],
                         ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
                          $filter
                                  ]
                    ]  
                               
                   ],
                   ['$sort' =>['created_time'=>-1]],
                   // ['$limit' =>2000]
                 
                    
                  ]);
            })->toArray();
             $total =  count($web_mention_result);
            // dd($web_mention_result);
             $data=[];
             $page_array=[];
             $data_message=[];

        foreach ($web_mention_result as  $key => $row) {
                $id ='';$datetime=NULL;
                $message ='';$unicode = '';
                $page_name ='';$title = '';
                
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['title'])) $title = $row['title'];
                if(isset($row['created_time']))
                {
                    $utcdatetime = $row["created_time"];
                    $datetime = $utcdatetime->toDateTime();
                    $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                    $datetime = $datetime->format('Y-m-d H:i:s');
                }
             
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
               


                  $data[] =[
                    'id' => $id,
                    'unicode' => $unicode,
                    'title'=> $title,
                    'message' => $message,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'sentiment' =>'',
                    'checked_sentiment' =>'',
                    'emotion' => '',
                    'checked_emotion' => '',
                    'created_time' =>$datetime,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

            }
            // dd($data);
         $path = storage_path('app/data_output/web_mention_'.$project_id.'.csv');

        if (file_exists($path)) {
            unlink($path) ;
        } 
        $this->doCSV($path,$data);

        $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_web_mentions FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,unicode,title,message,wb_message,page_name,sentiment,checked_sentiment,emotion,checked_emotion,created_time,created_at,updated_at)";


        $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
        $pdo->exec($query_load);


         }
             return $total;
    }  /*end of get_mention_data*/
    public function get_WbForWebMention()
    {
        // $query = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        // foreach($query as $project)
        // {
            $total  = 0;
            // $id = $project->id;
            $table = 'temp_web_mentions';
            $query = "select id,message from ".$table." where message <> '' and message is not null and ( wb_message is null or wb_message ='') order by timestamp(created_time) DESC LIMIT 500 "; 
            $result = DB::connection('mysql_cron_wunzin')->select($query);
            $total = count($result);
            $data = [];
            $data_message = [];
            foreach($result as $row)
            {
                  $request['id'] = $row->id;
                  $message = $row->message;
                  $request['message']  = $message;
                      // $z_count = substr_count(strtolower($message), 'zawgyi');
                      // $u_count = substr_count(strtolower($message), 'unicode');
                      //  if ($z_count == 1 && $u_count == 1)
                      //  {
                        
                      //          $request['message'] = $this ->fontCheck($message);
                              
                      //  }
                      
                      //  else{
                      //   $request['message']  = $message;
                      // }

                  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$request['message']);
                  $data[] =$request;
              
            }

            if(count($data_message)> 0)
            {
                  $client = new Client(['base_uri' => 'http://35.185.97.177:5002/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
                  $uri_wb = 'wb_posTag';
                  $formData = array(
                    'raw' =>  $data_message,
                ); 
                   $formData = json_encode($formData);
                   try{
                       $wb_response = $client->post($uri_wb, [
                                            'form_params' => [
                                            'raw' =>  $formData,
                                            ],
                                         ]);
                   
                        $wb_result = ($wb_response->getBody()->getContents());
                       //
                        $json_wb_array = json_decode($wb_result, true);
                       }
                    catch (\Exception $e) {
                        $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                        $error_message = explode('<html>',$error_message);
                        // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                        //dd($error_message[0]);
                        $error_message=$error_message[0];

                        //$error_message ='test';
                        $sms_data=json_encode([
                          'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                          'group_id' =>2,
                        ]);
                        $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                          'headers' => ['Content-Type' => 'application/json'],
                          'body' => $sms_data
                        ]);
                         //dd($sms_response->getBody()->getContents());
                          // return false;
                        }

                        $wb_result = $json_wb_array['raw'];
                         // dd($wb_result);
                        $count = (Int)count($data);
                        for($i=0;$i<$count;$i++)
                        {
                            $id = $data[$i]['id'];
                            $wb =  DB::connection('mysql_cron_wunzin')->table($table)->where('id', $id)->update(['wb_message' => $wb_result[$i],'message' => $data[$i]['message'],'updated_at' => now()->toDateTimeString()]);
                        }

       
            }
        
        // }
                $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Wb for web mention  Successfully. (Total = ' .$total . ') '. $date_time;
                 echo "";
    }
    public function get_SentiForWebMention()
    {
	        // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
	        $data =  [];
	        // foreach($projects as $project)
	        // {
            $total  = 0;
            // $id = $project->id;
            $table = "temp_web_mentions";
            $result = DB::connection('mysql_cron_wunzin')->table($table)->select('id','wb_message')->where('sentiment','')->limit(500)->get();
            $total = count($result);
            if(!$result->isEmpty()){
              foreach($result as $row)
               {
                  $wb_message[] = $row->wb_message;
                  $request['wb_message']= $row->wb_message;
                  $request['id'] = $row->id;

                  $data[]=$request;
                }
      
            if(count($wb_message) > 0)
           {
                  $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
                  $uri_sentiment = 'senti_emo_interest';
                 
                            $formData = array(
                    'raw' =>  $wb_message,
                    "threshold" => 0
                   
                );

                  $formData = json_encode($formData);
                  // $formData = htmlspecialchars(json_encode($formData), ENT_QUOTES, 'UTF-8');
                 try{
                        $api_response = $client->post($uri_sentiment, [
                                    'form_params' => [
                                    'raw' =>  $formData,
                                    ],
                                 ]);

                        $result = ($api_response->getBody()->getContents());

                        $json_result_array = json_decode($result, true);
                     }
                    catch (\Exception $e) {
  
                            $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                            $error_message = explode('<html>',$error_message);
                            // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                            //dd($error_message[0]);
                            $error_message=$error_message[0];

                            //$error_message ='test';
                            $sms_data=json_encode([
                              'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                              'group_id' =>2,
                            ]);
                            $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                              'headers' => ['Content-Type' => 'application/json'],
                              'body' => $sms_data
                            ]);
                             //dd($sms_response->getBody()->getContents());
                              // return false;
                      }
                $result = $json_result_array[0];
                $count = (Int)count($data);
                for($i=0;$i<$count;$i++)
                {
              
                    $id = $data[$i]['id'];
                    // $comment = DB::connection('mysql_cron_wunzin')->table($table)->where('id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                    // $sentiment = DB::connection('mysql_cron_wunzin')->table($table)->where('id',$id)->where('checked_sentiment','')->update(['checked_sentiment' => $result['sentiment'][$i]]);
                    $comment = DB::connection('mysql_cron_wunzin')->table($table)->where('id',$id)->update(['sentiment' => 'neutral','emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                    $sentiment = DB::connection('mysql_cron_wunzin')->table($table)->where('id',$id)->where('checked_sentiment','neutral')->update(['checked_sentiment' => $result['sentiment'][$i]]);
                }
            }
   		  }
     			 // }
               $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                $date_time = $date->format('d-m-Y H:i:s');
                echo 'Sentiment predict for web mention Successfully. (Total = '. $total .') '. $date_time;
                 echo "";
    }
    public function update_articleTitle()
    {
        // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->whereIn('id', [34, 35])->select('*')->get();
        // foreach ($projects as $project)
        // {
            $id_array = [];
            // $project_id = $project->id;
            // $project_id = 34;
            $total  = 0;
            // $id_array = $this->get_idArray($project_id,'web_mentions');
            $query = "SELECT id from temp_web_mentions where titleUpdated = 0 order by timestamp(created_time) desc limit 1000";
            $id_result = DB::connection('mysql_cron_wunzin')->select($query);
            foreach($id_result as $res) $id_array[] = $res->id;

            // $last_updated_date = $this->get_lastUpdatedDate('update_article_title');

            $comment_result=Website::raw(function ($collection) use($id_array) { //print_r($id_array);
            return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   // ['updatedAt' => ['$gte' => $last_updated_date]],
                   // ['updatedAt'=> ['$exists'=> true]],
                   ['id'=> ['$exists'=> true]],
                   ['title'=> ['$exists'=> true]],
                   ['id'=>[ '$in'=> $id_array ] ]
                            ] 
                          ],
              ]]);
             })->toArray();
            $total = count($comment_result);
            // dd($total);
             $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
             $updated_time = $date->format('Y-m-d H:i:s');
            
            foreach($comment_result as $row)
            {
                $title='';$id='';

                if(isset($row['title'])) $title=$row['title'];
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['created_time']))
                {
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                }
               
      
                  
                    DB::table("temp_web_mentions")->where('id',$id)->update([
                       
                       'title' =>$title,
                       'titleUpdated' => 1 ,
                       'created_time' =>$datetime,
                       'updated_at' => now()->toDateTimeString()
                     ]);
            }
            
      // }
        DB::table('tbl_logs')->where('table_name','update_article_title')->update(['lastUpdated' => $updated_time]); 
         $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
         $date_time = $date->format('d-m-Y H:i:s');
         echo 'update article  title  Successfully. (Total = ' .$total. ') '. $date_time;
         echo "";
    } /*end of update mention post*/
    public function update_article()
    {
        // $projects = DB::connection('mysql_cron_wunzin')->table('projects')->whereIn('id', [34, 35])->select('*')->get();
        // foreach ($projects as $project)
        // {
            $id_array = [];
            // $project_id = $project->id;
            // $project_id = 34;
            $total  = 0;
            $id_array = $this->get_idArray('web_mentions');
            // $query = "SELECT id from temp_".$project_id."_web_mentions order by timestamp(created_time) desc";
            // $id_result = DB::connection('mysql_cron_wunzin')->select($query);
            // foreach($id_result as $res) $id_array[] = $res->id;

            // $last_updated_date = $this->get_lastUpdatedDate('update_article_title');

            $comment_result=Website::raw(function ($collection) use($id_array) { //print_r($id_array);
            return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   // ['updatedAt' => ['$gte' => $last_updated_date]],
                   // ['updatedAt'=> ['$exists'=> true]],
                   ['id'=> ['$exists'=> true]],
                   // ['link'=> ['$exists'=> true]],
                   ['id'=>[ '$in'=> $id_array ] ]
                            ] 
                          ],
              ]]);
             })->toArray();
            $total = count($comment_result);
            // dd($total);
             $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
             $updated_time = $date->format('Y-m-d H:i:s');
            
            foreach($comment_result as $row)
            {
                $title='';$id='';$message='';$page_name='';$unicode='';$link='';

                if(isset($row['title'])) $title=$row['title'];
                if(isset($row['unicode'])) $unicode=$row['unicode'];
                if(isset($row['message'])) $message=$row['message'];
                if(isset($row['page_name'])) $page_name=$row['page_name'];
                if(isset($row['link'])) $link=$row['link'];
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['created_time']))
                {
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                }
               
      
                  
                    DB::table("temp_web_mentions")->where('id',$id)->update([
                       
                       'title' =>$title,
                       'unicode' => $unicode,
                       'message'=> $message,
                       'page_name' => $page_name,
                       'link' => $link,
                       'created_time' =>$datetime,
                       'updated_at' => now()->toDateTimeString()
                     ]);
            }
            
      // }
        DB::table('tbl_logs')->where('table_name','update_article')->update(['lastUpdated' => $updated_time]); 
         $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
         $date_time = $date->format('d-m-Y H:i:s');
         echo 'update all article  data  Successfully. (Total = ' .$total. ') '. $date_time;
         echo "";
    } /*end of update mention post*/
}
