<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManageDB extends Model
{
    protected $table = 'manage_dbs';
	protected $dates = ['created_at'];
    protected $fillable = [
        'id','user_id','db_name','brand_id','created_at',
    ];
}
