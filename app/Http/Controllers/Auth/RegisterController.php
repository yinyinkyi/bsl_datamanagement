<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\ManageDB;
use App\Project;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\GlobalController;
use Illuminate\Support\Facades\Input;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use GlobalController;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest');
    }

 

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
       public function showRegistrationForm()
    {
          /*$this->guard()->login($user);*/

          $title="Register";
        //    $source=Input::get('source');
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
            return view('auth.register')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('permission_data',$permission_data)
                                    ->with('title',$title);
                                
    }
    public function show_projectRegister()
    {
       
          $title="Project Register";
          $usernames = User::select('username','id')->get();
          $projects = Project::select('name','id')->get();
          if(empty($usernames)) $usernames = [];

          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
            return view('auth.project_register')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('permission_data',$permission_data)
                                    ->with('title',$title)
                                    ->with('usernames',$usernames)
                                    ->with('projects',$projects);
                                
    }
    public function ProjectRegister(Request $request)
    {
        // dd($this->validator($request->all())->validate());
        $project_id = $request->project_select;
        // dd($project_id);
        $user_id = $request->user_select;
        ManageDB::create(['user_id'=>$user_id,'brand_id' => $project_id,'db_name'=>'social_listener_wunzinn2home','created_at'=> now()->toDateTimeString(),'updated_at' => now()->toDateTimeString()]);
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);

        return redirect('projectRegister')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('message', 'Successfully assigned project to user.');
    }

      public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        
        $data = User::create(['name' => $request->name,'username' => $request->username,'email' => $request->email ,'password' => bcrypt($request->password) ,'role' => $request->role]);
        $user_id =$data->id;
        ManageDB::create(['user_id'=>$user_id,'db_name'=>'social_listener_wunzinn2home','created_at'=> now()->toDateTimeString(),'updated_at' => now()->toDateTimeString()]);
        /*$this->guard()->login($user);*/
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);

        return redirect('register')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('message', 'Successfully created a new account.');
    }
}
