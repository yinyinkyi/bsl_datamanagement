@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')


<div class="row page-titles">
  <div class="col-md-5 col-8 align-self-center">
    <h3 class="text-themecolor m-b-0 m-t-0">Posts</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="javascript:void(0)">Posts</a></li>
      <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>
    </ol>
  </div>
  <div class="col-md-7 col-4 align-self-center">
    <div class="d-flex m-t-10 justify-content-end">
       <div class="d-flex m-r-20 m-l-10 hidden-md-down">
        <div class='input-group mb-3'>
          <input type='text' class="form-control dateranges" style="color:#01c0c8 !important;width: 17em; padding: .2em .2em 0 ;" />
          <div class="input-group-append">
            <span class="input-group-text">
              <span class="ti-calendar"></span>
            </span>
          </div>
        </div>
      </div>
   <!--    <div class="">
        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
      </div> -->
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

<!-- Row -->

<!-- Row -->

<div class="row">
  <!-- Column -->
  <div class="col-lg-12">
    <div class="card">
 
        <ul class="nav nav-tabs profile-tab" role="tablist">
         <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Popular" role="tab">Posts</a> </li>
 
        </ul>
         <div class="tab-content">
         <div class="tab-pane active" id="Popular" role="tabpanel">
          <div class="card-body">
            <div class="d-flex no-block align-items-center">
                  <div><span class="text-primary" id="highlight_keyword" style="display:none">This is keyword name</span>
                      
                    @foreach($permission_data as $permission_data)
            @if ($permission_data['removing_keyword'] === 1)
             
                 <button type="button" class="btn btn-secondary btn-outline" name="btn_remove" id="btn_remove" style="display:none" data-toggle="tooltip" data-placement="top" title="Remove from highlighted">Remove Keyword</button>
                     @endif
                     @endforeach
                   
                  
             </div>
           <div class="ml-auto" style="float:right;">
 
            <ul class="list-inline">

              <li>
               <button type="button" class="btn btn-success" name="bookmark" id="btn_bookmark">Bookmark</button>
              </li>

            </ul>
          </div>
        </div>
         <div style="display:none"  align="center" style="vertical-align: top;" id="modal-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
        <table id="all_mention" class="table" style="width:100%;">
          <thead style='display:none;'>
            <tr>
              <th>Rendering engine</th>
              <th>Setting</th>
            </tr>
          </thead>

        </table>
         </div>
            </div>
            <div id="show-task" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Comment Type</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="message-box">
                                <div class="comment-widgets message-scroll  comment_data">
                                    <!-- Message -->
                                  
                                   
                                </div>
                            </div>
                                                            </div>
                                                     
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
         
       </div>
     

  
    </div>
  </div>

</div>




@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer> </script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script type="text/javascript">

  var startDate;
  var endDate;
  var mention_total;
  var bookmark_array=[];
  var bookmark_remove_array=[];
  var bookmark_comment_array=[];
  var bookmark_comment_remove_array=[];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

 $(document).ready(function() {

  $("#range_influencer").ionRangeSlider({
    grid: true,
    min: 0,
    max: 10,
    from: 0,
    prefix: "Score ",
    max_postfix: "+"
  });

  startDate = moment().subtract(3, 'month');
  endDate = moment();


  var periodType= '';
  $('.btn-group .active').each(function(){
    periodType= $(this).attr('id'); 

  });
  if(periodType == '')
  {
    periodType='month';
  }
  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };
 
  if(GetURLParameter('from_graph') === 'highlighted')
  {
    $("#highlight_keyword").html(GetURLParameter('highlight_text'));
    $('#highlight_keyword').show();
    $('#btn_remove').show();
  }

  function ChooseDate(start,end,keyword,calType)
{//alert(start);alert(end),alert(keyword)
 $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
 startDate=start;
 endDate=end;


 var brand_id = GetURLParameter('pid');


          Create_DataTable(keyword);
      

        }




$('.dateranges').daterangepicker({
  locale: {
    format: 'MMMM D, YYYY'
  },
  ranges: {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  startDate: startDate,
  endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'','');
      });




$(".btn-group > .btn").click(function(){
 periodType= $(this).attr('id'); 
       //$(this).addClass("active");
       ChooseDate(startDate,endDate,'','cal_graph');
         //Create_DataTable();


       });



ChooseDate(startDate,endDate);


function GetStartDate()
{
            //alert(startDate);
            return startDate.format('YYYY MM DD');
          }
          function GetEndDate()
          {
           // alert(endDate);
           return endDate.format('YYYY MM DD');
         }
    //Create_DataTable();


    function Create_DataTable(keyword)
    {

     All_Mention_DataTable(keyword);
   

    }

    function All_Mention_DataTable(keyword)
    {
      console.log(GetURLParameter('from_graph'));
      $(".popup").unbind('click');
 var oTable = $('#all_mention').DataTable({

        "lengthChange": false,
        "searching": true,
        "processing": true,
        "serverSide": true,
        "destroy": true,
        "ordering": false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getInboundPostPoint') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
          "data": {
            "fday": GetURLParameter('fday'),
            "sday": GetURLParameter('sday'),
            "brand_id": GetURLParameter('pid'),
            "period": GetURLParameter('period'),
            "type": GetURLParameter('type'),
            "category": GetURLParameter('category'),
            "from_graph": GetURLParameter('from_graph'),
            "emotion_type": GetURLParameter('emotion_type'),
            "highlight_text": GetURLParameter('highlight_text'),
            "keyword": GetURLParameter('keyword'),
          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
        },

        columns: [
        {data: 'post_div', name: 'post_div',orderable: false,searchable: true},
        {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        
      }).on('click', '.btn-bookmark', function () {
       //change css to bookmark star

       var current = $(this).attr('class');
       var id = $(this).attr('id');
       var name = $(this).attr('name');
  //if no bookmark prior 1- Change it to Bookmark css 2- Push to bookmark_array 3- remove from bookmark remove array if it is exist
       if(current == 'mdi mdi-star-outline text-yellow btn-bookmark')
       {
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star text-yellow btn-bookmark');
        if (jQuery.inArray(id, bookmark_array)=='-1') {
         bookmark_array.push(id) ;
        //need to remove it is exist in remove array
           if (jQuery.inArray(id, bookmark_remove_array)!='-1') {
        
              bookmark_remove_array.splice(jQuery.inArray(id,bookmark_remove_array), 1);
            

          } 
    
        } 

      }
//if  bookmark prior 1- Change it to No Bookmark css 2- Push to bookmark_remove_array 3- remove from bookmark array if it is exist
      else
      {
      /*  console.log("remove");
        console.log(name);*/
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star-outline text-yellow btn-bookmark');
           if (jQuery.inArray(id, bookmark_remove_array)=='-1') {
            bookmark_remove_array.push(id) ;
        if (jQuery.inArray(id, bookmark_array)!='-1') {
        
              bookmark_array.splice(jQuery.inArray(id,bookmark_array), 1);
            

          } 
    

          }
    
      //  console.log(bookmark_remove_array);

        
     }

    
    
  }).on('click', '.popup', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
 
    var post_id = $(this).attr('id');

    $("#modal-spin").show();
    $(".comment_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getInboundcomments") }}',
         type: 'GET',
         data: {id:post_id,cmt_type:name,brand_id:GetURLParameter('pid')},
         success: function(response) { console.log(response);
          var data=JSON.parse(response);
          console.log(data);

            for(var i in data) {
                        
              var html ='<div class="d-flex flex-row comment-row"> '+
                  ' <div class="user-img"> <span class="round">A</span> <span class="profile-status away pull-right"></span> </div> '+
                    '    <div class="comment-text w-100" style="padding:15px 0px 15px 10px;"> '+
               '<span class="text-muted pull-right">'+data[i].created_time+'</span>';
               html+='<p>Sentiment: <select class="form-control custom-select sentiment-color comment-select" id="sentiment_comment_'+data[i].id+'"  >';
                 if(data[i].sentiment == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="pos"';
                if(data[i].sentiment == "pos")
                html+= 'selected="selected"';
                html+= '>pos</option><option value="neg"';
                if(data[i].sentiment == "neg")
                html+= 'selected="selected"';
                html+= '>neg</option><option value="neutral"';
                if(data[i].sentiment == "neutral")
                html+= 'selected="selected"';      
                html+= '>neutral</option><option value="NA"';
                if(data[i].sentiment == "NA")
                html+= 'selected="selected"';  
                html+= '>NA</option></select>';

                html+= ' Emotion: <select class="form-control custom-select emotion-color comment-select" id="emotion_comment_'+data[i].id+'"  >';
                if(data[i].emotion == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="anger"';
                if(data[i].emotion == "anger")
                html+= 'selected="selected"';
                html+= '>anger</option><option value="interest"';
                if(data[i].emotion == "interest")
                html+= 'selected="selected"';     
                html+= '>interest</option><option value="disgust"';
                if(data[i].emotion == "disgust")
                html+= 'selected="selected"';  
                html+= '>disgust</option><option value="fear"';
                if(data[i].emotion == "fear")
                html+= 'selected="selected"'; 
                html+= '>fear</option><option value="joy"';
                if(data[i].emotion == "joy")
                html+= 'selected="selected"'; 
                html+= '>joy</option><option value="like"';
                if(data[i].emotion == "like")
                html+= 'selected="selected"'; 
                html+= '>like</option><option value="love"';
                if(data[i].emotion == "love")
                html+= 'selected="selected"'; 
                html+= '>love</option><option value="neutral"';
                if(data[i].emotion == "neutral")
                html+= 'selected="selected"';
                html+= '>neutral</option><option value="sadness"';
                if(data[i].emotion == "sadness")
                html+= 'selected="selected"';
                html+= '>sadness</option><option value="surprise"';
                if(data[i].emotion == "surprise")
                html+= 'selected="selected"';
                html+= '>surprise</option><option value="trust"';
                if(data[i].emotion == "trust")
                html+= 'selected="selected"';
                html+= '>trust</option><option value="NA"';
                if(data[i].emotion == "NA")
                html+= 'selected="selected"';
                html+= '>NA</option></select>';
                if(data[i].edit_permission === 1)
                html+= ' <a class="edit_predict_comment" id="'+data[i].id+'" href="javascript:void(0)" style="width:10%;"><i class="ti-pencil-alt"></i></a> ';
                html+=' </span></p> ' + 
                  ' <div class="m-b-5">'+data[i].message+'</div>'+
                 '<div class="comment-footer">'+
                  
                 '</div>'+
                 '</div>'+
                  '</div>';
       
                $(".comment_data").append(html);
        }


         $("#modal-spin").hide();
         $("#myModalLabel").text(name);
         $('#show-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.edit_post_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
  var sentiment = $('#sentiment_'+post_id+' option:selected').val();
  var emotion = $('#emotion_'+post_id+' option:selected').val();
 /* alert(post_id);*/
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) { /*alert(response)*/
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  });
    }
$(document).on('click', '.edit_predict_comment', function(e) {
     var comment_id = $(this).attr('id');
   
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_comment_'+comment_id+' option:selected').val();
  var emotion = $('#emotion_comment_'+comment_id+' option:selected').val();
  //alert(comment_id+sentiment + emotion);

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:comment_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {//alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
     
});
     

$( "#btn_remove" ).click(function() {
  
          event.preventDefault();

        $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("remove_keyword") }}',
         type: 'GET',
         data: {keyword_remove:GetURLParameter('highlight_text'),id:GetURLParameter('pid')},
         success: function(response) {
        var pid=GetURLParameter('pid');
        window.location.href = '{{url("dashboard?pid=")}}'+pid;
         
        }
      });


  
    });


$( "#btn_bookmark" ).click(function() {
  //alert(bookmark_array);
  //alert(bookmark_remove_array);
  if(Object.keys(bookmark_array).length > 0 || Object.keys(bookmark_remove_array).length > 0)
  {
        //save bookmark data;
        console.log(bookmark_remove_array);
        event.preventDefault();

        $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("in_postbookmark") }}',
         type: 'POST',
         data: {bookmark_array:bookmark_array,bookmark_remove_array:bookmark_remove_array,id:GetURLParameter('pid')},
         success: function(response) {//alert(response);
         swal({   
            title: "Bookmark!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
         
        }
      });


      }
    });



//local customize function



function kFormatter(num) {
  return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
function readmore(message){
      // alert("hi hi ");
      var string = String(message);
      var length = string.length; 
         // alert(length);
         if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.

            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
          }
          return string;


        }

        function judge_sentiment_icon(sentiment)
        {
         var emojis = ['0x1F60E', '0x1F626', '0x1F610'];//
         if (sentiment ==="pos") return  String.fromCodePoint(emojis[0]) + " positive"; 
         else if(sentiment ==="neg") return  String.fromCodePoint(emojis[1])+ " negative" ;
         else return String.fromCodePoint(emojis[2]) + " neutral" ;
       }

       function judge_emotion_icon(emotion)
       {
        var emojis = ['0x1F620', '0x1F604', '0x1F61D', '0x1F628', '0x1F604', '0x1F44D',
            '0x2764', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

            if (emotion ==="anger") return emojis[0]; 
            else if(emotion ==="anticipation") return emojis[1] ;
            if (emotion ==="disgust") return emojis[2] ; 
            else if(emotion ==="fear") return emojis[3] ;
            if (emotion ==="joy") return emojis[4]; 
            else if(emotion ==="like") return emojis[5] ;
            if (emotion ==="love") return emojis[6] ; 
            else if(emotion ==="neutral") return emojis[7] ;
            if (emotion ==="sadness")  return emojis[8]; 
            else if(emotion ==="surprise") return emojis[9]  ;
            else if(emotion === "trust") return emojis[10];

          }

        });
</script>
<link href="{{asset('css/own.css')}}" rel="stylesheet">

@endpush