<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\ManageDB;
use DB;
class UserController extends Controller
{
     use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data,$id)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users,username,'.$id,
            'email' => 'required|string|max:255|unique:users,email,'.$id,
           'password' => 'nullable|string|min:6|confirmed',
        ]);
    }

    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $title="Register";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $userdata = User::find($id);
          // dd($userdata);
          return view('auth.register',compact('userdata','title','project_data','count'));
    }
    public function edit_assignUser($uid,$pid,$id)
    {
          $title="Register";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          // $brand_id = $request->input('project_id');
          // $userdata = ManageDB::find($id);
          // $query = "select users.username,manage_dbs.user_id,projects.name,manage_dbs.brand_id,manage_dbs.id from manage_dbs join users on users.id=manage_dbs.user_id join projects on projects.id=manage_dbs.brand_id where manage_dbs.id = '".$id."' limit 1";
          // $assignlist = DB::table('manage_dbs')->join('users','users.id','=','manage_dbs.user_id')->join('projects','projects.id','=','manage_dbs.brand_id')->select('manage_dbs.brand_id','manage_dbs.user_id','users.username','projects.name')->get();
          // $assignlist = [];
          // foreach($list as $list)
          // {
          //   $assignlist['username'] = $list->username;
          //   $assignlist ['name']= $list->name;
          //   $assignlist ['id']= $list->id;
          //   $assignlist ['brand_id']= $list->brand_id;
          //   $assignlist ['user_id']= $list->user_id;
          // }
          // dd($assignlist['username']);
          // dd($uid);
          $users = DB::table('users')->select('id','username')->get();
          $projects = DB::table('projects')->select('id','name')->get();
          return view('auth.project_register',compact('users','projects','uid','pid','id','title','project_data','count'));
    }

            /**
             * Update the specified resource in storage.
             *
             * @param  \Illuminate\Http\Request  $request
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
    public function update(Request $request, $id)
    {
        $this->validator($request->all(),$id)->validate();
        $input_p['name']=$request->name;
        $input_p['username']=$request->username;
        $input_p['email']=$request->email;
        $input_p['role']=$request->role;
        if(isset($request->password))
        {
        $input_p['password']=Hash::make($request->password);
        }
    
        User::find($id)->update($input_p);
        return redirect()->route('register')
                        ->with('message','Record updated successfully');

    }
    public function update_assignUser(Request $request)
    {
        $uid = $request->user_select;
        $pid = $request->project_select;
        $id  = $request->id;
        // dd($id);
        DB::table('manage_dbs')->where('id',$id)->update(['user_id' => $uid,'brand_id' => $pid,'updated_at' => now()->toDateTimeString()]);

        return redirect()->route('projectRegister')
                        ->with('message','Record updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        User::find($id)->delete();
        $BSL_user = ManageDB::where('user_id',$id)->get()->toArray();
        if(!empty($BSL_user)) ManageDB::where('user_id',$id)->delete();
        return redirect()->route('register')
                        ->with('success','Record deleted successfully');
    }
    public function delete_assignUser($uid,$pid,$id)
    { 
          ManageDB::where('user_id',$uid)->where('brand_id',$pid)->where('id',$id)->delete();
        return redirect()->route('projectRegister')
                        ->with('success','Record deleted successfully');
    }

    //customize function
    public function getUserData()
    {
         $userlist = User::select(['id', 'name', 'username', 'email'])->orderBy('id');
         return Datatables::of($userlist)
           ->addColumn('action', function ($userlist) {
                    return '<a href="'. route('users.edit', $userlist->id) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
                  <a data-remote="'. route('deleteuser',$userlist->id) .'" class="btn btn-xs btn-danger del_user"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                })
           ->make(true);
    }
    public function getassignlist()
    {
         $assignlist = DB::table('manage_dbs')->join('users','users.id','=','manage_dbs.user_id')->join('projects','projects.id','=','manage_dbs.brand_id')->select('manage_dbs.brand_id','manage_dbs.user_id','manage_dbs.id','users.username','projects.name','users.role');
       
         return Datatables::of($assignlist)
           ->addColumn('action', function ($assignlist) {
                return '<a href="'. route('edit_assignUser', [$assignlist->user_id,$assignlist->brand_id,$assignlist->id] ).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
              <a data-remote="'. route('delete_assignUser',[$assignlist->user_id,$assignlist->brand_id,$assignlist->id] ).'" class="btn btn-xs btn-danger del_assignUser"><i class="glyphicon glyphicon-trash"></i> Delete</a><input type="hidden" name="project_id" value="'.$assignlist->brand_id.'"> ';
                })
           ->make(true);
    }
}
