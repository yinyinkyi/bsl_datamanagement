@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
@if(!isset($data))
                  <div class="row page-titles">
                    <div class="col-md-6 col-6 align-self-center">
                        <div class="form-group">
                            <select class="form-control custom-select" id="db_selection">
                              @foreach($databases as $db)
                              <option value="{{ $db->db_name }}"> {{ $db->db_name }} </option>
                              @endforeach
                             
                            </select>
                         </div>
                    
                    </div>
   
                </div>
                @else
                <div class="row page-titles">
                    <div class="col-md-6 col-6 align-self-center">
                        <div class="form-group">
                            <select class="form-control custom-select" id="db_selection">
                              @foreach($databases as $db)
                              <option value="{{ $db->db_name }}"> {{ $db->db_name }} </option>
                              @endforeach
                             
                            </select>
                         </div>
                    
                    </div>
       
                </div>
                @endif

@if(!isset($data))
<!-- <div class="box box-warning"> -->
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        <form method="POST" role="form" action="{{ route('tags.store') }}" aria-label="{{ __('Tags') }}">
                          <input type="hidden" name="brand_id" value="" id="brand">
                {{csrf_field()}}
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group">
                <label for="keywords" class="control-label">{{ __('Keywords') }}</label>
                 <input id="keywords" type="text"
               class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old('keywords') }}" required>
                 @if ($errors->has('keywords'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('keywords') }}</strong>
                    </span>
                @endif
            </div>


                         <div class="form-group">
                          <label for="projects" class="control-label">{{ __('Projects') }}</label>
                          <select class="form-control custom-select" id="project_filter">
                          @if (isset($project_ids))
                          @foreach($project_ids as $index =>$project_ids)
                          @if($index == 0 )
                          <option value="{{$project_ids->id}}"  selected>{{$project_ids->name}}</option>
                          @else
                           <option value="{{$project_ids->id}}" >{{$project_ids->name}}</option>
                          @endif
                          @endforeach
                          @endif
                         </select>
                         </div>

                
 
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Save') }}</button>
              </div>
             </form>
                            </div>

                        </div>
                    </div>
                </div>
            <!-- </div> -->
            @else
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        {!! Form::model($data, ['method' => 'PATCH','enctype'=>'multipart/form-data','route' => ['tags.update', $data->id]]) !!}
                {{csrf_field()}}
                <input type="hidden" name="brand_id" value="" id="brand">
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old( 'name', $data->name) }}"  required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group">
                <label for="keywords" class="control-label">{{ __('Keywords') }}</label>
                 <input id="keywords" type="text"
               class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old( 'keywords', $data->keywords) }}" required>
                 @if ($errors->has('keywords'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('keywords') }}</strong>
                    </span>
                @endif
            </div>

  
                         <div class="form-group">
                          <select class="form-control custom-select" id="project_filter">
                         
                       
                          @foreach($project_ids as $project_id)
                          @if($project_id->id==$data->brand_id)
                           <option value="{{ $project_id->id }}" selected="selected" >{{ $project_id->name }}</option>
                          @else
                          <option value="{{ $project_id->id }}" >{{ $project_id->name }}</option>
                          @endif
                          @endforeach
                         
                         </select>
                         </div>
                    

         
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Register') }}</button>
              </div>
             {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>

@endif
              
                  <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">Tags List</h4>
                             <div class="table-responsive m-t-40">
                                 <table id="tags_table" class="table dataTable no-footer" style="width:100%" >
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Keywords</th>
                    <th>Brand Name</th>
                    <th style="width:5%;"></th>
                    <th style="width:5%;"></th>
                    <th style="width:5%;"></th>
                   
                  </tr>
                  </thead>
                 <!--  <tbody>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Apple</a></td>
                    <td>2018-07-06</td>

                    <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Eleven</a></td>
                    <td>2018-07-25</td>
                  <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                    
                  </tr> what the hell is going with me  ? now i want to sleep in a nice place with a warm blanket
             
                  
                  </tbody> -->
                </table>
              </div>
                            </div>

                        </div>
                    </div>
                </div>  
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script type="text/javascript">

   $(document).ready(function() {

    var brand_id = $('#project_filter option:selected').val();
    document.getElementById("brand").value = brand_id;

   $( "#project_filter" ).change(function()   {
      // alert('hi');
       var brand_id = $('#project_filter option:selected').val();
       document.getElementById("brand").value = brand_id;
       
     })
    
    // alert(brand_id);

var tag_table = $('#tags_table').DataTable({
       "lengthChange": true,"info": false, "searching": true,
        "processing": true,
        "serverSide": false,
        "autoWidth": false,

        // ajax: '{!! route('gettaglist') !!}',
        "ajax": {
          "url": '{{ route('gettaglist') }}',

          "data": {
            // "brand_id": brand_id,
            
           
          }

        },

         columns: [
            {data: 'tag_name', name: 'tag_name' },
            {data: 'keywords', name: 'keywords'},
            {data: 'project_name', name: 'project_name'},
            // {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'edit', name: 'edit', orderable: false, searchable: false},
            {data: 'delete', name: 'delete', orderable: false, searchable: false},
            {data: 'convert', name: 'convert', orderable: false, searchable: false  }
        ],
                columnDefs: [
                          {
                            "targets": [2], // your case first column
                            "className": "text-center",
                            
                          }
                      ]

        
    }).on('click', '.convert_tag', function (e) {
       var tag_id = $(this).attr('id');
       // alert(tag_id);
       var text = document.getElementById(tag_id).innerHTML;
       // alert(text);

     


         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("convert_tag") }}',
         type: 'GET',
         data: {tag_id:tag_id,action:text},
         success: function(response) { 
          alert (response);
         //  var temp = table.row(5).data();
         //  temp[0] = 'Tom';
         // $('#tags_table').dataTable().fnUpdate('Zebra' , $('tag_i')[0], 1 );

          if(response > 0)
          {
            var item = tag_table.getItem(tag_id);
            item.tipo = "...";
            tag_table.refresh();
            // tag_table.row({selected:true}).data( response )
            // tag_table.ajax.reload();
            // tag_table.row({selected:true}).data( response['tag_name'] ).reload();
          }
           if(response = 0)
          {
            var item = tag_table.getItem(tag_id);
            item.tipo = "...";
            tag_table.refresh();
            // tag_table.ajax.reload();
            // tag_table.row({selected:true}).data( response )
           // tag_table.row({selected:true}).data( response['tag_name'] ).reload()p;
          }
          // if(response == 'success')
          //   tag_table.ajax.reload();
        }


            });
         e.handled = true;
          if (text=="Convert") document.getElementById(tag_id).innerHTML='Reverse';
            else document.getElementById(tag_id).innerHTML='Convert';

           
    }).on('click', '.tag_del[data-remote]', function (e) {
          // var tag_id =this.id;
          var url = $(this).data('remote');
              swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                // alert('go to del fun');
                   $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  data: {method: '_DELETE', submit: true}
              }).always(function (data) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                  $('#tags_table').DataTable().rows().invalidate().draw();
                  window.location.reload()
              });
                /*swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )*/
              }
            })
    })
   
    });

</script>
@endpush

