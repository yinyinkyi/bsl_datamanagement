<?php

namespace App\Http\Controllers;

use App\tags;
use Auth;
use DB;
use Rabbit;
use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Notifications\NewTagNotification;
use Illuminate\Support\Facades\Input;

class tagsController extends Controller
{
     use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected function validator(array $data,$id="")
    {
    return Validator::make($data, [
    // 'name' => 'required|string|max:15|unique:tags,name,brand_id,' . $data['brand_id'].','.$id,
    // // 'keywords' => 'required|string'
    // 'name' => 'required|string|max:15|unique:tags,name,NULL,NULL,brand_id,'.$data['brand_id'],
    // 'brand_id' => 'required|unique:tags,brand_id,NULL,NULL,name,'.$data['name'],
    'name' => 'required|string|max:80|unique:tags,name,'.$id.',id,brand_id,' . $data['brand_id'],
    ]);
    }
    // public function index()
    // {
        
    //        $title="Register";
    //       $project_data = $this->getProject();
    //       // dd($project_data);
    //       $permission_data = $this->getPermission(); 
    //       $count = $this->getProjectCount($project_data);

    //         return view('tag_entry')->with('project_data',$project_data)
    //                                 ->with('permission_data',$permission_data )
    //                                 ->with('count',$count)
    //                                 ->with('title',$title);
    // }
    public function index()
    {
      if($user=Auth::user())
        {
          $title="Register";
          $project_data = $this->getProject();
          // dd($project_data);
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);

          $user_id = Auth::user()->id;
          $project_id=0;
          $databases =  DB::table('manage_dbs')->select('*')->where('user_id',$user_id)->get();
          $db_name = $databases[0]->db_name;
          $query="Select monitor_pages,name,id from ".$db_name.".projects where active = 1 and id=".auth()->user()->brand_id;
          $project_ids = DB::select($query);
           if(count($project_ids)>0)
            $project_id = $project_ids[0]->id;
          $query="Select monitor_pages,name,id from ".$db_name.".projects Where  id=".$project_id;
          $project_monitor = DB::select($query);
           foreach ($project_monitor as $project_monitors) {
            $monitor_pages = $project_monitors->monitor_pages;
            $monitor_pages=explode(',', $monitor_pages);
             foreach ($monitor_pages as $monitor_page) {
             $pages[]=$monitor_page;
          }
      }
           return view('tag_entry',compact('databases','pages','project_ids','project_data','permission_data','count','title'));
      }
        else
        {
           $title="Login";
           return view('auth.login',compact('title'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd("hello before tags are stored");
         $brand_id =$request->brand_id;
         $brand_id = (int)$brand_id;
        // dd($request->all());
        $this->validator($request->all())->validate();
       
       tags::create([
            'name' => $request->name,
            'original_name'=> $request->name,
            'keywords' => $request->keywords,
            'brand_id' => $brand_id,
            'bsl'=>1
          
        ]);
                 /*$this->guard()->login($user);*/
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);
      
        $users = User::all();
         $login_user = auth()->user()->id;
      
      // if(\Notification::send($users,new NewTagNotification(tags::latest('id')->first())))
          if(\Notification::send($users,new NewTagNotification(tags::all())))
      {
        
                                    //  return redirect()->route('tags.index')->with('project_data',$project_data)
                                    // ->with('count',$count)
                                    // ->with('message', 'Successfully created a new account.');
      }
      return redirect('tags')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('message', 'Successfully created a new tag.');

        
    }

    public function notification()
    {
        return auth()->user()->unreadNotifications;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function show(tags $tags)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //  $title="Tag";
        //   $project_data = $this->getProject();
        //   $count = $this->getProjectCount($project_data);
        //   $data = tags::find($id);
        //  return view('tag_entry',compact('data','title','project_data','count'));

        if($user=Auth::user())
        {
          $title="Tag";
          $project_data = $this->getProject();
          // dd($project_data);
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
          $data = tags::find($id);
          // dd($data->brand_id);
          $user_id = Auth::user()->id;
          $project_id=0;
          $databases =  DB::table('manage_dbs')->select('*')->where('user_id',$user_id)->get();
          $db_name = $databases[0]->db_name;
          $query="Select monitor_pages,name,id from ".$db_name.".projects where id=".auth()->user()->brand_id;
          $project_ids = DB::select($query);
           if(count($project_ids)>0)
            $project_id = $project_ids[0]->id;
          $query="Select monitor_pages,name,id from ".$db_name.".projects Where  id=".$project_id;
          $project_monitor = DB::select($query);
           foreach ($project_monitor as $project_monitors) {
            $monitor_pages = $project_monitors->monitor_pages;
            $monitor_pages=explode(',', $monitor_pages);
             foreach ($monitor_pages as $monitor_page) {
             $pages[]=$monitor_page;
          }
      }
           return view('tag_entry',compact('databases','pages','project_ids','data','project_data','permission_data','count','title'));
      }
        else
        {
           $title="Login";
           return view('auth.login',compact('title'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
         $this->validator($request->all(),$id)->validate();
          // $tags = tags::find($id);
          // $brand_id= $tags->brand_id;
        $input_p['name']=$request->name;
        $input_p['original_name']=$request->name;
        $input_p['keywords']=$request->keywords;
        $input_p['brand_id']=$request->brand_id;
     
    
        tags::find($id)->update($input_p);
        return redirect()->route('tags.index')
                        ->with('message','Record updated successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
      
    //      tags::find($id)->delete();
    //     return redirect()->route('tags.index')
    //                     ->with('success','Record deleted successfully');
    // }//  return ' <a style="width:100%" href="'. route('deletetag',$taglist->tag_id) .'" class="btn btn-xs btn-danger tag_del"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            // })
               // ';
     public function gettaglist()
   {
     $brand_id = auth()->user()->brand_id;
     $taglist = "SELECT tags.id tag_id,tags.name tag_name,tags.keywords keywords,projects.name project_name,projects.id project_id FROM tags JOIN projects ON projects.id=tags.brand_id  WHERE tags.brand_id = ".$brand_id." ORDER BY project_id,tag_id";
     $taglist = DB::select($taglist);
     // dd($taglist);
     return Datatables::of($taglist)
       ->addColumn('edit', function ($taglist) {
               
        return '<a style="width:100%" href="'. route('tags.edit', $taglist->tag_id) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>  ';

            })
       ->addColumn('delete', function ($taglist) {
                return ' <a style="width:100%" data-remote="'. route('deletetag',$taglist->tag_id) .'" class="btn btn-xs btn-danger tag_del" id="'.$taglist->tag_id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            })
       ->addColumn('convert', function ($taglist) {
                return '<a style="width:100%" class="btn btn-xs btn-primary convert_tag" id="'.$taglist->tag_id.'" href="javascript:void(0)">Convert</a>';

            })
       ->rawColumns(['edit', 'delete','convert'])
       ->make(true);
        

 }
 public function convert()
 {
 	$id = Input::get('tag_id');
  $action = Input::get('action');
  $tags = tags::find($id);
  $original_name = $tags->name;
  $name = Rabbit::zg2uni($original_name);

  if($action == "Convert") $result = DB::table('tags')->where('id',$id)->update(['name'=> $name]);
  else $result = DB::table('tags')->where('id',$id)->update(['name'=> $original_name]);

 	// return redirect()->route('tags.index');
  return $result;

 }
public function destroy($id)
{
//
$brand_id=0;

// $project_data = $this->getProject();
// if(count($project_data)>0)
// {
// $brand_id = $project_data[0]['id'];
// }
// dd($brand_id);

$tags = tags::find($id);
if(!empty($tags))
{
$tagName=$tags->name;
$brand_id=$tags->brand_id;
$con1="SELECT id,checked_tags_id FROM temp_".$brand_id."_inbound_comments where checked_tags_id <> '' and checked_tags_id Like '%,".$id."%'";
$con1_result = DB::select($con1);
if(count($con1_result)>0)
{
$update="UPDATE temp_".$brand_id."_inbound_comments
SET checked_tags_id = REPLACE(checked_tags_id,',".$id."','') 
WHERE checked_tags_id like '%,".$id."%';";
$update_result = DB::select($update);
}
$con2="SELECT id,checked_tags_id FROM temp_".$brand_id."_inbound_comments where checked_tags_id <> '' and checked_tags_id Like '%".$id.",%'";
$con2_result = DB::select($con2);
if(count($con2_result)>0)
{
$update="UPDATE temp_".$brand_id."_inbound_comments
SET checked_tags_id = REPLACE(checked_tags_id,'".$id.",','') 
WHERE checked_tags_id like '%".$id.",%';";
$update_result = DB::select($update);
} 

$con3="SELECT id,checked_tags_id FROM temp_".$brand_id."_inbound_comments where checked_tags_id <> '' and checked_tags_id Like '%".$id."%'";
$con3_result = DB::select($con3);
if(count($con3_result)>0)
{
$update="UPDATE temp_".$brand_id."_inbound_comments
SET checked_tags_id = REPLACE(checked_tags_id,'".$id."','') 
WHERE checked_tags_id like '%".$id."%';";
$update_result = DB::select($update);
}

tags::find($id)->delete();
}
return redirect()->route('tags.index',['pid' => $brand_id])
->with('success','Record deleted successfully');
}
}
