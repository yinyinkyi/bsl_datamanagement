@extends('layouts')

     @section('content')
 
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <div class="form-group">
                            <select class="form-control custom-select" id="db_selection">
                              @foreach($databases as $db)
                              <option value="{{ $db->db_name }}"> {{ $db->db_name }} </option>
                              @endforeach
                             
                            </select>
                          </div>
                       </div>
                            
                    <div class="col-md-5 nopadding">
                      <div class="d-flex input-group mb-3">
                        <div class="d-flex input-group mb-3">
                          <!-- <div class='input-group mb-3'> -->
                            <input type='text' id="calender" class="form-control dateranges" style="color: #4267b2 !important; width: ; padding: 0.2em 0.2em 0px;" />
                            <div class="input-group-append">
                              <span class="input-group-text">
                                <span class="ti-calendar"></span>
                              </span>
                            </div>
                          <!-- </div> -->
                        </div>
   <!--    <div class="">
        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
      </div> -->
    </div>
  </div>
                
<!-- <div class="btn-group">
                                            <button type="button" class="btn btn-info">Select Database</button>
                                            <button value="" id="btn_source" type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu">
                                              @foreach($databases as $db)
                                                <a class="dropdown-item" href="">{{ $db->db_name }}</a>
                                                 @endforeach
                                              
                                               
                                            </div>
                                        </div> -->
                                


                  
                </div>
                            
               
              
               
                          <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Export your data as excel sheet</h4>
                                <!-- <label for="input-file-now">Your so fresh input file — Default version</label> -->
                                <!-- <input type="button" id="input-file-now" class="dropify" /> -->
                                <div class="form-group">
                                <button type="button" class="btn btn-primary" id="btn_export">Export</button>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
    
                
    
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
   <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}"></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
  <script type="text/javascript">
    var startDate;
    var endDate;

    $(document).ready(function() {
startDate = moment().subtract(3, 'month');
       endDate = moment();

        function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}

      $('li > a').click(function() {
          $('li').removeClass();
          $(this).parent().addClass('active');
      });
   
        $('.dateranges').daterangepicker({
  locale: {
    format: 'MMM D, YYYY'
  },
  ranges: {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  startDate: startDate,
  endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;

        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate);
      });



     $("#sidebarnav li a").click(function(){
    
      // var text = $('#sidebarnav .nav').attr('id');
    //alert(text);
       
      // var text = $("#sidebarnav li.active a").text();   /*needtocheck or confrim*/
      // alert(sectionId);
      var text = $(this).text();
       // alert(text);
      
       document.getElementById('comment-text').innerHTML = text;

      var e = document.getElementById("db_selection");
      var db_name = e.options[e.selectedIndex].text;
      // alert(startDate);
      // alert(endDate);
      // Create_DataTable(startDate,endDate,0,db_name,text);
     ChooseDate(startDate,endDate);
      e.preventDefault();
     

});

    

    var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };

   var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

  $( "#db_selection" ).change(function()   {

  
        var db_name = $(this).val();
        // alert(db_name);
        // var text = $('#sidebarnav .nav').attr('id');
        // var text = $('ul#sidebarnav').find('li.active').data('id');
           // alert(text);
           // alert($('#sidebarnav li.active').attr('value'));
           var text = $("#sidebarnav li.active").text();   /*needtocheck or confrim*/
           // alert(text);
        // data_table(db_name,text);
        // alert(startDate);
        // // alert(endDate);
        // Create_DataTable(startDate,endDate,0,db_name,text)
        ChooseDate(startDate,endDate);


    });




$('input[name=options]').mouseup(function(){
    //alert("Before change "+$('input[name=options]:checked').val());
}).change(function(){
     periodType=$('input[name=options]:checked').val();
     ChooseDate(startDate,endDate);
    //alert("After change "+$('input[name=options]:checked').val());
})

$( "#btn_export" ).click(function() {
            // alert(startDate);
            var db_name=$("#db_selection").val();
           data_table(GetStartDate(),GetEndDate(),db_name);

          });



ChooseDate(startDate,endDate);





    function ChooseDate(start,end)
   {
     $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
     startDate=start;
     endDate=end;


       var brand_id = GetURLParameter('pid');
       var brand_id = 17;
       var start = new Date(startDate);
       var end = new Date(endDate);
       var timeDiff = Math.abs(end.getTime() - start.getTime());
       var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 


          if(parseInt(diffDays) < 7) //in terms of day
          { 
            //alert("7");
            $("#day").show();
            $("#week").hide();
            $("#month").hide();
          }

          
         else if (parseInt(diffDays) == 7)//in terms of week
         {

           //alert("==7");
           $("#day").show();
           $("#week").show();
           $("#month").hide();


         }
          else if (parseInt(diffDays) > 7)//in terms of month
          {
            //alert(">7");

            $("#day").show();
            $("#week").show();
            $("#month").show();

          }
          var text = $("#sidebarnav li.active").text();   /*needtocheck or confrim*/
           var selected_db = document.getElementById("db_selection");
            var db_name = selected_db.options[selected_db.selectedIndex].text;
          //requestsentimentbycategory(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),is_search);
          Create_DataTable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          

        }



     function Create_DataTable(fday,sday)
    {
      // alert(fday);  
      // alert(sday);
      // alert(db_name);
      // alert(text);
     
            // var txtkeyword =$("#txtkeyword").val();
            var selected_db = document.getElementById("db_selection");
            var db_name = selected_db.options[selected_db.selectedIndex].text;

              // alert(db_name);
            var text = $('#sidebarnav li.active').text();  /*need to check or confirm*/
            //   alert(db_name);
            // alert(text);
            // alert(text);
            // var text = $('ul#sidebarnav').find('li.active').data('id');
           // var e = document.getElementById("sentiment_option");
            //ar opt_sentiment = e.options[e.selectedIndex].value;
        
     // top_posts(fday,sday,txtkeyword,opt_sentiment);
     // latest_posts(fday,sday,txtkeyword,opt_sentiment); 
      // alert(db_name);
      // alert(text);
     //data_table(fday,sday,db_name);
    

    }

  function data_table(fday,sday,db_name)
    { 
        // alert(fday);
        $.ajax({
      type: "GET",
   
     headers: { 
           'X-CSRF-TOKEN': '{{csrf_token()}}' 
    },
     url: "{{route('getExcelData')}}", // This is the URL to the API
      data: {db_name: db_name,fday:fday,sday:sday}
    })
    .done(function( response ) {
      console.log(response);
      location.href ='reports/'+response.title+'.xlsx';
   
    })
    .fail(function(xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

 
    }
    function doesFileExist(urlToFile) {
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();
     
    if (xhr.status == "404") {
        return false;
    } else {
        return true;
    }
}



      });
  </script>
   <link href="{{asset('css/own.css')}}" rel="stylesheet">
  
@endpush

