<?php

namespace App\Console\Commands;
use App\fb_token;
use Illuminate\Console\Command;
use DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;


class crawl_outbound_data extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl_outbound:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl outbound page data every 10 minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->where('id',35)->get();
        $data =  [];
      
      foreach($projects as $project)
      {
        
        $id = $project->id;
        // if($id != 17){
        $monitor_pages = $project->monitor_pages;
        $monitor_pages = explode(',', $monitor_pages);
       
        $admin_pages = $project->own_pages;
        $admin_pages = explode(',', $admin_pages);
        // dd($admin_pages);
        $admin_pages = $this->Format_Page_Name($admin_pages);

        $other_pages=array_values(array_diff($monitor_pages,$admin_pages));
        $other_pages = $this->Format_Page_Name($other_pages);
        // dd($other_pages);
        $admin_pages_count = count($admin_pages);
        $other_pages_count = count($other_pages);

               
        if($other_pages_count > 0)
        {
        for($i=0;$i<$other_pages_count;$i++)
        {
          
        
        $client = new Client(['base_uri' => 'http://54.255.246.191/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_crawl = 'dc2/apiv1/bound/crawl';

          $formData = array(
            'page_name' => $other_pages[$i],
            'type' => 'outbound_page'
        );

        // $formData = ['page_name' =>  $other_pages[$i],'type' => 'outbound_page'];
        // dd($formData);           
        // $formData = json_encode($formData)
          try{
        $api_response = $client->post($uri_crawl, [
                                'form_params' => [
                                        'page_name' => $other_pages[$i],
                                        'type' => 'outbound_page'
                                    ]
                             ]);
      }
                catch (\Exception $e) {
  
                $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                $error_message = explode('<html>',$error_message);
                // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                //dd($error_message[0]);
                $error_message=$error_message[0];

                //$error_message ='test';
                $sms_data=json_encode([
                  'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                  'group_id' =>2,
                ]);
                $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                  'headers' => ['Content-Type' => 'application/json'],
                  'body' => $sms_data
                ]);
                 //dd($sms_response->getBody()->getContents());
                  // return false;
                }


      }
    }


       // }
      }
      $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Competitor Comment Crawl Successfully.'. $date_time);
    }

    public function Format_Page_Name($array)
    {
      $new_array=[];
       foreach ($array as  $key => $row) {
        $arr_page_name=explode("-",$row);
        if(count($arr_page_name)>0)
        {
          $last_index=$arr_page_name[count($arr_page_name)-1];
          if(is_numeric($last_index))
          $new_array[] = $last_index;
          else
          $new_array[] = $row;
        }
        else
        {
          $new_array[]=$row;
        }
       
          
      
       }

      return  $new_array;
    }
}
