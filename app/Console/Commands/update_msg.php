<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use MongoDB;
use App\MongoboundComment;


class update_msg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:commentMsg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get comment Message from Mongo and update in mysql';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
              // dd(now()->toDateTimeString());
       $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        foreach($projects as $project)
      {
        $id = $project->id;
        $comment_table = "temp_".$id."_inbound_comments";
        // $id_array = "Select id from ".$comment_table." where is_update = 0 and message <> '' order by created_time desc limit 500";
        $id_array = "Select id from ".$comment_table." where is_update=0 and message <> '' order by timestamp(created_time) DESC limit 30";
        $id_array = DB::select($id_array);
      
      
        // $array = json_decode(json_encode($id_array), true);

        foreach ($id_array as $value) 
        {
          $array[] = $value->id;
        }

         $inbound_comment_result=MongoboundComment::raw(function ($collection) use($array) { 
         // print_r($array);
 
          return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                  
                   ['id'=> ['$exists'=> true]],
                   ['message'=> ['$exists'=> true]],
                   ['id'=>[ '$in'=> $array ] ],
                 
                  
                            ]
              ]  
                         
             ],
              
          ]);
  
})->toArray();
        // }
         // dd($inbound_comment_result);

      foreach($inbound_comment_result as $result)
      {
        $id = $result['id'];
         $data_message='';$original_message='';$unicode=0;
       if(isset($result['message'])) $data_message = $result['message'];
       $data_message=preg_replace('/(\r\n|\r|\n)+/', " ",$data_message);
        if(isset($result['original_message'])) $original_message = $result['original_message'];
        $original_message=preg_replace('/(\r\n|\r|\n)+/', " ",$original_message);
        if(isset($result['unicode'])) $unicode = $result['unicode'];
         // dd($data_message);
        // $query = "Update ".$comment_table." Set message = ".$data_message. " , is_update = 1 where id = ". $id ;
        //$res = DB::table($comment_table)->where('id',$id)->update(['message' => $data_message ,'is_update' => 1]);

         $res = DB::table($comment_table)->where('id',$id)->update(['message' => $data_message ,'original_message' => $original_message ,'unicode' =>$unicode,'is_update' =>1]);
        
        // dd($res);
       
      }
    }
      // dd($data_message);

}
}
