<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use MongoDB;
use App\Profile;
use App\MongoProfile;
use MongoDB\BSON\ObjectID;
use App\MongoboundComment;

class update_profileName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:profileName';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update profile name  every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
        foreach($projects as $project)
        {
        $id = $project->id;
        // $id = '17';
        $comment_table = "temp_".$id."_inbound_comments";
        $id_array = "Select profile from ".$comment_table." where (profile is not NULL or profile <> '') order by timestamp(created_time) desc limit 100";
        $id_array = DB::select($id_array);

      
 
            foreach($id_array as $result)
            { 
              $profile_id = $result->profile;

              if( $profile_id <> '') {
                       
                      $profile_result=MongoProfile::raw(function ($collection) use($profile_id) {
                        return $collection->aggregate([
                            [
                            '$match' =>
                                 ['_id'=> new ObjectId($profile_id) ],
                            
                                       
                           ],
                            
                        ]);
                
                     })->toArray();

              // dd($profile_result);  
                      foreach($profile_result as $data)
                      {

                        $_id ='';$name='';$type='';$id='';

                        if(isset($data['name'])) $name = $data['name']; 
                        if(isset($data['_id'])) $_id = $data['_id']; 
                        if(isset($data['_id'])) $type = $data['type']; 
                        if(isset($data['_id'])) $id = $data['id']; 
                       
                        // DB::table('temp_profiles')->where('temp_id',$_id)->first()->update(['name' => $name]);
                        DB::table('temp_profiles')
                            ->where('temp_id', $_id)
                            ->update(['name' => $name,'type' =>$type,'id'=>$id,'updated_at' => now()->toDateTimeString()]);
                      }
                  }

              
        
        
      }
    }
    $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Profile Name Updated Successfully.'. $date_time);
    }
}
