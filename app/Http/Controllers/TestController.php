<?php

namespace App\Http\Controllers;
use App\MongodbData;
use App\MongoPages;
use App\MongoFollowers;
use App\Comment;
use App\MongoInboundPost;
use App\MongoInboundComment;
use App\MongoProfile;
use MongoDB;
use Illuminate\Http\Request;
use Auth;
use App\Project;
use App\ProjectKeyword;
use App\InboundPages;
use GuzzleHttp\Client;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use DB;

use App\MongoboundPost;
use App\MongoboundComment;

class TestController extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
   public function getpagefilter($page_data)
{
  $conditional=[];
  
  $conditional_require_or=[];
  foreach($page_data as $i =>$element)
                            {

                            $require_keyword_filter[] = [ 'page_name' =>  $element];
                            $conditional_require_or['$or']=$require_keyword_filter;
                            }
 $conditional[] =$conditional_require_or;
 
 return  $conditional;
}

       public function getPageWhere($related_pages)
{
    $pages='';
     $filter_pages = '';
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  AND page_name in (".$pages.")";
      }
    return  $filter_pages;
}

   public function get_table_id($project_id,$dateBegin,$dateEnd,$table,$require_var,$related_pages=[])
    {
      $dateBegin = date('Y-m-d',strtotime($dateBegin));
      $dateEnd = date('Y-m-d',strtotime($dateEnd));
    
      $filter_pages=$this->getPageWhere($related_pages);
      
      $query = "SELECT ".$require_var." from temp_".$project_id."_".$table ."  Where 1=1 ".
      " AND (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $filter_pages;
      //dd($query);
      $id_result = DB::connection('mysql_cron_wunzin')->select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }
   public function get_common_table_id($table,$require_var)
    {
     
      $id_result=[];
      $query = "SELECT Distinct ".$require_var." from ".$table ."  Where 1=1 ";
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      foreach ($id_array as $key => $value) {
        if(null !== $value)
        $id_result[]=new MongoDB\BSON\ObjectId((string)$value);
        # code...
      }
      return $id_result;
    }
    public function get_comment_id()
    {
     
      $id_result=[];
      $query = "SELECT profile from temp_17_inbound_comments where profile<>'' and profile is not NULL Order By timestamp(created_time) DESC Limit 1000";
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, "profile");
      foreach ($id_array as $key => $value) {
        if(null !== $value)
        $id_result[]=new MongoDB\BSON\ObjectId((string)$value);
        # code...
      }
      return $id_result;
    }

     public function get_table_page($project_id,$table,$require_var)
    {
        
      
      $query = "SELECT ".$require_var." from temp_".$project_id."_".$table ;
      //dd($query);
      $id_result = DB::connection('mysql_cron_wunzin')->select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }

    public function getInboundPostId($brand_id,$related_pages)
    {
        $filter_pages='';
        $filter_pages=$this->getPageWhere($related_pages);
        $id_query="select id from temp_".$brand_id."_inbound_posts Where 1=1 " .$filter_pages;
        $id_result = DB::connection('mysql_cron_wunzin')->select($id_query);
        $id_array = array_column($id_result, 'id');
        return  $id_array;
    }

    function doCSV($path, $array)
{
   $fp = fopen($path, 'w');
$i = 0;
foreach ($array as $fields) {
    if($i == 0){
        fputcsv($fp, array_keys($fields));
    }
    fputcsv($fp, array_values($fields));
    $i++;
}

fclose($fp);
}

    function convertKtoThousand($s)
{
    if (strpos(strtoupper($s), "K") != false) {
    $s = rtrim($s, "kK");
    return floatval($s) * 1000;
  } else if (strpos(strtoupper($s), "M") != false) {
    $s = rtrim($s, "mM");
    return floatval($s) * 1000000;
  } else {
    return floatval($s);
  }
}

    public function Format_Page_Name($array)
    {
      $new_array=[];
       foreach ($array as  $key => $row) {
        $arr_page_name=explode("-",$row);
        if(count($arr_page_name)>0)
        {
          $last_index=$arr_page_name[count($arr_page_name)-1];
          if(is_numeric($last_index))
          $new_array[] = $last_index;
          else
          $new_array[] = $row;
        }
        else
        {
          $new_array[]=$row;
        }
       
          
      
       }

      return  $new_array;
    }

    public function sync_mongodata()
    {
      $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
      $data =  [];
      
      foreach($projects as $project)
      {
        // if($project->id === 17)
        // {
        $id = $project->id;
        $monitor_pages = $project->monitor_pages;
        $monitor_pages = explode(',', $monitor_pages);
        $monitor_pages = $this->Format_Page_Name($monitor_pages);
        $admin_pages = $project->own_pages;
        $admin_pages = explode(',', $admin_pages);
        $admin_pages = $this->Format_Page_Name($admin_pages);
        $other_pages=array_values(array_diff($monitor_pages,$admin_pages));
        $admin_pages_count = count($admin_pages);
        $other_pages_count = count($other_pages);

        // if($admin_pages_count>0) 
        // $this->insert_Inbound_Data($id,$admin_pages,'inbound_page');
        if($other_pages_count>0)
        $this->insert_Inbound_Data($id,$other_pages,'outbound_page'); 
       // $this->insert_Inbound_Other_Data($id,$other_pages);
        // }
       
      }
    }
      public function insert_Inbound_Data($project_id,$my_pages,$crawl_type)
    {

      $today = date('d-m-Y h:i:s');//22-09-2018//  date('d-m-Y')

     $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
     $date_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
      
     $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
    
     $date_End=date('Y-m-d h:i:s',strtotime($today));

     $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);
     dd( $dateEnd);
      //get 7 day id from mysql

      //for edit
     $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];
     
     // $keyword_data = $this->getprojectkeywork($project_id);
     // $filter['$or'] = $this->getkeywordfilter($keyword_data);


  
       
     $page['$or']= $this->getpagefilter($my_pages);
 


$inbound_post_id_array = $this->get_table_id($project_id,$date_Begin,$date_End,"inbound_posts","id",$my_pages);

 $inbound_post_result=MongoboundPost::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_post_id_array,$crawl_type) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             ['id'=> ['$exists'=> true]],
             // ['crawl_type'=>$crawl_type],
             ['id'=>[ '$nin'=> $inbound_post_id_array ] ],
             $page
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
 // return;
// dd($inbound_post_result);

$data=[];
 $page_array=[];
 $data_message=[];

    
 foreach ($inbound_post_result as  $key => $row) {
                $id ='';$datetime=NULL;
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';$original_message ='';$unicode ='';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                $reactUpdatedAt=NULL;$isDeleted=0;
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                if(isset($row['original_message'])) $original_message = $row['original_message'];
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                 if(isset($row['created_time']))
                {
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                }
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];

                if(isset($row['UpdatedAt']))
                {
                $utcdatetime = $row["UpdatedAt"];
                $reactUpdatedAt = $utcdatetime->toDateTime();
                $reactUpdatedAt=$reactUpdatedAt->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $reactUpdatedAt = $reactUpdatedAt->format('Y-m-d H:i:s');
                }
                  if(isset($row['isDeleted'])) $isDeleted = $row['isDeleted'];

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'original_message'=>$original_message,
                    'unicode'=>$unicode,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>$sentiment,
                    'checked_sentiment' =>$sentiment,
                    'decided_keyword' =>'',
                    'emotion' =>$emotion,
                    'checked_emotion' =>$emotion,
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'reactUpdatedAt' =>$reactUpdatedAt,
                    'isDeleted' =>$isDeleted,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

  }
  $path = storage_path('app/data_output/weekly_inbound_posts'.$crawl_type.'_'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data);

 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,original_message,
 unicode,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,created_time,change_predict,checked_predict,isBookMark,reactUpdatedAt, isDeleted,created_at,updated_at)";

// $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
//  $pdo->exec($query_load);
 if (file_exists($path)) {
    unlink($path) ;
}
/////////////////////////////////////////////////////////////////

 $post_id = $this->getInboundPostId($project_id,$my_pages);/*dd( $filter);*/
// dd($post_id);
 
$inbound_cmt_id_array = $this->get_table_id($project_id,$date_Begin,$date_End,"inbound_comments","id");

//$this->get_table_id($project_id,$date_Begin,$dateEnd,"inbound_posts","id",$my_pages);
  /*dd($inbound_cmt_id_array);*/
 $inbound_comment_result=MongoboundComment::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_cmt_id_array,$post_id) {//print_r($filter);
 
    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             ['id'=> ['$exists'=> true]],
             ['post_id'=>['$in'=>$post_id]],
             ['id'=>[ '$nin'=> $inbound_cmt_id_array ] ],
           
            
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();

 $profile_id=[];
  $data_comment=[];
 foreach ($inbound_comment_result as  $key => $row) {
                $id ='';
                $message ='';$original_message ='';$unicode ='';$profile ='';
                $post_id ='';
                $comment_count =0;
                $sentiment='';$emotion='';$parent='';
                $tags='';
                if(isset($row['profile']))
                {
                $profile = $row['profile'];
                $profile_id[]=new MongoDB\BSON\ObjectId($profile);
                } 

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['original_message'])) $original_message = $row['original_message'];
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['parent'])) $parent =  $row['parent'];
           

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'wb_message' =>'',
                    'original_message'=>$original_message,
                    'unicode'=>$unicode,
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    'sentiment' =>$sentiment,
                    'checked_sentiment' =>$sentiment,
                    'decided_keyword' =>'',
                    'emotion' =>$emotion,
                    'checked_emotion' =>$emotion,
                    'interest' =>'',
                    'profile'=>$profile,
                    'tags'=>'',
                    'checked_tags'=>'',
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'parent'=>$parent,
                     'action_status'=>'',
                    'action_taken'=>'',
                    'isBookMark' =>0,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }

$path = storage_path('app/data_output/weekly_inbound_comment'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_comment);
 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,
interest,profile,tags,checked_tags,change_predict,checked_predict,parent,action_status,action_taken,isBookMark,created_at,updated_at)";
 // $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
 // $pdo->exec($query_load);

if (file_exists($path)) {
    unlink($path) ;
} 

$profile_id_array = $this->get_common_table_id("temp_profiles","temp_id");

//$profile_id=$this->get_comment_id();
// dd($profile_id_array);
 $profiles_result=MongoProfile::raw(function ($collection) use($profile_id,$profile_id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
        '$and'=> [ 
          ['_id' => ['$in' => $profile_id ] ] ,
          ['_id'=>[ '$nin'=> $profile_id_array ] ],
            
      ]

        ]  
           ]
    
    ]);
})->toArray();
print_r($profiles_result);
return;
 $data_profiles=[];
 foreach ($profiles_result as  $key => $row) {
                 $_id ='';
                $id ='';
                $name ='';
                $type ='';
              
                if(isset($row['_id'])) $_id =$row['_id'];
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['name'])) $name =$row['name'];
                if(isset($row['type'])) $type = $row['type'];
              
          

                  $data_profiles[] =[
                    'temp_id' => $_id,
                    'id' => $id,
                    'name' =>$name,
                    'type' => $type,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }
 $path = storage_path('app/data_output/profiles'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_profiles);

$query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_profiles FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (temp_id,id,name,type,created_at,updated_at)";

$pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);

 if (file_exists($path)) {
    unlink($path) ;
}  


// $follow_id_array = $this->get_table_page($project_id,"followers","page_name");
// $follower_result=MongoFollowers::raw(function ($collection) use($my_pages,$follow_id_array) {
//  return $collection->aggregate([
//         [
//         '$match' =>[
//              '$and'=> [ 
//           ['page_name' => ['$in' => $my_pages ] ],
//           ['page_name'=>[ '$nin'=> $follow_id_array ] ], 
            
//       ]

//         ]  
//            ]
    
//     ]);
// })->toArray();


//  $data_followers=[];
//  foreach ($follower_result as  $key => $row) {
//                 $id ='';
//                 $page_name ='';
//                 $fan_count =0;
//                 $date ='';
             
   
//                 if(isset($row['id'])) $id =$row['id'];
//                 if(isset($row['page_name'])) $page_name =$row['page_name'];
//                 if(isset($row['fan_count'])) $fan_count = $row['fan_count'];
//                 if(isset($row['date'])) $date = $row['date'];
//                 if(isset($row['reaction'])) $reaction =$row['reaction'];
          
//                  $data_followers[] =[
//                     'id' => $id,
//                     'page_name' =>$page_name,
//                     'fan_count' => $fan_count,
//                     'date' =>$date,
//                     'created_at' => now()->toDateTimeString(),
//                     'updated_at' => now()->toDateTimeString()
//                     ];                 

//   }
//  $path = storage_path('app/data_output/weekly_followers'.$project_id.'.csv');

// if (file_exists($path)) {
//     unlink($path) ;
// } 
// $this->doCSV($path,$data_followers);

// $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_followers FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,page_name,fan_count,date,created_at,updated_at)";

// $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
//  $pdo->exec($query_load);
//  if (file_exists($path)) {
//     unlink($path) ;
// } 

//  $page_id_array = $this->get_table_page($project_id,"pages","page_name");
//  $pages_result=MongoPages::raw(function ($collection) use($my_pages,$page_id_array) {//print_r($filter);
//  return $collection->aggregate([
//         [
//         '$match' =>[
//              '$and'=> [ 
//           ['page_name' => ['$in' => $my_pages ] ] ,
//           [ 'page_name'=>[ '$nin'=> $page_id_array ] ],
            
//       ]

//         ]  
//            ]
    
//     ]);
// })->toArray();

//  $data_pages=[];
//  foreach ($pages_result as  $key => $row) {
//                 $page_name ='';
//                 $about ='';
//                 $name ='';
//                 $category ='';
//                 $sub_category ='';
//                 $profile='';
 
//                 if(isset($row['page_name'])) $page_name =$row['page_name'];
//                 if(isset($row['name'])) $name =$row['name'];
//                 if(isset($row['about'])) $about = $row['about'];
//                 if(isset($row['category'])) $category = $row['category'];
//                 if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
//                 if(isset($row['profile'])) $profile = $row['profile'];
//               //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

//                   $data_pages[] =[
//                     'page_name' => $page_name,
//                     'name' =>$name,
//                     'about' => $about,
//                     'category' =>$category,
//                     'sub_category' =>$sub_category,
//                     'profile'=>$profile,
//                     'created_at' => now()->toDateTimeString(),
//                     'updated_at' => now()->toDateTimeString()
//                     ];                 

//   }
//  $path = storage_path('app/data_output/weekly_pages'.$project_id.'.csv');

// if (file_exists($path)) {
//     unlink($path) ;
// } 
// $this->doCSV($path,$data_pages);

// $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_pages FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (page_name,name,about,category,sub_category,profile,created_at,updated_at)";

// $pdo = DB::connection('mysql_cron_wunzin')->getPdo();
//  $pdo->exec($query_load);

//  if (file_exists($path)) {
//     unlink($path) ;
// } 

 return true;

 

}

}
