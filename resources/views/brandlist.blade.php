@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title,'source',$source])
@section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor">Brand List</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                             <li class="breadcrumb-item active">Brand List</li>
                        </ol>

                    </div>
                <div class="col-md-6 align-self-center" style="padding-left: 300px;">
                  
                          @if ($permission_data['setting'] === 1)
              <a href="<?= URL::to('/brandList/create?source=')?>{{$source}}" class="btn  btn-info pull-right">Create New Brand</a>
                      @endif
                 
            </div>

                </div>

              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                 <table id="brand_table" class="table">
                  <thead>
                  <tr>
                    <th>Brand Name</th>
                    <th>Created At</th>
                    <th>Setting</th>
                   
                  </tr>
                  </thead>
                 <!--  <tbody>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Apple</a></td>
                    <td>2018-07-06</td>

                    <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Eleven</a></td>
                    <td>2018-07-25</td>
                  <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                    
                  </tr>
             
                  
                  </tbody> -->
                </table>
                            </div>

                        </div>
                    </div>
                </div>

              
                
              
                
       
@endsection
@push('scripts')

<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer ></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" ></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->

<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}} " defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" ></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" ></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" ></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" ></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" ></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" ></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" ></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" ></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" ></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" ></script>


<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" ></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" ></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" ></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" ></script>
  <script src="https://js.pusher.com/4.2/pusher.min.js"></script>


    <script type="text/javascript">
$(document).ready(function() {
        $('#brand_table').DataTable({
       "lengthChange": true,"info": false, "searching": true,
        processing: true,
        serverSide: true,
         "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getbrandlist') }}',

 
         "data": {
            "source":$('#btn_source').val(),
          }
 },

         columns: [
            {data: 'name', name: 'name'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
        
    }).on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
          var url = $(this).data('remote');
          // confirm then
          // alert("hello");
      /*    if (confirm('Are you sure you want to delete this?')) {
             alert("hello");
              $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  data: {method: '_DELETE', submit: true}
              }).always(function (data) {
                  $('#brand_table').DataTable().draw(false);
              });
          }*/
                      swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                   $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  data: {method: '_DELETE', submit: true}
              }).always(function (data) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                  $('#brand_table').DataTable().rows().invalidate().draw();
              });
                /*swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )*/
              }
            })
      });


    });
          


</script>
@endpush
