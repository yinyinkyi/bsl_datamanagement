@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('comparison')

<li class="nav-item dropdown">
                           <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            
                          <!--     <button onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" type="button" class="btn waves-effect waves-light btn-outline-warning">Comparison</button>  --> 
                                <button  onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" class="btn btn-secondary waves-effect waves-light" style="background:#e9ebee;color:#263238" type="button"><span class="btn-label"><i class="ti-settings"></i></span>Comparison</button>
                            </a>
 </li>
 <li class="nav-item dropdown">
                            <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            <button  onclick="window.location='{{route('insight', ['pid' => $pid])}}'" class="btn btn-secondary waves-effect waves-light" style="background:#e9ebee;color:#263238" type="button"><span class="btn-label"><i class="ti-settings"></i></span>Insight</button>  
                            </a>
                           
 </li>
@endsection
@section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                
                    <div class="col-md-5 col-8 align-self-center">
                   <!--      <h3 class="text-themecolor">Comparison</h3> -->
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Insight</a></li>
                            <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>

                      <input type="hidden" id="brand_name" name="brand_name" value="{{$project_name}}">
                        </ol>

                    </div>

                     <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                   
                         <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                         <div class='input-group mb-3'>
                         <input type='text' class="form-control dateranges" style="color:#01c0c8 !important;width: 17em; padding: .2em .2em 0 ;" />
                         <div class="input-group-append">
                         <span class="input-group-text">
                         <span class="ti-calendar"></span>
                         </span>
                         </div>
                         </div>
                         </div>
                          
                          </div>
                    </div>
                </div>

                <div id="add-project" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Add Brand</h4>
                                                                <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                         
                            <div class="message-box">
                            <div class="form-group has-success">
                                                    <label class="control-label">Brand</label>
                                                    <select id="brand_id" class="form-control custom-select">
                                                        <option value="">Choose</option>
                                                        @if (isset($project_data_exclude))
                                                        @foreach($project_data_exclude as $project_exclude)
                                                        <option value="{{$project_exclude->id}}">{{$project_exclude->name}}</option>
                                                        @endforeach
                                                        @endif
                                                   
                                          </select>
                                                    <small class="form-control-feedback"> Choose a brand to compare </small> </div>
                                                     <hr>
                                                    </button>
                                        <button type="button" id="btn_add_brand" class="btn btn-success"><i class="fa fa-check"></i> Add </button>
                            </div>
                  
                                                            </div>
                                                        
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row" id='insight-div-1'>

              <div class="col-lg-12">

                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Service Analysis</h3>
                                          
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="mention-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="mention-chart" style="width:100%; height:500px;"></div>   
                           
                            </div>
                        </div>
                    </div>
               
                </div>
                <div class="row" id='insight-div-2'>

              <div class="col-lg-12">

                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Emotion Rate Analysis</h3>
                                          
                                       
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="emotionRate-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="emotionRate-chart" style="width:100%; height:400px;"></div>   
                           
                            </div>
                        </div>
                    </div>
               
                </div>

                  <div class="row" id='insight-div-3'>

              <div class="col-lg-12">

                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Heatmap</h3>
                                          
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="postEmotionCmt-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="postEmotionCmt-chart" style="width:100%; height:400px;"></div>   
                           
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="row" id='insight-div-4'>

              <!-- <div class="col-lg-12">

                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Post and Emotion Analysis</h3>
                                          
                                       
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="postAnalysis-chart" style="width:100%; height:400px;"></div>   
                           
                            </div>
                        </div>
                    </div> -->
                    <div class="col-lg-12">

                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Post and Interaction Analysis</h3>
                                          
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="postInter-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="postInter-chart" style="width:100%; height:400px;"></div>   
                           
                            </div>
                        </div>
                    </div>
               
                </div>


              
                
       
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}"></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
   
   <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript')}}"></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
    <script type="text/javascript">
var startDate;
var endDate;



var colors =["#98c0d8","#7b858e","#01ad9d","#cb73a9","#a1c652","#dc3545","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];



/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

  $.noConflict();

  jQuery( document ).ready(function ($) {
  //generate legend data
  
    startDate = moment().subtract(3, 'month');
    endDate = moment();
    var periodType= '';
            $('.btn-group .active').each(function(){
                periodType= $(this).attr('id'); 
                
            });
            if(periodType == '')
            {
              periodType='month';
            }
                  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Insight'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();

function ChooseDate(start,end,brand_id)
{
   $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          startDate=start;
          endDate=end;
      var brand_id = brand_id;
         
        mentiondetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id);
        emotionRate(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id);
        //postAnalysisbyEmotion(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id);
        InteractionAnalysisbyPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id);
        CommentEmotionbypostType(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id);
            
}

 function mentiondetail(fday,sday,brand_id)
{//alert(brand_id);
      var mentionchart = document.getElementById('mention-chart');
      var mentionChart = echarts.init(mentionchart);

  $( "#mention-spin" ).show();


    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getserviceAnalysis')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType}
    })
    .done(function( data ) {$( "#mention-spin" ).hide(); console.log(Object.keys(data).length);
//alert("hihi");
   /* var data = {
   "charts": {
    "General": {"User trust"},
    "Customer Service":"Reliable",
    "ATM Service":"Customer Relationship",
    "Insurance": "Reliable",
    "Debit Credit Card": "User trust",
    "Currency Exchange": "Reliable",
    "ငွေထုတ်, ငွေသွင်း, ငွေလွှဲ": "Secure",
    "ငွေချေး":"Secure",
    "Mobile Banking": "Secure",

  }
}*/
/*console.log(data.seriesData);*/
   // var Data = ["General","Customer Service","ATM Service","Insurance","Debit Credit Card","Currency Exchange","ငွေထုတ်, ငွေသွင်း, ငွေလွှဲ","ငွေချေး","Mobile Banking","Customer Relationship","User trust","Reliable","Secure"];
   // var DataLabel = ["User trust","Reliable","Customer Relationship","Reliable","User trust","Reliable","Secure","Secure","Secure"];
   // var Label = ["Customer Relationship","User trust","Reliable","Secure"];

  /*data.seriesData1={
    "General": 1,
    "Customer Service": 2,
    "ATM Service": 3,
    "Insurance": 2,
    "Debit Credit Card": 4,
    "Currency Exchange":2,
    "ငွေထုတ်, ငွေသွင်း, ငွေလွှဲ":5,
    "ငွေချေး": 5,
    "Mobile Banking": 5
    
  };*/
  if(Object.keys(data).length> 0)
  {
  var x = genData(data);
  console.log(x);

  data.seriesData1=x.seriesData;
  console.log("other");

var option={
     color:colors,
      tooltip : {
            trigger: 'axis',
       formatter: function (params) {console.log(params);
        /*const LabelNames =["","Customer Relationship","User trust","Reliable","Secure"];*/
            return '<b>' + params[0].name + '</b><br/><span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[0].color+';"></span>'+ params[0].value +'%';
        }

        },

	  legend: {
           formatter: function (name) {
              return name ;
        },
	    data:['Service Rate'],
	       
	  },
    title: [{
     /*   text: 'Highlighted Text',
        textAlign: 'left'*/
    }
    ],
    grid: {
         
            left: '1%',
            right: '10%',
            bottom: '3%',
            containLabel: true
        },
    xAxis: [{
        type: 'value',
        name:'Service Rate',
     /*   max: 100,*/
        splitLine: {
            show: false
        },
         axisLabel: {
      formatter: function (value, index) {console.log(value);
    // Formatted to be month/day; display year only in the first label
    /*const LabelNames = ["","Customer Relationship","User trust","Reliable","Secure"];
    return  LabelNames[value];*/
    return value + "%";

}
    },

    /* axisLine: {
                lineStyle: {
                    color: "#f8f9fa"
                }
            }*/
   
    }],
    yAxis: [
    	{
	  type : 'category',
	  name: 'Service Name',
	  data: Object.keys(data.seriesData1),
	    axisLabel: {
            interval: 0,
            rotate: 10
        },
        splitLine: {
            show: false
        },
	/*   axisLine: {
                lineStyle: {
                      color: "#bac1c7"
                }
            }*/


	}
 ],
    series: [{
      name:'Service Rate',
        type: 'bar',
        color:colors[0],
        stack: 'chart',
        barMaxWidth:30,
        z:3,
        label: {
            normal: {
                position: 'right',
                show: false
            }
        },
        data: Object.keys(data.seriesData1).map(function (key) {
            return data.seriesData1[key];
        })
    } ]
};
function genData(data) {

  
     var series={};
    for(var key in data) 
        {
           
            var name=data[key].name;
            var value=data[key].value;

            series[name]=value;
           /* seriesData.push(series);*/
            
        }
        return {
            seriesData: series
      
    };
  };
 
    $( "#mention-spin" ).hide();
    mentionChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            mentionChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
}
           
   
         })
         .fail(function() {
              // If there is no communication between the server, show an error
             console.log( "error occured in mentions" );
            });

        }
  function emotionRate(fday,sday,brand_id)

        {
        	 var emotionrate_chart = document.getElementById('emotionRate-chart');
             var emotionRate_chart= echarts.init(emotionrate_chart);

              $("#emotionRate-spin").show();
                $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getemotionRate')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function(data) {
       var emotionRate = [];
       var RateLabel = [];
       var post_total=0;

       for(var i in data) 
       {
       	emotionRate.push(Math.round(data[i].post_count));
       	RateLabel.push(Math.round(data[i].emotionCategory));
       }

       $.each(emotionRate,function(){post_total+= Math.round(parseInt(this)) || 0;});
post_total = kFormatter(post_total);
   
option = {
  color:colors,

    tooltip : { },

	  legend: {
	    data:['Post Count'],
	          formatter: function (name) {
              return name  + ': ' + post_total;
  			}
	  },
      grid: {
          
            
            right: '20%',
            bottom: '5%',
            containLabel: true
        },
	  toolbox: {
	    show : true,
	    feature : {
	      mark : {show: false},
	      dataView : {show: false, readOnly: false},
	      magicType : {show: true, type: ['line','bar']},
	      restore : {show: true},
	      saveAsImage : {show: true}
	    }
	  },
	  calculable : true,
	  xAxis : [
	  {
	   type: 'category', 
	   name: 'Interest Degrees',
	   axisLabel: {
      formatter: function (value, index) {
     
    return  value + " degree";

}
    },
     /*axisLine: {
                lineStyle: {
                    color: "#f8f9fa"
                }
            },*/
	   data : RateLabel
	}
	],
	yAxis : [
	{
	  type : 'value',
	  name: 'Post Count',
	  axisLabel: {
	    formatter: function (e) {
	      return kFormatter(e);
	    }
	  },
	   /*axisLine: {
                lineStyle: {
                    color: colors[0]
                }
            }*/
	}
	],

	series : [
	{
	  name:'Post Count',
            type:'bar',
            color:colors[0],
            barMaxWidth:30,
            data:emotionRate,
            markPoint : {
              large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
	}

	]
	};
	$("#emotionRate-spin").hide();

	   // socialChart.setOption(option);
	   
	   emotionRate_chart.setOption(option, true), $(function() {
	    function resize() {//alert("hi");
	    setTimeout(function() {
	      emotionRate_chart.resize()
	    }, 100)
	  }
	  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
	});


    })

    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });


        }
 function postAnalysisbyEmotion(fday,sday,brand_id)
    {//alert(keyword);
      var postanalysischart = document.getElementById('postAnalysis-chart');
      var postAnalysischart = echarts.init(postanalysischart);

      $("#post-spin").show();
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getemotionRate')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function(data) {//alert(data);alert("hhi");
//var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_mention_total=[];

       var seriesList = [];var arr_Legend = ["Short Posts","Long Posts","image","video"];
       var commentEmotion=["anger","interest","disgust","fear","joy","like","love","neutral","sadness","surprise","trust"];
       var short=[10,20,50,100,200,400,100,800,700,400,500];
       var long=[20,40,60,500,800,900,400,500,800,700,400]; 
       var image=[70,800,500,500,700,500,900,500,800,500,400]; 
       var video=[100,800,400,300,800,400,200,900,500,100,400]; 
     
       var share_total=0;var post_total=0;


   
   seriesList.push({
  name:arr_Legend[0],
  type:'bar',
  data:short,

  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color:"#f5f2f2"
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},
{
  name:arr_Legend[1],
  type:'bar',
  data:long,

  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color:"#f5f2f2"
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},
{
  name:arr_Legend[2],
  type:'bar',
  data:image,

  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color:"#f5f2f2"
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},
{
  name:arr_Legend[3],
  type:'bar',
  data:video,

  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color:"#f5f2f2"
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
}
 );//end-push



 //console.log(seriesList);
option = {
  color: colors,

      tooltip: {
       },

  legend: {
    data:arr_Legend,
    /*formatter: '{name}: '+arr_social_total[sr_of_brand],*/
    padding :0,
  },
  toolbox: {
    show : true,
    feature : {
      mark : {show: false},
      dataView : {show: false, readOnly: false},
      magicType : {show: true, type: ['line','bar']},
      restore : {show: true},
      saveAsImage : {show: true}
    }
  },
  calculable : true,
  xAxis : [
  {
   type: 'category', 
   name:'Emotion',
   data : commentEmotion,
    /*axisLine: {
                lineStyle: {
                      color: "#f8f9fa"
                }
            }*/
}
],
yAxis : [
{
  type : 'value',
  name : 'comment count',
  axisLabel: {
    formatter: function (e) {
      return kFormatter(e);
    }
  },
/*  axisLine: {
                lineStyle: {
                      color: "#bac1c7"
                }
            }*/
}
],

series : seriesList,
};
$("#post-spin").hide();


   // socialChart.setOption(option);
   
   postAnalysischart.setOption(option, true), $(function() {
    function resize() {//alert("hi");
    setTimeout(function() {
      postAnalysischart.resize()
    }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
  

   postAnalysischart.resize();

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}

 function InteractionAnalysisbyPost(fday,sday,brand_id)
    {//alert(keyword);
      var postInterchart = document.getElementById('postInter-chart');
      var postInterChart = echarts.init(postInterchart);

      $("#postInter-spin").show();
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getInteractionByPostType')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function(data) {//alert(data);alert("hhi");
//var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_mention_total=[];
  $("#postInter-spin").hide();
       var seriesList = [];var arr_Legend = ["Interaction Count","Post Count"];
       var postcategory=["Short Posts","Long Posts","Short with image","Long with image","Short with video","Long with video"];
       var interaction=data[0];
       var posts=data[1];


   seriesList.push({
  name:arr_Legend[0],
  type:'bar',
  barMaxWidth:30,
  data:interaction,

  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color:"#f5f2f2"
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},
{
            name:arr_Legend[1],
            type:'line',
            color:colors[1],
            yAxisIndex: 1,
            data:posts,
            markPoint : {
              large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
 );//end-push



 //console.log(seriesList);
option = {
  color: colors,

      tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
        },
        formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
        params.forEach(item => {
            /*console.log(item);
            console.log(item.data);*/
            var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + kFormatter(item.data)  + '</p>'
            rez += xx;
        });

        return rez;
    }
    },
    grid: {
        right: '20%'
    },

  legend: {
    data:arr_Legend,
    /*formatter: '{name}: '+arr_social_total[sr_of_brand],*/
    padding :0,
  },
  toolbox: {
    show : true,
    feature : {
      mark : {show: false},
      dataView : {show: false, readOnly: false},
      magicType : {show: true, type: ['line','bar']},
      restore : {show: true},
      saveAsImage : {show: true}
    }
  },
  calculable : true,
  xAxis : [
  {
   type: 'category', 
   name:'Post',
   data : postcategory,
 /*    axisLine: {
                lineStyle: {
                      color: "#f8f9fa"
                }
            }*/
}
],
yAxis : [
{
  type : 'value',
  name : 'Interaction Count',
  position: 'left',
  axisLabel: {
    formatter: function (e) {
      return kFormatter(e);
    }
  },
  axisLine: {
                lineStyle: {
                      color: colors[0]
                }
            }
},
{
  type : 'value',
  name : 'Post Count',
  position: 'right',
  offset: 80,
  axisLabel: {
    formatter: function (e) {
      return kFormatter(e);
    }
  },
  axisLine: {
                lineStyle: {
                      color: colors[1]
                }
            }
}
],

series : seriesList,
};



   // socialChart.setOption(option);
   
   postInterChart.setOption(option, true), $(function() {
    function resize() {//alert("hi");
    setTimeout(function() {
      postInterChart.resize()
    }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
  

   postInterChart.resize();

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}

function CommentEmotionbypostType(fday,sday,brand_id)
    {//alert(keyword);
      var postEmotionCmtchart = document.getElementById('postEmotionCmt-chart');
      var postEmotionCmtChart = echarts.init(postEmotionCmtchart);

      $("#postEmotionCmt-spin").show();
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getcommentEmotionbyPostType')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function(data_arr) {//alert(data);alert("hhi");
//var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_mention_total=[];

$("#postEmotionCmt-spin").hide();

var xAxis=["anger","interest","disgust","fear","joy","like","love","neutral","sadness","surprise","trust"];
var yAxis =["Short Posts","Long Posts","Short with image","Long with image","Short with video","Long with video"];
//[y,x,count]
var data = data_arr[0];
var min_val=Math.min.apply(Math,data_arr[1]);
var max_val=Math.max.apply(Math,data_arr[1]);
/*var min_val=0;
var max_val=10;*/
data = data.map(function (item) {
    return [item[1], item[0], item[2] || '-'];
});

option = {
    tooltip: {
        position: 'top'
    },
    animation: false,
    grid: {
        height: '50%',
        y: '10%',
        left: '12%',
    },
    xAxis: {
        type: 'category',
        name : 'Emotion',
        data: xAxis,
        splitArea: {
            show: true
        }
    },
    yAxis: {
        type: 'category',
         name : 'Post Type',
        data: yAxis,
        splitArea: {
            show: true
        }
    },
    visualMap: {
        min: min_val,
        max: max_val,
        calculable: true,
        
        orient: 'horizontal',
        left: 'center',
        bottom: '15%'
    },
    series: [{
        name: 'Comment count',
        type: 'heatmap',
        data: data,
        label: {
            normal: {
                show: true
            }
        },
        itemStyle: {
            emphasis: {
                shadowBlur: 10,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    }]
};



   // socialChart.setOption(option);
   
   postEmotionCmtChart.setOption(option, true), $(function() {
    function resize() {//alert("hi");
    setTimeout(function() {
      postEmotionCmtChart.resize()
    }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
  

   postEmotionCmtChart.resize();

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}

        


$('.dateranges').daterangepicker({
    locale: {
            format: 'MMMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        seriesList=[];mentionLabel = [];arr_mention_total=[];
        Social_seriesList=[]; socialLabel = [];arr_social_total=[];
        Clear_emotion_table();
        Clear_reaction_table();
        compare_count=0;
        ChooseDate(startDate,endDate,GetURLParameter('pid'));
      });





    ChooseDate(startDate,endDate,GetURLParameter('pid'));




function appendtoReactiontable(brand_name,like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,sr_no)
{
  $("#tr_header").append("<th style='color:"+colors[sr_no]+"'>"+brand_name+"</th>");
  $("#tr_like").append("<td>"+kFormatter(like_total)+"</td>");
  $("#tr_love").append("<td>"+kFormatter(love_total)+"</td>");
  $("#tr_wow").append("<td>"+kFormatter(wow_total)+"</td>");
  $("#tr_haha").append("<td>"+kFormatter(haha_total)+"</td>");
  $("#tr_sad").append("<td>"+kFormatter(sad_total)+"</td>");
  $("#tr_angry").append("<td>"+kFormatter(angry_total)+"</td>");
  $("#tr_share").append("<td>"+kFormatter(share_total)+"</td>");
}
function Clear_emotion_table()
{
  $("#div_emotion").empty();
                   

 $("#div_emotion").append("<table  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_emotion_header'>"+
                           "<th>Emotion</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                            "<tr id='tr_anger'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('anger'))+"<a href='javascript:void(0)'> Anger</a></td>"+
                            "</tr>"+
                            "<tr id='tr_anticipation'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('interest'))+ "<a href='javascript:void(0)'> Interest</a></td>"+
                            "</tr>"+
                            "<tr id='tr_disgust'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('disgust'))+ " <a href='javascript:void(0)'>Disgust</a></td>"+
                            "</tr>"+
                            "<tr id='tr_fear'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('fear'))+ " <a href='javascript:void(0)'>Fear</a></td>"+
                            "</tr>"+
                            "<tr id='tr_joy'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('joy'))+ "<a href='javascript:void(0)'> Joy</a></td>"+
                            "</tr>"+
                            "<tr id='tr_e_like'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('like'))+ "<a href='javascript:void(0)'> Like</a></td>"+
                            "</tr>"+
                            "<tr id='tr_e_love'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('love'))+ "<a href='javascript:void(0)'> Love</a></td>"+
                            "</tr>"+
                            "<tr id='tr_neutral'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('neutral'))+ "<a href='javascript:void(0)'>Neutral</a></td>"+
                            "</tr>"+
                            "<tr id='tr_sadness'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('sadness'))+ " <a href='javascript:void(0)'> Sadness</a></td>"+
                            "</tr>"+
                            "<tr id='tr_surprise'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('surprise'))+ " <a href='javascript:void(0)'>Surprise</a></td>"+
                            "</tr>"+
                            "<tr id='tr_trust'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('trust'))+"<a href='javascript:void(0)'>Trust</a></td>"+
                            "</tr>"+
                            "</tbody>"+
                              "</table>");
}
function Clear_reaction_table()
{
 $("#div_reaction").empty();

 $("#div_reaction").append(" <table  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_header'>"+
                           "<th>Reaction</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                           "<tr id='tr_like'>"+
                           "<td><a href='javascript:void(0)'>Like</a></td>"+
                            "</tr>"+
                             "<tr id='tr_love'>"+
                             "<td><a href='javascript:void(0)'>Love</a></td>"+
                             "</tr>"+
                             "<tr id='tr_wow'>"+
                             "<td><a href='javascript:void(0)'>WOW</a></td>"+
                             " </tr>"+
                             "<tr id='tr_haha'>"+
                             "<td><a href='javascript:void(0)'>HA HA</a></td>"+
                             "</tr>"+
                             "<tr id='tr_sad'>"+
                             "<td><a href='javascript:void(0)'>Sad</a></td>"+
                             "</tr>"+
                             "<tr id='tr_angry'>"+
                             "<td><a href='javascript:void(0)'>Angry</a></td>"+
                              "</tr>"+
                              "<tr id='tr_share'>"+
                              "<td><a href='javascript:void(0)'>Share</a></td>"+
                              "</tr>"+
                              "</tbody>"+
                              "</table>");
}



//local customize function



     function kFormatter(num) {
    return num > 999 ? (num/1000).toFixed(1) + 'k' : Math.round(num)
}

    function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x1F60D', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }
 

       
  });
    </script>
<style>
.modal-dialog {
    position: absolute;
    top: 200px;
    right: 100px;
    bottom: 0;
    left: 100px;
    z-index: 10040;
    /* overflow: auto; */
}
</style>
@endpush


