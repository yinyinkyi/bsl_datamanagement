@extends('layouts')
@section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                               <h4 class="card-title">Page List</h4>
                              <div class="table-responsive m-t-40">
                                <table id="page_table" class="table dataTable no-footer" style="width:100%">
                                    <thead>
                                    <tr>
                                      <th>Page Name</th>
                                      <th>Last Modified Date</th>
                                      <th></th> 
                                    </tr>
                                    </thead>
                                </table>
                             </div>
             
                        </div>
                     </div>
                </div>
        </div>  

        <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                               <h4 class="card-title">Remove List</h4>
                              <div class="table-responsive m-t-40">
                                <table id="page_remove_table" class="table dataTable no-footer" style="width:100%">
                                    <thead>
                                    <tr>
                                      <th>Page Name</th>
                                      <th></th> 
                                    </tr>
                                    </thead>
                                </table>
                             </div>
             
                        </div>
                     </div>
                </div>
        </div>  

    
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<!-- <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script> -->
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<!-- <script src="{{asset('js/sidebarmenu.js')}}" defer></script> -->
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
   <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}"></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
  <script type="text/javascript">
    var startDate;
    var endDate;

    $(document).ready(function() {


    
    var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };
  // alert(GetURLParameter('pid'));

   var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });



$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   //alert(target);

   window.dispatchEvent(new Event('resize'));

 });

 data_table();
 page_memove_table();



    function data_table()
    { 
     
     // alert('jkdfkd');
      var voiceTable = $('#page_table').DataTable({
        "lengthChange": true,"info": false, "searching": true,
        "processing": true,
        "serverSide": false,
        "autoWidth": false,


        "ajax": {
          "url": '{{ route('get_Mongopages') }}',

          "data": {

           
           
          }

        },
        "initComplete": function( settings, json ) {
         console.log(json);       
        },
        drawCallback: function() {
     $('.select2').select2();
   },

             columns: [
             {data: 'page_name', name: 'page name',orderable: false,searchable: true},
             {data: 'last_crawl_date', name: 'Last Modified Date',orderable: false, searchable: false},
             {data: 'remove', name: 'remove',orderable: false, searchable: false}


             
        ]
        
      }).on('click', '.btn_remove[data-remote]', function (e) {
          var name = $(this).attr('id');
          // alert($("#"+name).val());
          // alert(name);
          // alert(name);
          var url = $(this).data('remote');
          // https://cms.baganintel.com/BSL_DataManagement/page_remove/1597134087164727
          // var  url = url;
          // alert(url);
              swal({
              title: 'Are you sure?',
              text: "You are removing a page ",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, remove it!'
            }).then((result) => {
              if (result.value) {
                // alert(url);
                   $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  data: {method: '_DELETE', submit: true }
              }).always(function (data) {
                // alert(data);
                swal(
                  'Deleted!',
                  'Your Page has been deleted.',
                  'success'
                )
                   $('#'+name).closest('tr').remove();
                  // $('#page_remove_table').DataTable().rows().invalidate().draw();
                   // page_memove_table();
                  // window.location.reload()
              });
                /*swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )*/
              }
            })
    });

    }



    function page_memove_table()
    { 
      var voiceTable = $('#page_remove_table').DataTable({
        "lengthChange": true,"info": false, "searching": true,
        "processing": true,
        "serverSide": false,
        "autoWidth": false,


        "ajax": {
          "url": '{{ route('get_Removepages') }}',

          "data": {

           
           
          }

        },
        "initComplete": function( settings, json ) {
         console.log(json);       
        },
        drawCallback: function() {
     $('.select2').select2();
   },

             columns: [
             {data: 'page_name', name: 'page name',orderable: false,searchable: true},
             {data: 'restore', name: 'restore',orderable: false, searchable: false}


             
        ]
        
      }).on('click', '.btn_restore[data-remote]', function (e) {
        var name = $(this).attr('id');
        // alert(name);
          // var id = $(this).attr('id');
          // alert(id);
          // alert($("#"+name).val());
          // alert(name);
          // alert(name);
          var url = $(this).data('remote');
          // alert(url);
          // alert($('#page_').val());
          // alert($('#'+id).val());
          // $('.btn_restore').val();
          // https://cms.baganintel.com/BSL_DataManagement/page_remove/1597134087164727
          // var  url = url;
          // alert(url);
              swal({
              title: 'Are you sure?',
              text: "You are restoring a page ",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, restore it!'
            }).then((result) => {
              if (result.value) {
                // alert(url);
                   $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  data: {method: '_DELETE', submit: true }
              }).always(function (data) {
                // alert(data);
                swal(
                  'Restored!',
                  'Your Page has been restored.',
                  'success'
                )
                 $('#'+name).closest('tr').remove();
                   // $('#'+name).closest('tr').remove();
                  // $('#page_table').DataTable().rows().invalidate().draw();

                  // window.location.reload()
              });
                /*swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )*/
              }
            })
    });

    }



// change font function



// end change font function



      });



  </script>
   <link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush

