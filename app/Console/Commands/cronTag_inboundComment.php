<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;


class cronTag_inboundComment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbound_comment_tag:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update tags in inbound comment table every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
      $projects = DB::table('projects')->select('*')->get();
      foreach($projects as $project){
      $project_id = $project->id;
      $table = 'temp_'.$project_id.'_inbound_comments';
      $comments = DB::table($table)->select('temp_id','message')->where('tag_flag',0)->limit(1000)->get();
      
      $tags =$this->gettags($project_id); 
    
      foreach ($comments as $comment) {
         $message = $comment->message;
         $message=preg_replace('/(\r\n|\r|\n|\s)+/', "",$message);
         
         $id = $comment->temp_id;
         $tag_string = "";  
         $tags_id = "";
      
             foreach ($tags as $key => $value) {
              
              $kw = $value->keywords;
              $kw = explode(",",$kw);

                foreach ($kw as $key_kw => $value_kw) {
               
                  $value_kw=preg_replace('/(\r\n|\r|\n|\s)+/', "",$value_kw);
                  if($value_kw != '-' && $value_kw !== '' && $value_kw != NULL && isset($value_kw)){

                  if (strpos($message,$value_kw) !== false)
                  {
                    $tag_string.= ',' .$value->name;
                   $tags_id.= ',' .$value->id;
       
                  }


                }
        
        }
        
      }
        if($tag_string != ""){
           $tag_string = substr($tag_string, 1); 
           }    
           if($tags_id != ""){
           $tags_id = substr($tags_id, 1); 
           }  
      

    $tag =  DB::table($table)->where('temp_id', $id)->update(['tags' => $tag_string,'tags_id' => $tags_id,'tag_flag'=> 1,'updated_at' => now()->toDateTimeString()]);
    $tag =  DB::table($table)->where('temp_id', $id)->where('checked_predict',0)->where('change_predict',0)->where('checked_tags',NULL)->update(['checked_tags' => $tag_string,'checked_tags_id' => $tags_id,'updated_at' => now()->toDateTimeString()]);
  
       
      }

  }
     
                      $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
                       $date_time = $date->format('dmYHis');
                       $this->info('Auto tag successully.'. $date_time);
      
    }

    public function gettags($project_id)
    {
      $query="select name,keywords,id from tags where brand_id = ".$project_id;
      $result = DB::select($query);
      return $result;
    }
}
