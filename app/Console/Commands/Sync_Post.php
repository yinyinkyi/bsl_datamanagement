<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\stdClass;
use Illuminate\Support\Facades\Input;
use DB;
use MongoDB;
use App\Project;
use App\MongoboundPost;

// update inbound post
class Sync_Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Sync:PostUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update post data from mongo bound post table where updatedAt is greater than last Updated date every two minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->where('id',34)->get();
      foreach ($projects as $project)
     {
        $id_array = [];
        $project_id = $project->id;
        $query = "SELECT id from temp_".$project_id."_inbound_posts order by created_time desc";
        $id_result = DB::connection('mysql_cron_wunzin')->select($query);
        foreach($id_result as $res) $id_array[] = $res->id;
        
        $query = "SELECT lastUpdated from tbl_logs where table_name='inbound_posts'";
        $result = DB::connection('mysql_cron_wunzin')->select($query);
       
        foreach($result as $log) $last_updated_date =  $log->lastUpdated;
        $last_updated_date=date('Y-m-d', strtotime($last_updated_date));
        $last_updated_date = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$last_updated_date))* 1000);

        $inbound_post_result=MongoboundPost::raw(function ($collection) use($id_array,$last_updated_date) { //print_r($id_array);
        return $collection->aggregate([
          [
          '$match' =>[
               '$and'=> [ 
               ['updatedAt' => ['$gte' => $last_updated_date]],
               ['updatedAt'=> ['$exists'=> true]],
               ['id'=> ['$exists'=> true]],
               ['id'=>[ '$in'=> $id_array ] ]
                        ] 
                      ],
          ]]);
         })->toArray();
         $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
         $updated_time = $date->format('Y-m-d H:i:s');
        
        foreach($inbound_post_result as $row)
        {
            $id ='';$datetime=NULL;
            $full_picture ='';
            $link ='';
            $name ='';
            $message ='';$type ='';$original_message ='';$unicode ='';
            $page_name ='';
            $share =0;
            $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
            $reactUpdatedAt=NULL;$isDeleted=0;

            if(isset($row['message'])) $message = $row['message'];
            if(isset($row['mark'])) 
              {
                if($row['mark'] == 'deleted')
              {
                 $isDeleted =1;
              }
               
              }
            if(isset($row['original_message'])) $original_message = $row['original_message'];
            if(isset($row['unicode'])) $unicode = $row['unicode'];
            if(isset($row['created_time']))
            {
            $utcdatetime = $row["created_time"];
            $datetime = $utcdatetime->toDateTime();
            $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
            $datetime = $datetime->format('Y-m-d H:i:s');
            }
            if(isset($row['id'])) $id =$row['id'];
           
            if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
            if(isset($row['link'])) $link = $row['link'];
            if(isset($row['name'])) $name = $row['name'];
            
            if(isset($row['type'])) $type = $row['type'];
            if(isset($row['page_name'])) $page_name = $row['page_name'];
            if(isset($row['share'])) $share = $row['share'];
            if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand_SyncMongo($row['reaction']['Like']);
            if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand_SyncMongo($row['reaction']['Love']);
            if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand_SyncMongo($row['reaction']['Wow']);
            if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand_SyncMongo($row['reaction']['Haha']);
            if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand_SyncMongo($row['reaction']['Sad']);
            if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand_SyncMongo($row['reaction']['Angry']);
            if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
            if(isset($row['emotion'])) $emotion = $row['emotion'];

             if(isset($row['UpdatedAt']))
            {
            $utcdatetime = $row["UpdatedAt"];
            $reactUpdatedAt = $utcdatetime->toDateTime();
            $reactUpdatedAt=$reactUpdatedAt->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
            $reactUpdatedAt = $reactUpdatedAt->format('Y-m-d H:i:s');
            }
              
                DB::table("temp_".$project_id."_inbound_posts")->where('id',$id)->update([
                  'full_picture' =>$full_picture,
                  'link' =>$link,
                  'name' =>$name,
                  'message' => $message,
                  'original_message'=>$original_message,
                  'unicode'=>$unicode,
                  'type' =>$type,
                  'page_name' =>$page_name,
                  'shared' =>$share,
                  'Liked' =>$Like,
                  'Love' =>$Love,
                  'Wow' =>$Wow,
                  'Haha' =>$Haha,
                  'Sad' =>$Sad,
                  'Angry' =>$Angry,
                  'isDeleted'=>$isDeleted,
                  'created_time' =>$datetime,
                  'updated_at' => now()->toDateTimeString()
                 ]);
        }
                DB::table('tbl_logs')->where('table_name','inbound_posts')->update(['lastUpdated' => $updated_time]);
      }
        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Post data updated Successfully.'. $date_time);

    }

function convertKtoThousand_SyncMongo($s)
{
    if (strpos(strtoupper($s), "K") != false) {
    $s = rtrim($s, "kK");
    return floatval($s) * 1000;
  } else if (strpos(strtoupper($s), "M") != false) {
    $s = rtrim($s, "mM");
    return floatval($s) * 1000000;
  } else {
    return floatval($s);
  }
}
}
