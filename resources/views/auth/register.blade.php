@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

               <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">User Register</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>



                </div>


@if(!isset($userdata))
<!-- <div class="box box-warning"> -->
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        <form method="POST" role="form" class="form-material m-t-10" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                {{csrf_field()}}
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group">
                <label for="username" class="control-label">{{ __('Username') }}</label>
                 <input id="username" type="text"
               class="form-control form-control-line{{ $errors->has('username') ? ' is-invalid' : '' }}"
               name="username" value="{{ old('username') }}" required>
                 @if ($errors->has('username'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                                <label for="role" class="control-label">{{ __('Role') }}</label>
                                <div class="demo-radio-button">
                                    <input name="role" type="radio" class="with-gap" id="radio_3" value="Admin" />
                                    <label for="radio_3">Admin</label>
                                    <input name="role" type="radio" id="radio_4" class="with-gap" value="Operator" checked="checked" />
                                    <label for="radio_4">Operator</label>
                                </div>
            </div>
             <div class="form-group">
                 <label for="email" class="control-label">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="form-control form-control-line{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                 <label for="password" class="control-label">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control form-control-line{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                </div>
            <div class="form-group">
                <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" class="form-control form-control-line" name="password_confirmation" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Register') }}</button>
              </div>
             </form>
                            </div>

                        </div>
                    </div>
                </div>
            <!-- </div> -->
            @else
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        {!! Form::model($userdata, ['method' => 'PATCH','class'=>'form-material m-t-10','enctype'=>'multipart/form-data','route' => ['users.update', $userdata->id]]) !!}
                {{csrf_field()}}
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old( 'name', $userdata->name) }}"  required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group">
                <label for="username" class="control-label">{{ __('Username') }}</label>
                 <input id="username" type="text"
               class="form-control form-control-line{{ $errors->has('username') ? ' is-invalid' : '' }}"
               name="username" value="{{ old( 'username', $userdata->username) }}" required>
                 @if ($errors->has('username'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                    <label for="role" class="control-label">{{ __('Role') }}</label>
                    <div class="demo-radio-button">
                        <input name="role" type="radio" class="with-gap" id="radio_3" value="Admin" <?php if($userdata->role == 'Admin') {?> checked='checked'<?php }?> />
                        <label for="radio_3">Admin</label>
                        <input name="role" type="radio" id="radio_4" class="with-gap" value="Operator" <?php if($userdata->role == 'Operator') { ?> checked='checked'<?php } ?> />
                        <label for="radio_4">Operator</label>
                    </div>
            </div>
             <div class="form-group">
                 <label for="email" class="control-label">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="form-control form-control-line{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old( 'email', $userdata->email) }}" required>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                 <label for="password" class="control-label">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control form-control-line{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                </div>
            <div class="form-group">
                <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" class="form-control form-control-line" name="password_confirmation" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Register') }}</button>
              </div>
             {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>

@endif
              
                  <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">User Accounts List</h4>
                            
                                 <table id="user_table" class="table">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>User Name</th>
                    <th>Email Address</th>
                    <th></th>
                   
                  </tr>
                  </thead>
                 <!--  <tbody>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Apple</a></td>
                    <td>2018-07-06</td>

                    <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Eleven</a></td>
                    <td>2018-07-25</td>
                  <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                    
                  </tr>
             
                  
                  </tbody> -->
                </table>
                            </div>

                        </div>
                    </div>
                </div>  
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script type="text/javascript">

$(document).ready(function() {
    var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };
        $('#user_table').DataTable({
       "lengthChange": true,"info": false, "searching": true,
        processing: true,
        serverSide: false,

                "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        "ajax": {
          "url": '{{ route('getuserlist') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
          "data": {
           
            "source": GetURLParameter('source'),
          }

        },
         columns: [
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
        
    }).on('click', '.del_user[data-remote]', function (e) {
          // var tag_id =this.id;
          var url = $(this).data('remote');
              swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                // alert('go to del fun');
                   $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  data: {method: '_DELETE', submit: true}
              }).always(function (data) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                  $('#user_table').DataTable().rows().invalidate().draw();
                  window.location.reload()
              });
                /*swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )*/
              }
            })
    })


    });

</script>
@endpush

