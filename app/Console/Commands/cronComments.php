<?php

namespace App\Console\Commands;

use Illuminate\Console\Command as BaseCommand;

use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;


// get_commentSentiForMention
class cronComments extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comment:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update comment every  minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
      $projects = DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
      $data =  [];
     
      
      foreach($projects as $project)
      {
     
        $id = $project->id;
        $comment_table = "temp_".$id."_comments";
        

          $comments = DB::connection('mysql_cron_wunzin')->table($comment_table)->select('temp_id','wb_message')->where('sentiment','')->limit(500)->get();
          if(!$comments->isEmpty()){
        foreach($comments as $comment)

        {

          $wb_message[] = $comment->wb_message;
          $request['wb_message']= $comment->wb_message;
          $request['id'] = $comment->temp_id;

          $data[]=$request;
        }
      
        if(count($wb_message) > 0)
        {
          $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_sentiment = 'senti_emo_interest';
         
            $formData = array(
    'raw' =>  $wb_message,
    'threshold' => 0
   
);

           $formData = json_encode($formData);
           // $formData = htmlspecialchars(json_encode($formData), ENT_QUOTES, 'UTF-8');
           try{
           $api_response = $client->post($uri_sentiment, [
                                'form_params' => [
                                'raw' =>  $formData,
                                ],
                             ]);

            $result = ($api_response->getBody()->getContents());

            $json_result_array = json_decode($result, true);
          }
                    catch (\Exception $e) {
  
                $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                $error_message = explode('<html>',$error_message);
                // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                //dd($error_message[0]);
                $error_message=$error_message[0];

                //$error_message ='test';
                $sms_data=json_encode([
                  'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                  'group_id' =>2,
                ]);
                $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                  'headers' => ['Content-Type' => 'application/json'],
                  'body' => $sms_data
                ]);
                 //dd($sms_response->getBody()->getContents());
                  // return false;
                }

  
            $result = $json_result_array[0];

            $count = (Int)count($data);

            for($i=0;$i<$count;$i++)
            {
              
                $id = $data[$i]['id'];
               
                $comment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'updated_at' => now()->toDateTimeString()]);
                $sentiment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('temp_id',$id)->where('checked_sentiment','')->update(['checked_sentiment' => $result['sentiment'][$i]]);
                 
            }

        }
      }
      }
      $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Senti Comment Successfully.'. $date_time);

    }

}
   
//     public function mongohandle()
//     {
//        $query_result = Comment::raw(function ($collection) {
//          return $collection->aggregate([
//         [
//         '$match' =>[
//              '$and'=> [ 
             
                 
//                  ['message'=> ['$exists'=> true]],
//                  ['id'=> ['$exists'=> true]],
//                  ['new'=>['$exists' => false]]
//               ]

//               ]  
//               ]
//               ,

//              ['$limit' => 2000],
//               [
//             '$sort' =>['updated_at'=>-1]


//             ]
 
//     ]);
//    })->toArray();
// // dd($query_result);
//            // 
//          $data = [];
//          $data_message = [];
//          foreach($query_result as $row)
//          {
        
         
//           $request['id'] = $row["id"];
//           $request['message'] = $row["message"];
        
//           $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row["message"]);
//           // $data[]=$data_message;
//           $data[] =$request;
          

//           //call API here with single message
   
//          }
//         if(count($data_message)>0)
//         {
   
//          $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
//           $uri_sentiment = 'sentiment_predict';
//           $uri_emotion = 'emotion_predict';
       
           
//             $formData = array(
//     'raw' =>  $data_message,
   
// );
       
    
//            $formData = json_encode($formData);
      
//           $sentiment_response = $client->post($uri_sentiment, [
//                                 'form_params' => [
//                                 'raw' =>  $formData,
//                                 ],
//                              ]);

//           $emotion_response = $client->post($uri_emotion, [
//                               'form_params' => [
//                               'raw' =>  $formData,
//                                 ],
//                              ]);
         

//             $sentiment_result = ($sentiment_response->getBody()->getContents());
    

//             $json_sentiment_array = json_decode($sentiment_result, true);
 
//             $sentiment_result = $json_sentiment_array['raw'];
//             $emotion_result = ($emotion_response->getBody()->getContents());

     
//             $json_emotion_array = json_decode($emotion_result, true);
//             $emotion_result = $json_emotion_array['raw'];
//             $count = (Int)count($data);
//             for($i=0;$i<$count;$i++)
//             {
//                 $id = $data[$i]['id'];
           

//             $data['sentiment']= str_replace(" ", "",str_replace("'", "", $sentiment_result[$i])) ;

//             $data['emotion']=str_replace(" ", "",str_replace("'", "",  $emotion_result[$i])) ;
  
          
//              $comment =Comment::where('id' , '=' ,$id)->first();
//              $comment->sentiment = $data['sentiment'];
//              $comment->emotion = $data['emotion'];
//              $comment->new = 1;
//              $comment->save();
            
//             }

            


// } 
//     }