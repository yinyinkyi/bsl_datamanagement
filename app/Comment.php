<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Comment extends Eloquent
{

    protected $connection = 'mongodb';
	protected $collection = 'mention_comments';
    protected $dates = ['created_time'];

  public function getDates() {
        return array();
  }
 protected $guarded = [];
 
}
