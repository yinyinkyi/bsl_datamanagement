<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\stdClass;
use Illuminate\Support\Facades\Input;
use DB;
use MongoDB;
use App\MongoboundComment;


// update_inboundComment
class Sync_Comment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Sync:CommentUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get page name from mongo bound comments table and update in mysql inbound_comments table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $projects = DB::connection('mysql_cron_wunzin')->table('projects')->where('id',34)->select('*')->get();

      foreach ($projects as $project)
     {
        $id_array = [];
        $project_id = $project->id;
        // $project_id = 34;
        $query = "SELECT id from temp_".$project_id."_inbound_comments  order by created_time desc ";
        $id_result = DB::connection('mysql_cron_wunzin')->select($query);
        foreach($id_result as $res) $id_array[] = $res->id;
        
        $query = "SELECT lastUpdated from tbl_logs where table_name='inbound_comments'";
        $result = DB::connection('mysql_cron_wunzin')->select($query);
         
        foreach($result as $log) $last_updated_date =  $log->lastUpdated;
        $last_updated_date=date('Y-m-d', strtotime($last_updated_date));
        $last_updated_date = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$last_updated_date))* 1000);

        $comment_result=MongoboundComment::raw(function ($collection) use($id_array,$last_updated_date) { //print_r($id_array);
        return $collection->aggregate([
          [
          '$match' =>[
               '$and'=> [ 
               ['updatedAt' => ['$gte' => $last_updated_date]],
               ['updatedAt'=> ['$exists'=> true]],
               ['id'=> ['$exists'=> true]],
               // ['page_name'=> ['$exists'=> true]],
               ['id'=>[ '$in'=> $id_array ] ]
                        ] 
                      ],
          ]]);
         })->toArray();
        // dd($comment_result);
         $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
         $updated_time = $date->format('Y-m-d H:i:s');
        
        foreach($comment_result as $row)
        {
            $page_name='';$id='';$post_id='';$message='';$original_message='';

            if(isset($row['page_name'])) $page_name=$row['page_name'];
            if(isset($row['id'])) $id =$row['id'];
            if(isset($row['post_id'])) $post_id=$row['post_id'];
            if(isset($row['message'])) $message=$row['message'];
            if(isset($row['original_message'])) $original_message=$row['original_message'];
            if(isset($row['created_time']))
            {
            $utcdatetime = $row["created_time"];
            $datetime = $utcdatetime->toDateTime();
            $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
            $datetime = $datetime->format('Y-m-d H:i:s');
            }
           
  
              
                DB::table("temp_".$project_id."_inbound_comments")->where('id',$id)->update([
                  'post_id' =>$post_id,
                  'message' =>$message,
                  'original_message' =>$original_message,
                  'page_name' =>$page_name,
                  'created_time' =>$datetime,
                  // 'sync' => 1,
                  'updated_at' => now()->toDateTimeString()
                 ]);
        }
                
      }
      DB::table('tbl_logs')->where('table_name','inbound_comments')->update(['lastUpdated' => $updated_time]);
        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info(' Comment data updated Successfully.'. $date_time);
    }
}
