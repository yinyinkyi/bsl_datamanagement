<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fb_token extends Model
{
    //
    protected $table = 'fb_tokens';
	protected $dates = ['created_at'];
}
