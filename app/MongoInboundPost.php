<?php


namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class MongoInboundPost extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'inbound_posts';
  protected $dates = ['created_time'];

  public function getDates() {
        return array();
  }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
 
 
}