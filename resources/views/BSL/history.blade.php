@extends('layouts')
@section('content') 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row"> 
                             <div class="d-flex m-t-8">
                                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                        <div class='input-group mb-3'>
                                          <input type='text' id="calender" class="form-control dateranges" style="color: #67757c !important; width:210px; padding: 0.2em 0.2em 0px;" />
                                          <div class="input-group-append">
                                            <span class="input-group-text">
                                              <span class="ti-calendar"></span>
                                            </span>
                                          </div>
                                        </div>
                                    </div>
                             </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <div class="row">
	        <div class="col-12">
	            <div class="card">
	                <div class="card-body">
				      <div class="table-responsive m-t-10">
				        <table id="history_table" class="table" style="width:100%">
				            <thead style="display: none">
				            <tr>
				              <!-- <th>Manual By</th>
				           	  <th>Manual Date</th>
				              <th>Checked Senti</th>
				              <th>Decided Kw</th>
				              <th>Checked tags</th>
				              <th>Hide</th> -->
				            </tr>
				            </thead>
				        </table>
				     </div>
 					</div>
				</div>
			</div>
		</div>
    
	@endsection
	@push('scripts')
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<!-- <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script> -->
	<!-- Bootstrap tether Core JavaScript -->
	<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
	<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
	<!--Wave Effects -->
	<script src="{{asset('js/waves.js')}}" defer></script>
	<!--Menu sidebar -->
	<!-- <script src="{{asset('js/sidebarmenu.js')}}" defer></script> -->
	<!--stickey kit -->
	<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
	<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
	<!--Custom JavaScript -->
	<script src="{{asset('js/custom.min.js')}}" defer></script>
	<!-- ============================================================== -->
	<!-- This page plugins -->
	<!-- ============================================================== -->



	<!-- Chart JS -->
	<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

	<!-- Flot Charts JavaScript -->
	<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
	<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
	<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
	<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

	<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
	<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

	<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
	<!-- Date range Plugin JavaScript -->
	<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
	<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
	<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

	<!-- DataTables -->
	  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
	  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
	  <!-- This is data table -->
	  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
	  <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}"></script>
	  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
	  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
	  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
	  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
	  <script type="text/javascript">
		    var startDate;
		    var endDate;

		    $(document).ready(function() {
		    	$('.dateranges').daterangepicker({
				  locale: {
				    format: 'MMM D, YYYY'
				  },
				  ranges: {
				    'Today': [moment(), moment()],
				    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				    'This Month': [moment().startOf('month'), moment().endOf('month')],
				    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				  },
				  startDate: startDate,
				  endDate: endDate,
			        },function(start, end) {//alert("hihi")
			        var startDate;
			        var endDate;

			        startDate = start;
			        endDate = end;
			        ChooseDate(startDate,endDate);
		     		 });
		        	startDate = moment().subtract(1, 'month');
		    		endDate = moment();

		            ChooseDate(startDate,endDate);

		            $('#calender').change(function(){
		                ChooseDate(startDate,endDate);
		            });

		    		function ChooseDate(start,end)
		   			{
					// alert(start);
					   $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
					   startDate=start;
					   endDate=end;

				       var start = new Date(startDate);
				       var end = new Date(endDate);
				       var timeDiff = Math.abs(end.getTime() - start.getTime());
				       var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

			          if(parseInt(diffDays) < 7) //in terms of day
			          { 
			            //alert("7");
			            $("#day").show();
			            $("#week").hide();
			            $("#month").hide();
			          }

		      
			         else if (parseInt(diffDays) == 7)//in terms of week
			         {
			           //alert("==7");
			           $("#day").show();
			           $("#week").show();
			           $("#month").hide();
			         }
			         else if (parseInt(diffDays) > 7)//in terms of month
			         {
			            //alert(">7");
			            $("#day").show();
			            $("#week").show();
			            $("#month").show();

			         }
		                Create_DataTable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));

		            }

		            function Create_DataTable(fday,sday)
		            {
		            	// alert('hey');
				      var historyTable = $('#history_table').DataTable({
				        "lengthChange": false,
				        "searching": false,
				        "processing": false,
				        "serverSide": true,
				        "ordering": false,
				        "bDestroy": true,
				        "headers": {
				          'X-CSRF-TOKEN': '{{csrf_token()}}' 
				        },
				        "ajax": {
				          "url": '{{ route('get_history') }}',

				          "data": {
				            "fday": fday,
				            "sday": sday,
				          }

				        },
				       
	                 columns: [
			            {data: 'manual_by', name: 'manual_by'},
			            {data: 'manual_date', name: 'manual_date'},
			            {data: 'checked_sentiment', name: 'checked_sentiment'},
			            {data: 'decided_keyword', name: 'decided_keyword'},
			            {data: 'checked_tags', name: 'checked_tags'},
			            {data: 'isHide', name: 'isHide'}
			        ],
		        
		      			})
				 
		            }



		   		});
    </script>
  <link href="{{asset('css/own.css')}}" rel="stylesheet">
  @endpush
