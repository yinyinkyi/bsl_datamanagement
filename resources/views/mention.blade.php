@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
@section('filter')
<li  class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search"></i></a>
<div id="search_form" class="dropdown-menu scale-up-left" id="dlDropDown"  style="padding-left: 5%">
<ul class="mega-dropdown-menu row">
 
<li class="col-lg-12" >
 <form>
   <div class="row p-t-20">
    <div class="col-md-6">
      <div class="form-group has-success">
        <label class="control-label">Sentiment</label>
        <select class="form-control custom-select" id="sentiment_option">
          <option value="">All</option>
          <option value="pos">Positive</option>
          <option value="neg">Negative</option>
        </select>
        <small class="form-control-feedback"> filter with Sentiment </small> </div>
      </div>
      <!--/span-->
      <div class="col-md-6">
        <div class="form-group has-success">
          <label class="control-label">Location</label>
          <select class="form-control custom-select">
            <option value="">All</option>
            <option value="">Yangon</option>
            <option value="">Mandalay</option>
            <option value="">Naypyitaw</option>
            <option value="">Pyay</option>
            <option value="">Taunggyi</option>
            <option value="">Monywa</option>
          </select>
          <small class="form-control-feedback"> filter with gender </small> </div>
        </div>
        <!--/span-->
      </div>
      <div class="row p-t-20">
        <div class="col-md-6">
          <div class="form-group has-success">
            <label class="control-label">Gender</label>
            <select class="form-control custom-select">
              <option value="">All</option>
              <option value="">Male</option>
              <option value="">Female</option>

            </select>
            <small class="form-control-feedback"> filter with gender </small> </div>
          </div>
          <!--/span-->
          <div class="col-md-6">
            <div class="form-group has-success">
              <label class="control-label">Keyword</label>
              <input type="text" class="form-control" id="txtkeyword" placeholder="Keyword">
              <small class="form-control-feedback"> filter with Keyword </small> </div>
            </div>
          </div>
          <div class="row p-t-20">
            <div class="col-md-6">
              <div class="form-group has-success">
                <label class="control-label">Influencer Score</label>
                <div class="form-control" id="range_influencer"></div>
                <small class="form-control-feedback"> filter with Influencer Score </small> </div>
              </div>
              <div class="col-md-6">
                <div class="form-group has-success">
                  <label class="control-label">Age</label>
                  <select class="form-control custom-select">
                    <option value="">10~19 years</option>
                    <option value="">20~45 years</option>
                    <option value="">46~60 years</option>
                    <option value="">greater than 60</option>
                  </select>
                  <small class="form-control-feedback"> filter with Age </small> </div>
                </div>
              </div>
               <div class="row p-t-20" align="center">
                 <div class="col-lg-12">
    <!-- <input type="button"  class="btn btn-success" Value="Search"><a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search"></i></a></input> -->
    <a nohref id="btn_search" class="nav-link dropdown-toggle  waves-effect waves-dark btn btn-success" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Search</a>    
               </div>
             </div>
 </form>
</li>
</ul>
</div>
</li>
@endsection
@section('comparison')

<li class="nav-item dropdown">
                       <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            
                          <!--     <button onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" type="button" class="btn waves-effect waves-light btn-outline-warning">Comparison</button>  --> 
                                <button  onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" class="btn btn-secondary waves-effect waves-light" style="background:#e9ebee;color:#263238" type="button"><span class="btn-label"><i class="ti-settings"></i></span>Comparison</button>
                            </a>
                           
 </li>
 <li class="nav-item dropdown">
                            <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            <button  onclick="window.location='{{route('insight', ['pid' => $pid])}}'" class="btn btn-secondary waves-effect waves-light" style="background:#e9ebee;color:#263238" type="button"><span class="btn-label"><i class="ti-settings"></i></span>Insight</button>  
                            </a>
                           
 </li>
@endsection

<div class="row page-titles">
  <div class="col-md-5 col-8 align-self-center">
    <h3 class="text-themecolor m-b-0 m-t-0">Mention</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="javascript:void(0)">Mention</a></li>
      <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>
    </ol>
  </div>
  <div class="col-md-7 col-4 align-self-center">
    <div class="d-flex m-t-10 justify-content-end">
<!--       <div class="btn-group btn-group-lg  mb-3" role="group" aria-label="Basic example">
        <button id="day" type="button" class="btn btn-success">Day</button>
        <button id="week" type="button" class="btn btn-success">Week</button>
        <button  id="month" type="button" class="btn btn-success">Month</button>
      </div> -->
       <div class="btn-group btn-group-lg  mb-3" data-toggle="buttons">
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options" value="day" autocomplete="off" class="btn btn-success" > Day
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options" value="week" autocomplete="off" class="btn btn-success"> Week
                                            </label>
                                            <label class="btn btn-primary active">
                                                <input type="radio" name="options" value="month" autocomplete="off" class="btn btn-success" checked> Month
                                            </label>
        </div>
      <div class="d-flex m-r-20 m-l-10 hidden-md-down">
        <div class='input-group mb-3'>
          <input type='text' class="form-control dateranges" style="datepicker" />
          <div class="input-group-append">
            <span class="input-group-text">
              <span class="ti-calendar"></span>
            </span>
          </div>
        </div>
      </div>
   <!--    <div class="">
        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
      </div> -->
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

<!-- Row -->

<div class="row" >
  <div class="col-lg-12">
    <div class="card">



      <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item" id="mention-div-1"> <a class="nav-link active" data-toggle="tab" href="#mentions" role="tab">Mentions</a> </li>
        <li class="nav-item" id="mention-div-2"> <a class="nav-link" data-toggle="tab" href="#media" role="tab">Social Media Reach</a> </li>

      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="mentions" role="tabpanel">
          <div class="card-body">
           <div style="display:none"  align="center" style="vertical-align: top;" id="mention-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>

           <div id="mention-chart" style="width:100%; height:400px;"></div>   
         </div>
       </div>

       <div class="tab-pane" id="media" role="tabpanel">
        <div class="card-body">
         <div style="display:none"  align="center" style="vertical-align: top;" id="socialmedia-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
         <div id="socialmedia-bar-chart" style="width:100%; height:400px;">

         </div>
       </div>
     </div>

   </div>  <!-- endof tab content -->



 </div>
</div>
</div>

<div class="row" >
  <div class="col-lg-12">
    <div class="card">



      <ul class="nav nav-tabs profile-tab" role="tablist">
       <li class="nav-item" id="mention-div-3"> <a class="nav-link active" data-toggle="tab" href="#reaction" role="tab">Reaction</a> </li>
       <li class="nav-item" id="mention-div-4"> <a class="nav-link" data-toggle="tab" href="#emotions" role="tab">Emotions</a> </li>
     </ul>

     <div class="tab-content">
      <div class="tab-pane active" id="reaction" role="tabpanel">
        <div class="card-body">
         <div style="display:none" width="100%" align="center" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
         <div id="reaction-bar-chart" style="width:100%; height:400px;"></div>
       </div>
     </div>
     <div class="tab-pane" id="emotions" role="tabpanel">
      <div class="card-body">
       <div style="display:none" width="100%" align="center" id="emotion-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
       <div id="emotion-bar-chart" style="width:100%; height:400px;"></div>
     </div>
   </div>

 </div>  <!-- endof tab content -->



</div>
</div>
</div>
<!-- Row -->

<div class="row" id="mention-div-5">
  <!-- Column -->
  <div class="col-lg-12">
    <div class="card">
 
        <ul class="nav nav-tabs profile-tab" role="tablist">
         <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Popular" role="tab">Posts Mentions</a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Latest" role="tab">Text</a> </li>
        </ul>
         <div class="tab-content">
         <div class="tab-pane active" id="Popular" role="tabpanel">
          <div class="card-body">
            <div class="d-flex no-block align-items-center">
           <div class="ml-auto" style="float:right;">
            <ul class="list-inline">
              <li>
               <button type="button" class="btn btn-success" name="bookmark" id="btn_bookmark">Bookmark</button>
               
              </li>

            </ul>
          </div>
        </div>
         <div style="display:none"  align="center" style="vertical-align: top;" id="modal-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
        <table id="all_mention" class="table" style="width:100%;">
          <thead style='display:none;'>
            <tr>
              <th>Rendering engine</th>
              <th>Setting</th>
            </tr>
          </thead>

        </table>
         </div>
            </div>
            <div id="show-task" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Comment Type</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="message-box">
                                <div class="comment-widgets message-scroll  comment_data">
                                    <!-- Message -->
                                  
                                   
                                </div>
                            </div>
                                                            </div>
                                                     
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
           <div class="tab-pane" id="Latest" role="tabpanel">
         <div class="card-body">
          <div class="d-flex no-block align-items-center">
           <div class="ml-auto" style="float:right;">
            <ul class="list-inline">
              <li>
               <button type="button" class="btn btn-success" name="bookmark_comment" id="btn_bookmark_comment">Bookmark</button>
               
              </li>

            </ul>
          </div>
        </div>
        <table id="all_mention_comment" class="table" style="width:100%;">
          <thead style='display:none;'>
            <tr>
              <th>Rendering engine</th>
              <th>Setting</th>
            </tr>
          </thead>

        </table>
        </div>
           </div>
       </div>
        <!-- <div class="d-flex no-block align-items-center">
          <h3 class="card-title">Posts</h3>
    
          <div class="ml-auto">
            <ul class="list-inline">
              <li>
               <button type="button" class="btn btn-success" name="bookmark" id="btn_bookmark">Bookmark</button>
                <span class="text-warning" display="none"> Done!</span>
              </li>

            </ul>
          </div>

        </div>
        <table id="all_mention" class="table">
          <thead style='display:none;'>
            <tr>
              <th>Rendering engine</th>
              <th>Setting</th>
            </tr>
          </thead>

        </table> -->



  
    </div>
  </div>

</div>




@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}"></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}"></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}"></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}"></script>
  <script type="text/javascript">

  var startDate;
  var endDate;
  var mention_total;
  var bookmark_array=[];
  var bookmark_remove_array=[];
  var bookmark_comment_array=[];
  var bookmark_comment_remove_array=[];

  var colors =["#98c0d8","#7b858e","#01ad9d","#cb73a9","#a1c652","#dc3545","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];
/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/
  $.noConflict();

  jQuery( document ).ready(function ($) {
 $('.select2').select2()
  $("#range_influencer").ionRangeSlider({
    grid: true,
    min: 0,
    max: 10,
    from: 0,
    prefix: "Score ",
    max_postfix: "+"
  });

  startDate = moment().subtract(3, 'month');
  endDate = moment();

  var periodType= '';
periodType=$('input[name=options]:checked').val();
            //alert(periodType);
            if(periodType == '')
            {
              periodType='month';
            }
  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };
  function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Mention'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();
  function ChooseDate(start,end,is_search,calType)
{//alert(start);alert(end),alert(keyword)
 $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
 startDate=start;
 endDate=end;


 var brand_id = GetURLParameter('pid');
 var start = new Date(startDate);
 var end = new Date(endDate);
 var timeDiff = Math.abs(end.getTime() - start.getTime());
 var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 


          if(parseInt(diffDays) < 7) //in terms of day
          { 
            //alert("7");
            $("#day").show();
            $("#week").hide();
            $("#month").hide();
          }

          
         else if (parseInt(diffDays) == 7)//in terms of week
         {

           //alert("==7");
           $("#day").show();
           $("#week").show();
           $("#month").hide();


         }
          else if (parseInt(diffDays) > 7)//in terms of month
          {
            //alert(">7");

            $("#day").show();
            $("#week").show();
            $("#month").show();

          }

          mentiondetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType,is_search);
          socialmediadetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType,is_search);
          Emotiondetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType,is_search);
          if(calType === '')
          {
          Create_DataTable(is_search);
          }

        }
        function mentiondetail(fday,sday,brand_id,periodType,is_search)
          {//alert(fday + "" + periodType + "" + brand_id);
          var mentionchart = document.getElementById('mention-chart');
          var mentionChart = echarts.init(mentionchart);
          if(is_search === 1)
          {
            var txtkeyword =$("#txtkeyword").val();
            var e = document.getElementById("sentiment_option");
            var opt_sentiment = e.options[e.selectedIndex].value;
           // alert(opt_sentiment);
          }


          $( "#mention-spin" ).show();


          $.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getmentiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType,keyword:txtkeyword,sentiment:opt_sentiment}
    })
    .done(function( data ) {//alert("hihi");

    //  $("#sentiment-spin").hide();
      //console.log(data);//mention
     // console.log(data[1]);//sentiment
/*       if(data.length > 0)
       {
       $("#mention-total").text("Total = "+data[data.length-1].total);
      // When the response to the AJAX request comes back render the chart with new data
    }*/
    var mentions = [];
    var positive = [];
    var negative = [];
    var mentionLabel = [];
    var sentimentLabel = [];
    var mention_total=0;

    for(var i in data) 
    {
        //alert(data[i].mentions); 
        mentions.push(data[i].mention);
        mentionLabel.push(data[i].periodLabel);

      }
      $.each(mentions,function(){mention_total+=Math.round(parseInt(this)) || 0;});

      option = {
        color:colors[0],

        tooltip : {
          trigger: 'axis',
          formatter: function (params) {
            return '<b>' + params[0].name + '</b><br/><span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[0].color+';"></span>'+ params[0].seriesName + ' : '+  kFormatter(params[0].value);
          }

        },

        legend: {
          data:['mention'],
          formatter: '{name}: '+mention_total,
          padding :0,


        },
        toolbox: {
          show : true,
          feature : {
            mark : {show: false},
            dataView : {show: false, readOnly: false},
            magicType : {show: true, type: ['line','bar']},
            restore : {show: true},
            saveAsImage : {show: true}
          }
        },
        calculable : true,
       /* xAxis : [
        {
           
            data : mentionLabel,
         formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    var date = new Date(value);
    var texts = [(date.getMonth() + 1), date.getDate()];
    if (idx === 0) {
        texts.unshift(date.getYear());
    }
    return texts.join('/');
}
            
        }
      
        ],*/
        xAxis: [{
         type: 'category', 
         axisLabel: {
          formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var arr = value.split(' - ');
    var date = new Date(arr[0]);
      // console.log(date);
      if(periodType === 'day')
      {
        var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];

      }
      else 
      {

        var texts = [date.getFullYear(), monthNames[date.getMonth()]];
      }



      return texts.join('-');

    }
  },
     data : mentionLabel/*,
    axisLabel: {
     formatter: function (value, index) {
        console.log(value);
    // Formatted to be month/day; display year only in the first label
    var date = new Date(value);
    var texts = [(date.getMonth() + 1), date.getDate()];
    if (idx === 0) {
        texts.unshift(date.getYear());
    }
    return texts.join('/');
}
}*/
}],

yAxis : [
{
  type : 'value'
}
],
series : [
{
  xAxes: [{ 
    ticks: {
                  fontColor: "#f5f2f2", // this here
                },
              }],

              name:'mention',
              type:'line',
              data:mentions,
              barMaxWidth:30,
              color:colors[0],
              markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
              }
            }

            ]
          };

          $( "#mention-spin" ).hide();

          mentionChart.setOption(option, true), $(function() {
            function resize() {
              setTimeout(function() {
                mentionChart.resize()
              }, 100)
            }
            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
          });


        })
.fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in mentions" );
            });

}


function socialmediadetail(fday,sday,brand_id,periodType,is_search)
    {//alert(keyword);
      var socialchart = document.getElementById('socialmedia-bar-chart');
      var socialChart = echarts.init(socialchart);

      var reactionchart = document.getElementById('reaction-bar-chart');
      var ReactionChart = echarts.init(reactionchart);

       if(is_search === 1)
          {
            var txtkeyword =$("#txtkeyword").val();
            var e = document.getElementById("sentiment_option");
            var opt_sentiment = e.options[e.selectedIndex].value;
          }


      $("#socialmedia-spin").show();
      $("#reaction-spin").show();
      var brand_id = GetURLParameter('pid');
        //var brand_id = 22;
       /* alert (brand_id);
       alert(periodType);*/
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getinteractiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType,keyword:txtkeyword,sentiment:opt_sentiment }
    })
    .done(function(data) {//alert(data);alert("hhi");


      //console.log("social");
      // console.log(data);//mention

       var Like = [];
       var Love = [];
       var Haha = [];
       var Wow = [];
       var Angry = [];
       var Sad = [];


       var Like_total =0;
       var Love_total =0;
       var Haha_total =0;
       var Wow_total =0;
       var Angry_total =0;
       var Sad_total =0;


       var socials = [];

       var socialLabel = [];
       var socialsfortotal = [];

       var social_total=0;



       for(var i in data) 
       {
        Like.push(Math.round(data[i].Like));
        Love.push(Math.round(data[i].Love));
        Haha.push(Math.round(data[i].Haha));
        Wow.push(Math.round(data[i].Wow));
        Angry.push(Math.round(data[i].Angry));
        Sad.push(Math.round(data[i].Sad));


        var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].post_count)
         // alert(mediaReach);
         socials.push(Math.round(mediaReach));
         socialsfortotal.push(Math.round(mediaReach));
         socialLabel.push(data[i].periodLabel);

       }

       $.each(Like,function(){Like_total+= Math.round(parseInt(this)) || 0;});
       $.each(Love,function(){Love_total+= Math.round(parseInt(this)) || 0;});
       $.each(Haha,function(){Haha_total+= Math.round(parseInt(this)) || 0;});
       $.each(Wow,function(){Wow_total+= Math.round(parseInt(this)) || 0;});
       $.each(Angry,function(){Angry_total+= Math.round(parseInt(this)) || 0;});
       $.each(Sad,function(){Sad_total+= Math.round(parseInt(this)) || 0;});
       $.each(socialsfortotal,function(){social_total+= Math.round(parseInt(this)) || 0;});

       social_total = kFormatter(social_total);

       /* $("#mention-total").text("Total = "+mention_total);*/
       reaction_option = {
        color:colors,


        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross'
          },
          formatter: function (params) {
            var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
            let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
       params.forEach(item => {
            /*console.log(item);
            console.log(item.data);*/
            var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + kFormatter(item.data)  + '</p>'
            rez += xx;
          });

       return rez;
     }
   },

   legend: {
    data:['Like','Love','Ha Ha','Wow','Angry','Sad' ],
    formatter: function (name) {
      if(name === 'Like')return name + ': ' + kFormatter(Like_total);
      if(name === 'Love')return name + ': ' + kFormatter(Love_total);
      if(name === 'Ha Ha')return name + ': ' + kFormatter(Haha_total);
      if(name === 'Wow')return name + ': ' + kFormatter(Wow_total);
      if(name === 'Angry')return name + ': ' + kFormatter(Angry_total);
      if(name === 'Sad')return name + ': ' + kFormatter(Sad_total);

    },
    padding :0,
  },
  toolbox: {
    show : true,
    feature : {
      mark : {show: false},
      dataView : {show: false, readOnly: false},
      magicType : {show: true, type: ['line','bar']},
      restore : {show: true},
      saveAsImage : {show: true}
    }
  },
  calculable : true,
  xAxis : [
  {
   type: 'category', 
   axisLabel: {
    formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var arr = value.split(' - ');
    var date = new Date(arr[0]);
       //console.log(date);
       if(periodType === 'day')
       {
        var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];

      }
      else 
      {

        var texts = [date.getFullYear(), monthNames[date.getMonth()]];
      }



      return texts.join('-');

    }
  },
  data : socialLabel
}
],
yAxis : [
{
  type : 'value',
  axisLabel: {
    formatter: function (e) {
      return kFormatter(e);
    }
  }

}
],

series : [
{
  name:'Like',
  type:'bar',
  data:Like,
  color:colors[0],
  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color: '#f8fbfb'
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }

},

{
  name:'Love',
  type:'bar',
  data:Love,
  color:colors[5],
  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color: '#f8fbfb'
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},

{
  name:'Ha Ha',
  type:'bar',
  data:Haha,
  color:colors[2],
  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color: '#f8fbfb'
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},

{
  name:'Wow',
  type:'bar',
  data:Wow,
  color:colors[3],
  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color: '#f8fbfb'
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},

{
  name:'Angry',
  type:'bar',
  data:Angry,
  color:colors[4],
  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color: '#f8fbfb'
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},

{
  name:'Sad',
  type:'bar',
  data:Sad,
  color:colors[1],
  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color: '#f8fbfb'
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
}

]
};

social_option = {
  color:colors,



  tooltip : {
    trigger: 'axis',
    formatter: function (params) {console.log("test");console.log(params);
      return '<b>' + params[0].name + '</b><br/><span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[0].color+';"></span>'+ params[0].seriesName + ' : '+  kFormatter(params[0].value);
    }

  },

  legend: {
    data:['social media reach' ],
    formatter: '{name}: '+social_total,
    padding :0,
  },
  toolbox: {
    show : true,
    feature : {
      mark : {show: false},
      dataView : {show: false, readOnly: false},
      magicType : {show: true, type: ['line','bar']},
      restore : {show: true},
      saveAsImage : {show: true}
    }
  },
  calculable : true,
  xAxis : [
  {
   type: 'category', 
   axisLabel: {
    formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var arr = value.split(' - ');
    var date = new Date(arr[0]);
       //console.log(date);
       if(periodType === 'day')
       {
        var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];

      }
      else 
      {

        var texts = [date.getFullYear(), monthNames[date.getMonth()]];
      }



      return texts.join('-');

    }
  },
  data : socialLabel
}
],
yAxis : [
{
  type : 'value',
  axisLabel: {
    formatter: function (e) {
      return kFormatter(e);
    }
  }
}
],

series : [
{
  name:'social media reach',
  type:'line',
  data:socials,
  barMaxWidth:30,
  color:colors[0],
  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color: '#f8fbfb'
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
}

]
};
$("#socialmedia-spin").hide();
$("#reaction-spin").hide();
   // socialChart.setOption(option);
   
   socialChart.setOption(social_option, true), $(function() {
    function resize() {//alert("hi");
    setTimeout(function() {
      socialChart.resize()
    }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   ReactionChart.setOption(reaction_option, true), $(function() {
    function resize() {
      setTimeout(function() {
        ReactionChart.resize()
      }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
  });


   socialChart.resize();

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function Emotiondetail(fday,sday,brand_id,periodType,is_search)
          {//alert(periodType);
            var emotionchart = document.getElementById('emotion-bar-chart');
            var emotionChart = echarts.init(emotionchart);
               if(is_search === 1)
          {
            var txtkeyword =$("#txtkeyword").val();
            var e = document.getElementById("sentiment_option");
            var opt_sentiment = e.options[e.selectedIndex].value;
          }
            $("#emotion-spin").show();
            $.ajax({
             type: "GET",
             dataType:'json',
             contentType: "application/json",
      url: "{{route('getemotiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType,keyword:txtkeyword,sentiment:opt_sentiment}
    })
    .done(function( data ) {//alert("hihi");

      $( "#emotion-spin" ).hide();

      //console.log(data);//emotion

      var anger = [];
      var anticipation = [];
      var disgust = [];
      var fear = [];
      var joy = [];
      var like = [];
      var love = [];
      var neutral = [];
      var sadness = [];
      var surprise = [];
      var trust = [];
      
      var EmotionLabel = [];


      var anger_total =0;
      var anticipation_total =0;
      var disgust_total =0;
      var fear_total =0;
      var joy_total =0;
      var like_total =0;
      var love_total =0;
      var neutral_total =0;
      var sadness_total =0;
      var surprise_total =0;
      var trust_total =0;



      for(var i in data) 
      {
        //alert(data[i].mentions); 
        anger.push(data[i].anger);
        anticipation.push(data[i].anticipation);
        disgust.push(data[i].disgust);
        fear.push(data[i].fear);
        joy.push(data[i].joy);
        like.push(data[i].like);
        love.push(data[i].love);
        neutral.push(data[i].neutral);
        sadness.push(data[i].sadness);
        surprise.push(data[i].surprise);
        trust.push(data[i].trust);

        EmotionLabel.push(data[i].periodLabel);

      }

      $.each(anger,function(){anger_total += Math.round(parseInt(this)) || 0;});
      $.each(anticipation,function(){anticipation_total += Math.round(parseInt(this)) || 0;});
      $.each(disgust,function(){disgust_total += Math.round(parseInt(this)) || 0;});
      $.each(fear,function(){fear_total += Math.round(parseInt(this)) || 0;});
      $.each(joy,function(){joy_total += Math.round(parseInt(this)) || 0;});
      $.each(like,function(){like_total += Math.round(parseInt(this)) || 0;});
      $.each(love,function(){love_total += Math.round(parseInt(this)) || 0;});
      $.each(neutral,function(){neutral_total += Math.round(parseInt(this)) || 0;});
      $.each(sadness,function(){sadness_total += Math.round(parseInt(this)) || 0;});
      $.each(surprise,function(){surprise_total += Math.round(parseInt(this)) || 0;});
      $.each(trust,function(){trust_total += Math.round(parseInt(this)) || 0;});

      /*console.log(EmotionLabel);*/

      emotionChart.title = 'Emotion Chart';
    

      emotion_option = {
        color:colors,

        tooltip : {
          trigger: 'axis',
            /* formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
        console.log(rez); //quite useful for debug
        params.forEach(item => {
             console.log("hihi"); 
            console.log(item);
            console.log(item.series.color); //quite useful for debug
            var xx = '<p>'   + colorSpan(item.series.color) + ' ' + item.seriesName + ': ' + item.data  + '</p>'
            rez += xx;
        });

        return rez;
      }*/

    },
    legend: {
      data: ['anger', 'interest', 'disgust', 'fear', 'joy' , 'like', 'love', 'neutral', 'sadness', 'surprise', 'trust'],
       formatter: function (name) {
      if(name === 'anger')return name + ': ' + kFormatter(anger_total);
      if(name === 'interest')return name + ': ' + kFormatter(anticipation_total);
      if(name === 'disgust')return name + ': ' + kFormatter(disgust_total);
      if(name === 'fear')return name + ': ' + kFormatter(fear_total);
      if(name === 'joy')return name + ': ' + kFormatter(joy_total);
      if(name === 'like')return name + ': ' + kFormatter(like_total);
      if(name === 'love')return name + ': ' + kFormatter(love_total);
      if(name === 'neutral')return name + ': ' + kFormatter(neutral_total);
      if(name === 'sadness')return name + ': ' + kFormatter(sadness_total);
      if(name === 'surprise')return name + ': ' + kFormatter(surprise_total);
      if(name === 'trust')return name + ': ' + kFormatter(trust_total);

    },
      padding :0,
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis:  {
      type: 'value'
    },
    yAxis: {
      type: 'category', 
      axisLabel: {
        formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var arr = value.split(' - ');
    var date = new Date(arr[0]);
    //console.log(date);
    if(periodType === 'day')
    {
      var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];

    }
    else 
    {

      var texts = [date.getFullYear(), monthNames[date.getMonth()]];
    }



    return texts.join('-');

  }
},
data:EmotionLabel
},
series: [
{
  name: 'anger',
  type: 'bar',
  barMaxWidth:30,
  color:colors[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: anger
},
{
  name: 'interest',
  type: 'bar',
  barMaxWidth:30,
  color:colors[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: anticipation
},
{
  name: 'disgust',
  type: 'bar',
  barMaxWidth:30,
  color:colors[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: disgust
},
{
  name: 'fear',
  type: 'bar',
  barMaxWidth:30,
  color:colors[3],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: fear
},
{
  name: 'joy',
  type: 'bar',
  barMaxWidth:30,
  stack: 'Total amount',
  color:colors[4],
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: joy
},
{
  name: 'like',
  type: 'bar',
  barMaxWidth:30,
  stack: 'Total amount',
  color:colors[5],
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: like
},
{
  name: 'love',
  type: 'bar',
  barMaxWidth:30,
  stack: 'Total amount',
  color:colors[6],
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: love
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  stack: 'Total amount',
  color:colors[7],
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: neutral
},
{
  name: 'sadness',
  type: 'bar',
  barMaxWidth:30,
  stack: 'Total amount',
  color:colors[8],
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: sadness
},
{
  name: 'surprise',
  type: 'bar',
  barMaxWidth:30,
  color:colors[9],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: surprise
},
{
  name: 'trust',
  type: 'bar',
  barMaxWidth:30,
  color:colors[10],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: trust
}
]
};



$("#emotion-spin").hide();

   // socialChart.setOption(option);
   emotionChart.setOption(emotion_option, true), $(function() {
    function resize() {
      setTimeout(function() {
        emotionChart.resize()
      }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
  });

   emotionChart.resize();

   emotionChart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data = 2018-08
   console.log(params.seriesName); //bar type name ="like"
   console.log(params.value);//count 74
   var pid = GetURLParameter('pid');
   var txtkeyword =$("#txtkeyword").val();
    var e = document.getElementById("sentiment_option");
   var opt_sentiment = e.options[e.selectedIndex].value;
   window.open("{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name+"&keyword="+ txtkeyword+"&sentiment="+ opt_sentiment +"&emotion_type="+params.seriesName+"&from_graph=emotion" , '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});
   
 })
.fail(function() {
              // If there is no communication between the server, show an error
              console.log(error);

            });

}

$('.dateranges').daterangepicker({
  locale: {
    format: 'MMMM D, YYYY'
  },
  ranges: {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  startDate: startDate,
  endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,0,'');
      });


     //   $('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {alert("hihi");
     //     $('#daterange-btn span').html(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
     //     startDate=picker.startDate;
     //     endDate=picker.endDate;
     //     ChooseDate(startDate,endDate,'');

     /*$('#all_mention').DataTable().rows().remove().draw();*/
          /* $('#all_mention').DataTable().destroy();
          $('#all_mention').empty();*/


//});


$('input[name=options]').mouseup(function(){
    //alert("Before change "+$('input[name=options]:checked').val());
}).change(function(){
     periodType=$('input[name=options]:checked').val();
     ChooseDate(startDate,endDate,0,'cal_graph');
    //alert("After change "+$('input[name=options]:checked').val());
})

$( "#btn_search" ).click(function(e) {//alert("hihi");
 // $("#dlDropDown").dropdown("toggle");
//e.preventDefault();
  //$("#search_form").hide();
  
//class="dropdown-menu scale-up-left"
/*$(this).parents('.nav-item dropdown mega-dropdown').find('nav-link dropdown-toggle text-muted waves-effect waves-dark').dropdown('toggle');*/
  ChooseDate(startDate,endDate,1,'');
  //Create_DataTable(txtkeyword);
//return true; 
});


ChooseDate(startDate,endDate,0,'');

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   //alert(target);

   window.dispatchEvent(new Event('resize'));

 });
function GetStartDate()
{
            //alert(startDate);
            return startDate.format('YYYY MM DD');
          }
          function GetEndDate()
          {
           // alert(endDate);
           return endDate.format('YYYY MM DD');
         }
    //Create_DataTable();


    function Create_DataTable(is_search)
    {
      if(is_search === 1)
          {
            var txtkeyword =$("#txtkeyword").val();
            var e = document.getElementById("sentiment_option");
            var opt_sentiment = e.options[e.selectedIndex].value;
          }

     All_Mention_DataTable(txtkeyword,opt_sentiment);
     All_Mention_Comment_DataTable(txtkeyword,opt_sentiment);

    }

    function All_Mention_DataTable(keyword,sentiment)
    {
      $(".popup").unbind('click');
 var oTable = $('#all_mention').DataTable({

        "lengthChange": false,
        "searching": false,
        "processing": true,
        "serverSide": false,
        "destroy": true,
        "ordering": false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getallmention') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
          "data": {
            "fday": GetStartDate(),
            "sday": GetEndDate(),
            "keyword": keyword,
            "sentiment": sentiment,
            "brand_id": GetURLParameter('pid'),
          }

        },
        "initComplete": function( settings, json ) {
          console.log(json);
        },

        columns: [
        {data: 'post_div', name: 'post_div',orderable: false,searchable: true},
        {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        
      }).on('click', '.btn-bookmark', function () {
       //change css to bookmark star

       var current = $(this).attr('class');
       var id = $(this).attr('id');
       var name = $(this).attr('name');
  //if no bookmark prior 1- Change it to Bookmark css 2- Push to bookmark_array 3- remove from bookmark remove array if it is exist
       if(current == 'mdi mdi-star-outline text-yellow btn-bookmark')
       {
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star text-yellow btn-bookmark');
        if (jQuery.inArray(id, bookmark_array)=='-1') {
         bookmark_array.push(id) ;
        //need to remove it is exist in remove array
           if (jQuery.inArray(id, bookmark_remove_array)!='-1') {
        
              bookmark_remove_array.splice(jQuery.inArray(id,bookmark_remove_array), 1);
            

          } 
    
        } 

      }
//if  bookmark prior 1- Change it to No Bookmark css 2- Push to bookmark_remove_array 3- remove from bookmark array if it is exist
      else
      {
      /*  console.log("remove");
        console.log(name);*/
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star-outline text-yellow btn-bookmark');
           if (jQuery.inArray(id, bookmark_remove_array)=='-1') {
            bookmark_remove_array.push(id) ;
        if (jQuery.inArray(id, bookmark_array)!='-1') {
        
              bookmark_array.splice(jQuery.inArray(id,bookmark_array), 1);
            

          } 
    

          }
    
      

        
     }

      
    
  }).on('click', '.popup', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".comment_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getcomments") }}',
         type: 'GET',
         data: {id:post_id,cmt_type:name,brand_id:GetURLParameter('pid')},
         success: function(response) { console.log(response);
          var data=JSON.parse(response);
          console.log(data);

            for(var i in data) {

              var html ='<div class="d-flex flex-row comment-row"> '+
                  ' <div class="user-img"> <span class="round">A</span> <span class="profile-status away pull-right"></span> </div> '+
                    '   <div class="comment-text w-100" style="padding:15px 0px 15px 10px;"> '+
               '<span class="text-muted pull-right">'+data[i].created_time+'</span>';
               html+='<p>Sentiment: <select class="form-control custom-select sentiment-color comment-select" id="sentiment_comment_'+data[i].id+'"  >';
                 if(data[i].sentiment == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="pos"';
                if(data[i].sentiment == "pos")
                html+= 'selected="selected"';
                html+= '>pos</option><option value="neg"';
                if(data[i].sentiment == "neg")
                html+= 'selected="selected"';     
                html+= '>neg</option><option value="neutral"';
                if(data[i].sentiment == "neutral")
                html+= 'selected="selected"';  
                html+= '>neutral</option><option value="NA"';
                if(data[i].sentiment == "NA")
                html+= 'selected="selected"';  
                html+= '>NA</option></select>';

                html+= ' Emotion: <select class="form-control custom-select sentiment-color comment-select" id="emotion_comment_'+data[i].id+'"  >';
                if(data[i].emotion == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="anger"';
                if(data[i].emotion == "anger")
                html+= 'selected="selected"';
                html+= '>anger</option><option value="interest"';
                if(data[i].emotion == "interest")
                html+= 'selected="selected"';     
                html+= '>interest</option><option value="disgust"';
                if(data[i].emotion == "disgust")
                html+= 'selected="selected"';  
                html+= '>disgust</option><option value="fear"';
                if(data[i].emotion == "fear")
                html+= 'selected="selected"'; 
                html+= '>fear</option><option value="joy"';
                if(data[i].emotion == "joy")
                html+= 'selected="selected"'; 
                html+= '>joy</option><option value="like"';
                if(data[i].emotion == "like")
                html+= 'selected="selected"'; 
                html+= '>like</option><option value="love"';
                if(data[i].emotion == "love")
                html+= 'selected="selected"'; 
                html+= '>love</option><option value="neutral"';
                if(data[i].emotion == "neutral")
                html+= 'selected="selected"';
                html+= '>neutral</option><option value="sadness"';
                if(data[i].emotion == "sadness")
                html+= 'selected="selected"';
                html+= '>sadness</option><option value="surprise"';
                if(data[i].emotion == "surprise")
                html+= 'selected="selected"';
                html+= '>surprise</option><option value="trust"';
                if(data[i].emotion == "trust")
                html+= 'selected="selected"';
                html+= '>trust</option><option value="NA"';
                if(data[i].emotion == "NA")
                html+= 'selected="selected"';
                html+= '>NA</option></select>';
                if(data[i].edit_permission === 1)
                html+= ' <a class="edit_predict_comment" id="'+data[i].id+'" href="javascript:void(0)" style="width:10%;"><i class="ti-pencil-alt" ></i></a> ';
                html+=' </span></p> ' + 
                  ' <div class="m-b-5">'+data[i].message+'</div>'+
                 '<div class="comment-footer">'+
                  
                 '</div>'+
                 '</div>'+
                  '</div>';
       
                $(".comment_data").append(html);
        }


         $("#modal-spin").hide();
         $("#myModalLabel").text(name);
         $('#show-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.edit_post_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
  var sentiment = $('#sentiment_'+post_id+' option:selected').val();
  var emotion = $('#emotion_'+post_id+' option:selected').val();
 /* alert(post_id);*/
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) { /*alert(response)*/
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  });
    }
$(document).on('click', '.edit_predict_comment', function(e) {
     var comment_id = $(this).attr('id');
   
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_comment_'+comment_id+' option:selected').val();
  var emotion = $('#emotion_comment_'+comment_id+' option:selected').val();
  //alert(comment_id+sentiment + emotion);

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:comment_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {//alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
     
});
     function All_Mention_Comment_DataTable(keyword,sentiment)
     {
      var oTable = $('#all_mention_comment').DataTable({

        "lengthChange": false,
        "searching": false,
        "processing": true,
        "serverSide": false,
        "destroy": true,
        "ordering": false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getallmentioncomment') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
          "data": {
            "fday": GetStartDate(),
            "sday": GetEndDate(),
            "keyword": keyword,
            "sentiment":sentiment,
            "brand_id": GetURLParameter('pid'),
          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
        },

        columns: [
        {data: 'post_div', name: 'post_div',orderable: false,searchable: true},
        {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        
      }).on('click', '.btn-bookmark-comment', function () { 
       //change css to bookmark star

       var current = $(this).attr('class');
       var id = $(this).attr('id');
       var name = $(this).attr('name');
  //if no bookmark prior 1- Change it to Bookmark css 2- Push to bookmark_array 3- remove from bookmark remove array if it is exist
       if(current == 'mdi mdi-star-outline text-yellow btn-bookmark-comment')
       {
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star text-yellow btn-bookmark-comment');
        if (jQuery.inArray(id, bookmark_comment_array)=='-1') {
         bookmark_comment_array.push(id) ;
        //need to remove it is exist in remove array
           if (jQuery.inArray(id, bookmark_comment_remove_array)!='-1') {
        
              bookmark_comment_remove_array.splice(jQuery.inArray(id,bookmark_comment_remove_array), 1);
            

          } 
    
        } 

      }
//if  bookmark prior 1- Change it to No Bookmark css 2- Push to bookmark_remove_array 3- remove from bookmark array if it is exist
      else
      {
      /*  console.log("remove");
        console.log(name);*/
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star-outline text-yellow btn-bookmark-comment');
           if (jQuery.inArray(id, bookmark_comment_remove_array)=='-1') {
            bookmark_comment_remove_array.push(id) ;
        if (jQuery.inArray(id, bookmark_comment_array)!='-1') {
        
              bookmark_comment_array.splice(jQuery.inArray(id,bookmark_comment_array), 1);
            

          } 
    

          }
    
      //  console.log(bookmark_remove_array);

        
     }

    
    
  }).on('click', '.edit_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_'+post_id+' option:selected').val();
  var emotion = $('#emotion_'+post_id+' option:selected').val();
  //alert(post_id);
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {// alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  });
     }




$( "#btn_bookmark" ).click(function() {
  //alert(bookmark_array);
  //alert(bookmark_remove_array);
  if(Object.keys(bookmark_array).length > 0 || Object.keys(bookmark_remove_array).length > 0)
  {
        //save bookmark data;
        console.log(bookmark_remove_array);
        event.preventDefault();

        $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("postbookmark") }}',
         type: 'POST',
         data: {bookmark_array:bookmark_array,bookmark_remove_array:bookmark_remove_array,id:GetURLParameter('pid')},
         success: function(response) {//alert(response);
         swal({   
            title: "Bookmark!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
         
        }
      });


      }
    });


$( "#btn_bookmark_comment" ).click(function() {
/*  alert(bookmark_comment_array);
  alert(bookmark_comment_remove_array);*/
  if(Object.keys(bookmark_comment_array).length > 0 || Object.keys(bookmark_comment_remove_array).length > 0)
  {
        //save bookmark data;
    
        event.preventDefault();

        $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("post_comment_bookmark") }}',
         type: 'POST',
         data: {bookmark_comment_array:bookmark_comment_array,bookmark_comment_remove_array:bookmark_comment_remove_array,id:GetURLParameter('pid')},
         success: function(response) {//alert(response);
         swal({   
            title: "Bookmark!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
         
        }
      });


      }
    });




//local customize function



function kFormatter(num) {
  return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
function readmore(message){
      // alert("hi hi ");
      var string = String(message);
      var length = string.length; 
         // alert(length);
         if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.

            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
          }
          return string;


        }

        function judge_sentiment_icon(sentiment)
        {
         var emojis = ['0x1F60E', '0x1F626', '0x1F610'];//
         if (sentiment ==="pos") return  String.fromCodePoint(emojis[0]) + " positive"; 
         else if(sentiment ==="neg") return  String.fromCodePoint(emojis[1])+ " negative" ;
         else return String.fromCodePoint(emojis[2]) + " neutral" ;
       }

       function judge_emotion_icon(emotion)
       {
        var emojis = ['0x1F620', '0x1F604', '0x1F61D', '0x1F628', '0x1F604', '0x1F44D',
            '0x2764', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

            if (emotion ==="anger") return emojis[0]; 
            else if(emotion ==="anticipation") return emojis[1] ;
            if (emotion ==="disgust") return emojis[2] ; 
            else if(emotion ==="fear") return emojis[3] ;
            if (emotion ==="joy") return emojis[4]; 
            else if(emotion ==="like") return emojis[5] ;
            if (emotion ==="love") return emojis[6] ; 
            else if(emotion ==="neutral") return emojis[7] ;
            if (emotion ==="sadness")  return emojis[8]; 
            else if(emotion ==="surprise") return emojis[9]  ;
            else if(emotion === "trust") return emojis[10];

          }

        });
</script>
<link href="{{asset('css/own.css')}}" rel="stylesheet">

@endpush