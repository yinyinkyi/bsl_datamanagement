<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Website extends Eloquent
{

    protected $connection = 'mongodb';
	protected $collection = 'websites';
    protected $dates = ['created_time'];

  public function getDates() {
        return array();
  }
 protected $guarded = [];
 
}
