<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class update_oldTag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:oldTag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update tags  from api to old tags in comment table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects =DB::connection('mysql_cron_wunzin')->table('projects')->select('*')->get();
      $data =  [];
      
      foreach($projects as $project)
      {
        // dd("hell");
        $id = $project->id;
        $comment_table = "temp_".$id."_inbound_comments";
        $comments = DB::connection('mysql_cron_wunzin')->table($comment_table)->select('temp_id','wb_message')->where('checked_tags','')->where('wb_message','<>','')->where('created_time','>','2018-10-1 00:00:00')->orderby('created_time','desc')->limit(500)->get();
         // $comments = DB::connection('mysql_cron_wunzin')->table($comment_table)->select('temp_id','wb_message')->where('sentiment','')->orderby('created_time','desc')->limit(500)->get();
        // dd($comments);
         if(!$comments->isEmpty()){
        foreach($comments as $comment)
        {

          $wb_message[] = $comment->wb_message;
          $request['wb_message']= $comment->wb_message;
          $request['id'] = $comment->temp_id;

          $data[]=$request;
        //   if($request['wb_message'] == null ){
        //   $comment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('wb_message','')->update(['sentiment' => "NA",'checked_sentiment' => "NA",'emotion'=> "NA",'checked_predict' => 1,'interest'=> 0,'updated_at' => now()->toDateTimeString()]);
        // }

        }
      
        if(count($wb_message) > 0)
        {
          // dd('ehhh');
          $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_sentiment = 'senti_emo_interest';
         
            $formData = array(
    'raw' =>  $wb_message,
    'threshold'=>0
   
);
           

           $formData = json_encode($formData);
           
           try{
           $api_response = $client->post($uri_sentiment, [
                                'form_params' => [
                                'raw' =>  $formData,
                                
                                ],
                             ]);

            $result = ($api_response->getBody()->getContents());

            $json_result_array = json_decode($result, true);
          }
                    catch (\Exception $e) {
  
                $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                $error_message = explode('<html>',$error_message);
                // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                //dd($error_message[0]);
                $error_message=$error_message[0];

                //$error_message ='test';
                $sms_data=json_encode([
                  'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                  'group_id' =>2,
                ]);
                $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                  'headers' => ['Content-Type' => 'application/json'],
                  'body' => $sms_data
                ]);
                 //dd($sms_response->getBody()->getContents());
                  // return false;
                }

            $result = $json_result_array[0];
            // $this->info(dd($result));
            //            return;
            $count = (Int)count($data);

            for($i=0;$i<$count;$i++)
            {
              // dd($result['tags'][$i]);
              
                $id = $data[$i]['id'];
                
                $comment = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('temp_id',$id)->update(['tags' => $result['tags'][$i],'updated_at' => now()->toDateTimeString()]);
                //update checksentiment in another update query because sentiment API may train again next time. 
               
                $tags = DB::connection('mysql_cron_wunzin')->table($comment_table)->where('temp_id',$id)->where('checked_tags','')->update(['checked_tags' => $result['tags'][$i]]);
                
                 
            }

        }
      }
      }
       $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Old  Tags  Updated Successfully.'. $date_time);
    }
}
