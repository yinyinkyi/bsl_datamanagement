<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class TagCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev_inbound_comment:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update dev inbound comment (senti_emo_interest_tags) every  minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $projects =DB::connection('mysql_cron_dev')->table('projects')->select('*')->get();
      $data =  [];
      
      foreach($projects as $project)
      {
        $project_id = $project->id;
        $comment_table = "temp_".$project_id."_inbound_comments";
        $comments = DB::connection('mysql_cron_dev')->table($comment_table)->select('temp_id','wb_message')->where('tags','')->where('wb_message','<>','')->where('api_tagflag',0)->orderby('created_time','desc')->limit(500)->get();
       // dd($comments);
         if(!$comments->isEmpty()){
        foreach($comments as $comment)
        {
          $wb_message[] = $comment->wb_message;
          $request['wb_message']= $comment->wb_message;
          $request['id'] = $comment->temp_id;
          $data[]=$request;
        }
        if(count($wb_message) > 0)
        {
         
          $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_sentiment = 'senti_emo_interest';
         
                $formData = array(
        'raw' =>  $wb_message,
        'threshold'=>0
       
    );

           $formData = json_encode($formData);
           try{

               $api_response = $client->post($uri_sentiment, [
                                    'form_params' => [
                                    'raw' =>  $formData,
                                    
                                    ],
                                 ]);

                $result = ($api_response->getBody()->getContents());

                $json_result_array = json_decode($result, true);
          }
          catch (\Exception $e) {
  
                $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());

                $error_message = explode('<html>',$error_message);
                // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
                //dd($error_message[0]);
                $error_message=$error_message[0];

                //$error_message ='test';
                $sms_data=json_encode([
                  'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
                  'group_id' =>2,
                ]);
                $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
                  'headers' => ['Content-Type' => 'application/json'],
                  'body' => $sms_data
                ]);
                 //dd($sms_response->getBody()->getContents());
                  // return false;
                }


            $result = $json_result_array[0];
            $count = (Int)count($data);

           

            for($i=0;$i<$count;$i++)
            {
               $tags_array=[];
               $tags_id_array=[];
               $tags_string='';
               $tags_id_string='';
              $id = $data[$i]['id'];
              $tags =explode(',',$result['tags'][$i]); 
              foreach($tags as $tag)
              {
                if(isset($tag) && $tag != ''){
               $query = "Select * from social_listener_dev.tags where brand_id=".$project_id." and name ='".$tag."'";
               $tags_result =DB::connection('mysql_cron_dev')->select($query);
           
                if (empty($tags_result)) { 
            $new_tags_id =DB::connection('mysql_cron_dev')->table('tags')->insertGetId(['name' => $tag, 'original_name' => $tag,'fromApi' => 1,'bsl'=>1,'brand_id' => $project_id,'created_at' => now()->toDateTimeString(),'updated_at' => now()->toDateTimeString()]);
                  $tags_array[] = $tag;
                  $tags_id_array[] = $new_tags_id;
                   }
                   else
                   {
                     $tags_array[] = $tag;
                     $tags_id_array[] = $tags_result[0]->id;

                   }



               }
               $tags_string= implode(',',$tags_array);
               $tags_id_string= implode(',',$tags_id_array);

              

                
              }
               DB::connection('mysql_cron_dev')->table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'tags'=> $tags_string,'tags_id'=> $tags_id_string,'api_tagflag' => 1,'updated_at' => now()->toDateTimeString()]);
                //update checksentiment in another update query because sentiment API may train again next time. 
                $sentiment = DB::connection('mysql_cron_dev')->table($comment_table)->where('temp_id',$id)->where('checked_sentiment','')->update(['checked_sentiment' => $result['sentiment'][$i]]);
                $tags = DB::connection('mysql_cron_dev')->table($comment_table)->where('temp_id',$id)->where('checked_tags','')->update(['checked_tags' => $tags_string,'checked_tags_id'=> $tags_id_string]);
              
                 
            }
           
        }
      }
    }
      
       
       $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Comment Senti Crawl Successfully for DEV.'. $date_time);
    }
}
