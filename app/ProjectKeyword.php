<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProjectKeyword extends Model
{
    //
     use Notifiable;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','project_id','main_keyword','require_keyword', 'exclude_keyword'
    ];
    protected $table='project_keywords';

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

}
