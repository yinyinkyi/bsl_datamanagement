<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class checkedTag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkedTagId:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update checked tag id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects =DB::connection('mysql_cron_dev')->table('projects')->select('*')->get();
      $data =  [];
      
      foreach($projects as $project)
      {
        $project_id = 34;
        $comment_table = "temp_".$project_id."_inbound_comments";
        $comments = DB::connection('mysql_cron_dev')->table($comment_table)->select('temp_id','checked_tags')->where('checked_tags','<>','')->where('checked_tags_id','')->where('update_CTID',0)->orderby('created_time','desc')->limit(500)->get();
       // dd($comments);
         if(!$comments->isEmpty()){
        foreach($comments as $comment)
        {

                   $checked_tags =$comment->checked_tags;
                   $id =$comment->temp_id;
                   $checked_tags_arr=explode(',', $checked_tags);
                       $tags_array=[];
                       $tags_id_array=[];
                       $tags_string='';
                       $tags_id_string='';  

                    foreach($checked_tags_arr as $tag)
              {
                if(isset($tag) && $tag != ''){
               $query = "Select * from social_listener_dev.tags where brand_id=".$project_id." and name ='".$tag."'";
               $tags_result =DB::connection('mysql_cron_dev')->select($query);
           
                if (! empty($tags_result)) { 
                      $tags_id_array[] = $tags_result[0]->id;

                 }
                 



               }

              

                
              }

               $tags_id_string= implode(',',$tags_id_array);

                 $tags = DB::connection('mysql_cron_dev')->table($comment_table)->where('temp_id',$id)->where('checked_tags_id','')->update(['checked_tags_id' => $tags_id_string,'update_CTID' => 1]);


        }
          
        
      }
    }
      
       
       $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Comment Senti Crawl Successfully for DEV.'. $date_time);
    }
}
