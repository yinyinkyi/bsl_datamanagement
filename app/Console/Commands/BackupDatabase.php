<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $process;

    public function __construct()
    {
         $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
         $date_time = $date->format('dmYHis');
        parent::__construct();

        $this->process = new Process(sprintf(
            'mysqldump -u%s -p%s %s > %s',
            config('database.connections.mysql_cron_wunzin.username'),
            config('database.connections.mysql_cron_wunzin.password'),
            config('database.connections.mysql_cron_wunzin.database'),
            storage_path('backups/backup_'.$date_time.'.sql')
        ));
         $this->process->setTimeout(null);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         try {
            $this->process->mustRun();

            $this->info('The backup has been proceed successfully.');
        } catch (ProcessFailedException $exception) {
            $this->error('The backup process has been failed.');
        }
    }
}
